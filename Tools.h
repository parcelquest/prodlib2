
DWORD DLLAPI GetRefPtr(LPVOID lpVoid);

WORD DLLAPI LoByte(WORD w);
WORD DLLAPI HiByte(WORD w);
WORD DLLAPI MakeWord(WORD bHi, WORD bLo);
WORD DLLAPI RShiftWord(WORD w, WORD c);
WORD DLLAPI LShiftWord(WORD w, WORD c);
WORD DLLAPI HiWord(DWORD dw);
WORD DLLAPI LoWord(DWORD dw);
DWORD DLLAPI MakeDWord(DWORD wHi, DWORD wLo);
DWORD DLLAPI RShiftDWord(DWORD dw, unsigned c);
DWORD DLLAPI LShiftDWord(DWORD dw, unsigned c);
short DLLAPI ExistFile(LPSTR lpFileName);

#ifndef WIN32
int DLLAPI Interrupt(WORD wInt,
					 LPDWORD pAx, LPDWORD pBx,
					 LPDWORD pCx, LPDWORD pDx,
					 LPDWORD pSi, LPDWORD pDi );
#endif

