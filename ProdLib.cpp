// cddlib.cpp : Defines the entry point for the DLL application.
//

// Standard error code:
//       0 = successful
//      <0 = fail
//  others = returned value

#include "stdafx.h"
#include "prodlib.h"

HANDLE hInst;
BOOL  bOpened = false;
BOOL  bWritten = false;
int   iNdx = 0;
int   iNumFlds = 0;
int   iRecLen=0;
char  strINIFile[128];
CDA_FLDDEF *pFieldDef;

BOOL APIENTRY DllMain( HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
   switch (ul_reason_for_call)
   {
      case DLL_PROCESS_ATTACH:
         // The DLL is being mapped into the process's address space
         hInst = hModule;
         break;
      case DLL_THREAD_ATTACH:
      case DLL_THREAD_DETACH:
      case DLL_PROCESS_DETACH:
         // The DLL is being unmapped from the process's address space
         hInst = 0;
         break;
   }
   return TRUE;
}


void __stdcall rTrim(char *pString)
{
   char *pTmp = pString;
   int  i;

   // remove trailing spaces and tabs and other non-printables
   i = strlen(pString)-1;
   while (i >= 0)
   {
      if ((pString[i] > ' ') && (pString[i] <= '~'))
         break;
      pString[i] = 0;
      i--;
   }

}

void __stdcall lTrim(char *pString)
{
   char *pTmp = pString;

   // remove leading spaces
   while (*pTmp == ' ')
      pTmp++;
   
   if (pTmp != pString)
      strcpy(pString, pTmp);
}

int __stdcall ParseString(char *pString, char Delimiter, int MaxFields, char *Fields[])
{
   int i;
   int j = 1;
   int k;
   char *ptr;
   int InsideQuotes = 0;

   rTrim(pString);
   k = (int)strlen(pString);

   for (i=0; (i<k && j<MaxFields); i++)
   {                
      if (pString[i] == '\0' || pString[i] == 10)
         break;
      if (pString[i] == '\"')
         InsideQuotes = (InsideQuotes+1) % 2;
      if (InsideQuotes)
         continue;
      if (pString[i] == Delimiter)
      {
         pString[i] = 0;
         j++;
      }
   }

   ptr = pString;
   for (i=0; i<j; i++)
   {
      Fields[i] = ptr;
      ptr += strlen(Fields[i])+1;      // Point to next field
   }

   for (i=j; i<MaxFields; i++)
      Fields[i] = NULL;

   return(j);
}

// Parse string ignore quote
int __stdcall ParseStringIQ(char *pString, char Delimiter, int MaxFields, char *Fields[])
{
   int i;
   int j = 1;
   int k;
   char *ptr;

   k = (int)strlen(pString);

   for (i=0; (i<k && j<MaxFields); i++)
   {                
      if (pString[i] == '\0' || pString[i] == 10)
         break;
      if (pString[i] == Delimiter)
      {
         pString[i] = 0;
         j++;
      }
   }

   ptr = pString;
   for (i=0; i<j; i++)
   {
      Fields[i] = ptr;
      ptr += strlen(Fields[i])+1;      // Point to next field
   }

   for (i=j; i<MaxFields; i++)
      Fields[i] = NULL;

   return(j);
}

// Parse string no quote
int __stdcall ParseStringNQ(char *pString, char Delimiter, int MaxFields, char *Fields[])
{
   int i;
   int j = 1;
   int k;
   char *ptr;
   int InsideQuotes = 0;

   rTrim(pString);
   k = (int)strlen(pString);

   for (i=0; (i<k && j<MaxFields); i++)
   {                
      if (pString[i] == '\0' || pString[i] == 10)
         break;
      if (pString[i] == '\"')
         InsideQuotes = (InsideQuotes+1) % 2;
      if (InsideQuotes)
         continue;
      if (pString[i] == Delimiter)
      {
         pString[i] = 0;
         j++;
      }
   }

   ptr = pString;
   for (i=0; i<j; i++)
   {
      if (*ptr == '\"')
         ptr++;
      Fields[i] = ptr;
      ptr += strlen(Fields[i])+1;      // Point to next field
      if (*(ptr-2) == '\"')
         *(ptr-2) = 0;
   }

   // Trim all leading space
   for (i=0; i<j; i++)
   {
      if (*Fields[i] == ' ')
         lTrim(Fields[i]);
   }

   for (i=j; i<MaxFields; i++)
      Fields[i] = NULL;

   return(j);
}

/*****************************************************************************************
 *
 * Special version that handle double quote within quote.  When this happen, it replaces
 * the odd double quote with space.
 *
 *****************************************************************************************/

int __stdcall ParseStringNQ1(char *pString, char Delimiter, int MaxFields, char *Fields[])
{
   int i;
   int j = 1;
   int k;
   char *ptr;
   int InsideQuotes = 0;

   rTrim(pString);
   k = (int)strlen(pString);

   for (i=0; (i<k && j<MaxFields); i++)
   {                
      if (pString[i] == '\0' || pString[i] == 10)
         break;
      if (pString[i] == '\"')
      {
         if (!InsideQuotes)
            InsideQuotes = 1;
         else if (pString[i+1] == Delimiter)
            InsideQuotes = 0;
         else
            pString[i] = ' ';
      }
      if (InsideQuotes)
         continue;
      if (pString[i] == Delimiter)
      {
         pString[i] = 0;
         j++;
      }
   }

   ptr = pString;
   for (i=0; i<j; i++)
   {
      if (*ptr == '\"')
         ptr++;
      Fields[i] = ptr;
      ptr += strlen(Fields[i])+1;      // Point to next field
      if (*(ptr-2) == '\"')
         *(ptr-2) = 0;
   }

   for (i=j; i<MaxFields; i++)
      Fields[i] = NULL;

   return(j);
}

/******************************************************************************
 *
 * Indexing helper functions
 *
 ******************************************************************************/

// Create new county definition file
int __stdcall CddNew(LPCSTR lpFilename)
{
   CddClose();

   strcpy(strINIFile, lpFilename);
   return 0;
}   


//Initialize structure by reading from file
int __stdcall CddOpen(LPCSTR lpFilename)
{
   int   iRet;
   FILE *fdRecDef;
   char  szBuf[256], *pTmp;
   char  *pszFields[NUMELEM];

   CddClose();

   iNumFlds = GetPrivateProfileInt("Data", "RecordNumber", 0, lpFilename);
   if (iNumFlds > 0)
   {
      int iTmp;
      CDA_FLDDEF   sFld;

      fdRecDef = fopen(lpFilename, "r");
      if (!fdRecDef)
         return -1;

      pFieldDef = new CDA_FLDDEF [iNumFlds];

      do {
         pTmp = fgets(szBuf, 256, fdRecDef);
      } while (pTmp && _memicmp(pTmp, "[RecordDef]", 11));

      if (!pTmp)
      {
         fclose(fdRecDef);
         return -2;
      }

      // Save INI file name
      strcpy(strINIFile, lpFilename);
      for (iRecLen = 0, iTmp = 0; iTmp < iNumFlds; iTmp++)
      {
         pTmp = fgets(szBuf, 256, fdRecDef);
         pTmp = strchr(szBuf, '=');
         if (!pTmp)
            continue;

         pTmp++;
         iRet = ParseString(pTmp, ';', NUMELEM, pszFields);
         memset(&sFld, 0, sizeof(CDA_FLDDEF));

         if (iRet == NUMELEM)
         {
            iRecLen += sFld.iLength;
            sFld.iID = atoi(pszFields[FLD_ID]);
            strcpy(sFld.acTag, pszFields[FLD_TAG]);
            sFld.iType = atoi(pszFields[FLD_TYPE]);
            sFld.iOffset = atoi(pszFields[FLD_OFFSET]);
            sFld.iLength = atoi(pszFields[FLD_LENGTH]);
            sFld.bIndexed = atoi(pszFields[FLD_INDEXED]);
            sFld.bSorted = atoi(pszFields[FLD_SORTED]);
            sFld.iRecordArrayLength = atoi(pszFields[FLD_ARRAYLEN]);
            sFld.lTerms = atol(pszFields[FLD_TERMS]);
            sFld.lUniqueTerms = atol(pszFields[FLD_UTERMS]);
            sFld.bGroup = atoi(pszFields[FLD_GROUP]);
            sFld.bIndex = atoi(pszFields[FLD_INDEX]);
            sFld.bDisplay = atoi(pszFields[FLD_DISPLAY]);
            if (pszFields[FLD_TERMFILE])
               strcpy(sFld.acTermFile, pszFields[FLD_TERMFILE]);
            if (pszFields[FLD_NDXFILE])
               strcpy(sFld.acIndexFile, pszFields[FLD_NDXFILE]);
            if (pszFields[FLD_RECFILE])
               strcpy(sFld.acRecordFile, pszFields[FLD_RECFILE]);
            if (pszFields[FLD_WORDFILE])
               strcpy(sFld.acWordFile, pszFields[FLD_WORDFILE]);
            if (pszFields[FLD_MEMBEROF])
            {
               rTrim(pszFields[FLD_MEMBEROF]);
               strcpy(sFld.acMemberOf, pszFields[FLD_MEMBEROF]);
            }

            iRecLen += sFld.iLength;
         } else
         {
            iRet = -3;
            break;
         }

         //MyArray.SetAt(sFld.iID, sFld);
         pFieldDef[sFld.iID] = sFld;

      }
      fclose(fdRecDef);
      bOpened = true;
   }
   return iRet;
}

int CddWriteFile()
{
   FILE  *fdRecDef, *fdTmp;
   char  acTmpFile[128], acBakFile[128];
   char  szBuf[1024], *pTmp;

   int iTmp;
   CDA_FLDDEF   sFld;

   fdRecDef = fopen(strINIFile, "r");
   if (!fdRecDef)
      return -1;
   
   // Creating temp filename
   strcpy(acTmpFile, strINIFile);
   strcpy(acBakFile, strINIFile);
   pTmp = strrchr(acTmpFile, '.');
   if (pTmp)
   {
      *(++pTmp) = '\0';
      strcpy(acBakFile, acTmpFile);
      strcat(acBakFile, "BAK");
      strcat(acTmpFile, "TMP");
   } else
      return -2;

   // Open output file
   fdTmp = fopen(acTmpFile, "w");
   if (!fdTmp)
   {
      fclose(fdRecDef);
      return -1;
   }

   // Save the top part of the file
   do {
      pTmp = fgets(szBuf, 256, fdRecDef);
      fputs(szBuf, fdTmp);
   } while (pTmp && _memicmp(pTmp, "[RecordDef]", 11));
   fclose(fdRecDef);

   // Now write all remaining fields to file
   for (iTmp = 0; iTmp < iNumFlds; iTmp++)
   {
      sFld = pFieldDef[iTmp];

      if (sFld.acTag[0] < '0')
         break;

      // ID,Tag,Type,Offset,Length,Indexed,Sorted,ArrayLen;Terms,UTerms,Group,
      // Index,Display,Termfile,Indexfile,Recordfile,Wordfile,MemberOf.
      sprintf(szBuf, "ID%.1d=%.1d;%s;%.1d;%.1d;%.1d;%.1d;%.1d;%.1d;%ld;%ld;%.1d;%.1d;%.1d;%s;%s;%s;%s;%s\n",
         sFld.iID, sFld.iID, sFld.acTag, sFld.iType, sFld.iOffset, sFld.iLength, sFld.bIndexed, sFld.bSorted, 
         sFld.iRecordArrayLength, sFld.lTerms, sFld.lUniqueTerms, sFld.bGroup, sFld.bIndex, sFld.bDisplay,
         sFld.acTermFile, sFld.acIndexFile, sFld.acRecordFile, sFld.acWordFile, sFld.acMemberOf);
      fputs(szBuf, fdTmp);
   }
   fclose(fdTmp);

   iNumFlds = iTmp;

   if (!_access(acBakFile, 0))
      remove(acBakFile);

   // Rename original file to backup file
   rename(strINIFile, acBakFile);

   // Rename temp file to the right name
   rename(acTmpFile, strINIFile);

   bWritten = false;
   return 0;
}

int __stdcall CddUpdate()
{
   int   iRet = 0;
   char  acNumFlds[10];

   if (bWritten)
   {
      iRet = CddWriteFile();

      // Update number of records
      sprintf(acNumFlds, "%.2d", iNumFlds);
      WritePrivateProfileString("Data", "RecordNumber", acNumFlds, strINIFile);
   }
   //iNumFlds=0;
   return iRet;
}

//CDDLIB_API void CddClose()
int __stdcall CddClose()
{
   int iRet = 0;
   if (bOpened)
   {
      iRet = CddUpdate();
      delete pFieldDef;
      //MyArray.RemoveAll();
      bOpened = false;
      iNumFlds = 0;
      strINIFile[0] = '\0';
   }
   return iRet;
}

//CDDLIB_API int CddGetRec(CDA_FLDDEF *lpResult, int iID, LPCSTR lpTag)
int __stdcall CddGetRec(CDA_FLDDEF *lpResult, int iID, LPCSTR lpTag)
{
   int   iRet=sizeof(CDA_FLDDEF);

   if (iID >= iNumFlds)
      return -1;

   memcpy((char *)lpResult, (char *)&pFieldDef[iID], sizeof(CDA_FLDDEF));
   /*
   strcpy(lpResult->acTag, (char *)&pFieldDef[iID].acTag[0]);
   lpResult->bDisplay = pFieldDef[iID].bDisplay;
   lpResult->bGroup = pFieldDef[iID].bGroup;
   lpResult->bIndex = pFieldDef[iID].bIndex;
   lpResult->bIndexed = pFieldDef[iID].bIndexed;
   lpResult->bSorted = pFieldDef[iID].bSorted;
   lpResult->iID = pFieldDef[iID].iID;
   lpResult->iLength = pFieldDef[iID].iLength;
   lpResult->iOffset = pFieldDef[iID].iOffset;
   lpResult->iType = pFieldDef[iID].iType;
   lpResult->lTerms = pFieldDef[iID].lTerms;
   lpResult->lUniqueTerms = pFieldDef[iID].lUniqueTerms;
   strcpy(lpResult->acTermFile, (char *)&pFieldDef[iID].acTermFile[0]);
   strcpy(lpResult->acIndexFile, (char *)&pFieldDef[iID].acIndexFile[0]);
   strcpy(lpResult->acRecordFile, (char *)&pFieldDef[iID].acRecordFile[0]);
   strcpy(lpResult->acMemberOf, (char *)&pFieldDef[iID].acMemberOf[0]);
   */
   return iRet;
}

CDA_FLDDEF* __stdcall CddGetFldList()
{
   return pFieldDef;
}

int __stdcall CddPutRec(CDA_FLDDEF *lpRec)
{
   int    iRet=-1;
   CDA_FLDDEF  sFld;
   
   if (lpRec->iID < 0 || iNumFlds == 0)
      return iRet;

   if (lpRec->iID < iNumFlds)
   {
      memcpy((char *)&sFld, (char *)lpRec, sizeof(CDA_FLDDEF));
      sFld.acTag[TAGLEN-1] = 0; rTrim(sFld.acTag);
      sFld.acTermFile[NAMELEN-1] = 0; rTrim(sFld.acTermFile);
      sFld.acIndexFile[NAMELEN-1] = 0; rTrim(sFld.acIndexFile);
      sFld.acRecordFile[NAMELEN-1] = 0; rTrim(sFld.acRecordFile);
      sFld.acWordFile[NAMELEN-1] = 0; rTrim(sFld.acWordFile);
      sFld.acMemberOf[TAGLEN-1] = 0; rTrim(sFld.acMemberOf);

      pFieldDef[sFld.iID] = sFld;
      bWritten = true;
      iRet = 0;
   }
   return iRet;
}

int __stdcall CddRecLen(void)
{
   return iRecLen;
}

int __stdcall CddNumFlds(void)
{
   return iNumFlds;
}

int __stdcall CddNewFldList(int iNumRecs, LPSTR pFilename)
{
   int   iRet = 0;

   delete pFieldDef;
   iNumFlds = iNumRecs;
   pFieldDef = new CDA_FLDDEF [iNumFlds];
   bOpened = true;

   // Setup filename for new file
   if (!strINIFile[0])
      strcpy(strINIFile, pFilename);

   return iRet;
}

long __stdcall CddChmod(LPSTR pFilename, long iMode)
{
   return (long)_chmod(pFilename, (int)iMode);
}

// Check for file existence
BOOL __stdcall isFileExist(LPSTR lpFileName)
{                  
   if (lpFileName == NULL)
      return false;
   
   return (!_access(lpFileName, 0));
}

// Return 0 if no error
int __stdcall ParseMapLink(MAPLINK *pMapLink, char *pString)
{
   char  *pTmp, *pTmp1;
   int   iRet, iTmp, iCnt;

   pTmp = pString;
   iRet = 0;

   //Initialize
   iTmp = 0;
   iCnt = 0;
   while (*pTmp != '[' && iTmp < 3)
   {
      pMapLink->acPrefix[iTmp++] = *pTmp;
      pTmp++;
   }
   pMapLink->acPrefix[iTmp] = '\0';

   while (*pTmp == '[' && iRet == 0 && iCnt < 3)
   {
      pTmp++;
      pMapLink->iFldNum = atoi(pTmp);
      pTmp1 = strchr(pTmp, ',');
      if (pTmp1)
      {
         pMapLink->iStart = atoi(++pTmp1);
         pTmp = strchr(pTmp1, ',');
         if (pTmp)
         {
            pMapLink->iLength = atoi(++pTmp);
            pTmp1 = strchr(pTmp, ']');       // Look for end of field description
            if (!pTmp1++) iRet = 3;
         } else
            iRet = 2;
      } else
         iRet = 1;

      pTmp = pTmp1;

      if (!*pTmp)
         break;

      //Initialize
      iTmp = 0;
      while (iRet == 0 && *pTmp != '[' && iTmp < 3)
      {
         pMapLink->acPostfix[iTmp++] = *pTmp;
         pTmp++;
      }
      pMapLink->acPostfix[iTmp] = '\0';

      pMapLink++;
      iCnt++;
   }     // end of while loop
   return iRet;
}

int __stdcall ParseLink(MAPLINK *pMapLink, char *pString)
{
   char  *pTmp, *pTmp1;
   int   iRet;

   pTmp = pString;
   iRet = 0;

   if (*pTmp == '[')
   {
      pTmp++;
      pMapLink->iFldNum = atoi(pTmp);
      pTmp1 = strchr(pTmp, ',');
      if (pTmp1)
      {
         pMapLink->iStart = atoi(++pTmp1);
         pTmp = strchr(pTmp1, ',');
         if (pTmp)
         {
            pMapLink->iLength = atoi(++pTmp);
            pTmp1 = strchr(pTmp, ']');       // Look for end of field description
            if (!pTmp1) iRet = 3;
         } else
            iRet = 2;
      } else
         iRet = 1;
   }
   return iRet;
}

// return 0 if successful, -1 if file not found
__int64 __stdcall fileStat(LPCSTR lpFilename, FILESTAT *lpResult)
{
   __int64   iRet;
   struct _stati64 sResult;

   iRet = _stati64(lpFilename, &sResult);
   lpResult->ftCreationTime = sResult.st_ctime;
   lpResult->ftLastAccessTime = sResult.st_atime;
   lpResult->ftLastWriteTime = sResult.st_mtime;
   //lpResult->nFileSizeHigh = (unsigned long)(sResult.st_size >> 31);
   //lpResult->nFileSizeLow = (unsigned long)(sResult.st_size & 0x000000008FFFFFFF);
   lpResult->nFileSize = (double)sResult.st_size;

   return iRet;
}

int __stdcall CountRecs(LPCSTR lpFilename, int iRecLen)
{
   int     iRet;
   struct  _stati64 sResult;

   _stati64(lpFilename, &sResult);
   iRet = (int)(sResult.st_size / iRecLen);

   return iRet;
}

void __stdcall timeString(LPCSTR strTime, time_t lTime)
{
   char  *pTmp;
   time_t myTime;

   myTime = lTime;
   pTmp = ctime(&myTime);
   strcpy((char *)strTime, pTmp);
}

void __stdcall dateString(LPCSTR strTime, time_t lTime)
{
   struct tm *myDate;
   time_t myTime;

   if (!lTime)
      time(&myTime);
   else
      myTime = lTime;

   myDate = localtime(&myTime);
   sprintf((char *)strTime, "%.4d%.2d%.2d", myDate->tm_year+1900, myDate->tm_mon+1, myDate->tm_mday);
}
