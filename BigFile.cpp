#include "stdafx.h"
#include "prodlib.h"
#include "logs.h"

#define  MAX_RECSIZE    8192
#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004
#define  NOTFOUND_ERR   0xF0000008
#define  BADRECSIZE_ERR 0xF0000010


int __stdcall doCopyRaw(LPCSTR strInFile, LPCSTR strOutFile, int iFileCnt, int iRecSize)
{
   char     acPath[_MAX_PATH], acRawFile[_MAX_PATH];
   char     *pTmp, acBuf[MAX_RECSIZE];
   long     lRet=0;
   int      iCnt=1;
   
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;

   // Setup file names
   strcpy(acPath, strInFile);
   pTmp = strrchr(acPath, '.');
   if (pTmp)
      *pTmp = '\0';

   if (iFileCnt > 1)
      sprintf(acRawFile, "%s.RA%.1d", acPath, iCnt++);
   else
      strcpy(acRawFile, strInFile);

   // Make sure input file exist
   if (_access(acRawFile, 0))
      return NOTFOUND_ERR;

   // Open files
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return OPEN_ERR;


   // Validate record size
   if (iRecSize > MAX_RECSIZE)
      return BADRECSIZE_ERR;

    // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Compression loop
   while (ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // Reaching EOF, set up new file if available
         if (iFileCnt >= iCnt)
         {
            CloseHandle(fhIn);
            sprintf(acRawFile, "%s.RA%.1d", acPath, iCnt++);
            if (_access(acRawFile, 0))
               break;

            // Open files
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                   FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }
            ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL);
         } else
         {
            // No more file, get out
            break;
         }

      }

      bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (!nBytesRead || nBytesRead != nBytesWritten)
         break;

   }

   close_log();

   // Close files
   if (fhIn)
      CloseHandle(fhIn);

   FlushFileBuffers(fhOut);
   CloseHandle(fhOut);
   return lRet;
}

int __stdcall doCopy(LPCSTR strInFile, LPCSTR strOutFile)
{
   char     acBuf[MAX_RECSIZE];
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0;

   // Make sure input file exist
   if (_access(strInFile, 0))
      return NOTFOUND_ERR;

   // Open files
   fhIn = CreateFile(strInFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

    // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Copy loop
   while (ReadFile(fhIn, acBuf, MAX_RECSIZE, &nBytesRead, NULL))
   {
      if (nBytesRead > 0)
      {
         bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      }

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      // Check for EOF
      if (!nBytesRead || nBytesRead != nBytesWritten)
         break;

   }

   // Close log
   close_log();

   // Close files
   if (fhIn)
      CloseHandle(fhIn);

   FlushFileBuffers(fhOut);
   CloseHandle(fhOut);
   return lRet;
}

int __stdcall doCopyAddCR(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize)
{
   char     acOutBuf[MAX_RECSIZE+2];
   long     lRet=0;
   
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;

   // Make sure input file exist
   if (_access(strInFile, 0))
      return NOTFOUND_ERR;

   // Open files
   fhIn = CreateFile(strInFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return OPEN_ERR;


   // Validate record size
   if (iRecSize > MAX_RECSIZE)
      return BADRECSIZE_ERR;

    // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Copy loop
   while (ReadFile(fhIn, acOutBuf, iRecSize, &nBytesRead, NULL))
   {
      if (nBytesRead > 0)
      {
         acOutBuf[nBytesRead] = 13;
         acOutBuf[nBytesRead+1] = 10;
         nBytesRead += 2;
         bRet = WriteFile(fhOut, acOutBuf, nBytesRead, &nBytesWritten, NULL);
      }

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (nBytesRead <= iRecSize || nBytesRead != nBytesWritten)
         break;
   }

   // Close log fie
   close_log();

   // Close files
   if (fhIn)
      CloseHandle(fhIn);

   FlushFileBuffers(fhOut);
   CloseHandle(fhOut);
   return lRet;
}


int __stdcall doEBC2ASC(LPCSTR strInFile, LPCSTR strOutFile)
{
   char     acBuf[MAX_RECSIZE], acOutBuf[MAX_RECSIZE];
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0;

   // Make sure input file exist
   if (_access(strInFile, 0))
      return NOTFOUND_ERR;

   // Open files
   fhIn = CreateFile(strInFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

    // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Copy loop
   while (ReadFile(fhIn, acBuf, MAX_RECSIZE, &nBytesRead, NULL))
   {
      if (nBytesRead > 0)
      {
         ebc2asc((unsigned char *)&acOutBuf[0], (unsigned char *)&acBuf[0], nBytesRead);
         bRet = WriteFile(fhOut, acOutBuf, nBytesRead, &nBytesWritten, NULL);
      }

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      // Check for EOF
      if (!nBytesRead || nBytesRead != nBytesWritten)
         break;

   }

   // Close log fie
   close_log();

   // Close files
   if (fhIn)
      CloseHandle(fhIn);

   FlushFileBuffers(fhOut);
   CloseHandle(fhOut);
   return lRet;
}

int __stdcall doEBC2ASCAddCR(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize)
{
   char     acBuf[MAX_RECSIZE+2], acOutBuf[MAX_RECSIZE+2];
   long     lRet=0;
   
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, lRecCnt=0;
   DWORD    nBytesWritten;
   BOOL     bRet;

   // Make sure input file exist
   if (_access(strInFile, 0))
      return NOTFOUND_ERR;

   // Open files
   fhIn = CreateFile(strInFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

    // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Validate record size
   if (iRecSize > MAX_RECSIZE)
      return BADRECSIZE_ERR;

   // Compression loop
   while (ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL))
   {
      lRecCnt++;
      if (nBytesRead > 0)
      {
         ebc2asc((unsigned char *)&acOutBuf[0], (unsigned char *)&acBuf[0], nBytesRead);
         acOutBuf[nBytesRead] = 13;
         acOutBuf[nBytesRead+1] = 10;
         nBytesRead += 2;
         bRet = WriteFile(fhOut, acOutBuf, nBytesRead, &nBytesWritten, NULL);
      } else
      {
         if (!nBytesRead || acBuf[0] == 26)
            break;

         // Log error and let the program continue
         acBuf[40] = 0;
         LogMsg("Bad record [%s] at %u.  Invalid rec length [%u] vs %d", acBuf, lRecCnt, nBytesRead, iRecSize);
         bRet = 1;
         nBytesRead = nBytesWritten;
      }

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (nBytesRead <= iRecSize || nBytesRead != nBytesWritten)
         break;
   }

   // Close log fie
   close_log();

   // Close files
   if (fhIn)
      CloseHandle(fhIn);

   FlushFileBuffers(fhOut);
   CloseHandle(fhOut);
   return lRet;
}

int __stdcall doASC2EBC(LPCSTR strInFile, LPCSTR strOutFile)
{
   char     acBuf[MAX_RECSIZE], acOutBuf[MAX_RECSIZE];
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0;

   // Make sure input file exist
   if (_access(strInFile, 0))
      return NOTFOUND_ERR;

   // Open files
   fhIn = CreateFile(strInFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Copy loop
   while (ReadFile(fhIn, acBuf, MAX_RECSIZE, &nBytesRead, NULL))
   {
      if (nBytesRead > 0)
      {
         asc2ebc((char *)&acOutBuf[0], (char *)&acBuf[0], nBytesRead);
         bRet = WriteFile(fhOut, acOutBuf, nBytesRead, &nBytesWritten, NULL);
      }

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      // Check for EOF
      if (!nBytesRead || nBytesRead != nBytesWritten)
         break;

   }

   // Close log fie
   close_log();

   // Close files
   if (fhIn)
      CloseHandle(fhIn);

   FlushFileBuffers(fhOut);
   CloseHandle(fhOut);
   return lRet;
}

int __stdcall doASC2EBCAddCR(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize)
{
   char     acBuf[MAX_RECSIZE+2], acOutBuf[MAX_RECSIZE+2];
   long     lRet=0;
   
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, lRecCnt=0;
   DWORD    nBytesWritten;
   BOOL     bRet;

   // Make sure input file exist
   if (_access(strInFile, 0))
      return NOTFOUND_ERR;

   // Open files
   fhIn = CreateFile(strInFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return OPEN_ERR;


   // Validate record size
   if (iRecSize > MAX_RECSIZE)
      return BADRECSIZE_ERR;

   // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Conversion loop
   while (ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL))
   {
      lRecCnt++;
      if (nBytesRead > 0)
      {
         asc2ebc((char *)&acOutBuf[0], (char *)&acBuf[0], nBytesRead);
         acOutBuf[nBytesRead] = 13;
         acOutBuf[nBytesRead+1] = 10;
         nBytesRead += 2;
         bRet = WriteFile(fhOut, acOutBuf, nBytesRead, &nBytesWritten, NULL);
      } else
      {
         if (!nBytesRead || acBuf[0] == 26)
            break;                  // EOF

         // Log error and let the program continue
         LogMsg("Bad record [%s] at %u.  Invalid rec length [%u] vs %d", acBuf, lRecCnt, nBytesRead, iRecSize);
         bRet = 1;
         nBytesRead = nBytesWritten;
      }

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (nBytesRead <= iRecSize || nBytesRead != nBytesWritten)
         break;
   }

   // Close log fie
   close_log();

   // Close input files
   if (fhIn)
      CloseHandle(fhIn);

   FlushFileBuffers(fhOut);
   CloseHandle(fhOut);
   return lRet;
}

// iMode: low word is number of bytes to remove, high word is conversion/adding mode
//    Add CRLF              = 0x00000003     Add CR=2 + Add LF=1
//    Add CR only           = 0x00000002     Add CR=2
//    Add LF only           = 0x00000001     Add LF=1

int __stdcall doConvertEx(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize, 
                          int iRemBytes, int iAddMode, int iConvMode)
{
   char     acBuf[MAX_RECSIZE+2], acOutBuf[MAX_RECSIZE+2];
   long     lRet=0;
   
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, lRecCnt=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   int      iRecLen = iRecSize;

   // Make sure input file exist
   if (_access(strInFile, 0))
      return NOTFOUND_ERR;

   // Validate record size
   if (iRecLen > MAX_RECSIZE)
      return BADRECSIZE_ERR;

   if (iRecLen == 0)
      iRecLen = MAX_RECSIZE;

   // Open files
   fhIn = CreateFile(strInFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   // If append mode, use open for append
   if (iConvMode & CONV_APPEND)
   {
      fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

      // Try to move hFile's file pointer some huge distance 
      nBytesRead = SetFilePointer (fhOut, 0, NULL, FILE_END); 
 
      // Test for failure
      if (nBytesRead == 0xFFFFFFFF && (nBytesRead = GetLastError()) != NO_ERROR)
      { 
          // Deal with failure 
          return OPEN_ERR;
      }  

   } else
   {
      fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   }

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      CloseHandle(fhIn);
      return OPEN_ERR;
   }

   // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Conversion loop
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      lRecCnt++;
      //if (!(lRecCnt % 10000))
      //   bRet = true;
      if ((nBytesRead == iRecLen || iRecLen == MAX_RECSIZE) && nBytesRead > 0)
      {
         nBytesRead -= iRemBytes;
         if (iConvMode & CONV_ASC2EBC)
            asc2ebc((char *)&acOutBuf[0], (char *)&acBuf[0], nBytesRead);
         else if (iConvMode & CONV_EBC2ASC)
            ebc2asc((unsigned char *)&acOutBuf[0], (unsigned char *)&acBuf[0], nBytesRead);
         else
            memcpy((char *)&acOutBuf[0], (char *)&acBuf[0], nBytesRead);

         if (iAddMode & CONV_ADDCR)
            acOutBuf[nBytesRead++] = 13;
         if (iAddMode & CONV_ADDLF)
            acOutBuf[nBytesRead++] = 10;
         
         bRet = WriteFile(fhOut, acOutBuf, nBytesRead, &nBytesWritten, NULL);
      } else
      {

         if (!nBytesRead || acBuf[0] == 26)
            break;                  // EOF

         // Log error and let the program continue
         acBuf[40] = 0;
         LogMsg("Bad record [%s] at %u.  Invalid rec length [%u] vs %d", acBuf, lRecCnt, nBytesRead, iRecSize);
         bRet = 1;
         nBytesRead = nBytesWritten;
      }

      if (!bRet)
      {
#ifdef _DEBUG1
         printf("Error occurs: %d\n", GetLastError());
#endif
         lRet = WRITE_ERR;
         break;
      }

      if (nBytesRead < (iRecLen-iRemBytes) || nBytesRead != nBytesWritten)
      {
#ifdef _DEBUG1
         printf("Break out readfile loop: iRecLen=%d, nBytesRead=%d, nBytesWritten=%d\n", iRecLen, nBytesRead, nBytesWritten);
#endif
         break;
      }
   }

   // Close log fie
   close_log();

   // Close files
   FlushFileBuffers(fhOut);
   CloseHandle(fhIn);
   CloseHandle(fhOut);
   return lRet;
}

// This function copies fixed length text file with CRLF at the end.
// Function returns number of records output
int __stdcall doCopyFixed(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize, int iPadding)
{
   char     acBuf[MAX_RECSIZE+2], acOutBuf[MAX_RECSIZE+2], acPad[1024], *pTmp;
   int      iRet=0, iBadRecs=0, iGoodRecs=0;
   FILE     *fdIn, *fdOut, *fdBad;

   // Prepare padding
   if (iPadding > 0)
      memset(acPad, ' ', 1024);

   if (iPadding > 1024)
      return -1;

   fdIn = fopen(strInFile, "r");
   fdOut = fopen(strOutFile, "w");
   strcpy(acBuf, strInFile);
   pTmp = strrchr(acBuf, '.');
   if (pTmp)
      *pTmp = 0;
   strcat(acBuf, ".bad");
   fdBad = fopen(acBuf, "w");

   while (!feof(fdIn))
   {
      acBuf[iRecSize-2] = 0;
      pTmp = fgets(acBuf, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      if (acBuf[iRecSize-2] != '\n')
      {
         iRet = fputs(acBuf, fdBad);
         iBadRecs++;
      } else
      {
         if (iPadding > 0)
         {
            acBuf[iRecSize-2] = 0;
            sprintf(acOutBuf, "%s%*.s\n", acBuf, iPadding, acPad);
            iRet = fputs(acOutBuf, fdOut);
         } else
            iRet = fputs(acBuf, fdOut);
         iGoodRecs++;
      }
   }
   
   fflush(fdOut);
   fflush(fdBad);
   fclose(fdIn);
   fclose(fdOut);
   fclose(fdBad);

   return iRet;
}

// Split files into multiple smaller files
int __stdcall doSplit(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize, int lNumRecs)
{
   char     acOutFile[_MAX_PATH];
   char     acBuf[4096];
   long     lRet=0;
   int      iCnt=1;
   
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   //long     lNumRecs = iNumRecsPerFile;

   LogMsg("doSplit %s into %d recs (%d) per file", strInFile, lNumRecs, iRecSize);

   // Setup file names
   sprintf(acOutFile, strOutFile, iCnt);

   // Make sure input file exist
   if (_access(strInFile, 0))
      return NOTFOUND_ERR;

   // Open files
   fhIn = CreateFile(strInFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return OPEN_ERR;


   // Validate record size
   if (iRecSize > 4096)
      return BADRECSIZE_ERR;

    // Open log file
   open_log(DEFAULT_LOG, "a+");

  // Copy loop
   while (ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL))
   {
      if (nBytesRead > 0)
      {
         // Temporary hard code output record size for LA
         //bRet = WriteFile(fhOut, acBuf, 1147, &nBytesWritten, NULL);
         //if (!bRet || (nBytesWritten != 1147))
         bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
         if (!bRet || (nBytesWritten != nBytesRead))
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
         lRet++;
         if (lRet >= lNumRecs)
         {
            // CLose existing file
            CloseHandle(fhOut);

            // Open new output file
            iCnt++;
            sprintf(acOutFile, strOutFile, iCnt);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                   FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhOut == INVALID_HANDLE_VALUE)
               break;

            lRet = 0;
         }
      }

      //if (!nBytesRead || 1147 != nBytesWritten)
      if (!nBytesRead || nBytesRead != nBytesWritten)
         break;

   }

   // Close log fie
   close_log();

   // Close files
   FlushFileBuffers(fhOut);
   CloseHandle(fhIn);
   CloseHandle(fhOut);
   return 0;
}

int __stdcall doConvert(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize, int iPadding, int iNumRecs, int iConvMode)

{
   char     acBuf[MAX_RECSIZE+2], acOutBuf[MAX_RECSIZE+2];
   char     acOutFile[_MAX_PATH];
   
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet = 0;
   int      iCnt = 1;               // Count number of output files
   int      iRecCnt=0;
   int      iAdjBytes = 0;          // Adjust record length
   int      iRecLen = iRecSize;

   // Make sure input file exist
   if (_access(strInFile, 0))
      return NOTFOUND_ERR;

   // Validate record size
   if (iRecLen > MAX_RECSIZE)
      return BADRECSIZE_ERR;

   if (iRecLen == 0)
      iRecLen = MAX_RECSIZE;

   // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Setup file names
   sprintf(acOutFile, strOutFile, iCnt);

   LogMsg("doConvert %s to %s", strInFile, acOutFile);


   // Open files
   fhIn = CreateFile(strInFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return OPEN_ERR;

   // If append mode, use open for append
   if (iConvMode & CONV_APPEND)
   {
      fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

      // Try to move hFile's file pointer some huge distance 
      nBytesRead = SetFilePointer (fhOut, 0, NULL, FILE_END); 
 
      // Test for failure
      if (nBytesRead == 0xFFFFFFFF && (nBytesRead = GetLastError()) != NO_ERROR)
      { 
          // Deal with failure 
          return OPEN_ERR;
      }  

   } else
   {
      fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   }

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      CloseHandle(fhIn);
      return OPEN_ERR;
   }

   if (iConvMode & CONV_REMCRLF)
      iAdjBytes = -2;
   else if (iConvMode & CONV_REMLF || iConvMode & CONV_REMCR)
      iAdjBytes = -1;

   // Conversion loop
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      iRecCnt++;
      if ((nBytesRead == iRecLen || iRecLen == MAX_RECSIZE) && nBytesRead > 0)
      //if (nBytesRead == iRecLen)
      {
         nBytesRead += iAdjBytes;
         if (iConvMode & CONV_ASC2EBC)
            asc2ebc((char *)&acOutBuf[0], (char *)&acBuf[0], nBytesRead);
         else if (iConvMode & CONV_EBC2ASC)
            ebc2asc((unsigned char *)&acOutBuf[0], (unsigned char *)&acBuf[0], nBytesRead);
         else
            memcpy((char *)&acOutBuf[0], (char *)&acBuf[0], nBytesRead);

         if (iConvMode & CONV_ADDCRLF)
         {
            acOutBuf[nBytesRead++] = 13;
            acOutBuf[nBytesRead++] = 10;
         }
         bRet = WriteFile(fhOut, acOutBuf, nBytesRead, &nBytesWritten, NULL);

         // Check for split option 
         if ((iRecCnt >= iNumRecs) && (iConvMode & CONV_SPLIT))
         {
            // CLose existing file
            CloseHandle(fhOut);

            // Open new output file
            iCnt++;
            sprintf(acOutFile, strOutFile, iCnt);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS,
                   FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhOut == INVALID_HANDLE_VALUE)
               break;

            iRecCnt = 0;
         }
      } else
      {
         if (!nBytesRead || acBuf[0] == 26)
            break;                  // EOF

         // Log error and let the program continue
         acBuf[40] = 0;
         LogMsg("Bad record [%s] at %d.  Invalid rec length [%u] vs %d", acBuf, iRecCnt, nBytesRead, iRecSize);
         bRet = 1;
         nBytesRead = nBytesWritten;
      }

      if (!bRet)
      {
#ifdef _DEBUG1
         printf("Error occurs: %d\n", GetLastError());
#endif
         lRet = WRITE_ERR;
         break;
      }

      if (nBytesRead < (iRecLen+iAdjBytes) || nBytesRead != nBytesWritten)
      {
         printf("Break out readfile loop: iRecLen=%d, nBytesRead=%d, nBytesWritten=%d iRecCnt=%d\n", iRecLen, nBytesRead, nBytesWritten, iRecCnt);
         break;
      }
   }

   // Close log fie
   close_log();

   // Close files
   FlushFileBuffers(fhOut);
   CloseHandle(fhIn);
   CloseHandle(fhOut);
   return lRet;
}

int __stdcall doAppendFix(LPCSTR strFile1, LPCSTR strFile2, LPCSTR strOutFile, int iRecSize)
{
   char     acBuf[MAX_RECSIZE];
   long     lRet=0, lCnt=0;
   int      iCnt=1;
   
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;

    // Open log file
   open_log(DEFAULT_LOG, "a+");

   // Make sure input files exist
   if (_access(strFile1, 0) || _access(strFile2, 0))
   {
      LogMsg("Error access input file: %d\n", GetLastError());
      lRet = NOTFOUND_ERR;
      goto doAppendError;
   }

   // Open files
   fhIn = CreateFile(strFile1, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("Error open input file: %d\n", GetLastError());
      lRet = OPEN_ERR;
      goto doAppendError;
   }

   fhOut = CreateFile(strOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("Error create outfile: %d\n", GetLastError());
      lRet = OPEN_ERR;
      goto doAppendError;
   }


   // Validate record size
   if (iRecSize > MAX_RECSIZE)
   {
      LogMsg("Error bad record size: %d\n", iRecSize);
      lRet = BADRECSIZE_ERR;
      goto doAppendError;
   }

   // Copy loop
   while (ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL))
   {
      if (nBytesRead != iRecSize)
      {
         // No data, get out
         break;
      }

      lCnt++;              // # of output recs
      bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (nBytesRead != nBytesWritten)
         break;

   }

   // Close files
   if (fhIn)
      CloseHandle(fhIn);

   if (!lRet)
   {
      // Open 2nd file
      fhIn = CreateFile(strFile2, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
      if (fhIn == INVALID_HANDLE_VALUE)
      {
         CloseHandle(fhOut);              // Close output file
         lRet = OPEN_ERR;
         goto doAppendError;
         //return OPEN_ERR;
      }

      // Copy loop
      while (ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL))
      {
         if (nBytesRead != iRecSize)
            break;

         lCnt++;              // # of output recs
         bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            lRet = WRITE_ERR;
            break;
         }

         if (nBytesRead != nBytesWritten)
            break;
      }

      // Close files
      if (fhIn)
         CloseHandle(fhIn);
   }

   FlushFileBuffers(fhOut);
   CloseHandle(fhOut);
   LogMsg("Total records output: %d\n", lCnt);

   doAppendError:
   close_log();

   return lRet;
}
