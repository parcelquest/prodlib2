//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by testlib.rc
//
#define IDD_TESTLIB_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDD_TESTADR                     129
#define IDD_TESTNAME                    130
#define IDD_EBC2ASC                     131
#define IDD_BUILDCMP                    132
#define IDD_TESTCMP                     133
#define IDC_EDITINPUT                   1000
#define IDC_EDITOUTPUT                  1001
#define IDC_RECSIZE                     1002
#define IDC_TESTADDR1                   1003
#define IDC_TESTADDR2                   1004
#define IDC_STRNUM                      1005
#define IDC_STRDIR                      1006
#define IDC_INPUTNAME                   1006
#define IDC_STRNAME                     1007
#define IDC_SWAPNAME                    1007
#define IDC_INPUTFILE                   1007
#define IDC_STRSFX                      1008
#define IDC_LASTNAME                    1008
#define IDC_OUTPUTFILE                  1008
#define IDC_CITYNAME                    1009
#define IDC_FIRSTNAME                   1009
#define IDC_ADDCRLF                     1009
#define IDC_STATEABR                    1010
#define IDC_MIDDLENAME                  1010
#define IDC_RECLEN                      1010
#define IDC_LASTNAME2                   1011
#define IDC_DEFFILE                     1011
#define IDC_TESTADDR3                   1011
#define IDC_FIRSTNAME2                  1012
#define IDC_TESTADDR4                   1012
#define IDC_MIDDLENAME2                 1013
#define IDC_STRUNIT                     1013
#define IDC_OWNERNAME                   1014
#define IDC_TESTADDR5                   1014
#define IDC_OWNERNAME2                  1015
#define IDC_ZIPCODE                     1015
#define IDC_CAREOF                      1016
#define IDC_VESTING                     1017
#define IDC_INPUTNAME2                  1018
#define IDC_INPUTNAME3                  1019
#define IDC_INPUTNAME4                  1020
#define IDC_NAMETITLE                   1021
#define IDC_PREVIOUS                    1023
#define IDC_NEXTITEM                    1024
#define IDC_PREVADR                     1024
#define IDC_FIRSTITEM                   1025
#define IDC_NEXTADR                     1025
#define IDC_CNTYCODE                    1025
#define IDC_FIRSTADR                    1026
#define IDC_WORKDIR                     1026
#define IDC_STATUS                      1028
#define IDC_SKIPREC                     1029
#define IDC_INFILEXT                    1030
#define IDC_CMPFILE                     1030
#define IDC_ENABLESTOP                  1031
#define IDC_CMPREC                      1031
#define IDC_RECNUM                      1032
#define IDC_LOADCMP                     1033

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1034
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
