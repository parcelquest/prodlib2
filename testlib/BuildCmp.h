#if !defined(AFX_BUILDCMP1_H__294FA9E2_62A8_420A_8FBE_5F21176D6E28__INCLUDED_)
#define AFX_BUILDCMP1_H__294FA9E2_62A8_420A_8FBE_5F21176D6E28__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BuildCmp.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// BuildCmp dialog

class BuildCmp : public CDialog
{
// Construction
public:
	BuildCmp(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(BuildCmp)
	enum { IDD = IDD_BUILDCMP };
	CButton	m_cmdExit;
	CButton	m_cmdStart;
	CString	m_strCntyCode;
	CString	m_strStatus;
	CString	m_strWorkDir;
	int		m_iRecLen;
	CString	m_strInfileExt;
	BOOL	m_bSkipFirstRec;
	BOOL	m_bStopable;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BuildCmp)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation     

protected:

	// Generated message map functions
	//{{AFX_MSG(BuildCmp)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BUILDCMP1_H__294FA9E2_62A8_420A_8FBE_5F21176D6E28__INCLUDED_)
