// testlib.h : main header file for the TESTLIB application
//

#if !defined(AFX_TESTLIB_H__0C17FD8B_2229_11D5_9E77_0060971C5823__INCLUDED_)
#define AFX_TESTLIB_H__0C17FD8B_2229_11D5_9E77_0060971C5823__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTestlibApp:
// See testlib.cpp for the implementation of this class
//

class CTestlibApp : public CWinApp
{
public:
	CTestlibApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestlibApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTestlibApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTLIB_H__0C17FD8B_2229_11D5_9E77_0060971C5823__INCLUDED_)
