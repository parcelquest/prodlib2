// TestName.cpp : implementation file
//

#include "stdafx.h"
#include "testlib.h"
#include "..\prodlib.h"
#include "Utils.h"
#include "TestName.h"
#include "NameList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define  CRESIZ_NAME1   29

typedef struct _tOwnerName
{
   char  acName1[64];
   char  acName2[64];
   char  acSwapName[64];
   char  acCareOf[32];
   char  acVest[16];
   char  acTitle[16];
   char  acOF[32];
   char  acOM[32];
   char  acOL[32];
   char  acSF[32];
   char  acSM[32];
   char  acSL[32];
} OWNER;

#define  MAX_GLE_VEST_CODE 3
#define  MAX_LAS_VEST_CODE 6

char asVestCode_LAS[6][5]=
{
   "JT", "TC", "CP", "HWCP", "HWJT", "WHJT"
};

char asVestCode_GLE[MAX_GLE_VEST_CODE][4]=
{
   "J/T", "T/C", "C/P"
};

char asVestCode3[20][4]=
{
   " HW"," SM"," SE"," SW"," MM"," MS"," MW"," UM"," UW"," WD",
   " WR"," WS"," WH"," JT"," TC"," CP"," CR"," PT","",""
};

char asVestCode4[12][6]=
{
   "HWJT","HWCP","HWTC","WHJT","MSJT","WSJT","UMSE","UWJT","SWSE","WDLE","SWJT",""
};

char asVestCode5[12][6]=
{
   "HW JT","HW CP","HW TC","WH JT","MS JT","WS JT","UM SE","UW JT","SW SE","WD LE","SW JT",""
};

long  *pNameList, *pNameSrc;

OWNER myOwners;

char *getVestingCode_LAS(char *pVesting, char *pVestCode);
char *getVestingCode_GLE(char *pVesting, char *pVestCode);
void splitOwner_LAS(char *pNames, OWNER *pOwners);
void splitOwner_DNX(char *pNames, OWNER *pOwners);
void splitOwner_GLE(char *pNames, OWNER *pOwners);
void splitOwner_SBX(char *pNames, OWNER *pOwners);
void splitOwner_SCR(char *pNames, OWNER *pOwners);
void splitOwner_SJX(char *pNames, OWNER *pOwners);
int  splitOwner(char *pNames, OWNER *pOwners, int iParseType=2);
void Org_MergeOwner(char *pName1, char *pName2, char *pName3, char *pName4);
void Imp_MergeOwner(char *pNames, OWNER *pOwners);
void Slo_MergeOwner(char *pNames, OWNER *pOwners);
void Mod_MergeOwner(char *pNames);
void Lax_MergeOwner(char *pN1, char *pN2, char *pN3);
void Ama_MergeOwner(char *pNames, OWNER *pOwners);
void Hum_MergeOwner(char *pNames, OWNER *pOwners);
void Sac_MergeOwner(char *pNames, OWNER *pOwners);
void Sta_MergeOwner(char *pNames, OWNER *pOwners);
void Ccx_MergeOwner(char *pName1, char *pName2);
void Sfx_MergeOwner(char *pName1, char *pName2);
void Kin_MergeOwner(char *pName1, char *pName2);
void Fre_MergeOwner(char *pName1, char *pName2);
void cleanName(char *pSrcName, char *pDstName, int iLen=0);
//void blankRem(char *pBuf, int iLen=0);
void MergeName(char *pName1, char *pName2, char *pNames, char bChk=',');
int  MergeName2(char *pOwner1, char *pOwner2, char *pNames, char bChk=',');
void Sdx_MergeOwner(char *pRollRec);
void Mrn_MergeOwner(char *pNames);
void Sbd_MergeOwner(char *pName1, char *pName2, char *pVest1, char *pVest2);
void Smx_MergeOwner(char *pName1, char *pName2);
void Iny_MergeOwner(char *pNames);
void Scl_MergeOwner(char *pName1);
void Edx_MergeOwner(char *pName1);
void Tul_MergeOwner(char *pName1);
void Sbt_MergeOwner(char *pNames, OWNER *pOwners);

/////////////////////////////////////////////////////////////////////////////
// CTestName dialog


CTestName::CTestName(CWnd* pParent /*=NULL*/)
	: CDialog(CTestName::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestName)
	m_strFName1 = _T("");
	m_strFName2 = _T("");
	m_strInput  = _T("");
	m_strInput2 = _T("");
	m_strInput3 = _T("");
	m_strInput4 = _T("");
	m_strLName1 = _T("");
	m_strLName2 = _T("");
	m_strMName1 = _T("");
	m_strMName2 = _T("");
	m_strSwapName = _T("");
	m_strOwners = _T("");
	m_strOwner2 = _T("");
	m_strCareOf = _T("");
	m_strVesting = _T("");
	m_strTitle = _T("");
	//}}AFX_DATA_INIT
   m_iListCount = SBT_NAMECNT-1;
   m_iCurItem = 0;
   pNameList = (long *)&Sbt_NameList[m_iCurItem];
   pNameSrc  = (long *)&Sbt_NameList[0];
   m_strInput = Sbt_NameList[m_iCurItem];
}


void CTestName::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestName)
	DDX_Text(pDX, IDC_FIRSTNAME, m_strFName1);
	DDX_Text(pDX, IDC_FIRSTNAME2, m_strFName2);
	DDX_Text(pDX, IDC_INPUTNAME, m_strInput);
	DDX_Text(pDX, IDC_LASTNAME, m_strLName1);
	DDX_Text(pDX, IDC_LASTNAME2, m_strLName2);
	DDX_Text(pDX, IDC_MIDDLENAME, m_strMName1);
	DDX_Text(pDX, IDC_MIDDLENAME2, m_strMName2);
	DDX_Text(pDX, IDC_SWAPNAME, m_strSwapName);
	DDX_Text(pDX, IDC_OWNERNAME, m_strOwners);
	DDX_Text(pDX, IDC_OWNERNAME2, m_strOwner2);
	DDX_Text(pDX, IDC_CAREOF, m_strCareOf);
	DDX_Text(pDX, IDC_VESTING, m_strVesting);
	DDX_Text(pDX, IDC_INPUTNAME2, m_strInput2);
	DDX_Text(pDX, IDC_INPUTNAME3, m_strInput3);
	DDX_Text(pDX, IDC_INPUTNAME4, m_strInput4);
	DDX_Text(pDX, IDC_NAMETITLE, m_strTitle);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestName, CDialog)
	//{{AFX_MSG_MAP(CTestName)
	ON_BN_CLICKED(IDOK, OnTestName)
	ON_BN_CLICKED(IDC_PREVIOUS, OnPreviousItem)
	ON_BN_CLICKED(IDC_NEXTITEM, OnNextitem)
	ON_BN_CLICKED(IDC_FIRSTITEM, OnFirstitem)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestName message handlers

void CTestName::OnTestName() 
{
   char     acTmp1[128], acTmp2[128], acTmp3[128], acTmp4[128], *pTmp;
//   char     acOF[32], acOL[32], acOM[32], acSF[32], acSM[32], acSL[32];
//   char     acSpouse[256];
   int      iNameFlg=0, iTmp=0;
   CString  strTmp;

   UpdateData(true);
	
   //m_strOwners = swapName(m_strInput, acTmp);
   //UpdateData(false);
   //return;

   // Remove extra comma and every thing after # or %
   strcpy(acTmp1, m_strInput);
   strcpy(acTmp2, m_strInput2);
   strcpy(acTmp3, m_strInput3);
   strcpy(acTmp4, m_strInput4);

   // LAX
   /*
   if (!memcmp(acTmp3, "C/O", 3))
   {
      pTmp = (char *)&acTmp3[4];
      while (*pTmp == ' ') pTmp++;
      m_strCareOf = pTmp;
   }
   */
   m_strTitle = "";

   //splitOwner_GLE(acTmp1, &myOwners);
   //splitOwner_LAS(acTmp1, &myOwners);
   //splitOwner_DNX(acTmp1, &myOwners);
   //splitOwner_SBX(acTmp1, &myOwners);
   //splitOwner_SCR(acTmp1, &myOwners);
   //splitOwner_SJX(acTmp1, &myOwners);

   //Org_MergeOwner(acTmp1, acTmp2, acTmp3, acTmp4);
   //Imp_MergeOwner(acTmp1, &myOwners);
   //Slo_MergeOwner(acTmp1, &myOwners);
   //Lax_MergeOwner(acTmp1, acTmp2, acTmp4);
   //Ama_MergeOwner(acTmp1, &myOwners);
   //Hum_MergeOwner(acTmp1, &myOwners);
   //Sac_MergeOwner(acTmp1, &myOwners);
   //Sta_MergeOwner(acTmp1, &myOwners);
   //Ccx_MergeOwner(acTmp1, acTmp2);
   //Sfx_MergeOwner(acTmp2, acTmp1);
   //Kin_MergeOwner(acTmp2, acTmp1);
   /*
   cleanName(acTmp1, acTmp3, 0);
   cleanName(acTmp2, acTmp4, 0);
   iTmp = MergeName2(acTmp3, acTmp4, acTmp, ',');
   m_strOwner2 = "";
   if (iTmp == 1)
      m_strOwners = acTmp;
   else if (iTmp == 2)
      m_strOwners = acTmp1;
   else
   {
      m_strOwners = acTmp1;
      m_strOwner2 = acTmp2;
   }
   */

   //Sdx_MergeOwner(acTmp1);
   //Fre_MergeOwner(acTmp1, acTmp2);
   //Mrn_MergeOwner(acTmp1);
   //Sbd_MergeOwner(acTmp1, acTmp2, acTmp3, acTmp4);
   //Smx_MergeOwner(acTmp1, acTmp2);
   //Iny_MergeOwner(acTmp1);
   //Scl_MergeOwner(acTmp1);
   //Edx_MergeOwner(acTmp1);
   //Tul_MergeOwner(acTmp1);
   Sbt_MergeOwner(acTmp1, &myOwners);
   m_strFName1 = myOwners.acOF;
   m_strFName2 = myOwners.acSF;
   m_strMName1 = myOwners.acOM;
   m_strMName2 = myOwners.acSM;
   m_strLName1 = myOwners.acOL;
   m_strLName2 = myOwners.acSL;
   m_strCareOf = myOwners.acCareOf;

   m_strOwners = myOwners.acName1;
   m_strOwner2 = myOwners.acName2;
   m_strSwapName = myOwners.acSwapName;
   m_strVesting = myOwners.acVest;
   m_strTitle = myOwners.acTitle;

   if (myOwners.acTitle[0] == '1')
   {
      m_strTitle = (char *)&myOwners.acTitle[1];
      pTmp = strchr(myOwners.acName1, '&');
      if (pTmp)
      {
         strTmp = --pTmp;
         *++pTmp = 0;
         m_strOwners = myOwners.acName1;
         m_strOwners += m_strTitle;
         m_strOwners += strTmp;
      } else
      {
         if (m_strOwners.GetAt(m_strOwners.GetLength()-1) == ' ')
            m_strOwners += m_strTitle;
         else
         {
            m_strOwners += " ";
            m_strOwners += m_strTitle;
         }
      }
   } else if (myOwners.acTitle[0] == '2')
   {
      m_strTitle = (char *)&myOwners.acTitle[1];
      if (m_strOwner2.GetAt(m_strOwner2.GetLength()-1) == ' ')
         m_strOwner2 += m_strTitle;
      else
      {
         m_strOwner2 += " ";
         m_strOwner2 += m_strTitle;
      }
   } else if (myOwners.acTitle[0] && !strstr(myOwners.acName1, myOwners.acTitle))
   {
      m_strTitle = myOwners.acTitle;
      pTmp = strchr(myOwners.acName1, '&');
      if (pTmp)
      {
         strTmp = --pTmp;
         *++pTmp = 0;
         m_strOwners = myOwners.acName1;
         m_strOwners += m_strTitle;
         m_strOwners += strTmp;
      } else
      {
         if (m_strOwners.GetAt(m_strOwners.GetLength()-1) == ' ')
            m_strOwners += m_strTitle;
         else
         {
            m_strOwners += " ";
            m_strOwners += m_strTitle;
         }
      }
   }

   UpdateData(false);
}

void CTestName::OnCancel() 
{
	CDialog::OnCancel();
}

void CTestName::OnPreviousItem() 
{
   char  *pTmp;
   if (m_iCurItem > 0)
   {
      m_iCurItem--;	
      pNameList--;
      pTmp = (char *)*pNameList;
      m_strInput = pTmp;
      UpdateData(false);
   }
}

void CTestName::OnNextitem() 
{
   char  *pTmp;
   if (m_iCurItem < m_iListCount)
   {
      m_iCurItem++;	
      pNameList++;
      pTmp = (char *)*pNameList;
      m_strInput = pTmp;
      UpdateData(false);
   }
}

void CTestName::OnFirstitem() 
{
   m_iCurItem = 0;	
   pNameList = pNameSrc;
   m_strInput = (char *)*pNameList;
   UpdateData(false);
}

/********************************************************************

char *isNumIncluded(char *pBuf, int iLen)
{
   int   iRet, iCnt;
   char  *pRet=NULL;

   if (iLen > 0)
      iCnt = iLen;
   else
      iCnt = strlen(pBuf);

   for (iRet = 0; iRet < iCnt; iRet++)
   {
      if (isdigit(*pBuf))
	   {
		   pRet = pBuf;
         break;
	   }
      pBuf++;
   }
   return pRet;
}

/********************************************************************/

void cleanName(char *pSrcName, char *pDstName, int iLen)
{
   char  acTmp[128];
   char  *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);
   blankRem((char *)&acTmp[0]);

   // Remove 1/2 or 25%
   if ((pTmp=strchr(acTmp, '/')) || (pTmp=strchr(acTmp, '%')) )
   {
      do {
         pTmp--;
      } while (pTmp > acTmp && (isdigit(*pTmp) || *pTmp == '.'));

      if (pTmp > acTmp)
         *pTmp = 0;
   }
   
   // PALMA, SALVATORE T 27.5
   if (pTmp=strchr(acTmp, '.'))
   {
      if (isdigit(*(pTmp+1)))
      {
         while (pTmp > acTmp && *pTmp > ' ') pTmp--;

         if (pTmp > acTmp)
            *pTmp = 0;
      }
   }

   if (pTmp=strstr(acTmp, " - "))
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " ETUX"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET UX"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET AL"))
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " TSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUCC TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUC TRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TR "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TSTES"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO TRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " COTRST"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " LIFE EST"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " M D"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " FAMILY TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " FMLY TR"))
      *pTmp = 0;

   // Check for TR - last word only
   if (pTmp = strrchr(acTmp, ' '))
   {
      if (!strcmp(pTmp, " TR") || !strcmp(pTmp, " JT") || !strcmp(pTmp, " TC") || 
          !strcmp(pTmp, " CP") || !strcmp(pTmp, " L EST") || !strcmp(pTmp, " ESTATE"))
      {
         if (*(pTmp-1) == ',') pTmp--;
         *pTmp = 0;
      }
   }

   strcpy(pDstName, acTmp);
}

/************************************* MergeName **********************************
 *
 * Merge names that has the same last name.
 *
 **********************************************************************************/

void MergeName(char *pName1, char *pName2, char *pNames, char bChk)
{
   char *pTmp, acTmp[128];
   int iTmp, iRet = 1;

   strcpy(acTmp, pName1);
   if (acTmp[strlen(pName1)-1] != '&')
      strcat(acTmp, " & ");

   pTmp = strchr(pName2, bChk);
   if (pTmp)
      strcat(acTmp, ++pTmp);
   else
   {
      // In case there is no comma after last name
      iTmp = 0;
      while (*(pName1+iTmp) == *(pName2+iTmp))
         iTmp++;

      // If last name is matching up OK, we can merge them
      if (iTmp > 2 && *(pName1+iTmp) == bChk && *(pName2+iTmp) == ' ')
         strcat(acTmp, pName2+iTmp);

   }
   strcpy(pNames, acTmp);
}

/************************************* MergeName1 *********************************
 *
 * Merge names that has the same last name.  If mergable, return 1.
 * If same last and first name, return 2.  Return 0 if not mergable.
 *
 **********************************************************************************/

int MergeName2(char *pOwner1, char *pOwner2, char *pNames, char bChk)
{
   char  acOwner1[128], acOwner2[128], acTmp[128];
   char  *pName1, *pName2, *pTmp1, *pTmp2;

   *pNames = 0;

   // If the same name, return
   if (!strcmp(pOwner1, pOwner2))
      return 2;

   strcpy(acOwner1, pOwner1);
   strcpy(acOwner2, pOwner2);

   pName1 = strchr(acOwner1, bChk);
   pName2 = strchr(acOwner2, bChk);
   // If either name is single word, do nothing
   if (!pName1 || !pName2)
      return 0;

   *pName1 = 0;
   *pName2 = 0;

   // If last name are not the same, do nothing
   if (strcmp(acOwner1, acOwner2))
      return 0;

   // If they have the same first name, don't combine and return 2
   pTmp1 = strchr(pName1+2, ' ');
   pTmp2 = strchr(pName2+2, ' ');
   if (pTmp1 && pTmp2)
   {
      *pTmp1 = 0;
      *pTmp2 = 0;
      if (!strcmp(pName1+1, pName2+1))
         return 2;
      
      *pTmp1 = ' ';
      *pTmp2 = ' ';
   } else if (!pTmp1 && !pTmp2)
   {
      if (!strcmp(pName1+1, pName2+1))
         return 2;
   }

   // Combine owners
   *pName1 = ' ';       // Keep Name1 as is
   pName2++;            // Skip last name of owner2

   sprintf(acTmp, "%s & %s", acOwner1, pName2);
   blankRem(acTmp);
   strcpy(pNames, acTmp);

   return 1;
}

/********************************************************************/

char *getVestingCode_LAS(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_LAS_VEST_CODE; iIdx++)
   {
      if (!strcmp(asVestCode_LAS[iIdx], pVesting))
      {
         strcpy(pVestCode, pVesting);
         break;
      }
   }

   return pVestCode;
}

/********************************************************************/

char *getVestingCode_GLE(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_GLE_VEST_CODE; iIdx++)
   {
      if (!memcmp(asVestCode_GLE[iIdx], pVesting, 3))
      {
         strcpy(pVestCode, asVestCode_LAS[iIdx]);
         break;
      }
   }

   return pVestCode;
}

/******************************** blankRem() *******************************
 *
 * Reduce multi-space into single one
 *
 ***************************************************************************

void blankRem(char *pBuf, int iLen)
{
   char *pTmp, acTmp[2048];
   int   iTmp=0;
   bool  bSpace = false;

   if (iLen > 0)
      *(pBuf+iLen) = 0;

   // Remove leading space
   pTmp = pBuf;
   while (*pTmp == ' ')
      pTmp++;

   // Remove extra space in the middle
   while (*pTmp)
   {
      if (*pTmp == ' ')
      {
         if (!bSpace)
         {
            bSpace = true;
            acTmp[iTmp++] = ' ';
         }
      } else
      {
         acTmp[iTmp++] = *pTmp;
         bSpace = false;
      }

      pTmp++;
   }

   if (acTmp[iTmp-1] == ' ')
      acTmp[iTmp-1] = 0;
   else
      acTmp[iTmp] = 0;
   strcpy(pBuf, acTmp);
}

/******************************** quoteRem() *******************************
 *
 * Remove all single and double quotes in string
 *
 ***************************************************************************

void quoteRem(char *pBuf)
{
   char *pTmp, acTmp[2048];
   int   iTmp=0;
   bool  bSpace = false;

   pTmp = pBuf;

   while (*pTmp)
   {
      if (*pTmp != 34 && *pTmp != 39)
         acTmp[iTmp++] = *pTmp;
      pTmp++;
   }

   acTmp[iTmp] = 0;
   strcpy(pBuf, acTmp);
}

/********************************************************************

char *myTrim(char *pString)
{
   int i;

   // remove trailing spaces and tabs and other non-printables
   i = strlen(pString)-1;
   while (i >= 0)
   {
      if ((pString[i] > ' ') && (pString[i] <= '~'))
         break;
      pString[i] = 0;
      i--;
   }

   return pString;
}
*/

#define  SIZ_NAME1   29

/********************************************************************
 *
 * Return value:
 *    -1 : cannot parse
 *     0 : swap normal, name combine
 *     1 : combine names, spouse last name ignore
 *     2 : break into two separate names
 *
 ********************************************************************/

int splitOwner(char *pNames, OWNER *pOwners, int iParseType)
{
   char  strSwapName[128], strOwners[128], strOwner2[64];
   char  acOF[32], acOL[32], acOM[64], acSF[32], acSM[64], acSL[32];
   char  acSpouse[256], acTitle[16];
   int   iFlg=0;

   memset((void *)pOwners, 0, sizeof(OWNER));
   strOwner2[0] = 0;
   acTitle[0] = 0;
   switch (iParseType)
   {
      case 1:
         iFlg = ParseOwnerName1(pNames, acOF, acOM, acOL, acSF, acSM, acSL);
         break;
      case 2:
         iFlg = ParseOwnerName2(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
         break;
      case 3:
         iFlg = ParseOwnerName3(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
         break;
      case 4:
         iFlg = ParseOwnerName4(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
         break;
      case 5:
         iFlg = ParseOwnerName5(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
         break;
      case 6:
         iFlg = ParseOwnerName6(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
         break;
      default:
         ParseOwnerName(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
   }

   if (iFlg > 0)
   {
      int i = 0;

      iFlg = 0;
      // Strip off extra spaces
      while (*pNames)
      {
         if (!iFlg || *pNames > ' ')
         {
            pOwners->acName1[i] = *pNames;
            pOwners->acSwapName[i] = *pNames;
            i++;
            if (*pNames == ' ')
               iFlg = 1;
            else
               iFlg = 0;
         } 
         pNames++;
      }
      return -1;
   }

   strcpy(strSwapName, acOF);
   if (acOM[0])
   {
      strcat(strSwapName, " ");
      strcat(strSwapName, acOM);
   }

   // If spouse last name avail,, concat name1 and name2
   if (acSL[0])
   {
      strcat(strSwapName, " ");
      strcat(strSwapName, acOL);

      // Make owner2
      if (acSM[0])
         sprintf(strOwner2, "%s %s %s", acSL, acSF, acSM);
      else
         sprintf(strOwner2, "%s %s", acSL, acSF);

      // Make Owner1
      if (acOM[0])
         sprintf(strOwners, "%s %s %s", acOL, acOF, acOM);
      else
         sprintf(strOwners, "%s %s", acOL, acOF);

      iFlg = 2;        // 2 separate names
   } else
   {
      iFlg = 0;     
      if (acSF[0])
      {
         sprintf(acSpouse, " & %s ", acSF);
         if (acSM[0])
         {
            if (strcmp(acSM, acOL))
            {
               char *pTmp;

               pTmp = strstr(acSM, acOL);
               if (pTmp)
               {
                  *(--pTmp) = 0;
                  iFlg = 1;            // ignore spouse last name
               }
               strcat(acSpouse, acSM);
               strcat(acSpouse, " ");
            } else
            {
               iFlg = 1;               // ignore spouse last name
               acSM[0] = 0;
            }
         }
      } else
         strcpy(acSpouse, " ");

      if (acOL[0] > ' ')
      {
         // Make Owner1
         if (acOM[0])
            sprintf(strOwners, "%s %s %s%s", acOL, acOF, acOM, acSpouse);
         else
            sprintf(strOwners, "%s %s%s", acOL, acOF, acSpouse);

         // Add spouse to swap name
         strcat(acSpouse, acOL);
         strcat(strSwapName, acSpouse);
      } else
      {
         strcpy(strSwapName, pNames);
         strcpy(strOwners, pNames);
         iFlg = -1;
      }
   }
   strOwners[sizeof(pOwners->acName1)-1] = 0;
   strcpy(pOwners->acName1, strOwners);
   strcpy(pOwners->acName2, strOwner2);
   strSwapName[sizeof(pOwners->acSwapName)-1] = 0;
   strcpy(pOwners->acSwapName, strSwapName);
   strcpy(pOwners->acOF, acOF);
   acOM[sizeof(pOwners->acOM)-1] = 0;
   strcpy(pOwners->acOM, acOM);
   strcpy(pOwners->acOL, acOL);
   strcpy(pOwners->acSF, acSF);
   acSM[sizeof(pOwners->acSM)-1] = 0;
   strcpy(pOwners->acSM, acSM);
   strcpy(pOwners->acSL, acSL);
   strcpy(pOwners->acTitle, acTitle);

   return iFlg;
}

/********************************************************************/

void splitOwner_LAS(char *pNames, OWNER *pOwners)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acTmp1[64], acSave[64], *pTmp, *pTmp1, *pName2;
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Point to name2
   pName2 = pNames+SIZ_NAME1;

   // Check owner2 for # and %
   if (*pName2 == '#' )
   {
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      strcpy(pOwners->acCareOf, (char *)(pName2+2));
      *pName2 = 0;
   //} else if (!memicmp(pName2, "C/O", 3) ) // Check for Care of
   //{
   //   strcpy(pOwners->acCareOf, (char *)(pName2+4));
   //   *pName2 = 0;
   } else if (pTmp = strstr(pName2, "C/O"))
   {
      strcpy(pOwners->acCareOf, (char *)(pTmp+4));
      *pName2 = 0;
   } else if (!memcmp(pName2, "ATTN", 4))
   {
      *pName2 = 0;
      if (*(pName2+4) == ' ')
         strcpy(pOwners->acCareOf, pName2+5);
      else
         strcpy(pOwners->acCareOf, pName2+6);
   } else if (strchr(pName2, '&'))
   {
      // Process as two names
      memcpy(acOwners, pNames, SIZ_NAME1);
      acOwners[SIZ_NAME1] = 0;
      
      // Check for vesting
      pTmp1 = strrchr(myTrim(acOwners), ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_LAS(acSave, acTmp1);
         if (*pTmp)
         {
            strcpy(pOwners->acVest, acTmp1);
            *pTmp1 = 0;
         }
      } 

      if (pTmp = strstr(acOwners, " ALL AS"))
         *pTmp = 0;

      splitOwner(acOwners, &myOwner);
      strcpy(pOwners->acName1, myOwner.acName1);
      strcpy(pOwners->acSwapName, myOwner.acSwapName);
      if (myOwner.acVest[0] > ' ')
         strcpy(pOwners->acVest, myOwner.acVest);

      if (*pName2 == '&')
         memcpy(acOwners, pName2+2, SIZ_NAME1-2);
      else
         memcpy(acOwners, pName2, SIZ_NAME1);
      acOwners[SIZ_NAME1] = 0;

      // Check for vesting
      pTmp1 = strrchr(myTrim(acOwners), ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_LAS(acSave, acTmp1);
         if (*pTmp)
         {
            strcpy(pOwners->acVest, acTmp1);
            *pTmp1 = 0;
         }
      } 
      splitOwner(acOwners, &myOwner);
      strcpy(pOwners->acName2, myOwner.acName1);
      if (myOwner.acVest[0] > ' ')
         strcpy(pOwners->acVest, myOwner.acVest);
      return;
   }

   // Remove multiple spaces
   pTmp = pNames;
   iTmp = 0;
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      if (*pTmp == ' ')
         while (*pTmp && *pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Check for AKA - replace it with '&'
   pTmp = strstr(acTmp, " AKA ");
   if (pTmp)
   {
      *(pTmp+1) = '&';
      *(pTmp+2) = ' ';
      *(pTmp+3) = ' ';
   }

   // Blank out "SUC TRS" and "CO TRS", "DVA"
   if (pTmp = strstr(acTmp, " SUC TRS"))
      *pTmp = 0;
   else if (pTmp = strstr(acTmp, " CO TRS"))
      *pTmp = 0;
   else if (pTmp = strstr(acTmp, " DVA"))
      *pTmp = 0;

  // Check for vesting
   pTmp1 = strrchr(acTmp, ' ');
   if (pTmp1)
   {
      strcpy(acSave, ++pTmp1);
      pTmp = getVestingCode_LAS(acSave, acTmp1);
      if (*pTmp)
      {
         strcpy(pOwners->acVest, acTmp1);
         *pTmp1 = 0;
      } else
      {
         if (pTmp1 = strstr(acTmp, " JT"))
         {
            strcpy(pOwners->acVest, "JT");
            *pTmp1 = 0;
         } else if (pTmp1 = strstr(acTmp, " TC"))
         {
            strcpy(pOwners->acVest, "TC");
            *pTmp1 = 0;
         } else if (pTmp1 = strstr(acTmp, " CP"))
         {
            strcpy(pOwners->acVest, "CP");
            *pTmp1 = 0;
         }
      }
   } 

   // Save data to append later
   pTmp = strstr(acTmp, "FAM TRUST");
   if (pTmp)
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Now parse owners
   splitOwner(acTmp, &myOwner, 1);
   strcpy(pOwners->acName1, myOwner.acName1);
   if (strcmp(myOwner.acName1, myOwner.acName2))
      strcpy(pOwners->acName2, myOwner.acName2);
   strcpy(pOwners->acSwapName, myOwner.acSwapName);
   if (myOwner.acVest[0] > ' ')
      strcpy(pOwners->acVest, myOwner.acVest);

   if (acSave[0])
   {
      strcat(pOwners->acName1, acSave);
   }

   strcpy(pOwners->acOF, myOwner.acOF);
   strcpy(pOwners->acOM, myOwner.acOM);
   strcpy(pOwners->acOL, myOwner.acOL);
   strcpy(pOwners->acSF, myOwner.acSF);
   strcpy(pOwners->acSM, myOwner.acSM);
   strcpy(pOwners->acSL, myOwner.acSL);
}

/********************************************************************/

void splitOwner_DNX(char *pNames, OWNER *pOwners)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acSave[64];
   char  *pName1, *pName2, *pTmp, *pTmp1;
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));
   strncpy(acOwners, pNames, 63);
   acOwners[63] = 0;
   pName1 = myTrim(acOwners);

   // Point to name2
   pName2 = pName1+SIZ_NAME1;

   // Drop trustee
   if (pTmp = strstr(pName1, "TRUSTEE"))
   {      
      if (!memcmp(pName2-2, "  ", 2))        // Name2 is a trustee
         *pName2 = 0;
      else if (!memcmp(pTmp-3, "CO-", 3))    // Remove CO-TRUSTEE
         *(pTmp-3) = 0;
      else
         *pTmp = 0;                          // Remove TRUSTEE
   }

   // Drop Estate of
   if (pTmp = strstr(pName1, " ESTATE OF"))
   {
      *pTmp = 0;
   }

   // Translate OR into AND
   if (pTmp = strstr(pName1, " OR "))
   {
      *(pTmp+1) = '&';
      *(pTmp+2) = ' ';
   }

   // Check for Care of
   if (!memcmp(pName2, "C/O", 3) ) 
   {
      if (*(pName2+4) == ' ')
         iTmp = 5;
      else
         iTmp = 4;
      strcpy(pOwners->acCareOf, (char *)(pName2+iTmp));
      *pName2 = 0;
   } else if (!memcmp(pName2, "SPACE ", 6) )
      *pName2 = 0;

   // Keep last word in case written over into Name2
   pTmp = pName2;
   if (*(pTmp-1) != ' ' && *pTmp != ' ')
   {
      // Word continuation
      while (*pTmp >= '0')
         pTmp++;

      if (*pTmp == ',')
         *pName2 = 0;
      else if (*(pTmp+2) == '.')
         *(pTmp+2) = 0;
      else
         *pTmp = 0;
   } else if (*(pTmp+1) == '.')
   {
      *(pTmp+1) = 0;
   } else if (*pTmp == ' ')
   {
   } else
   {
      *pName2 = 0;
   }


   // Remove multiple spaces
   int iCommaCnt=0;

   pTmp = pName1;
   iTmp = 0;
   while (*pTmp)
   {
      if (*pTmp == ',')
      {
         iCommaCnt++;
         if (iCommaCnt > 1)            // Avoid bad name entry
         {
            *pTmp = 0;
            break;
         }
      }

      acTmp[iTmp++] = *pTmp;
      if (*pTmp == ' ')
         while (*pTmp && *pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      //if (*pTmp == '.' || *pTmp == '\'' || *pTmp == '-')
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }

   if (acTmp[iTmp-1] == ' ')
      iTmp--;
   acTmp[iTmp] = 0;

   // Save data to append later
   acSave[0] = 0;
   pTmp = strstr(acTmp, " FAMILY TR");
   if (pTmp)
   {
      strcpy(acSave, " FAMILY TRUST");
      *pTmp = 0;
   } else
   {
      pTmp1 = strrchr(acTmp, ' ');
      if (pTmp1)
      {
         if (!memcmp(pTmp1, " LIVIN", 6))
         {
            strcpy(acSave, " LIVING TRUST");
            *pTmp1 = 0;
         }
      } 
   }

   // Now parse owners
   splitOwner(acTmp, &myOwner);
   strcpy(pOwners->acName1, myOwner.acName1);
   strcpy(pOwners->acName2, myOwner.acName2);
   strcpy(pOwners->acSwapName, myOwner.acSwapName);

   if (acSave[0])
      strcat(pOwners->acName1, acSave);

   strcpy(pOwners->acOF, myOwner.acOF);
   strcpy(pOwners->acOM, myOwner.acOM);
   strcpy(pOwners->acOL, myOwner.acOL);
   strcpy(pOwners->acSF, myOwner.acSF);
   strcpy(pOwners->acSM, myOwner.acSM);
   strcpy(pOwners->acSL, myOwner.acSL);
}

/**************************************************************************
 *
 * # : Owner 2
 * % : Part of Owner1 or Owner2 if has different last name or no '&' at the
 *     end of name1
 *
 **************************************************************************/

void splitOwner_GLE(char *pNames, OWNER *pOwners)
{
   int   iTmp, iNameFlg=5;
   char  acTmp[64], acTmp1[64], acSave[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], *pName1, *pName2;
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Point to name2
   memcpy(acName1, pNames, CRESIZ_NAME1);
   acName1[CRESIZ_NAME1] = 0;
   memcpy(acName2, pNames+CRESIZ_NAME1, CRESIZ_NAME1);
   acName2[CRESIZ_NAME1] = 0;
   pName1 = myTrim(acName1);
   pName2 = myTrim(acName2);

   // Check owner2 for # and %
   if (*pName2 == '#' )
   {
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      if (strstr(pName1, "COUNTY OF") || strstr(pName1, "CITY OF") )
      {
         strcat(pName1, pName2+1);
         *pName2 = 0;
      } else if (acName1[strlen(acName1) -1] == '&' && !strchr(pName2, '&'))
      {  // Continuation of name1.  However, in following cases:
         // LANDBERG BETTY & KAY &       % HILDEBRAND JAN LE
         // B OF A &                     % FEENEY DONNA L TRS
         // Name2 has to be parse separately
         acName1[strlen(acName1) -1] = 0;
         if (strchr(pName1, '&') || !memcmp(pName1, "B OF A", 6))
            strcpy(acName2, pName2+2);
         else
         {
            acName1[strlen(acName1) -1] = '&';
            strcat(pName1, pName2+1);
            *pName2 = 0;
            iNameFlg = 4;
         }
      } else
      {  
         strcpy(acName2, pName2+2);
      }

   } else if (!memcmp(pName2, "C/O", 3))
   {
      strcpy(pOwners->acCareOf, pName2+4);
      *pName2 = 0;
   } else if (!memcmp(pName2, "ATTN", 4))
   {
      *pName2 = 0;
      if (*(pName2+4) == ' ')
         strcpy(pOwners->acCareOf, pName2+5);
      else
         strcpy(pOwners->acCareOf, pName2+6);
   }

   // Blank out "SUC TRS" and "CO TRS", "DVA"
   if (pTmp = strstr(pName1, "SUC TRS"))
      memcpy(pTmp, "          ", 7);
   else if ((pTmp = strstr(pName1, " CO TRS")) || (pTmp = strstr(pName1, " CO-TRS")))
      memcpy(pTmp, "          ", 7);
   else if (pTmp = strstr(pName1, " EST OF"))
      memcpy(pTmp, "          ", 7);
   else if ((pTmp = strstr(pName1, "S/S")) || (pTmp = strstr(pName1, "C/B")))
      memcpy(pTmp, "          ", 3);
   else if ((pTmp = strstr(pName2, "S/S")) || (pTmp = strstr(pName2, "C/B")))
      memcpy(pTmp, "          ", 3);
   else if (pTmp = strstr(pName1, " ETAL"))
      *pTmp = 0;

   // Remove multiple spaces
   pTmp = pName1;
   iTmp = 0;
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      if (*pTmp == ' ')
         while (*pTmp && *pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

  // Check for vesting
   pTmp1 = strrchr(acTmp, ' ');
   if (pTmp1)
   {
      strcpy(acSave, ++pTmp1);
      pTmp = getVestingCode_GLE(acSave, acTmp1);
      if (*pTmp)
      {
         strcpy(pOwners->acVest, acTmp1);
         *pTmp1 = 0;
      }
   } 

   acSave[0] = 0;
   
   // Now parse owners
   splitOwner(acTmp, &myOwner, iNameFlg);
   strcpy(pOwners->acName1, myOwner.acName1);
   if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      strcpy(pOwners->acName2, myOwner.acName2);
   strcpy(pOwners->acSwapName, myOwner.acSwapName);
   if (myOwner.acVest[0] > ' ')
      strcpy(pOwners->acVest, myOwner.acVest);

   if (acSave[0])
   {
      strcat(pOwners->acName1, acSave);
   }

   strcpy(pOwners->acOF, myOwner.acOF);
   strcpy(pOwners->acOM, myOwner.acOM);
   strcpy(pOwners->acOL, myOwner.acOL);
   strcpy(pOwners->acTitle, myOwner.acTitle);

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' '))
   {
      // Check for vesting
      pTmp1 = strrchr(acName2, ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_GLE(acSave, acTmp1);
         if (*pTmp)
         {
            if (pOwners->acVest[0] <= ' ')
               strcpy(pOwners->acVest, acTmp1);
            *pTmp1 = 0;
         }
      } 

      iTmp = splitOwner(acName2, &myOwner, 5);
      if (iTmp >= 0)
      {
         strcpy(pOwners->acName2, myOwner.acName1);
         strcpy(pOwners->acSF, myOwner.acOF);
         strcpy(pOwners->acSM, myOwner.acOM);
         strcpy(pOwners->acSL, myOwner.acOL);
      } else
         strcpy(pOwners->acName2, acName2);
   } else
   {
      strcpy(pOwners->acSF, myOwner.acSF);
      strcpy(pOwners->acSM, myOwner.acSM);
      strcpy(pOwners->acSL, myOwner.acSL);
   }

}

/********************************************************************/

void splitOwner_SBX(char *pNames, OWNER *pOwners)
{
   int   iTmp, iTmp1;
   char  acOwners[128], acTmp[128], acTmp1[64], acSave[64], *pTmp, *pTmp1;
   char  acName2[64];
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   if (!memcmp(pNames, "DECLARATION", 11))
   {
      strncpy(pOwners->acName1, pNames, 63);
      pOwners->acName1[63] = 0;
      return;
   }

   // Remove multiple spaces
   pTmp = strcpy(acOwners, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // If there is "(for)", save it and append later after "/"
   if (pTmp=strstr(acTmp, " (for) "))
   {
      strcpy(acName2, pTmp+7);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " (FOR) "))
   {
      strcpy(acName2, pTmp+7);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " CUSTODIAN FOR"))
   {
      strcpy(acName2, pTmp+15);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " OF THE "))
   {
      strcpy(acName2, pTmp+8);
      *pTmp = 0;
   } else
      acName2[0] = 0;

   // Drop everything from these words   
   if (pTmp=strstr(acTmp, " AKA "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL "))
      *pTmp = 0;
   //else if (pTmp=strstr(acTmp, " DATED "))
   //   *pTmp = 0;
   else if (pTmp=strstr(acTmp, " MD TR "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TRUST"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " INTERVIVOS "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " EST OF"))
      *pTmp = 0;

   // Drop everything after these words
   //if (pTmp=strstr(acTmp, " TRUST "))
   //   *(pTmp+6) = 0;
   //else if (pTmp=strstr(acTmp, " TR "))
   //   *(pTmp+3) = 0;

   // Save data to append later
   if (pTmp1=strstr(acTmp, "TRUST"))
   {
      if (pTmp=strstr(acTmp, " FAM"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " REVOCABLE"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " INDIVIDUAL"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " LIV"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " AMENDED "))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else
      {
         strcpy(acSave, --pTmp1);
         *pTmp1 = 0;
      }
   } else if (pTmp=strstr(acTmp, "FAM TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "FAMILY TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "LIV TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "REV TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "REVOC TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "PERSONAL TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Check for year that goes before TRUST
   iTmp = 0;
   while (isdigit(acTmp[iTmp]))
      iTmp++;
   while (!isdigit(acTmp[iTmp]) && (char *)&acTmp[iTmp] < pTmp)
      iTmp++;
   if ((char *)&acTmp[iTmp] < pTmp)
   {
      strcpy(acTmp1, (char *)&acTmp[--iTmp]);      // Copy the preceeding space too
      strcat(acTmp1, acSave);
      strcpy(acSave, acTmp1);
      acTmp[iTmp] = 0;
   }


   // Translate "/" to "&"
   pTmp = strchr(acTmp, '/');
   if (pTmp && !isdigit(*(pTmp+1)))
      *pTmp = '&';

   // Now parse owners
   splitOwner(acTmp, &myOwner, 0);
   strcpy(pOwners->acName1, myOwner.acName1);
   if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      strcpy(pOwners->acName2, myOwner.acName2);
   strcpy(pOwners->acSwapName, myOwner.acSwapName);
   if (myOwner.acVest[0] > ' ')
      strcpy(pOwners->acVest, myOwner.acVest);

   if (acSave[0])
   {
      strcat(pOwners->acName1, acSave);
   }

   if (acName2[0])
   {
      // Translate "/" to "&"
      pTmp = strchr(acName2, '/');
      if (pTmp && !isdigit(*(pTmp+1)))
         *pTmp = '&';

      strcat(pOwners->acName1, "/ ");
      iTmp = strlen(pOwners->acName1);
      iTmp1 = strlen(acName2);
      if (iTmp+iTmp1 < 64)
         strcat(pOwners->acName1, acName2);
      else
      {
         acName2[63-iTmp] = 0;
         strcat(pOwners->acName1, acName2);
      }
   }
   strcpy(pOwners->acOF, myOwner.acOF);
   strcpy(pOwners->acOM, myOwner.acOM);
   strcpy(pOwners->acOL, myOwner.acOL);
   strcpy(pOwners->acSF, myOwner.acSF);
   strcpy(pOwners->acSM, myOwner.acSM);
   strcpy(pOwners->acSL, myOwner.acSL);
   strcpy(pOwners->acTitle, myOwner.acTitle);
}

/**************************************************************************/

void splitOwner_SCR(char *pNames, OWNER *pOwners)
{
   int   iTmp, iTmp1;
   char  acOwners[128], acTmp[128], acTmp1[64], acSave[64], *pTmp, *pTmp1;
   char  acName2[64], acVesting[8];
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   if (!memcmp(pNames, "DECLARATION", 11))
   {
      strncpy(pOwners->acName1, pNames, 63);
      pOwners->acName1[63] = 0;
      return;
   }

   // Remove multiple spaces
   pTmp = strcpy(acOwners, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   acName2[0] = 0;

   // Drop everything from these words   
   if (pTmp=strstr(acTmp, " AKA "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUCCESSOR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " MD TR "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TRUS"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " INTERVIVOS "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " EST OF"))
      *pTmp = 0;
   else if (!memcmp((char *)&acTmp[36], "TRUS", 4))
      acTmp[36] = 0;
   else if (!memcmp((char *)&acTmp[36], "CO-T", 4))
      acTmp[36] = 0;

   // Save data to append later
   if (pTmp1=strstr(acTmp, "TRUST"))
   {
      if (pTmp=strstr(acTmp, " FAM"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " REVOCABLE"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " INDIVIDUAL"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " LIV"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " AMENDED "))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else
      {
         strcpy(acSave, --pTmp1);
         *pTmp1 = 0;
      }
   } else if (pTmp=strstr(acTmp, "FAM TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "FAMILY TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "LIV TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "REV TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "REVOC TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "PERSONAL TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Check for vesting
   acVesting[0] = 0;
   if ((pTmp=strstr(acTmp, " H/")) || (pTmp=strstr(acTmp, " W/")))
   {
      *pTmp = 0;
      strcpy(acVesting, "HW");
   } else if (pTmp=strstr(acTmp, " M/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "MM");
   } else if (pTmp=strstr(acTmp, " M/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "MW");
   } else if (pTmp=strstr(acTmp, " U/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "UM");
   } else if (pTmp=strstr(acTmp, " U/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "UW");
   } else if (pTmp=strstr(acTmp, " S/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SM");
   } else if (pTmp=strstr(acTmp, " S/P"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SP");
   } else if (pTmp=strstr(acTmp, " S/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SW");
   } else if (pTmp=strstr(acTmp, " JT"))
   {
      *pTmp = 0;
      strcpy(acVesting, "JT");
   }

   // Check for year that goes before TRUST
   iTmp = 0;
   while (isdigit(acTmp[iTmp]))
      iTmp++;
   while (!isdigit(acTmp[iTmp]) && (char *)&acTmp[iTmp] < pTmp)
      iTmp++;
   if ((char *)&acTmp[iTmp] < pTmp)
   {
      strcpy(acTmp1, (char *)&acTmp[--iTmp]);      // Copy the preceeding space too
      strcat(acTmp1, acSave);
      strcpy(acSave, acTmp1);
      acTmp[iTmp] = 0;
   }

   // Now parse owners
   splitOwner(acTmp, &myOwner, 3);
   strcpy(pOwners->acName1, myOwner.acName1);
   if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      strcpy(pOwners->acName2, myOwner.acName2);
   strcpy(pOwners->acSwapName, myOwner.acSwapName);
   if (myOwner.acVest[0] > ' ')
      strcpy(pOwners->acVest, myOwner.acVest);
   else if (acVesting[0])
      strcpy(pOwners->acVest, acVesting);

   if (acSave[0])
   {
      strcat(pOwners->acName1, acSave);
   }

   if (acName2[0])
   {
      // Translate "/" to "&"
      pTmp = strchr(acName2, '/');
      if (pTmp && !isdigit(*(pTmp+1)))
         *pTmp = '&';

      strcat(pOwners->acName1, "/ ");
      iTmp = strlen(pOwners->acName1);
      iTmp1 = strlen(acName2);
      if (iTmp+iTmp1 < 64)
         strcat(pOwners->acName1, acName2);
      else
      {
         acName2[63-iTmp] = 0;
         strcat(pOwners->acName1, acName2);
      }
   }
   strcpy(pOwners->acOF, myOwner.acOF);
   strcpy(pOwners->acOM, myOwner.acOM);
   strcpy(pOwners->acOL, myOwner.acOL);
   strcpy(pOwners->acSF, myOwner.acSF);
   strcpy(pOwners->acSM, myOwner.acSM);
   strcpy(pOwners->acSL, myOwner.acSL);
   strcpy(pOwners->acTitle, myOwner.acTitle);
}

/********************************************************************/

void splitOwner_SJX(char *pNames, OWNER *pOwners)
{
   int   iTmp;
   char  acTmp[64], *pTmp;
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Remove multiple spaces
   pTmp = pNames;
   iTmp = 0;
   strcpy(acTmp, pNames);

   if (pTmp=strstr(acTmp, " COTRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " LF EST"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " LIFE EST"))
      *pTmp = 0;

   // Now parse owners
   splitOwner(acTmp, &myOwner, 0);
   strcpy(pOwners->acName1, myOwner.acName1);
   if (strcmp(myOwner.acName1, myOwner.acName2))
      strcpy(pOwners->acName2, myOwner.acName2);
   strcpy(pOwners->acSwapName, myOwner.acSwapName);
   if (myOwner.acVest[0] > ' ')
      strcpy(pOwners->acVest, myOwner.acVest);

   strcpy(pOwners->acOF, myOwner.acOF);
   strcpy(pOwners->acOM, myOwner.acOM);
   strcpy(pOwners->acOL, myOwner.acOL);
   strcpy(pOwners->acSF, myOwner.acSF);
   strcpy(pOwners->acSM, myOwner.acSM);
   strcpy(pOwners->acSL, myOwner.acSL);
}

void Org_MergeOwner(char *pName1, char *pName2, char *pName3, char *pName4)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acTmp[128], acTmp1[64], acTmp2[64], acTmp3[64];
   char  acLName[64], acCareOf[64];
   char  *pTmp;
   bool  bRet;

   OWNER myOwner;


   acCareOf[0] = 0;
   strcpy(acTmp1, pName1);
   strcpy(acTmp3, pName3);
   if (*pName4)
   {
      strcat(acTmp3, " ");
      strcat(acTmp3, pName4);
   }

   // Check for CareOf
   if (acTmp3[0] == '%')
   {
      strcpy(acCareOf, (char *)&acTmp3[1]);
      acTmp3[0] = 0;
      strcpy(acTmp2, pName2);
   } else if (*pName2 == '%')
   {
      if (*pName3)
      {
         sprintf(acTmp2, "%s %s", pName2+1, acTmp3);
         acTmp3[0] = 0;
      } else
         strcpy(acTmp2, pName2+1);
      //iTmp = strlen(acTmp2);
      //if (iTmp > 20)
      //   iTmp = 20;
      strcpy(acCareOf, acTmp2);
      acTmp2[0] = 0;
   } else
   {
      strcpy(acTmp2, pName2);
   }

   // Save last name
   strcpy(acLName, pName1);
   pTmp = strchr(acLName, ',');
   if (pTmp) *pTmp = 0;


   // If pName1 is a company/trust, ignore the rest
   // If pName2 doesn't contain comma, it is part of Name1.  If it does
   // contain comma, it is Name2.  Merge name if they have the same last name
   // Otherwise, keep them as two separate names.

   // First check to see if Name2 is continuation of Name1
   if (acTmp2[0] > ' ' && !strchr(acTmp2, ',') && memcmp(acLName, acTmp2, strlen(acLName)))
   {
      // Do not merge if Name1 is a TRUST, TRUSTEE, or TR
      bRet = false;
      pTmp = strrchr(acTmp1, ' ');
      if (pTmp && (!memcmp(pTmp, " TR", 3) || strstr(acTmp1, " TR ")))
         bRet = true;

      // Merge Name1 and Name2
      if (!bRet)
      {
         sprintf(acTmp, "%s %s", acTmp1, acTmp2);
         strcpy(acTmp1, acTmp);
         acTmp2[0] = 0;                      // Remove Name2 after merged
      }
   }
   if (acTmp3[0] > ' ' && !strchr(acTmp3, ','))
   {
      // Merge Name2 and Name3
      sprintf(acTmp, "%s %s", acTmp2, acTmp3);
      strcpy(acTmp2, acTmp);
      acTmp3[0] = 0;
   }

   strcpy(acOwner1, acTmp1);
   acOwner2[0] = 0;
   if (acTmp2[0])
      strcpy(acOwner2, acTmp2);
   else if (acTmp3[0])
      strcpy(acOwner2, acTmp3);

   if (pTmp=strstr(acOwner1, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " TR "))
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " ET AL"))
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " EST OF"))
      *pTmp = 0;

   // If Owner1 and Owner2 has the same last name, combine them
   if (!pTmp && acOwner2[0])
   {
      pTmp = strchr(acOwner1, ',');
      if (pTmp)
      {
         iTmp = pTmp - &acOwner1[0];
         if (!memcmp(acOwner1, acOwner2, iTmp))
         {
            MergeName(acOwner1, acOwner2, acOwner1);
            acOwner2[0] = 0;
         }
      }
   }


   // Now parse owners
   splitOwner(acOwner1, &myOwners, 3);
   if (acCareOf[0])
   {
      acCareOf[sizeof(myOwners.acCareOf)-1] = 0;
      strcpy(myOwners.acCareOf, acCareOf);
   }
   if (acOwner2[0])
   {
      splitOwner(acOwner2, &myOwner, 3);
      strcpy(myOwners.acName2, myOwner.acName1);
      strcpy(myOwners.acSF, myOwner.acOF);
      strcpy(myOwners.acSM, myOwner.acOM);
      strcpy(myOwners.acSL, myOwner.acOL);
   }
   acTmp[63] = 0;

}

/********************************************************************/

void Imp_MergeOwner(char *pNames, OWNER *pOwners)
{
   int   iTmp, iTmp1;
   char  acOwners[128], acTmp[64], acSave[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acVesting[8];
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Remove multiple spaces
   pTmp = strcpy(acOwners, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Filter out words
   if (pTmp=strstr(acTmp, " 1/2 INT"))
   {
      *pTmp = 0;
      if (*(pTmp+8))
         strcat(acTmp, pTmp+8);
   }

   // Drop everything from these words - DO NOT MOVE THIS SECTION
   if (pTmp=strstr(acTmp, " AKA "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUCCESSOR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUCC "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUC TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " INT ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TR "))
   {
      *pTmp = 0;
      if (pTmp1 = strchr(pTmp+3, '&'))
         strcat(acTmp, pTmp+3);

   } else if (pTmp=strstr(acTmp, " TRS "))
   {
      *pTmp = 0;
      if (pTmp1 = strchr(pTmp+4, '&'))
         strcat(acTmp, pTmp+4);
   } else if (pTmp=strstr(acTmp, " INTERVIVOS "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " EST OF"))
      *pTmp = 0;

   // Save data to append later
   if (pTmp1=strstr(acTmp, " TRUST"))
   {
      if (pTmp=strstr(acTmp, " FAM"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " REVOCABLE"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " INDIVIDUAL"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " LIV"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " AMENDED "))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " GBAC "))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else
      {
         strcpy(acSave, --pTmp1);
         *pTmp1 = 0;
      }
   } else if (pTmp=strstr(acTmp, "FAM TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "FAMILY TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "LIV TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "REV TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "REVOC TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "PERSONAL TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Check for vesting
   acVesting[0] = 0;
   if ((pTmp=strstr(acTmp, " H/")) || (pTmp=strstr(acTmp, " W/")))
   {
      *pTmp = 0;
      strcpy(acVesting, "HW");
   } else if (pTmp=strstr(acTmp, " M/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "MM");
   } else if (pTmp=strstr(acTmp, " M/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "MW");
   } else if (pTmp=strstr(acTmp, " U/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "UM");
   } else if (pTmp=strstr(acTmp, " U/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "UW");
   } else if (pTmp=strstr(acTmp, " S/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SM");
   } else if (pTmp=strstr(acTmp, " S/P"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SP");
   } else if (pTmp=strstr(acTmp, " S/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SW");
   } else if (pTmp=strstr(acTmp, " JT"))
   {
      strcpy(acVesting, "JT");
      *pTmp = 0;

      pTmp1 = strstr(pTmp+3, " JT");
      if (pTmp1)
      {  // IMP - NELSON C E & V A JT & BISHOP B B & K E JT
         *pTmp1 = 0;
         strcat(acTmp, pTmp+3);
         if (*(pTmp1+3))
            strcat(acTmp, pTmp1+3);
      } else if (!memcmp(pTmp+strlen(pTmp+2), "JT", 2))
      {  // GILLESPIE J J & M A JT & GILLESPIE W J & DJT
         *(pTmp+strlen(pTmp+2)) = 0;
         strcat(acTmp, pTmp+3);
      } else
      {  // SCHMELZLE R & B L JT 1/2 & CRONE STELLA 1/2
         if (pTmp1 = strchr(pTmp+3, '&'))
            strcat(acTmp, pTmp+3);
         else if (pTmp1 = strstr(pTmp+3, " AND "))
         {  // SOEGAARD J & M JT AND SOEGAARD ANDREW G
            *pTmp1 = 0;
            strcat(acTmp, pTmp+3);
            strcat(acTmp, " &");
            strcat(acTmp, pTmp1+4);
         }
      }
   }

   // Check for year that goes before TRUST
   iTmp = iTmp1 =0;   
   while (acTmp[iTmp])
   {
      if (!isdigit(acTmp[iTmp]) && acTmp[iTmp] != '/' && acTmp[iTmp] != '%')
         acOwners[iTmp1++] = acTmp[iTmp];
      iTmp++;
   }
   acOwners[iTmp1] = 0;

   // Chop off second '&'
   acName1[0] = 0;
   acName2[0] = 0;
   if (pTmp = strchr(acOwners, '&'))
   {
      if (pTmp1 = strchr(++pTmp, '&'))
      {
         iTmp = iTmp1 =0;
         while (acOwners[iTmp])
         {
            acName1[iTmp1++] = acOwners[iTmp];
            if (acOwners[iTmp] == '&' && acOwners[iTmp+1] != ' ')
               acName1[iTmp1++] = ' ';
            else if (acOwners[iTmp] != ' ' && acOwners[iTmp+1] == '&')
               acName1[iTmp1++] = ' ';
            iTmp++;
         }
         acName1[iTmp1] = 0;
         //strcpy(acName1, acOwners);
         *pTmp1 = 0;
      }
   }

   // Now parse owners
   splitOwner(acOwners, &myOwner, 0);
   if (acName1[0])
   {
      strcpy(pOwners->acName1, acName1);
      // WRIGHT W R JR & D JT & WRIGHT G G & G E JT
      myOwner.acTitle[0] = 0;
   } else
   {
      strcpy(pOwners->acName1, myOwner.acName1);
      if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
         strcpy(pOwners->acName2, myOwner.acName2);

   }
   strcpy(pOwners->acSwapName, myOwner.acSwapName);
   if (myOwner.acVest[0] > ' ')
      strcpy(pOwners->acVest, myOwner.acVest);
   else if (acVesting[0])
      strcpy(pOwners->acVest, acVesting);

   if (acSave[0])
      strcat(pOwners->acName1, acSave);

   strcpy(pOwners->acOF, myOwner.acOF);
   strcpy(pOwners->acOM, myOwner.acOM);
   strcpy(pOwners->acOL, myOwner.acOL);
   strcpy(pOwners->acSF, myOwner.acSF);
   strcpy(pOwners->acSM, myOwner.acSM);
   strcpy(pOwners->acSL, myOwner.acSL);
   strcpy(pOwners->acTitle, myOwner.acTitle);
}

/*******************************************************************/

void Slo_MergeOwner(char *pNames, OWNER *pOwners)
{
   int   iTmp;
   char  acTmp[64], *pTmp;
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Remove multiple spaces
   iTmp = 0;
   strcpy(acTmp, myTrim(pNames));

   // Remove parenthesis
   if (pTmp=strchr(acTmp, '('))
      *pTmp = 0;

   // Remove filtered words
   if (pTmp=strstr(acTmp, " TRE ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETUX"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL"))
      *pTmp = 0;
   //else if (pTmp=strstr(acTmp, " PTP"))
   //   *pTmp = 0;
   else if (pTmp=strstr(acTmp, " LIFE EST"))
      *pTmp = 0;

   iTmp = strlen(acTmp);
   if (!memcmp((char *)&acTmp[iTmp-3], "TRE", 3))
      acTmp[iTmp-4] = 0;

   // Now parse owners
   splitOwner(acTmp, &myOwner, 0);
   strcpy(pOwners->acName1, myOwner.acName1);
   if (strcmp(myOwner.acName1, myOwner.acName2))
      strcpy(pOwners->acName2, myOwner.acName2);
   strcpy(pOwners->acSwapName, myOwner.acSwapName);
   if (myOwner.acVest[0] > ' ')
      strcpy(pOwners->acVest, myOwner.acVest);

   strcpy(pOwners->acOF, myOwner.acOF);
   strcpy(pOwners->acOM, myOwner.acOM);
   strcpy(pOwners->acOL, myOwner.acOL);
   strcpy(pOwners->acSF, myOwner.acSF);
   strcpy(pOwners->acSM, myOwner.acSM);
   strcpy(pOwners->acSL, myOwner.acSL);
   strcpy(pOwners->acTitle, myOwner.acTitle);
}

/********************************************************************/

void Mod_MergeOwner(char *pNames)
{
   int   iTmp;
   char  acTmp1[64], acSave[64];
   char  acName1[64], acName2[64], acCareOf[64], acVest[8];
   char  *pTmp, *pTmp1, *pTmp2, *pName2;
   OWNER myOwner;

   // Initialize
   memset((void *)&myOwners, 0, sizeof(OWNER));

   // Point to name2
   pName2 = pNames+CRESIZ_NAME1;
   acName2[0] = 0;
   acCareOf[0] = 0;
   acVest[0] = 0;

   // Remove '.'
   if (pTmp = strchr(pNames, '.'))
   {  
      while (*pTmp)
      {
         if (*pTmp == '.')
            *pTmp = ' ';
         pTmp++;
      }
   } 

   // Save name1
   strncpy(acName1, pNames, CRESIZ_NAME1);
   acName1[CRESIZ_NAME1] = 0;

   // Check owner2 for # and %
   if (*pName2 == '#' )
   {
      *pName2 = 0;
      strcpy(acName1, pNames);
      strcpy(acName2, pName2+2);
   } else if (*pName2 == '%' )
   {
      *pName2 = 0;
      strcpy(acName1, pNames);
      strcpy(acCareOf, pName2+2);
   } else if (*pName2 == '&' )
   {  // Name2 is continuation of name1
      strcpy(acName1, pNames);
   } else if (!memcmp(pName2, "C/O", 3))
   {
      *pName2 = 0;
      strcpy(acName1, pNames);
      strcpy(acCareOf, pName2+4);
   } else if (!memcmp(pName2, "ATTN:", 5))
   {
      *pName2 = 0;
      strcpy(acName1, pNames);
      strcpy(acCareOf, pName2+6);
   } else if (*pName2 > ' ' && strchr(acName1, '&'))
   {
      if (strchr(pName2, ','))
         strcpy(acName2, pName2);
      else
         strcpy(acName1, pNames);
   } else if (!strchr(acName1, ',') && !strchr(pName2, ','))
   {  // HAMZA HOLDINGS LIMITED       PO BOX 236/FIRST ISLAND HOUSE
      if (memcmp(pName2, "PO BOX", 6) && !isdigit(*pName2))
         strcpy(acName1, pNames);
   } else if (strchr(pName2, ','))
   {
      strcpy(acName2, pName2);
   }

   // Check for vesting
   if (pTmp=strstr(acName2, " JT"))
   {
      *pTmp = 0;
      strcpy(acVest, "JT");
   } else if (pTmp=strstr(acName1, " JT"))
   {
      *pTmp = 0;
      strcpy(acVest, "JT");
   }
  
   myTrim(acName1);
   myTrim(acName2);

   // Remove things within parenthesis
   if (pTmp = strchr(acName1, '('))
   {  
      while (*pTmp)
      {
         if (*pTmp == ')')
         {
            *pTmp = ' ';
            break;
         } else
            *pTmp = ' ';
         pTmp++;
      }
   } 
   
   if (pTmp=strstr(acName1, " 1/"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " - "))
      *pTmp = 0;
   if (pTmp=strstr(acName1, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " ET AL"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " ETUX"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " ET UX"))
      *pTmp = 0;
   if (pTmp=strstr(acName1, " TSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " SUCC  TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TSTES"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TR "))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " COTRST"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " ESTATE OF"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " LIFE ESTATE"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " M D"))
      *pTmp = 0;

   // Check on name2
   if (acName2[0])
   {
      if (pTmp1=strstr(acName2, " 1/"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " - "))
         *pTmp1 = 0;
      if (pTmp1=strstr(acName2, " ETAL"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ET AL"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ETUX"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ET UX"))
         *pTmp1 = 0;
      if (pTmp1=strstr(acName2, " TSTEE"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " SUCC  TR"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TRUSTE"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TR "))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TRS"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TSTES"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " CO-TR"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " COTRST"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ESTATE OF"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " LIFE ESTATE"))
         *pTmp1 = 0;
      else if (!memcmp(acName2, "PO BOX", 6))
         acName2[0] = 0;
   }

   // Process as two names if name2 has multiple names
   pTmp = strchr(acName1, '/');
   pTmp2 = strchr(acName2, '/');
   pTmp1 = strchr(acName2, '&');
   if ((pTmp && !isdigit(*(pTmp+1))) || (pTmp2 && !isdigit(*(pTmp2+1))) || pTmp1)
   {
      if (pTmp)
         *pTmp = '&';
      if (pTmp2)
         *pTmp2 = '&';

      // Check for vesting
      pTmp1 = strrchr(acName1, ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_LAS(acSave, acTmp1);
         if (*pTmp)
         {
            strcpy(myOwners.acVest, acTmp1);
            *pTmp1 = 0;
         }
      } 

      splitOwner(acName1, &myOwner,3);
      strcpy(myOwners.acName1, myOwner.acName1);
      strcpy(myOwners.acSwapName, myOwner.acSwapName);
      strcpy(myOwners.acOF, myOwner.acOF);
      strcpy(myOwners.acOM, myOwner.acOM);
      strcpy(myOwners.acOL, myOwner.acOL);
      if (myOwner.acVest[0] > ' ' && !myOwners.acVest[0])
         strcpy(myOwners.acVest, myOwner.acVest);

      // Check for vesting
      pTmp1 = strrchr(acName2, ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_LAS(acSave, acTmp1);
         if (*pTmp && !myOwners.acVest[0])
         {
            strcpy(myOwners.acVest, acTmp1);
            *pTmp1 = 0;
         }
      } 
      splitOwner(acName2, &myOwner,3);
      strcpy(myOwners.acName2, myOwner.acName1);
      strcpy(myOwners.acSF, myOwner.acOF);
      strcpy(myOwners.acSM, myOwner.acOM);
      strcpy(myOwners.acSL, myOwner.acOL);

      if (acCareOf[0])
         strcpy(myOwners.acCareOf, acCareOf);

      return;
   }

   // If Owner1 and Owner2 has the same last name, combine them
   if (acName2[0])
   {
      pTmp = strchr(acName1, ',');
      if (pTmp)
      {
         iTmp = pTmp - &acName1[0];
         if (!memcmp(acName1, acName2, iTmp))
         {
            MergeName(acName1, acName2, acName1);
            acName2[0] = 0;
         }
      }
   }

   // Now parse owners
   splitOwner(acName1, &myOwners, 3);
   if (acName2[0])
   {
      splitOwner(acName2, &myOwner, 3);
      strcpy(myOwners.acName2, myOwner.acName1);
      strcpy(myOwners.acSF, myOwner.acOF);
      strcpy(myOwners.acSM, myOwner.acOM);
      strcpy(myOwners.acSL, myOwner.acOL);
   }
   if (acCareOf[0])
   {
      acCareOf[sizeof(myOwners.acCareOf)-1] = 0;
      strcpy(myOwners.acCareOf, acCareOf);
   }
   if (acVest[0])
      strcpy(myOwners.acVest, acVest);
}

/********************************************************************
 *
 * Assuming that the longest last name may have up to 3 words.  We 
 * marks all space within last name so we can parse it easier.
 *
 * Return TRUE if there is comma in name
 *
 ********************************************************************/

bool markLastName(char *pName)
{
   char *pTmp, *pTmp1;
   bool  bRet=false;

   if (pTmp = strchr(pName, ','))
   {
      bRet = true;
      *pTmp = 0;
      pTmp1 = pName;
      while (*++pTmp1)
      {
         if (*pTmp1 == ' ')
            *pTmp1 = '+';
      }
      *pTmp = ' '; // Replace null with space
   }

   return bRet;
}

void clearMarker(char *pName)
{
   char *pTmp;

   pTmp = pName;
   while (*++pTmp)
   {
      if (*pTmp == '+')
         *pTmp = ' ';
   }
}

void Lax_MergeOwner(char *pName1_1, char *pName1_2, char *pName2)
{
   int   iTmp, iNameFlg=5;
   char  acTmp[64], acSave[64];
   char  acName1[64], acName2[64], acVest[8];
   char  *pTmp, *pTmp1;
   bool  bMarkName1=false, bMarkName2=false;
   OWNER myOwner;

   // Initialize
   memset((void *)&myOwners, 0, sizeof(OWNER));

   acName2[0] = 0;
   acVest[0] = 0;
   acSave[0] = 0;

   // Save Name2
   if (*pName2 >= 'A')
   {
      strcpy(acName2, pName2);
      bMarkName2 = markLastName(myTrim(acName2));
   }

   // Mark name1
   strcpy(acName1, pName1_1);
   bMarkName1 = markLastName(myTrim(acName1));

   // Looking for 'AND' in name1
   pTmp = (char *)&acName1[strlen(acName1)-4];
   if (!memcmp(pTmp, " AND", 4))
   {
      if (*pName1_2 > ' ')
      {
         strcpy(acTmp, pName1_2);
         if (markLastName(myTrim(acTmp)))
            iNameFlg = 4;   // Set this flag to remember that after AND there is another last name

         // Drop 'AND' at the end of Name1_2 then append to Name1_1
         iTmp = strlen(acTmp);
         if (iTmp > 4)
         {
            pTmp = (char *)&acTmp[iTmp-4];
            if (!memcmp(pTmp, " AND", 4))
               *pTmp = 0;
         }
         strcat(acName1, " ");
         strcat(acName1, acTmp);
      } else if (acName2[0] > ' ')
      {
         // If there are more than one names in Name2, process it separately
         // Name1 = LUNA,JOSE C AND NORMA AND  Name2=VALADEZ,XAVIER AND RAQUEL 
         if (!strstr(acName2, " AND "))
         {
            // If Name1_1 has two 'AND' then we cannot append Name2 to Name1_1
            // Name1=HERNANDEZ,ALEX P AND TANYA A AND   Name2=WHETSTINE,DARLENE
            if (!strstr(acName1, " AND "))
            {
               // Append Name2 to Name1_1 if either one seems to contain a last name
               if (bMarkName1 && bMarkName2)
               {
                  strcat(acName1, " ");
                  strcat(acName1, acName2);
                  acName2[0] = 0;
                  if (bMarkName2)
                     iNameFlg = 4;
               } else
                  *pTmp = 0;           // Drop 'AND' from Name1_1
            } else
               *pTmp = 0;
         } else
            *pTmp = 0;  
      } else
         *pTmp = 0;
   } else if (!memcmp(pName1_2, "AND ", 4))
   {  // FRIEDLANDER BERKOWITZ, BARBARA  AND BERKOWITZ,STEVEN
      strcpy(acTmp, pName1_2+4);
      if (markLastName(myTrim(acTmp)))
         iNameFlg = 4;   
      
      // Drop 'AND' at the end of Name1_2 then append to Name1_1
      iTmp = strlen(acTmp);
      if (iTmp > 4)
      {
         pTmp = (char *)&acTmp[iTmp-4];
         if (!memcmp(pTmp, " AND", 4))
            *pTmp = 0;
      }
      strcat(acName1, " AND ");
      strcat(acName1, acTmp);
   } else if (acName2[0] <= ' ' && *pName1_2 > ' ')
   {  
      // If this is a TRUST name, keep this as is, don't parse
      if (strstr(pName1_2, " TRUST"))
      {
         strcpy(acSave, pName1_2);
         iTmp = strlen(acSave);
         if (iTmp > 4)
         {
            pTmp = (char *)&acSave[iTmp-4];
            if (!memcmp(pTmp, " AND", 4))
               *pTmp = 0;
         }
      } else
      {
         strcat(acName1, " ");
         strcat(acName1, pName1_2);
      }
   }
  
   if (pTmp=strstr(acName1, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " ET AL"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " CO TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TR "))
      *pTmp = 0;

   // Check on name2
   if (acName2[0])
   {
      if (pTmp1=strstr(acName2, " ETAL"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ET AL"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " CO TR"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TR "))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TRS"))
         *pTmp1 = 0;
   }

   // Now parse owners
   splitOwner(acName1, &myOwners, iNameFlg);
   if (myOwners.acName2[0] <= ' ' && acName2[0] >= 'A')
   {
      splitOwner(acName2, &myOwner, 5);
      strcpy(myOwners.acName2, myOwner.acName1);
      strcpy(myOwners.acSF, myOwner.acOF);
      strcpy(myOwners.acSM, myOwner.acOM);
      strcpy(myOwners.acSL, myOwner.acOL);
   }
   clearMarker(myOwners.acName1);
   if (myOwners.acName2[0] > ' ')
      clearMarker(myOwners.acName2);
   else if (acSave[0] >= 'A')
      strcpy(myOwners.acName2, acSave);
   if (myOwners.acSwapName[0] > ' ')
      clearMarker(myOwners.acSwapName);



   //if (myOwner.acVest[0])
   //   strcpy(myOwners.acVest, myOwner.acVest);
}

/********************************************************************/

void Ama_MergeOwner(char *pNames, OWNER *pOwners)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], acSave[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave[0] = 0;
   acSave1[0] = 0;
   
   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;
   
   // Check for year that goes before TRUST
   iTmp =0;   
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // Save it - Only do it for individual trust
   if (acTmp[iTmp] && !strchr(acTmp, '&'))
   {
      iTmp--;
      strcpy(acSave, (char *)&acTmp[iTmp]);
      acTmp[iTmp] = 0;
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      *(pTmp+13) = 0;
      strcpy(acSave1, acTmp);

      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      *(pTmp+13) = 0;
      strcpy(acSave1, acTmp);

      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE  
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *(pTmp+6) = 0;
      else
         *pTmp = 0;        // Drop TRUST only if not family trust

      strcpy(acName1, acTmp);
      pTmp1 = pTmp+9;
      iTmp1 = strlen(pTmp1);
      for (iTmp = 0; iTmp < iTmp1; iTmp++)
      {
         if (acName1[iTmp] != *pTmp1)
            break;
         pTmp1++;
      }

      // Skip first word after & if it is the same as last name
      if (iTmp > 2 && *(pTmp1-1) == ' ')
      {
         strcat(acName1, " & ");
         // Search and remove TRUST in second part of name
         if (pTmp=strstr(pTmp1, " TRUST"))
            *pTmp = 0;
         strcat(acName1, pTmp1);
      } else
      {
         strcpy(acName2, pTmp+9);
      }
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) 
             || (pTmp=strstr(acTmp, " LAND TRUST")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUSTE"))
   {  // Drop TRUSTEE
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (acSave[0])
      {
         strcpy(acTmp1, acSave);
         strcpy(acSave, pTmp);
         strcat(acSave, acTmp1);
      } else
      {
         if (isdigit(*(pTmp-1)) )
            while (isdigit(*(--pTmp)));

         strcpy(acSave, pTmp);
      }
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " SUCCESSOR")) )
      *pTmp = 0;
   if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " SUCCESSOR")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(pOwners->acName1, myOwner.acName1);
      if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
         strcpy(pOwners->acName2, myOwner.acName2);

      if (myOwner.acVest[0] > ' ')
         strcpy(pOwners->acVest, myOwner.acVest);

      // Concat what in saved buffer if it is not " TRUST"
      if (acSave[0] && strcmp(acSave, " TRUST"))
      {
         if (pOwners->acName1[strlen(pOwners->acName1)-1] == ' ')
            strcat(pOwners->acName1, (char *)&acSave[1]);      // avoid double blank
         else
            strcat(pOwners->acName1, acSave);
      }

      // If name is not swapable, use Name1 for it
      if (iRet == -1)
         strcpy(pOwners->acSwapName, pOwners->acName1);
      else
         strcpy(pOwners->acSwapName, myOwner.acSwapName);

      strcpy(pOwners->acOF, myOwner.acOF);
      strcpy(pOwners->acOM, myOwner.acOM);
      strcpy(pOwners->acOL, myOwner.acOL);
      strcpy(pOwners->acTitle, myOwner.acTitle);
   } else if (acSave1[0])
   {
      strcpy(pOwners->acName1, acSave1);
      strcpy(pOwners->acSwapName, acSave1);
   } else
   {
      if (acSave[0])
      {
         strcat(acName1, acSave);
         acSave[0] = 0;
         strcpy(pOwners->acName1, acName1);
         strcpy(pOwners->acSwapName, acName1);
      } else
      {
         // Couldn't split names
         strcpy(pOwners->acName1, pNames);
         strcpy(pOwners->acSwapName, pNames);
         acName2[0] = 0;
         return;
      }
   }

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' '))
   {
      if ((pTmp=strstr(acName2, " REVOC")) || (pTmp=strstr(acName2, " FAMILY "))
         || (pTmp=strstr(acName2, " INCOME TR")) || (pTmp=strstr(acName2, " LIVING TR")) )
      {
         strcpy(pOwners->acName2, acName2);
      } else
      {
         if (pTmp=strstr(acName2, " TRUST"))
         {
            *pTmp = 0;
            // If Name2 doesn't have full name, leave it the same
            // as in STANLEY TRUST;STREET TRUST
            if (!strchr(acName2, ' '))
               *pTmp = ' ';
         }
         iRet = splitOwner(acName2, &myOwner, 3);
         if (iRet >= 0)
         {
            strcpy(pOwners->acName2, myOwner.acName1);
            strcpy(pOwners->acSF, myOwner.acOF);
            strcpy(pOwners->acSM, myOwner.acOM);
            strcpy(pOwners->acSL, myOwner.acOL);
         } else
            strcpy(pOwners->acName2, acName2);
      }
   } else
   {
      strcpy(pOwners->acSF, myOwner.acSF);
      strcpy(pOwners->acSM, myOwner.acSM);
      strcpy(pOwners->acSL, myOwner.acSL);
   }
}

/********************************************************************/

void Hum_MergeOwner(char *pNames, OWNER *pOwners)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], acSave[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acVesting[8];
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Copy data
   strcpy(acTmp1, pNames);

   // Replace comma with space
   if (pTmp = strrchr(acTmp1, ','))
      *pTmp = ' ';

   // Remove multiple spaces
   pTmp = acTmp1;
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '*' too
      if (*pTmp == '*' || *pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave[0] = 0;
   acSave1[0] = 0;
   acVesting[0] = 0;
   
   // If name ending with CR, it is a company
   if (!memcmp((char *)&acTmp[iTmp-3], " CR", 3))
   {
      acTmp[iTmp-3] = 0;
      strcpy(pOwners->acName1, acTmp);
      strcpy(pOwners->acSwapName, acTmp);
      return;
   }

   // Check for vesting on last 5 letters
   pTmp = (char *)&acTmp[iTmp-5];
   iTmp1 = 0;
   while (asVestCode5[iTmp1][0])
   {
      if (!memcmp(pTmp, asVestCode5[iTmp1], 5))
      {
         strcpy(acVesting, asVestCode4[iTmp1]);
         *pTmp = 0;
         break;
      }
      iTmp1++;
   }
   if (*pTmp)
   {
      // Check for vesting on last 4 letters
      pTmp = (char *)&acTmp[iTmp-4];
      iTmp1 = 0;
      while (asVestCode4[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode4[iTmp1], 4))
         {
            strcpy(acVesting, asVestCode4[iTmp1]);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }
   if (*pTmp)
   {
      // Check for vesting on last 3 letters
      pTmp = (char *)&acTmp[iTmp-3];
      iTmp1 = 0;
      while (asVestCode3[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode3[iTmp1], 3))
         {
            strcpy(acVesting, pTmp+1);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }

   // Check for year that goes before TRUST
   iTmp =0;   
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      strcpy(pOwners->acName1, acTmp);
      return;
   }

   // If TRUST appears after number, save from number forward
   if (acTmp[iTmp])
   {
      if (pTmp=strstr((char *)&acTmp[0], " PENSION & 401(K)") )
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;         
      } else if (strstr((char *)&acTmp[iTmp], " TRUST") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || 
       (pTmp=strstr(acTmp, " ET AL")) || 
       (pTmp=strstr(acTmp, " & FBO")) ||
       (pTmp=strstr(acTmp, " TRUSTEE")) )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " & TRUST "))
   {
      // Break into two name
      *pTmp = 0;
      strcpy(acName1, acTmp);
      strcpy(pOwners->acName2, pTmp+3);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // MARTIN PATSY L TRUST & WILDER EVERETT E & HEWITT B
      // Break names in two parts
      *pTmp = 0;        
      strcpy(acSave1, " TRUST");
      strcpy(acName1, acTmp);
      strcpy(pOwners->acName2, pTmp+9);
   }
 
   // If more than one "&", put second part into name2
   if (pTmp = strchr(acTmp, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1 = 0;
         strcpy(acName2, pTmp1+2);
      }
   }
   
   if (pTmp=strstr(acTmp, " REVOCABLE FAMILY TRUST"))
   {
      strcpy(acSave1, " REVOCABLE FAMILY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY REVOCABLE TRUST"))
   {
      strcpy(acSave1, " REVOCABLE LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE LIVING TRUST"))
   {
      strcpy(acSave1, " REVOCABLE LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY LIVING TRUST"))
   {
      strcpy(acSave1, " FAMILY LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST"))
   {  
      strcpy(acSave1, " FAMILY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FRAMILY TRUST"))
   {  
      strcpy(acSave1, " FAMILY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST"))
   {  
      strcpy(acSave1, " REV TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST"))
   {
      strcpy(acSave1, " LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REV TRUST"))
   {  
      strcpy(acSave1, " REV TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOC TRUST"))
   {  
      strcpy(acSave1, " REV TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " EXEMPTION TRUST"))
   {  
      strcpy(acSave1, " EXEMPTION TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {  
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " PROPERTY TRUST"))
   {  
      strcpy(acSave1, " PROPERTY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " SURVIVOR'S TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST UNDER"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST NO"))
   {
      strcpy(acSave1, " TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST DATE"))
   {
      strcpy(acSave1, " TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST TR"))
   {
      strcpy(acSave1, " TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST THE"))
   {  // Reverse the word order
      strcpy(acName1, "THE ");
      *(pTmp+6) = 0;
      strcat(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[strlen(acTmp)-6], " TRUST", 6))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      acTmp[strlen(acTmp)-6] = 0;
      strcpy(acName1, acTmp);
      strcpy(acSave1, " TRUST");
   } else
      strcpy(acName1, acTmp);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   // Only parse if owner has more than one word
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(pOwners->acName1, myOwner.acName1);
      if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
         strcpy(pOwners->acName2, myOwner.acName2);

      if (myOwner.acVest[0] > ' ')
         strcpy(pOwners->acVest, myOwner.acVest);

      // Concat what in saved buffer 
      //if (acSave1[0] && strcmp(acSave, " TRUST"))
      if (acSave1[0])
      {
         if (pOwners->acName1[strlen(pOwners->acName1)-1] == ' ')
            strcat(pOwners->acName1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(pOwners->acName1, acSave1);
      }

      // If name is not swapable, use Name1 for it
      if (iRet == -1)
         strcpy(pOwners->acSwapName, pOwners->acName1);
      else
         strcpy(pOwners->acSwapName, myOwner.acSwapName);

      strcpy(pOwners->acOF, myOwner.acOF);
      strcpy(pOwners->acOM, myOwner.acOM);
      strcpy(pOwners->acOL, myOwner.acOL);
      strcpy(pOwners->acTitle, myOwner.acTitle);
   } else
   {
      if (acSave1[0])
      {
         strcat(acName1, acSave1);
         acSave1[0] = 0;
         strcpy(pOwners->acName1, acName1);
         strcpy(pOwners->acSwapName, acName1);
      } else
      {
         // Couldn't split names
         strcpy(pOwners->acName1, pNames);
         strcpy(pOwners->acSwapName, pNames);
         //acName2[0] = 0;
         //return;
      }
   }

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' '))
   {
      if ((pTmp=strstr(acName2, " REVOC")) || (pTmp=strstr(acName2, " FAMILY "))
         || (pTmp=strstr(acName2, " INCOME TR")) || (pTmp=strstr(acName2, " LIVING TR")) )
      {
         strcpy(pOwners->acName2, acName2);
      } else
      {
         if (pTmp=strstr(acName2, " TRUST"))
         {
            *pTmp = 0;
            // If Name2 doesn't have full name, leave it the same
            // as in STANLEY TRUST;STREET TRUST
            if (!strchr(acName2, ' '))
               *pTmp = ' ';
         }
         iRet = splitOwner(acName2, &myOwner, 3);
         if (iRet >= 0)
         {
            strcpy(pOwners->acName2, myOwner.acName1);
            strcpy(pOwners->acSF, myOwner.acOF);
            strcpy(pOwners->acSM, myOwner.acOM);
            strcpy(pOwners->acSL, myOwner.acOL);
         } else
            strcpy(pOwners->acName2, acName2);
      }
   } else
   {
      strcpy(pOwners->acSF, myOwner.acSF);
      strcpy(pOwners->acSM, myOwner.acSM);
      strcpy(pOwners->acSL, myOwner.acSL);
   }

   if (acVesting[0])
      strcpy(pOwners->acVest, acVesting);
}

/********************************************************************/

void Sac_MergeOwner(char *pNames, OWNER *pOwners)
{
   int   iTmp, iTmp1, iRet, iParseType=3;
   char  acTmp1[128], acTmp2[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];
   bool  bTrust=false;
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Remove multiple spaces
   pTmp = strcpy(acTmp, pNames);
   iTmp = 0;
   acName2[0] = 0;
   acSave1[0] = 0;

   if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Eliminate ETAL
   pTmp = strrchr(acTmp, '/');
   if (pTmp)
   {
      if (!memcmp(pTmp, "/ETAL", 5))
      {
         *pTmp = 0;
         pTmp = strrchr(acTmp, '/');
      }
      if (pTmp)
      {
         if (!memcmp(pTmp, "/TR", 3))
            *pTmp = 0;
      }
   }

   // Drop filtered words
   if (pTmp=strstr(acTmp, " TR "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " FBO "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, "/EST OF"))
      // KURASAKI JOHN/EST OF/KAZUKO  KURASAKI/ETAL
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTEE"))
      *pTmp = 0;

   // Replace / with & and move it to name1 and name2
   pTmp = (char *)&acTmp[0];
   pTmp1 = (char *)&acTmp1[0];
   iTmp = 0;
   while (*pTmp)
   {
      if (*pTmp == '/')
      {
         iTmp++;
         if (iTmp == 2)
         {
            iTmp = 0;
            *pTmp1 = 0;
            pTmp1 = (char *)&acName2[0];
         } else
         {
            strcpy(pTmp1, " & ");
            pTmp1 += 3;
         }
      } else
         *pTmp1++ = *pTmp;
      pTmp++;
   }
   *pTmp1 = 0;


   if (pTmp=strstr(acTmp1, " TRUST &"))
   {  
      *(pTmp+6) = 0;
      strcpy(acName1, acTmp1);
      if (acName2[0])
      {
         sprintf(acTmp, "%s & %s", pTmp+9, acName2);
         strcpy(acName2, acTmp);
      } else
         strcpy(acName2, pTmp+9);
   } else if (pTmp=strstr(acTmp1, " LLC &"))
   {  
      *(pTmp+4) = 0;
      strcpy(acName1, acTmp1);
      strcpy(acName2, pTmp+7);
   } else
      strcpy(acName1, acTmp1);

   if (pTmp=strstr(acName1, " TRUST"))
   {
      bTrust = true;
      if ((pTmp1=strstr(acName1, " FAMILY")) || (pTmp1=strstr(acName1, " REVOCABLE")) )
      {
         pTmp = acName1;
         while (pTmp < pTmp1 && !isdigit(*(pTmp)))
            pTmp++;
         if (pTmp < pTmp1)
         {
            --pTmp;
            strcpy(acSave1, pTmp);
            *pTmp = 0;
         } else
         {
            strcpy(acSave1, pTmp1);
            *pTmp1 = 0;
         }
      } else if (pTmp1=strstr(acName1, " LIVING"))
      {
         strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else if (pTmp1=strstr(acName1, " TEST "))
      {
         strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;
      }
   }

   // Try to break name
   if (!bTrust)
   {
      char *apTmp1[8], *apTmp2[8];

      pTmp = strchr(acName1, '&');
      if (pTmp)
      {
         *pTmp = 0;
         strcpy(acTmp, acName1);
         iTmp = ParseString(acTmp, ' ', 8, apTmp1); 
         *pTmp = '&';
      }

      // We don't break HENRY M/LUPE C GONZALES
      if (pTmp && (iTmp > 2 || (iTmp > 1 && strlen(apTmp1[1]) > 1)) )
      {

         strcpy(acTmp2, pTmp+2);
         iTmp = ParseString(acTmp2, ' ', 8, apTmp2); 

         // BUI DAVID PHUC H/VIVIAN PHUONG PHAN
         // VELASQUEZ ANTHONY M/CARRIE ANN A
         if (iTmp > 1 && strlen(apTmp2[iTmp-1]) > 1)
         {  
            // If same last name, drop it
            if (!memcmp(apTmp2[iTmp-1], acName1, strlen(apTmp2[iTmp-1])))
               acTmp1[0] = 0;
            else
               // Use last word as last name
               strcpy(acTmp1, apTmp2[iTmp-1]);

            for (iTmp1=0; iTmp1 < iTmp-1; iTmp1++)
            {
               strcat(acTmp1, " ");
               strcat(acTmp1, apTmp2[iTmp1]);
            }

            // Sale last name, append to name1, else make it name2
            if (acTmp1[0] == ' ')
               strcpy(pTmp+1, acTmp1);
            else
            {
               // Terminate name1
               *(pTmp-1) = 0;

               iParseType=4;
               // If name2 already have data, append it to acTmp1
               if (acName2[0])
               {
                  sprintf(acTmp, "%s & %s", acTmp1, acName2);
                  strcpy(acName2, acTmp);
               } else
                  strcpy(acName2, acTmp1);
            }
         }
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, iParseType);
      if (iRet == 1 || !bTrust)
         strcpy(pOwners->acName1, myOwner.acName1);
      else
         strcpy(pOwners->acName1, acName1);

      if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
         strcpy(pOwners->acName2, myOwner.acName2);

      if (myOwner.acVest[0] > ' ')
         strcpy(pOwners->acVest, myOwner.acVest);

      // Concat what in saved buffer
      if (acSave1[0])
      {
         if (pOwners->acName1[strlen(pOwners->acName1)-1] == ' ')
            strcat(pOwners->acName1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(pOwners->acName1, acSave1);
      }

      // If name is not swapable, use Name1 for it
      if (myOwner.acSwapName[0] && (!bTrust || iRet == 1))
         strcpy(pOwners->acSwapName, myOwner.acSwapName);
      else
         strcpy(pOwners->acSwapName, acName1);

      strcpy(pOwners->acOF, myOwner.acOF);
      strcpy(pOwners->acOM, myOwner.acOM);
      strcpy(pOwners->acOL, myOwner.acOL);
      strcpy(pOwners->acTitle, myOwner.acTitle);
   } else if (acSave1[0])
   {
      strcat(acName1, acSave1);
      strcpy(pOwners->acName1, acName1);
      strcpy(pOwners->acSwapName, acName1);
   } else
   {
      // Couldn't split names
      strcpy(pOwners->acName1, acName1);
      strcpy(pOwners->acSwapName, acName1);
   }

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' '))
   {
      iRet = splitOwner(acName2, &myOwner, iParseType);
      if (iRet >= 0)
      {
         strcpy(pOwners->acName2, myOwner.acName1);
         strcpy(pOwners->acSF, myOwner.acOF);
         strcpy(pOwners->acSM, myOwner.acOM);
         strcpy(pOwners->acSL, myOwner.acOL);
      } else
         strcpy(pOwners->acName2, acName2);
   } else
   {
      strcpy(pOwners->acSF, myOwner.acSF);
      strcpy(pOwners->acSM, myOwner.acSM);
      strcpy(pOwners->acSL, myOwner.acSL);
   }
}

/******************************** Sta_MergeOwner **************************

  Special cases:
      ROBIE W JOHN ET UX TRS & ROBIE W JOHN ET UX TRS KA
      GRAUPNER KARL E TRS & ZETTIA L & GRAUPNER & ZETTIA
      MORAN CLARE DUNN LIFE ESTATE
      GALLO ROBERT J LIFE EST
      BENNETT KAREL HEIRS OF ET AL
      GLOR DONALD D & VSEIA-GLOR VALORIE ANN
      AIKEN RAYMOND E ET UX TRS & AIKEN RAYMOND E ET UX
      VAN VLIET JOHN M & VAN VLIET MARY S
      O MEARA JACK B TRS & O MEARA MARIANNE H
      LA ROSA MICHAEL J & LA ROSA NANCY B

 *************************************************************************/

void Sta_MergeOwner(char *pNames, OWNER *pOwners)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp2[128], acTmp[128], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Copy data
   strcpy(acTmp1, pNames);

   // Replace comma with space
   if (pTmp = strrchr(acTmp1, ','))
      *pTmp = ' ';

   // Remove multiple spaces
   pTmp = acTmp1;
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '*' too
      if (*pTmp == '*' || *pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;

   // Check for year that goes before TRUST
   iTmp =0;   
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      strcpy(pOwners->acName1, acTmp);
      return;
   }

   // check to see if same name is repeated.  If so, remove the later part
   if (pTmp = strchr(acTmp, '&'))
   {
      *pTmp = 0;
      strcpy(acTmp1, acTmp);
      strcpy(acTmp2, pTmp+2);
      if (pTmp1 = strstr(acTmp1, " TRS"))
         *pTmp1 = 0;
      if (pTmp1 = strstr(acTmp2, " TRS"))
         *pTmp1 = 0;
      if (strcmp(acTmp1, acTmp2))
         *pTmp = '&';               // Different names
   }

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || 
       (pTmp=strstr(acTmp, " ET AL")) || 
       (pTmp=strstr(acTmp, " & ET UX")) ||
       (pTmp=strstr(acTmp, " ET UX")) ) 
      *pTmp = 0;

   // If more than one "&", put second part into name2
   if (pTmp = strchr(acTmp, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1 = 0;
         strcpy(acName2, pTmp1+2);
      }
   }
   
   if ((pTmp=strstr(acTmp, " LIFE EST")) || (pTmp=strstr(acTmp, " HEIRS OF")) )
   {
      *pTmp = 0;
   } 
   strcpy(acName1, acTmp);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   // Only parse if owner has more than one word
   if (strchr(acName1, ' '))
   {
      if (strchr(acName1, '-'))
         iRet = splitOwner(acName1, &myOwner, 4);
      else
         iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(pOwners->acName1, myOwner.acName1);
      if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
         strcpy(pOwners->acName2, myOwner.acName2);

      if (myOwner.acVest[0] > ' ')
         strcpy(pOwners->acVest, myOwner.acVest);

      // If name is not swapable, use Name1 for it
      if (iRet == -1)
         strcpy(pOwners->acSwapName, pOwners->acName1);
      else
         strcpy(pOwners->acSwapName, myOwner.acSwapName);

      strcpy(pOwners->acOF, myOwner.acOF);
      strcpy(pOwners->acOM, myOwner.acOM);
      strcpy(pOwners->acOL, myOwner.acOL);
      strcpy(pOwners->acTitle, myOwner.acTitle);
   } else
   {
      // Couldn't split names
      strcpy(pOwners->acName1, pNames);
      strcpy(pOwners->acSwapName, pNames);
   }

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' '))
   {
      iRet = splitOwner(acName2, &myOwner, 3);
      if (iRet >= 0)
      {
         strcpy(pOwners->acName2, myOwner.acName1);
         strcpy(pOwners->acSF, myOwner.acOF);
         strcpy(pOwners->acSM, myOwner.acOM);
         strcpy(pOwners->acSL, myOwner.acOL);
      } else
         strcpy(pOwners->acName2, acName2);
   } else
   {
      strcpy(pOwners->acSF, myOwner.acSF);
      strcpy(pOwners->acSM, myOwner.acSM);
      strcpy(pOwners->acSL, myOwner.acSL);
   }
}

/********************************************************************************/

void Ccx_MergeOwner(char *pName1, char *pName2)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acTmp[128], acTmp1[64], acTmp2[64];
   char  acName2[64];
   char  *pTmp;

   OWNER myOwner;

   strcpy(acTmp1, pName1);
   if (*pName2 > ' ')
   {
      strcpy(acTmp2, pName2);
   } else
      acTmp2[0] = 0;

   strcpy(acOwner1, myTrim(acTmp1));
   // Check for special char
   if (pTmp = strchr(acOwner1, '/'))
   {
      *pTmp = 0;
      sprintf(acTmp, "%s & %s", acOwner1, ++pTmp);
      strcpy(acOwner1, acTmp);
   }

   acOwner2[0] = 0;
   if (acTmp2[0])
      strcpy(acOwner2, myTrim(acName2));

   if (pTmp=strstr(acOwner1, " TRE"))
   {
      if (strlen(pTmp) == 4)
         *pTmp = 0;
   }

   // If Owner1 and Owner2 has the same last name, combine them
   if (acOwner2[0])
   {
      if (pTmp=strstr(acOwner2, " TRE"))
      {
         if (strlen(pTmp) == 4)
            *pTmp = 0;
      }

      // If Name1 is not a combined name, it might be merged with Name2
      if (!strchr(acOwner1, '&') && !strchr(acOwner2, '&'))
      {
         for (iTmp = 0; iTmp < strlen(acOwner1); iTmp++)
         {
            if (acOwner1[iTmp] != acOwner2[iTmp])
               break;
         }
         if (acOwner1[iTmp-1] == ' ')
         {
            // Merge names
            sprintf(acTmp, " & %s", (char *)&acOwner2[iTmp]);
            acOwner2[0] = 0;
            strcat(acOwner1, acTmp);
         }
      }
   }

   // Now parse owners
   splitOwner(acOwner1, &myOwners, 3);
   if (acOwner2[0])
   {
      splitOwner(acOwner2, &myOwner, 3);
      strcpy(myOwners.acName2, myOwner.acName1);
      strcpy(myOwners.acSF, myOwner.acOF);
      strcpy(myOwners.acSM, myOwner.acOM);
      strcpy(myOwners.acSL, myOwner.acOL);
   }
   acTmp[63] = 0;

}

/********************************************************************************/

void Sfx_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];

   // Clear output buffer if needed
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   if (!memcmp(pOutbuf, "0026T038A", 9))
      iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      // Fix problem as RAMON&PURISIMA
      if (*pTmp == '&' && acTmp[iTmp-1] != ' ')
      {
         acTmp[iTmp++] = ' ';
         acTmp[iTmp++] = *pTmp;
         if (*(pTmp+1) != ' ')
            acTmp[iTmp++] = ' ';
      } else
         acTmp[iTmp++] = toupper(*pTmp);

      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove unwanted characters
      if (*pTmp == '`' || *pTmp == ':' || *pTmp == 39)
         pTmp++;

      // Replace '.' with space
      if (*pTmp == '.')
         *pTmp = ' ';
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acSave1[0] = 0;

   if ((pTmp=strstr(acTmp, " 1/2")) || (pTmp=strstr(acTmp, " 1/4")) ||
      (pTmp=strstr(acTmp, " 1/6")) || (pTmp=strstr(acTmp, " 1/3")) ||
      (pTmp=strstr(acTmp, " 2/3")) || (pTmp=strstr(acTmp, " 28/270")) ||
      (pTmp=strstr(acTmp, " W/H")) || (pTmp=strstr(acTmp, " H/W")) || 
      (pTmp=strstr(acTmp, " & &"))  )
      *pTmp = 0;

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      //if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      //memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      //memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
     if ( (pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")))
        *pTmp = 0;

      // If TRUST appears after number, save from number forward
      if ((pTmp=strstr((char *)&acTmp[iTmp], " LIVING")) || (pTmp=strstr((char *)&acTmp[iTmp], " REVOC") ) ||
          (pTmp=strstr((char *)&acTmp[iTmp], " TRUST")) )
      {
         if (&acTmp[iTmp] < pTmp)
         {
            iTmp--;
            strcpy(acSave1, (char *)&acTmp[iTmp]);
            acTmp[iTmp] = 0;
         }
      }
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words, things in parenthesis
   // MONDANI NELLIE M ESTATE OF

   if ((pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR")) || 
      (pTmp=strstr(acTmp, " CO-TR"))    || (pTmp=strstr(acTmp, " COTR")) || 
      (pTmp=strstr(acTmp, " LIFE EST")) || (pTmp=strstr(acTmp, " ET AL TRUSTEE")) || 
      (pTmp=strstr(acTmp, ",TRSTE"))    || (pTmp=strstr(acTmp, " TSTE")) || 
      (pTmp=strstr(acTmp, ",TRUSTEE"))  || (pTmp=strstr(acTmp, " TRUSTEE")) ||
      (pTmp=strstr(acTmp, " TRST"))     || (pTmp=strstr(acTmp, " TR ")) ||
      (pTmp=strstr(acTmp, " TRES"))     || (pTmp=strstr(acTmp, " TTEE")) ||
      (pTmp=strstr(acTmp, " ESTATE OF"))|| (pTmp=strstr(acTmp, " ESTS OF")) ||
      (pTmp=strstr(acTmp, " EST OF"))   || (pTmp=strstr(acTmp, ",ET AL")) || 
      (pTmp=strstr(acTmp, " ETAL"))     || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if (!memcmp((char *)&acTmp[strlen(acTmp)-3], " RE", 3))
      acTmp[strlen(acTmp)-3] = 0;
   else if (!memcmp((char *)&acTmp[strlen(acTmp)-4], " REV", 4))
      acTmp[strlen(acTmp)-4] = 0;

   if ((pTmp=strstr(acTmp, " JT")) || (pTmp=strstr(acTmp, " CP")) ||
      (pTmp=strstr(acTmp, " LIVING")))
      *pTmp = 0;

   // Ignore MD
   if (pTmp=strstr(acTmp, " MD &"))
   {  // BRYCE I. MORRICE, MD & ELLEN S
      *pTmp = 0;
      pTmp += 3;           
      strcat(acTmp, pTmp);
   } else if (!memcmp(acTmp, "DR ", 3))
   {  // Ignore DR in "DR ANVAR M VELJI & PARI VELJI"
      pTmp = (char *)&acTmp[3];
      strcpy(acTmp, pTmp);
   }

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCC TR")) ||
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);

   } else if (pTmp=strstr(acTmp, " FMLY"))
   {  // RAMON&PURISIMA LACONICO FMLY T
      *pTmp = 0;
      strcpy(acName1, acTmp);
      strcpy(acSave1, " FAMILY TR");
   } else if (pTmp=strstr(acTmp, " EXEMPT TR"))
   {  // PANELLI RENZO R EXEMPT TR
      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " FAMILY ")) || (pTmp=strstr(acTmp, " REVOC")) ||
              (pTmp=strstr(acTmp, " LVG TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUS")) 
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwners, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwners, 6);
/*      
      // If parsable, use it.  Otherwise, keep the original
      if (iRet >= 0)
         strcpy(acTmp1, myOwner.acName1);
      else
         strcpy(acTmp1, acName1);

      // Check for saving?
      if (acSave1[0])
      {
         myTrim(acTmp1);
         strcat(acTmp1, acSave1);
      }

      iTmp = strlen(acTmp1);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 for swap
      if (iRet == -1)
      {
         iTmp = strlen(acName1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
      }

   } else
   {
      iTmp = strlen(pNames);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, pNames, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, pNames, iTmp);
*/
   }
}

/********************************************************************************/

void Kin_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];
   OWNER myOwner;

   /*
   memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   memset(pOutbuf+OFF_VEST, ' ', SIZ_VEST);
   memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);
   memset(pOutbuf+OFF_NAME_TYPE1, ' ', SIZ_NAME_TYPE1);
   memset(pOutbuf+OFF_NAME_TYPE2, ' ', SIZ_NAME_TYPE2);
   */

   // Clear output buffer if needed
   memset(acTmp, 0, 128);
   memset((void *)&myOwner, 0, sizeof(OWNER));
   memset((void *)&myOwners, 0, sizeof(OWNER));

#ifdef _DEBUG
   if (!memcmp(pOutbuf, "0026T038A", 9))
      iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      // Fix problem as RAMON&PURISIMA
      if (*pTmp == '&' && acTmp[iTmp-1] != ' ')
      {
         acTmp[iTmp++] = ' ';
         acTmp[iTmp++] = *pTmp;
         if (*(pTmp+1) != ' ')
            acTmp[iTmp++] = ' ';
      } else
         acTmp[iTmp++] = toupper(*pTmp);

      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove unwanted characters
      if (*pTmp == '`' || *pTmp == ':' || *pTmp == 39)
         pTmp++;

      // Replace '.' with space
      //if (*pTmp == '.')
      //   *pTmp = ' ';
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acSave1[0] = 0;

   // Remove % of ownership
   if (pTmp=strchr((char *)&acTmp[0], '%'))
   {
      do
      {
         pTmp--;
      } while (*pTmp != ' ');
      *pTmp = 0;
   }

   // Check for vesting - temporary remove, will reinstated later
   if (pTmp=strstr(acTmp, " H/W")) 
   {
      *pTmp = 0;
      pTmp1 = pTmp+4;
      if (!*pTmp1)
         strcpy(myOwner.acVest, "HW");
      else
      {
         if (pTmp=strstr(pTmp1, "JT"))
            strcpy(myOwner.acVest, "HWJT");
         else if (pTmp=strstr(pTmp1, "CP"))
            strcpy(myOwner.acVest, "HWCP");
         else
         {
            strcpy(myOwner.acVest, "HW");
         }
      }
   } else if (pTmp=strstr(acTmp, " W/H")) 
   {
      strcpy(myOwner.acVest, "WH");
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " T/C")) 
   {
      strcpy(myOwner.acVest, "TC");
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " S/M")) 
   {
      strcpy(myOwner.acVest, "SM");
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " M/M")) 
   {
      strcpy(myOwner.acVest, "MM");
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " JT")) 
   {
      *pTmp = 0;
      strcpy(myOwner.acVest, "JT");
   } else if (pTmp=strstr(acTmp, " CP")) 
   {
      strcpy(myOwner.acVest, "CP");
      *pTmp = 0;
   }

   // If there is no comma in name, don't parse
   if (!strchr(acTmp, ','))
   {
      if (pTmp = strstr(acTmp, " THE"))
         *pTmp = 0;

      strcpy(myOwners.acName1, acTmp);
      strcpy(myOwners.acSwapName, acTmp);
      if (myOwner.acVest[0] > ' ')
         strcpy(myOwners.acVest, myOwner.acVest);
      return;
   }

   if ((pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR")) || 
      (pTmp=strstr(acTmp, " CO-TR"))    || (pTmp=strstr(acTmp, " COTR"))  || 
      (pTmp=strstr(acTmp, ",ET AL"))    || (pTmp=strstr(acTmp, " ET AL")) || 
      (pTmp=strstr(acTmp, " ETAL"))     || (pTmp=strstr(acTmp, " ET-AL")) ||
      (pTmp=strstr(acTmp, ",TRSTE"))    || (pTmp=strstr(acTmp, " TSTE"))  || 
      (pTmp=strstr(acTmp, ",TRUSTEE"))  || (pTmp=strstr(acTmp, " TTEE"))  ||
      (pTmp=strstr(acTmp, " TRST"))     || (pTmp=strstr(acTmp, " TRES")) )       
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " RVC TR")) || (pTmp=strstr(acTmp, " REVOC")) ||
       (pTmp=strstr(acTmp, " REV TR")) || (pTmp=strstr(acTmp, " REV LIV")) ||
       (pTmp=strstr(acTmp, " REVC TR"))|| (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " LVG TR")) || (pTmp=strstr(acTmp, " REV LVNG")) ||
       (pTmp=strstr(acTmp, " LIV TR")) || (pTmp=strstr(acTmp, " RVC LVG")) ||
       (pTmp=strstr(acTmp, " LVNG TR"))|| (pTmp=strstr(acTmp, " BYPASS TR")) ||
       (pTmp=strstr(acTmp, " FAM TR")) || (pTmp=strstr(acTmp, " FAMILY ")) ||
       (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF")) ||
       (pTmp=strstr(acTmp, " LF EST")) || (pTmp=strstr(acTmp, " LIVING ")) ||
       (pTmp=strstr(acTmp, " ESTATE")) || (pTmp=strstr(acTmp, " SURVIVOR")) ||
       (pTmp=strstr(acTmp, " TESTAMENTARY")) || (pTmp=strstr(acTmp, " IRREVOCABLE")) ||
       (pTmp=strstr(acTmp, " TRUST")) 
       )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST TESTAMENTARY
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));
      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwners, 6);
      if (acSave1[0])
      {
         if (myOwners.acName1[strlen(myOwners.acName1)-1] == ' ')
            strcat(myOwners.acName1, (char *)&acSave1[1]);
         else            
            strcat(myOwners.acName1, acSave1);
      }

      if (myOwner.acVest[0] > ' ')
         strcpy(myOwners.acVest, myOwner.acVest);
/*      
      // If parsable, use it.  Otherwise, keep the original
      if (iRet >= 0)
         strcpy(acTmp1, myOwner.acName1);
      else
         strcpy(acTmp1, acName1);

      // Check for saving?
      if (acSave1[0])
      {
         myTrim(acTmp1);
         strcat(acTmp1, acSave1);
      }

      iTmp = strlen(acTmp1);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 for swap
      if (iRet == -1)
      {
         iTmp = strlen(acName1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
      }

   } else
   {
      iTmp = strlen(pNames);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, pNames, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, pNames, iTmp);
*/
   }
}

/*******************************************************************************/

void Sdx_MergeOwner(char *pRollRec)
{
   int   iTmp, iRet;
   char  acOwner1[128], acOwners[64], acTmp[128];
   char  *pTmp, *pTmp1, *pName2, *pName3, bNameFlg1, bNameFlg2;

   SDX_ROLL *pRec;
   SDX_NAME sNames[3];

   pRec = (SDX_ROLL *)pRollRec;

   memset(&sNames, 0, sizeof(SDX_NAME)*3);
   memset(&myOwners, 0, sizeof(OWNER));

   // Initialize
   memcpy(acOwner1, pRec->OwnerName, RSIZ_OWNER_NAME);
   //myTrim(acOwner1, RSIZ_OWNER_NAME);

   bNameFlg1=bNameFlg2 = ' ';

   // Find name ending char and terminate it
   pTmp = strchr(acOwner1, '\\');
   if (pTmp)
      *pTmp = 0;

   // Adding space to '&' if needed
   iTmp = 0;
   pTmp = acOwner1;
   while (*pTmp)
   {
      if (*pTmp == '&' && acTmp[iTmp-1] != ' ')
      {
         acTmp[iTmp++] = ' ';
         acTmp[iTmp++] = '&';
         acTmp[iTmp++] = ' ';
      } else
         acTmp[iTmp++] = *pTmp;
      pTmp++;
   }
   acTmp[iTmp] = 0;

   // Check for multiple names
   if (pRec->FractIntCode > '1')
   {
      pTmp = strchr(acTmp, '*');
      if (pTmp)
      {
         *pTmp++ = 0;
         memcpy(sNames[1].M_Flag, pTmp, 4);
         pName2 = strchr(pTmp, '#');
         if (pName2)
         {
            pTmp = strchr(pName2, '*');
            if (pTmp)
            {
               *pTmp++ = 0;
               memcpy(sNames[2].M_Flag, pTmp, 4);
               pName3 = strchr(pTmp, '#');
               if (pName3)
                  strcpy(sNames[2].Name, pName3+1);
            }
            strcpy(sNames[1].Name, pName2+1);
         }
      }
   }

   // Find Name1
   pTmp = strchr(acTmp, '#');
   if (pTmp) 
      strcpy(sNames[0].Name, pTmp+1);
   else
      strcpy(sNames[0].Name, acTmp);

   // Check for <LF>
   if (!memcmp(pRec->Matital_Status, "LF", 2))
   {
      if (pTmp = strstr(sNames[0].Name, "<LF>"))
         *pTmp = 0;

      //iTmp = strlen(sNames[0].Name);
      //memcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, iTmp);
      //memcpy(pOutbuf+OFF_NAME1, sNames[0].Name, iTmp);
      strcpy(myOwners.acSwapName, sNames[0].Name);
      strcpy(myOwners.acName1, sNames[0].Name);
      return;
   }

   // Check for CO
   if (!memcmp(pRec->Matital_Status, "CO", 2))
   {
      // Remove DBA
      if (pTmp = strstr(sNames[0].Name, "<DBA"))
         *pTmp = 0;

      //iTmp = strlen(sNames[0].Name);
      //memcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, iTmp);
     // memcpy(pOutbuf+OFF_NAME1, sNames[0].Name, iTmp);
      strcpy(myOwners.acSwapName, sNames[0].Name);
      strcpy(myOwners.acName1, sNames[0].Name);
      return;
   }

   // Check for PA
   if (!memcmp(pRec->Matital_Status, "PA", 2) || 
       !memcmp(pRec->Matital_Status, "WE", 2) ||
       !memcmp(pRec->Matital_Status, "RE", 2))
   {
      //iTmp = strlen(sNames[0].Name);
      //memcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, iTmp);
      //memcpy(pOutbuf+OFF_NAME1, sNames[0].Name, iTmp);
      strcpy(myOwners.acSwapName, sNames[0].Name);
      strcpy(myOwners.acName1, sNames[0].Name);
      return;
   }

   // Check for <PF> - drop everything after this
   if (pTmp = strstr(sNames[0].Name, "<PF>"))
   {
      *pTmp = 0;
      bNameFlg1 = 'O';
   }
   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<PF>")) )
   {
      *pTmp = 0;
      bNameFlg2 = 'O';
   }

   // Check for <LE> - drop everything after this
   if (pTmp = strstr(sNames[0].Name, "<LE>"))
   {
      *pTmp = 0;
      bNameFlg1 = 'E';
   }
   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<LE>")) )
   {
      *pTmp = 0;
      bNameFlg2 = 'E';
   }

   // Check for <
   if (pTmp = strchr(sNames[0].Name, '<'))
      *pTmp = 0;

   if ((pTmp = strstr(sNames[0].Name, " ET AL")) || (pTmp = strstr(sNames[0].Name, " ETAL")) )
      *pTmp = 0;
   if ((pTmp = strstr(sNames[1].Name, " ET AL")) || (pTmp = strstr(sNames[1].Name, " ETAL")) )
      *pTmp = 0;

   // Populate Name1
   strcpy(acOwners, sNames[0].Name);
   //memcpy(pOutbuf+OFF_NAME1, sNames[0].Name, strlen(sNames[0].Name));

   // Format swap name
   if (!memcmp(pRec->Matital_Status, "HW", 2))
   {
      splitOwner(sNames[0].Name, &myOwners, 3);
      //iTmp = strlen(myOwner.acSwapName);
      //if (iTmp > SIZ_NAME_SWAP)
      //   iTmp = SIZ_NAME_SWAP;
      //memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
   } else if (!memcmp(pRec->Matital_Status, "NS", 2))
   {
      strcpy(acOwner1, sNames[0].Name);
      if (pTmp = strchr(acOwner1, '('))
         *pTmp = 0;

      if ((pTmp1=strstr(acOwner1, " FAMILY")) || 
          (pTmp1=strstr(acOwner1, " REVOCABLE")) ||
          (pTmp1=strstr(acOwner1, " RESIDUAL")) ||
          (pTmp1=strstr(acOwner1, " LIVING")) ||
          (pTmp1=strstr(acOwner1, " SEPARATE")) 
          )
      {
         pTmp = acOwner1;
         while (pTmp < pTmp1 && !isdigit(*(pTmp)))
            pTmp++;
         if (pTmp < pTmp1)
         {
            --pTmp;
            *pTmp = 0;
         } else
            *pTmp1 = 0;

         bNameFlg1 = '1';
      } else if (pTmp1=strstr(acOwner1, " REVOC"))
      {  // Sometimes REVOC goes before FAMILY
         *pTmp1 = 0;
         bNameFlg1 = '1';
      } else if ((pTmp1=strstr(acOwner1, " EST OF")) ||
                 (pTmp1=strstr(acOwner1, " ESTATE")) ||
                 (pTmp1=strstr(acOwner1, " SURVIVORS")) ||
                 (pTmp1=strstr(acOwner1, " DECEDENTS")) ||
                 (pTmp1=strstr(acOwner1, " INTERVIVOS")) ||
                 (pTmp1=strstr(acOwner1, " INTER VIVOS")) )
      {
         *pTmp1 = 0;
         bNameFlg1 = '1';
      } else if (pTmp1=strstr(acOwner1, " TRUST"))
      {
         pTmp = acOwner1;
         while (pTmp < pTmp1 && !isdigit(*(pTmp)))
            pTmp++;
         if (pTmp < pTmp1)
         {
            --pTmp;
            *pTmp = 0;
         } else
            *pTmp1 = 0;
         bNameFlg1 = '1';
      } else if (pTmp1=strstr(acOwner1, " PROFIT SHARING"))
      {
         *pTmp1 = 0;
         bNameFlg1 = '1';
      }

      iRet = splitOwner(acOwner1, &myOwners, 3);
      if (iRet < 0)
      {
         //iTmp = strlen(sNames[0].Name);
         //if (iTmp > SIZ_NAME_SWAP)
         //   iTmp = SIZ_NAME_SWAP;
         //memcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, iTmp);
         strcpy(myOwners.acSwapName, sNames[0].Name);
      } else
      {
         //iTmp = strlen(myOwner.acSwapName);
         //if (iTmp > SIZ_NAME_SWAP)
         //   iTmp = SIZ_NAME_SWAP;
         //memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
      }
   } else
   {
      splitOwner(sNames[0].Name, &myOwners, 3);
      //iTmp = strlen(myOwner.acSwapName);
      //if (iTmp > SIZ_NAME_SWAP)
      //   iTmp = SIZ_NAME_SWAP;
      //memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001081009", 9))
   //   iTmp = 1;
#endif

   // Check Name2
   /*
   if (sNames[1].Name[0])
   {
      //iTmp = strlen(sNames[1].Name);
      //if (iTmp > SIZ_NAME2)
      //   iTmp = SIZ_NAME2;
      //memcpy(pOutbuf+OFF_NAME2, sNames[1].Name, iTmp);

      // Check for posible merging names
      if (bNameFlg1 == '1' || !memcmp(sNames[1].M_Flag, "NS", 2) ||
          strchr(sNames[0].Name, '&') || strchr(sNames[1].Name, '&')
         )
         iTmp = 0;
      else
         iTmp = MergeName2(sNames[0].Name, sNames[1].Name, acTmp, ' ');
      if (iTmp == 1)
      {
         strcpy(myOwners.acName1, acTmp);

         OWNER oTmp;
         splitOwner(acTmp, &oTmp, 3);
         strcpy(myOwners.acSwapName, oTmp.acSwapName);
         myOwners.acName2[0] = 0;
      } else
      {
         if (iTmp == 0)
            strcpy(myOwners.acName2, sNames[1].Name);
         strcpy(myOwners.acName1, acOwners);
      }
      
   } else
   {
      strcpy(myOwners.acName1, acOwners);
      myOwners.acName2[0] = 0;
   }
   */

   if (sNames[1].Name[0])
   {
      //iTmp = strlen(sNames[1].Name);
      //if (iTmp > SIZ_NAME2)
      //   iTmp = SIZ_NAME2;
      //memcpy(pOutbuf+OFF_NAME2, sNames[1].Name, iTmp);
      strcpy(myOwners.acName2, sNames[1].Name);
   }
   strcpy(myOwners.acName1, acOwners);

   //memcpy(pOutbuf+OFF_VEST, pRec->Matital_Status, 4);
   memcpy(myOwners.acVest, pRec->Matital_Status, 4);
}

/*
// Replace a substring in s Str with something else
void replStr(char *pStr, char *pFind, char *pRepl)
{
   char  acTmp[1024], *pTmp;

   pTmp = strstr(pStr, pFind);
   if (pTmp)
   {
      *pTmp = 0;
      sprintf(acTmp, "%s%s%s", pStr, pRepl, pTmp+strlen(pFind));
      strcpy(pStr, acTmp);
   }

}
*/

/******************************************************************************/

void Fre_MergeOwner(char *pName1, char *pName2)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acOwners[128], acTmp[128], acTmp1[64], acTmp2[64];
   char  *pTmp;
   bool  bXlat;

   OWNER myOwner;

   bXlat = false;

   strcpy(acTmp1, pName1);
   if (*pName2 > ' ')
   {
      strcpy(acTmp2, pName2);
      blankRem(acTmp2);
   } else
      acTmp2[0] = 0;

   blankRem(acTmp1);
   iTmp = strlen(acTmp1)-1;
   if (acTmp1[iTmp] == '-')
   {
      acTmp1[iTmp] = ' ';
      myTrim(acTmp1);
   }

   strcpy(acOwner1, acTmp1);
   // Check for special char
   if (pTmp = strchr(acOwner1, '/'))
   {
      if (!memcmp(pTmp-1, "C/F", 3) || !memcmp(pTmp-1, "R/L", 3))
         *(pTmp-2) = 0;
      else
      {
         *pTmp = 0;
         sprintf(acTmp, "%s & %s", acOwner1, ++pTmp);
         strcpy(acOwner1, acTmp);
      }
   }
   if (pTmp = strchr(acOwner1, '*'))
      *pTmp = 0;

   if ((pTmp=strstr(acOwner1, " ET AL")) || (pTmp=strstr(acOwner1, " ETAL"))    ||
       (pTmp=strstr(acOwner1, " TSTE"))  || (pTmp=strstr(acOwner1, " TRUSTEE")) ||
       (pTmp=strstr(acOwner1, " TTEE"))  )
      *pTmp = 0;

   strcpy(acOwners, acOwner1);
   if ((pTmp=strstr(acOwner1, " LIFE EST"))  || (pTmp=strstr(acOwner1, " BYPASS TR")) ||
       (pTmp=strstr(acOwner1, " BY DEMAND")) || 
       (pTmp=strstr(acOwner1, " TRUST")) ) 
      *pTmp = 0;

   acOwner2[0] = 0;
   if (acTmp2[0] && strcmp(acTmp1, acTmp2))
      strcpy(acOwner2, acTmp2);

   // Now parse owners
   // Translate OR to & before parsing
   if (pTmp = strstr(acOwner1, " OR "))
   {
      bXlat = true;
      *pTmp = 0;
      sprintf(acTmp, "%s & %s", acOwner1, pTmp+4);
      strcpy(acOwner1, acTmp);
   }

   iTmp = splitOwner(acOwner1, &myOwners, 3);
   if (iTmp < 0)
      strcpy(myOwners.acSwapName, acOwners);
   if (bXlat)
      replStr(myOwners.acSwapName, "&", "OR");

   strcpy(myOwners.acName1, acOwners);
   if (acOwner2[0])
   {
      if ((pTmp=strstr(acOwner2, " ET AL")) || (pTmp=strstr(acOwner2, " ETAL"))    ||
          (pTmp=strstr(acOwner2, " TSTE"))  || (pTmp=strstr(acOwner2, " TRUSTEE")) ||
          (pTmp=strstr(acOwner2, " TTEE"))  )
         *pTmp = 0;

      if (pTmp = strchr(acOwner2, '*'))
         *pTmp = 0;

      iTmp = splitOwner(acOwner2, &myOwner, 3);
      strcpy(myOwners.acName2, acOwner2);
      strcpy(myOwners.acSF, myOwner.acOF);
      strcpy(myOwners.acSM, myOwner.acOM);
      strcpy(myOwners.acSL, myOwner.acOL);
   }
}

/******************************************************************************
   - Cases:  DUBINSKY ANNE &                 ALTMAN MICHAEL J
             CULHANE NANCY M                 KARJALAINEN PERTTI V
             HOWARD DANIEL F &               HOWARD SANDRA E
             TUMMINIA CHARLES J & JANICE L
             SCHMID PETER & CHARLOTTE CO-TRS
             RULEY NICHOLAS /TR/ &           RULEY ANNE /TR/
             PETTY LEE A /TR &               PETTY DIANE S /TR/
             ASAY RALPH C TR &               ASAY KATHRYN TR
             PALMER CLARK M /TR/&            PALMER JOYCE T /TR/
             MICHAL JOHANN & TR              MICHAL ERIKA A TR
             CHASE JOSEPH J & JUANITA M TRUST
             TUTTLE DOROTHY N 1994 TRUST
             DOLAN RHODA M LIVING TRUST
             PAGE ELIZABETH A TR ETAL        MC GRANE FORBES R TR ETAL
             GATELY SHIRLEY R ETAL           JOHNSON SHARON L G & ETAL
             MOATS RONALD B /TR/  & ETAL     MOATS JOELLA /TR/ ETAL
             SEIFERT KATIE S /TR/ ETAL       SEIFERT TIMOTHY A /TR/ ETAL
             HANLEY JOHN F & ETAL            HANLEY DARLENE S ETAL
             STROUB MAUREEN V 33.333% ETAL   STROUB ADRIENNE E 33.333% ETAL
             FREITAS HENRY J TR 50% ETAL     FREITAS BARBARA A 50% ETAL
             HARTWELL JOAN TR L/E 50% ETAL   HARTWELL JOAN TR L/E 50% ETAL
             SAMADANI SIROOS 50%             SAMADANI FERESHTEH 50%
             I H S HOME BLDG ETAL            BESS WALTER B ETAL
             REED JOHN A III &               CARSON JO L
             FLINK/BONNEY FAMILY REVOC TRUST (16504304)
 ******************************************************************************/
/*
bool isBlank(char *pStr)
{
   bool bRet = true;

   while (*pStr)
   {
      if (*pStr != ' ')
      {
         bRet = false;
         break;
      }
      pStr++;
   }

   return bRet;
}
*/
void Mrn_MergeOwner(char *pNames)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acSave1[128], acSave2[128];
   char  acTmp[128], acTmp1[128], acTmp2[128];
   char  *pTmp, *pName2;
   bool  bAnd;

   OWNER myOwner1, myOwner2;

   memset((void *)&myOwners, 0, sizeof(OWNER));
   memset(acTmp, 0, 128);
   strcpy(acTmp, pNames);
   memcpy(acTmp1, acTmp, 32);
   acTmp1[32] = 0;
   pName2 = &acTmp[32];

   if (*pName2 == ' ' && *(pName2+1) >= 'A')
   {
      // Append name2 to name 1
      strcat(acTmp1, pName2);
      acOwner2[0] = 0;
   } else if (!isBlank(pName2))
   {
      strcpy(acTmp2, pName2);
      if (pTmp = strstr(acTmp2, "/TR"))
      {
         *pTmp = 0;
         if (*(pTmp+3) == '/')
            pTmp += 4;
         else
            pTmp += 3;
         sprintf(acOwner2, "%sTR %s", acTmp2, pTmp);
      } else
         strcpy(acOwner2, acTmp2);

      blankRem(acOwner2);

      // Remove ETAL
      if (pTmp=strstr(acOwner2, " ETAL")) 
         *pTmp = 0;
      // Remove percentage owned
      if (pTmp=strchr(acOwner2, '%')) 
      {
         while (*pTmp != ' ')
            pTmp--;
         *pTmp = 0;
      }

      iTmp = strlen(acOwner2);
      pTmp = &acOwner2[iTmp-1];
      if (*pTmp == '&') 
         *pTmp = ' ';
      else if (!memcmp(&acOwner2[iTmp-4], "& TR", 4))
      {
         acOwner2[iTmp-4] = ' ';
         blankRem(acOwner2);
      }
   } else
      acOwner2[0] = 0;

   // Check for CO-TRS, TR, /TR, /TR/, EST OF, L/E, LIVING TRUST ...
   if (pTmp = strstr(acTmp1, "/TR"))
   {
      *pTmp = 0;
      if (*(pTmp+3) == '/')
         pTmp += 4;
      else
         pTmp += 3;
      sprintf(acOwner1, "%sTR %s", acTmp1, pTmp);
   } else
      strcpy(acOwner1, acTmp1);

   // Remove ETAL
   if (pTmp=strstr(acOwner1, " ETAL")) 
      *pTmp = 0;
   // Remove percentage owned
   if (pTmp=strchr(acOwner1, '%')) 
   {
      while (*pTmp != ' ')
         pTmp--;
      *pTmp = 0;
   }
   // Remove '&' if appeared at the end of name
   myTrim(acOwner1);
   iTmp = strlen(acOwner1);
   pTmp = &acOwner1[iTmp-1];
   if (*pTmp == '&') 
   {
      bAnd = true;
      *pTmp = ' ';
   } else if (!memcmp(&acOwner1[iTmp-4], "& TR", 4))
   {
      bAnd = true;
      acOwner1[iTmp-4] = ' ';
   } else
      bAnd = false;

   // Save owner1
   blankRem(acOwner1);
   strcpy(acSave1, acOwner1);

   // Strip off special words to form swapped name
   if ((pTmp=strstr(acOwner1, " EST OF")) || (pTmp=strstr(acOwner1, " ESTATE OF")) ||
       (pTmp=strstr(acOwner1, " L/E")) )
      *pTmp = 0;

   if (pTmp=strstr(acOwner1, " TRUST"))
   {
      *pTmp-- = 0;

      // Skip 1994 TRUST
      while (isdigit(*pTmp))
         pTmp--;
      if (*pTmp != ' ') pTmp++;
      *pTmp = 0;

      if ((pTmp=strstr(acOwner1, " FAMILY")) || (pTmp=strstr(acOwner1, " LIVING"))  ||
          (pTmp=strstr(acOwner1, " REVOC")) )
         *pTmp = 0;
   }

   // Now parse owners
   iTmp = splitOwner(acOwner1, &myOwner1, 3);
   if (iTmp < 0)
      strcpy(myOwner1.acSwapName, acSave1);

   if (acOwner2[0] && strcmp(acSave1, acOwner2) )
   {
      strcpy(acSave2, acOwner2);
      // Strip off special words to form swapped name
      if ((pTmp=strstr(acOwner2, " EST OF")) || (pTmp=strstr(acOwner2, " ESTATE OF")) ||
          (pTmp=strstr(acOwner2, " L/E")) )
         *pTmp = 0;

      if (pTmp=strstr(acOwner2, " TRUST"))
      {
         *pTmp = 0;
         if ((pTmp=strstr(acOwner2, " FAMILY")) || (pTmp=strstr(acOwner2, " LIVING"))  ||
             (pTmp=strstr(acOwner2, " REVOC")) )
            *pTmp = 0;
      }

      iTmp = splitOwner(acOwner2, &myOwner2, 3);
      if (iTmp < 0)
      {
         strcpy(myOwners.acName2, acSave2);
      } else if (bAnd)
      {
         if (!strcmp(myOwner1.acOL, myOwner2.acOL))
         {
            sprintf(myOwners.acName1, "%s %s %s & %s %s", myOwner1.acOL, myOwner1.acOF,
               myOwner1.acOM, myOwner2.acOF, myOwner2.acOM);
            sprintf(myOwners.acSwapName, "%s %s & %s %s %s", myOwner1.acOF,
               myOwner1.acOM, myOwner2.acOF, myOwner2.acOM, myOwner1.acOL);
            blankRem(myOwners.acName1);
            blankRem(myOwners.acSwapName);
         } else
         {
            strcpy(myOwners.acName2, acSave2);
            strcpy(myOwners.acName1, acSave1);
            strcpy(myOwners.acSwapName, myOwner1.acSwapName);
         }
      } else
      {
         strcpy(myOwners.acName2, acSave2);
         strcpy(myOwners.acName1, acSave1);
         strcpy(myOwners.acSwapName, myOwner1.acSwapName);
      }
      strcpy(myOwners.acSF, myOwner2.acOF);
      strcpy(myOwners.acSM, myOwner2.acOM);
      strcpy(myOwners.acSL, myOwner2.acOL);
   } else
   {
      strcpy(myOwners.acName1, acSave1);
      strcpy(myOwners.acSwapName, myOwner1.acSwapName);
      strcpy(myOwners.acSF, myOwner1.acSF);
      strcpy(myOwners.acSM, myOwner1.acSM);
   }
   strcpy(myOwners.acOF, myOwner1.acOF);
   strcpy(myOwners.acOM, myOwner1.acOM);
   strcpy(myOwners.acOL, myOwner1.acOL);
}

/******************************************************************************

    TOLSON, L & A REVOC LIV TR 9-7-5
    FORD, ROBERT D FAMILY TRUST 4-2-91
    PRATKO, MADELINE M TRUST
    SAUNDERS, ROY D AND ANNA M TR
    FERGUSON FAMILY REV LIVING TRUST 12/
    LOPEZ, PAUL A JR
    GARCIA, JOSEPHINE H - EST OF
    VANDEN BOSCH, ADRIAN J AND PATSY L
    CHAVEZ MARIO (SP ARACELI)

 ******************************************************************************/

void Sbd_MergeOwner(char *pName1, char *pName2, char *pVest1, char *pVest2)
{
   char  acOwner1[128], acOwner2[128], acTmp[128], acSave[64];
   char  acLName[64];
   char  *pTmp;

   OWNER myOwner;

   // Save last name
   strcpy(acLName, pName1);
   strcpy(acSave, pVest1);
   pTmp = strchr(acLName, ',');
   if (pTmp) *pTmp = 0;

   // Copy Name1
   strcpy(acOwner1, pName1);
   pTmp = &acOwner1[strlen(acOwner1)-1];
   if (*pTmp == '/' || *pTmp == '-')
      *pTmp = 0;
   blankRem(acOwner1);

   // If name2 is the same as name1, ignore it
   if (*pName2 > ' ' && strcmp(pName1, pName2))
   {
      strcpy(acOwner2, pName2);
      blankRem(acOwner2);
   } else
   {
      acOwner2[0] = 0;
      acSave[0] = 0;
   }

   // Check for trust
   if (!memcmp(pVest1, "TR", 2) || !memcmp(pVest1, "TU", 2) ||
       !memcmp(pVest1, "PT", 2) || !memcmp(pVest1, "CT", 2) ||
       strstr(acOwner1, " TRS") || strstr(acOwner1, " TR ") || strstr(acOwner1, "EST OF")
      )
   {                        
      memset(&myOwners, 0, sizeof(OWNER));
      if (pTmp = strchr(acOwner1, ','))
      {
         *pTmp++ = 0;
         strcat(acOwner1, pTmp);
      }
      strcpy(myOwners.acName1, acOwner1);
      if (pTmp = strchr(acOwner2, ','))
      {
         *pTmp++ = 0;
         strcat(acOwner2, pTmp);
      }
      strcpy(myOwners.acName2, acOwner2);
      strcpy(myOwners.acSwapName, acOwner1);
      return;
   }

   if (acOwner2[0])
   {
      // If Owner1 and Owner2 are "HW" and have the same last name, combine them
      if (!memcmp(pVest1, pVest2, 2) && 
          !memcmp(acLName, acOwner2, strlen(acLName)) 
         )
      {
         MergeName(acOwner1, acOwner2, acOwner1);
         acOwner2[0] = 0;
      }
   }

   // Now parse owners
   splitOwner(acOwner1, &myOwners, 3);
   if (myOwners.acTitle[0] > ' ')
      strcat(myOwners.acName1, myOwners.acTitle);
   if (acOwner2[0])
   {
      splitOwner(acOwner2, &myOwner, 3);
      if (!strcmp(myOwners.acOL, myOwner.acOL) && !memcmp(pVest1, pVest2, 2))
      {
         sprintf(acTmp, "%s & %s %s", myOwners.acName1, myOwner.acOF, myOwner.acOM);
         strcpy(myOwners.acName1, acTmp);
         sprintf(acTmp, "%s %s & %s", myOwners.acOF, myOwners.acOM, myOwner.acSwapName);
         blankRem(acTmp);
         strcpy(myOwners.acSwapName, acTmp);
      } else
      {
         strcpy(myOwners.acName2, myOwner.acName1);
         if (myOwner.acTitle[0] > ' ')
            strcat(myOwners.acName2, myOwner.acTitle);
         strcpy(myOwners.acSF, myOwner.acOF);
         strcpy(myOwners.acSM, myOwner.acOM);
         strcpy(myOwners.acSL, myOwner.acOL);
      }
   }
   strcpy(myOwners.acVest, acSave);
   acTmp[63] = 0;

}

/******************************************************************************/

void Smx_MergeOwner(char *pName1, char *pName2)
{
   char  acOwner1[128], acOwner2[128], acTmp[128], acSave[64];
   char  acLName[64];
   char  *pTmp;

   OWNER myOwner;

   // Save last name
   strcpy(acLName, pName1);
   pTmp = strchr(acLName, ',');
   if (pTmp) *pTmp = 0;

   // Copy Name1
   strcpy(acOwner1, pName1);
   //pTmp = &acOwner1[strlen(acOwner1)-1];
   //if (*pTmp == '/' || *pTmp == '-')
   //   *pTmp = 0;
   blankRem(acOwner1);

   // If name2 is the same as name1, ignore it
   if (*pName2 > ' ' && strcmp(pName1, pName2))
   {
      strcpy(acOwner2, pName2);
      blankRem(acOwner2);
   } else
   {
      acOwner2[0] = 0;
      acSave[0] = 0;
   }

   if (pTmp = strstr(acOwner1, " ET AL") )
      *pTmp = 0;

   // Check for trust
   if (strstr(acOwner1, " TRUST") ||
       strstr(acOwner1, " L/E") ||
       strstr(acOwner1, " EST OF")
      )
   {                        
      memset(&myOwners, 0, sizeof(OWNER));
      if (pTmp = strchr(acOwner1, ','))
      {
         *pTmp++ = 0;
         strcat(acOwner1, pTmp);
      }
      strcpy(myOwners.acName1, acOwner1);
      if (pTmp = strchr(acOwner2, ','))
      {
         *pTmp++ = 0;
         strcat(acOwner2, pTmp);
      }
      strcpy(myOwners.acName2, acOwner2);
      strcpy(myOwners.acSwapName, acOwner1);
      return;
   }

   if (acOwner2[0])
   {
      // If Owner1 and Owner2 are "HW" and have the same last name, combine them
      if (!memcmp(acLName, acOwner2, strlen(acLName)) 
         )
      {
         MergeName(acOwner1, acOwner2, acOwner1);
         acOwner2[0] = 0;
      }
   }

   // Now parse owners
   splitOwner(acOwner1, &myOwners, 3);
   if (myOwners.acTitle[0] > ' ')
      strcat(myOwners.acName1, myOwners.acTitle);
   if (acOwner2[0])
   {
      splitOwner(acOwner2, &myOwner, 3);
      if (!strcmp(myOwners.acOL, myOwner.acOL))
      {
         sprintf(acTmp, "%s & %s %s", myOwners.acName1, myOwner.acOF, myOwner.acOM);
         strcpy(myOwners.acName1, acTmp);
         sprintf(acTmp, "%s %s & %s", myOwners.acOF, myOwners.acOM, myOwner.acSwapName);
         blankRem(acTmp);
         strcpy(myOwners.acSwapName, acTmp);
      } else
      {
         strcpy(myOwners.acName2, myOwner.acName1);
         if (myOwner.acTitle[0] > ' ')
            strcat(myOwners.acName2, myOwner.acTitle);
         strcpy(myOwners.acSF, myOwner.acOF);
         strcpy(myOwners.acSM, myOwner.acOM);
         strcpy(myOwners.acSL, myOwner.acOL);
      }
   }
}

/********************************************************************/

void Iny_MergeOwner(char *pNames)
{
   char  acOwners[64], acTmp1[64], acSave[64];
   char  acName1[64], acCareOf[64], acVest[8], acTitle[8];
   char  *pTmp, *pTmp1, *pName2;

   // Initialize
   memset((void *)&myOwners, 0, sizeof(OWNER));

   // Point to name2
   pName2 = pNames+CRESIZ_NAME1;
   acCareOf[0] = 0;
   acSave[0]=acTitle[0]=acVest[0] = 0;

   // Remove '.'
   if (pTmp = strchr(pNames, '.'))
   {  
      while (*pTmp)
      {
         if (*pTmp == '.')
            *pTmp = ' ';
         pTmp++;
      }
   } 

   // Save name1
   strncpy(acName1, pNames, CRESIZ_NAME1*2);
   acName1[CRESIZ_NAME1*2] = 0;

   if (!memcmp(pName2, "C/O", 3))
   {
      *pName2 = 0;
      strcpy(acName1, pNames);
      strcpy(acCareOf, pName2+4);
   } else if (*pName2 == '%')
   {
      pTmp = &acName1[CRESIZ_NAME1];
      *pTmp-- = 0;
      while (*pTmp == ' ')
         pTmp--;
      while (isdigit(*pTmp))
         pTmp--;
      *(++pTmp) = 0;
   }

   myTrim(acName1);
   if ((pTmp=strstr(acName1, " 1/")) || (pTmp=strstr(acName1, " 2/3")) )
   {
      if (*(pTmp-1) == ',')
         pTmp--;
      *pTmp = 0;
   } else if (pTmp=strstr(acName1, "/100"))
   {
      *(pTmp-3) = 0;
   }

   // Check for Trust after last name
   if (pTmp=strstr(acName1, " TRUST,"))
   {
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      pTmp1 = pTmp + 7;
      while (*pTmp1)
      {
         if (isdigit(*pTmp1))
            break;
         pTmp1++;
      }
      if (*pTmp1)
      {
         sprintf(acSave, "%s TRUST", pTmp1);
         *pTmp1 = 0;
      } else
         strcpy(acSave, "TRUST");

      strcat(acTmp1, pTmp+7);
      strcpy(acName1,acTmp1);

   } else if (pTmp=strstr(acName1, " TR,"))
   {
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+4);
      strcpy(acName1,acTmp1);
   } else if (pTmp=strstr(acName1, " III,"))
   {
      memcpy(acTitle, "III", 3);
      acTitle[3] = 0;
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+5);
      strcpy(acName1,acTmp1);
   } else if (pTmp=strstr(acName1, " LIFE ESTATE,"))
   {
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+13);
      strcpy(acName1,acTmp1);
      strcpy(acSave, "LIFE ESTATE");
   } else if (pTmp=strstr(acName1, " LIFE EST,"))
   {
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+10);
      strcpy(acName1,acTmp1);
      strcpy(acSave, "LIFE EST");
   } else if (pTmp=strstr(acName1, " ESTATE OF,"))
   {
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+11);
      strcpy(acName1,acTmp1);
      strcpy(acSave, "ESTATE OF");
   } else if ((pTmp=strstr(acName1, " JR,")) || (pTmp=strstr(acName1, " SR,")) )
   {
      memcpy(acTitle, pTmp+1, 2);
      acTitle[2] = 0;
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+4);
      strcpy(acName1,acTmp1);
   } else if (pTmp=strstr(acName1, " M D,"))
   {
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+5);
      strcpy(acName1,acTmp1);
   } else if (pTmp=strstr(acName1, " MD,"))
   {
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+4);
      strcpy(acName1,acTmp1);
   } else if (pTmp=strstr(acName1, " MD INC,"))
   {
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+8);
      strcpy(acName1,acTmp1);
      strcpy(acSave, "MD INC");
   } else if (pTmp=strstr(acName1, " RPT,"))
   {
      *pTmp = 0;
      strcpy(acTmp1, acName1);
      strcat(acTmp1, pTmp+5);
      strcpy(acName1,acTmp1);
   }

   if (pTmp=strstr(acName1, " ESTATE OF"))
   {
      memset(pTmp, ' ', 10);
      strcpy(acSave, "ESTATE OF");
   } else if (pTmp=strstr(acName1, " EST OF"))
   {
      memset(pTmp, ' ', 7);
      strcpy(acSave, "EST OF");
   } else if (pTmp=strstr(acName1, " TRUST "))
   {
      if (pTmp1 = strchr(pTmp, ','))
      {
         *pTmp1++ = 0;
         strcpy(acSave, pTmp+1);
         strcpy(pTmp+1, pTmp1);
      }
   }

   if (pTmp=strstr(acName1, " BENF"))
      *pTmp = 0;
   else if ((pTmp=strstr(acName1, " ET AL")) || (pTmp=strstr(acName1, " ETAL")) )
      *pTmp = 0;
   
   blankRem(acName1);
   // Remove incomplete word
   if (acName1[strlen(acName1)-1] == '-')
      acName1[strlen(acName1)-1] = 0;

   strcpy(acOwners, acName1);

   if (pTmp=strstr(acName1, " TRUSTEE"))
      *pTmp = 0;

   // Now parse owners
   if (strstr(acOwners, " COMP/")   || strstr(acOwners, " TRUSTEES") || 
       strstr(acOwners, " COLLEGE") || strstr(acOwners, " CSD") ||
       !memcmp(acOwners, "OWNERS", 6)  || strstr(acOwners, " CSD") 
       )
   {
      strcpy(myOwners.acName1, acOwners);
      strcpy(myOwners.acSwapName, acOwners);
   } else
   {
      if (!memcmp(acName1, "VER ", 4))
         splitOwner(acName1, &myOwners, 2);
      else
         splitOwner(acName1, &myOwners, 3);
      if (acSave[0] >= '0')
         strcat(myOwners.acName1, acSave);
   }

   if (acCareOf[0])
   {
      acCareOf[sizeof(myOwners.acCareOf)-1] = 0;
      strcpy(myOwners.acCareOf, acCareOf);
   }
   if (acVest[0])
      strcpy(myOwners.acVest, acVest);
   if (acTitle[0])
      strcpy(myOwners.acTitle, acTitle);
}

/******************************************************************************/

#define   RSIZ_SCL_NAME1            43
#define   RSIZ_SCL_DOCNUM1          7
#define   RSIZ_SCL_NAME2            43

void Scl_MergeOwner(char *pName1)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acTmp[128], acCareOf[64], acSave1[64], acSave2[64];
   char  *pTmp, *pTmp1, *pName2;
   bool  bNoMerge=false, bKeep=false;

   OWNER myOwner;

   strncpy(acOwner1, pName1, RSIZ_SCL_NAME1);
   blankRem(acOwner1, RSIZ_SCL_NAME1);
   acCareOf[0] = 0;
   acSave1[0]=acSave2[0] = 0;

   pName2 = pName1+RSIZ_SCL_NAME1+RSIZ_SCL_DOCNUM1;
   if (*pName2 > ' ')
   {
      strcpy(acOwner2, pName2);
      blankRem(acOwner2);
      if (*pName2 == '*')
      {
         strcpy(acOwner1, &acOwner2[1]);
         acOwner2[0] = 0;

         // Copy Name1 to seller
      } else if (!memcmp(acOwner2, "C/O", 3))
      {
         strcpy(acCareOf, &acOwner2[4]);
         acOwner2[0] = 0;
      } else
      {
         if (pTmp = strstr(acOwner2, " TRUSTE"))
            *pTmp = 0;
         if ((pTmp = strstr(acOwner2, " ET AL")) || (pTmp = strstr(acOwner2, " ETAL")) )
            *pTmp = 0;

         strcpy(acSave2, acOwner2);
         pTmp = isNumIncluded(acOwner2, 0);
         if (pTmp || strstr(acOwner2, " TRUST ") || strstr(acOwner2, " AND ")  )
            bNoMerge = true;
      }
   } else
      acOwner2[0] = 0;

   // Drop TRUSTEE and ET AL
   if (pTmp = strstr(acOwner1, " TRUSTE"))
      *pTmp = 0;
   if ((pTmp = strstr(acOwner1, " & ET AL")) || 
       (pTmp = strstr(acOwner1, " ET AL")) || 
       (pTmp = strstr(acOwner1, " ETAL")) || 
       (pTmp = strstr(acOwner1, " R/O")))
      *pTmp = 0;

   pTmp = &acOwner1[RSIZ_SCL_NAME1-4];
   if (!memcmp(pTmp, "TRUS", 4) || !memcmp(pTmp, " TRU", 4) )
      *pTmp = 0;

   // If name starts with a digit, do not parse
   if (isdigit(acOwner1[0]))
   {
      strcpy(myOwners.acName1, acOwner1);
      strcpy(myOwners.acSwapName, acOwner1);
      strcpy(myOwners.acName2, acOwner2);
      return;
   }

   if (!strcmp(acOwner1, acOwner2))
      acOwner2[0] = 0;

   /*
   // If name1 and name2 have the same last name and doesn't have "AND", merge them
   if (acOwner2[0] >= 'A' && !strstr(acOwner1, " AND ") && !strstr(acOwner2, " AND ") )
   {
      pTmp = strchr(acOwner1, ' ');
      if (pTmp)
      {
         if (!memcmp(acOwner1, acOwner2, pTmp - acOwner1))
         {
            MergeName(acOwner1, acOwner2, acOwners, ' ');
            acOwner2[0] = 0;
            strcpy(acOwner1, acOwners);
         }
      }
   }
   */

   // Save owner1
   strcpy(acSave1, acOwner1);

   pTmp = isNumIncluded(acOwner1, 0);
   if (pTmp || strstr(acOwner1, " TRUST ") || strstr(acOwner1, " AND ")  )
      bNoMerge = true;

   // Check for trust
   if ((pTmp = strstr(acOwner1, " FAMILY")) ||
       (pTmp = strstr(acOwner1, " LIFE ESTATE")) ||
       (pTmp = strstr(acOwner1, " TRUST")) ||
       (pTmp = strstr(acOwner1, " EST OF"))
      )
   {        
      *pTmp = 0;
      bKeep = true;
   }

   // Now parse owners
   if (pTmp = strchr(acOwner1, '.'))
   {
      *pTmp = ' ';
      if (pTmp1 = strchr(pTmp, '.'))
         *pTmp1 = ' ';
      blankRem(acOwner1);
   }

   iTmp = splitOwner(acOwner1, &myOwners, 2);
   if (iTmp == -1)
   {
      strcpy(myOwners.acSwapName, acSave1);
      bNoMerge = true;
   } else if (strchr(myOwners.acOM, '/'))
   {
      pTmp = strchr(acSave1, '/');
      if (pTmp && *(pTmp-1) == ' ' && *(pTmp+1) == ' ')
      {
         *pTmp = '&';
         if (pTmp = strchr(myOwners.acSwapName, '/'))
            *pTmp = '&';
      }
   }

   if (!strcmp(myOwners.acOL, myOwners.acSM) && !strcmp(myOwners.acOF, myOwners.acSL))
   {
      myOwners.acSF[0] = 0;
      myOwners.acSL[0] = 0;
      myOwners.acSM[0] = 0;
      myOwners.acName2[0] = 0;
   }

   if (acOwner2[0])
   {
      if (!bNoMerge)
      {
         // Parse name2 - if they have same last name, merge them
         splitOwner(acOwner2, &myOwner, 2);
         if (!strcmp(myOwners.acOL, myOwner.acOL))
         {
            // Ignore name2 if name1 and name2 have the same first and last name
            if (strcmp(myOwners.acOF, myOwner.acOF))
            {
               sprintf(acTmp, "%s %s & %s", myOwners.acOF, myOwners.acOM, myOwner.acSwapName);
               blankRem(acTmp);
               strcpy(myOwners.acSwapName, acTmp);

               // Merge name1 if not protected
               if (!bKeep)
               {
                  sprintf(acTmp, "%s & %s %s", myOwners.acName1, myOwner.acOF, myOwner.acOM);
                  blankRem(acTmp);
                  strcpy(myOwners.acName1, acTmp);
                  acSave1[0] = 0;
               } else
                  strcpy(myOwners.acName2, acSave2);
            }
         } else
         {
            strcpy(myOwners.acSF, myOwner.acOF);
            strcpy(myOwners.acSM, myOwner.acOM);
            strcpy(myOwners.acSL, myOwner.acOL);
            
            strcpy(myOwners.acName2, acSave2);
         }
      } else
         strcpy(myOwners.acName2, acSave2);
   } else
      myOwners.acName2[0] = 0;

   if (acCareOf[0] > ' ')
      strcpy(myOwners.acCareOf, acCareOf);

   if (acSave1[0])
      strcpy(myOwners.acName1, acSave1);
   myOwners.acTitle[0] = 0;
}

/******************************************************************************/

#define  ROFF_NAME1                 0
#define  ROFF_ETAL                  26
#define  ROFF_NAME2                 30

#define  RSIZ_NAME1                 26
#define  RSIZ_ETAL                  4 
#define  RSIZ_NAME2                 29

void Edx_MergeOwner(char *pName1)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acTmp[128];
   char  acSave1[64], acSave2[64], acCareOf[64];
   char  *pTmp, *pName2;
   bool  bNoMerge=false, bKeep=false;

   OWNER myOwner;

   memset(&myOwners, 0, sizeof(OWNER));
   acSave1[0]=acSave2[0]=acCareOf[0]=acOwner2[0] = 0;
   if (!memcmp(pName1, "NO OWNER", 8))
   {
      memcpy(myOwners.acName1, pName1, RSIZ_NAME1);
      memcpy(myOwners.acSwapName, pName1, RSIZ_NAME1);
      return;
   }

   pName2 = pName1 + ROFF_NAME2;

   if (*pName2 == '%' || !memcmp(pName2, "C/O", 3) || !memcmp(pName2, "ATTN", 4))
   {
      pTmp = strchr(pName2, ' ');
      strcpy(acCareOf, pTmp+1);
      blankRem(acCareOf);
   } else if (*pName2 > ' ')
   {
      strcpy(acOwner2, pName2);
      if ((pTmp = strstr(acOwner2, " LE%")) || (pTmp = strstr(acOwner2, " LE %")) )
         *pTmp = 0;
      if ((pTmp = strchr(acOwner2, '%')) || (pTmp = strchr(acOwner2, '(')) )
         *pTmp = 0;
      strcpy(acSave2, acOwner2);
      if ((pTmp = isNumIncluded(acOwner2, 0)) || 
          strstr(acOwner2, " TRUST ")  ||
          strstr(acOwner2, " AKA ")  )
         bNoMerge = true;
      else
      {
         if ((pTmp=strstr(acOwner2, " SUCC CO")) ||
             (pTmp=strstr(acOwner2, " CO TR"))   || 
             (pTmp=strstr(acOwner2, " CO SUC"))  ||             
             (pTmp=strstr(acOwner2, " SURV T"))  ||
             (pTmp=strstr(acOwner2, " MD TR"))   || 
             (pTmp=strstr(acOwner2, " SUC TR"))  ||
             (pTmp=strstr(acOwner2, " SUCC T")) )
         {
            *pTmp = 0;
         } else
         {
            pTmp = &acOwner2[strlen(acOwner2)-3];
            if (!memcmp(pTmp, " TR", 3) || (pTmp=strstr(acOwner2, " TR ")))
               *pTmp = 0;
         }
      }

      if ((pTmp = strstr(acOwner2, " TRUSTE")) ) 
         *pTmp = 0;

      if ((pTmp = strstr(acOwner2, "CORP")) || 
          (pTmp = strstr(acOwner2, "LLC")) )
         acOwner2[0] = 0;
      else
         blankRem(acOwner2);
   } 

   // Process name1
   memcpy(acOwner1, pName1, RSIZ_NAME1);
   acOwner1[RSIZ_NAME1] = 0;
   if ((pTmp = strstr(acOwner1, " LE%"))  || 
       (pTmp = strstr(acOwner1, " LE %")) ||
       (pTmp = strstr(acOwner1, " AKA ")) )
      *pTmp = 0;
   if ((pTmp = strchr(acOwner1, '%')) || (pTmp = strchr(acOwner1, '(')) )
      *pTmp = 0;
   strcpy(acSave1, acOwner1);

   // Drop TR, TRUSTEE and ET AL
   if ((pTmp=strstr(acOwner1, " SUCC CO")) ||
       (pTmp=strstr(acOwner1, " CO TR"))   || 
       (pTmp=strstr(acOwner1, " CO SUC"))  ||
       (pTmp=strstr(acOwner1, " SURV T"))  ||
       (pTmp=strstr(acOwner1, " MD TR"))   ||
       (pTmp=strstr(acOwner1, " SUCC T"))  ||
       (pTmp=strstr(acOwner1, " SUC TR")))
   {
      *pTmp = 0;
      bKeep = true;
   } else
   {
      pTmp = &acOwner1[strlen(acOwner1)-3];
      if (!memcmp(pTmp, " TR", 3))
         *pTmp = 0;
      else if (!memcmp(--pTmp, " CO ", 4) || (pTmp=strstr(acOwner1, " TR ")))
         *pTmp = 0;
   }

   if (pTmp = strstr(acOwner1, " TRUSTE"))
      *pTmp = 0;

   // If name starts with a digit, do not parse
   if (isdigit(acOwner1[0]))
   {
      strcpy(myOwners.acName1, acOwner1);
      strcpy(myOwners.acSwapName, acOwner1);
      strcpy(myOwners.acName2, acOwner2);
      return;
   }
   blankRem(acOwner1);

   // Check for trust
   if ((pTmp = strstr(acOwner1, " FAMILY")) ||
       (pTmp = strstr(acOwner1, " LIFE ESTATE")) ||
       (pTmp = strstr(acOwner1, " TRUST")) ||
       (pTmp = strstr(acOwner1, " EST OF"))
      )
   {        
      *pTmp = 0;
      bKeep = true;
   }

   // Now parse owners
   iTmp = splitOwner(acOwner1, &myOwners, 5);
   if (iTmp == -1)
   {
      strcpy(myOwners.acName1, acSave1);
      strcpy(myOwners.acSwapName, acSave1);
      bNoMerge = true;
   } else if (!strcmp(myOwners.acOL, myOwners.acSM) && !strcmp(myOwners.acOF, myOwners.acSL))
   {
      myOwners.acSF[0] = 0;
      myOwners.acSL[0] = 0;
      myOwners.acSM[0] = 0;
      myOwners.acName2[0] = 0;
   } else if (strchr(acOwner1, '/'))
   {
      strcpy(myOwners.acSwapName, acOwner1);
      bNoMerge = true;
   }

   if (acOwner2[0])
   {
      if (!bNoMerge)
      {
         // Parse name2 - if they have same last name, merge them
         splitOwner(acOwner2, &myOwner, 5);
         if (!strcmp(myOwners.acOL, myOwner.acOL))
         {
            // Ignore name2 if name1 and name2 have the same first and last name
            if (strcmp(myOwners.acOF, myOwner.acOF))
            {
               sprintf(acTmp, "%s %s & %s", myOwners.acOF, myOwners.acOM, myOwner.acSwapName);
               blankRem(acTmp);
               strcpy(myOwners.acSwapName, acTmp);

               // Merge name1 if not protected
               if (!bKeep)
               {
                  sprintf(acTmp, "%s & %s %s", myOwners.acName1, myOwner.acOF, myOwner.acOM);
                  blankRem(acTmp);
                  strcpy(myOwners.acName1, acTmp);
                  acSave1[0] = 0;
               } else
                  strcpy(myOwners.acName2, acSave2);
            }
         } else
         {
            strcpy(myOwners.acSF, myOwner.acOF);
            strcpy(myOwners.acSM, myOwner.acOM);
            strcpy(myOwners.acSL, myOwner.acOL);
            
            strcpy(myOwners.acName2, acSave2);
         }
      } else
         strcpy(myOwners.acName2, acSave2);
   }

   // Keep original name if no merge occured
   if (acSave1[0])
   {
      strcpy(myOwners.acName1, acSave1);
      if (strstr(acSave1, " A CA ") || strstr(acSave1, " UNTD "))
         strcpy(myOwners.acSwapName, acSave1);
   }
   if (acCareOf[0])
      strcpy(myOwners.acCareOf, acCareOf);
}

/******************************************************************************/

void Tul_MergeOwner(char *pName1)
{
   int   iTmp;
   char  acOwner1[128];
   char  acSave1[64];
   char  *pTmp;
   bool  bNoMerge=false, bKeep=false;

   memset(&myOwners, 0, sizeof(OWNER));


   // Process name1
   strcpy(acOwner1, pName1);
   //memcpy(acOwner1, pName1, RSIZ_NAME1);
   //acOwner1[RSIZ_NAME1] = 0;
   if (pTmp = strchr(acOwner1, '('))
      *pTmp = 0;
   strcpy(acSave1, acOwner1);

   if (!memcmp(acOwner1, "THE ", 4) && (pTmp=strstr(acOwner1, " TRUST")) )
   {
      bKeep = true;
   } else if ((pTmp=strstr(acOwner1, " FAMILY T"))  ||
              (pTmp=strstr(acOwner1, " REVOCABLE")) || 
              (pTmp=strstr(acOwner1, " TRUST")))
   {
      *pTmp = 0;
      bNoMerge = true;
   }

   blankRem(acOwner1);

   // Now parse owners
   if (bNoMerge)
      strcpy(myOwners.acSwapName, acOwner1);
   else if (bKeep)
      strcpy(myOwners.acSwapName, acSave1);
   else
   {
      iTmp = splitOwner(acOwner1, &myOwners, 5);
      if (iTmp == -1)
         strcpy(myOwners.acSwapName, acSave1);
   }

   // Keep original name if no merge occured
   strcpy(myOwners.acName1, acSave1);
}


/********************************************************************/

void Sbt_MergeOwner(char *pNames, OWNER *pOwners)
{
   int   iTmp, iRet, iParseType=3;
   char  acTmp1[128], acTmp[128], acSave1[64];
   char  acName1[64], *pTmp, *pTmp1, *pTmp2;
   bool  bKeep=false;
   OWNER myOwner;

   // Initialize
   memset((void *)pOwners, 0, sizeof(OWNER));

   // Remove multiple spaces
   pTmp = strcpy(acTmp, pNames);
   iTmp = 0;
   acSave1[0] = 0;

   if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Eliminate ETAL
   if ((pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL")) ||
       (pTmp=strstr(acTmp, " ETUX"))  || (pTmp=strstr(acTmp, " ET UX")) ||
       (pTmp=strstr(acTmp, " ETVIR")) || (pTmp=strstr(acTmp, " ET VIR")) )
      *++pTmp = 0;

   // Replace - with & and move it to name1.  If more than one found, drop
   // everything after the second one except when only one word goes before
   // the first '-'
   if (pTmp = strrchr(acTmp, '-'))
   {
      *pTmp++ = 0;

      // Remove TRUST in middle of name: 
      // BARBOSA RAYMOND A TRUST-VIRGINIA
      if ((pTmp1 = strstr(acTmp, "REV LIV TR")) || (pTmp1 = strstr(acTmp, "TRUST")) )
         *pTmp1 = 0;

      // Search for second '-'
      // FIGUEROA SILVINO-NATIVIDAD C-JOSIE C 
      // ANDERSON-PAPILLION NATHAN-GERLIE
      // HANETA ATAE-SUMIKO REV LIVE TR-SUMIKO
      if (pTmp2 = strchr(acTmp, '-'))
      {
         *pTmp2 = 0;       // Temporarily remove '-'
         if (pTmp1 = strchr(acTmp, ' '))
         {
            pTmp = pTmp2+1;
         } else
            *pTmp2 = '-';  // Put '-' back and keep it in name1
      }
      sprintf(acTmp1, "%s & %s ", acTmp, pTmp);
      strcpy(acTmp, acTmp1);
   }

   // Remove TRUSTEES
   if ((pTmp1=strstr(acTmp, " TRUSTEE")) || (pTmp1=strstr(acTmp, " TRS")) )
      *pTmp1 = 0;

   // Save Owner1
   strcpy(acSave1, acTmp);
   strcpy(acName1, acTmp);

   // Remove TRUST to format swapname
   if ((pTmp1=strstr(acName1, " REVOC "))    || 
       (pTmp1=strstr(acName1, " REVOCABLE")) ||
       (pTmp1=strstr(acName1, " REV "))      || 
       (pTmp1=strstr(acName1, " IRREV"))     ||
       (pTmp1=strstr(acName1, " FAMILY "))   ||
       (pTmp1=strstr(acName1, " FAM "))      ||
       (pTmp1=strstr(acName1, " LIVING "))   ||
       (pTmp1=strstr(acName1, " LIV "))      ||
       (pTmp1=strstr(acName1, " TESTAMENTARY")) || 
       (pTmp1=strstr(acName1, " INTER VIVOS"))  ||
       (pTmp1=strstr(acName1, " DECLARATION OF TRUST")) || 
       (pTmp1=strstr(acName1, " TRUST"))     ||
       (pTmp1=strstr(acName1, " TR ")) 
      )
   {
      pTmp = acName1;
      while (pTmp < pTmp1 && !isdigit(*(pTmp)))
         pTmp++;
      if (pTmp < pTmp1)
         *--pTmp = 0;
      else
         *pTmp1 = 0;
   } else if ((pTmp1=strstr(acName1, " LIFE EST")) ) 
       *pTmp1 = 0;

   // Do not parse if there is certain word in name
   if (strstr(acName1, " OF ")) 
       bKeep = true;;

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   blankRem(acName1);
   if (!bKeep && strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, iParseType);
      // If name is not swapable, use Name1 for it
      if (myOwner.acSwapName[0])
         strcpy(pOwners->acSwapName, myOwner.acSwapName);
      else
         strcpy(pOwners->acSwapName, acName1);

      strcpy(pOwners->acOF, myOwner.acOF);
      strcpy(pOwners->acOM, myOwner.acOM);
      strcpy(pOwners->acOL, myOwner.acOL);
      strcpy(pOwners->acTitle, myOwner.acTitle);
      strcpy(pOwners->acSF, myOwner.acSF);
      strcpy(pOwners->acSM, myOwner.acSM);
      strcpy(pOwners->acSL, myOwner.acSL);
   } else
      strcpy(pOwners->acSwapName, acSave1);

   strcpy(pOwners->acName1, acSave1);
}

