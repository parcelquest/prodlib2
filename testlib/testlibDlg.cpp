// testlibDlg.cpp : implementation file
//

#include "stdafx.h"
#include "testlib.h"
#include "Utils.h"
#include "testlibDlg.h"
#include "..\prodlib.h"
#include "tiff.h"

#define  MB_CHAR_FEE_PRCL             0
#define  MB_CHAR_POOLS                1
#define  MB_CHAR_USECAT               2
#define  MB_CHAR_QUALITY              3
#define  MB_CHAR_YRBLT                4
#define  MB_CHAR_BLDGSQFT             5
#define  MB_CHAR_GARSQFT              6
#define  MB_CHAR_HEATING              7
#define  MB_CHAR_COOLING              8
#define  MB_CHAR_HEATING_SRC          9
#define  MB_CHAR_COOLING_SRC          10
#define  MB_CHAR_BEDS                 11
#define  MB_CHAR_FBATHS               12
#define  MB_CHAR_HBATHS               13
#define  MB_CHAR_FP                   14
#define  MB_CHAR_ASMT                 15
#define  MB_CHAR_HASSEPTIC            16
#define  MB_CHAR_HASSEWER             17
#define  MB_CHAR_HASWELL              18

typedef struct _tCharRec
{
   char  Asmt[12];
   char  NumPools[2];
   char  LandUseCat[2];
   char  QualityClass[6];
   char  YearBuilt[4];
   char  BuildingSize[9];
   char  SqFTGarage[9];
   char  Heating[2];
   char  Cooling[2];
   char  HeatingSource[2];
   char  CoolingSource[2];
   char  NumBedrooms[3];
   char  NumFullBaths[3];
   char  NumHalfBaths[3];
   char  NumFireplaces[2];
   char  FeeParcel[12];
   char  HasSeptic;
   char  HasSewer;
   char  HasWell;
   char  CRLF[2];
} MB_CHAR;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int   iRecLen, iApnLen;
bool  bEnCode, bDebug, bUseSfxXlat;
long  lLastRecDate, lToday;
char  acRawTmpl[_MAX_PATH], acDocPath[_MAX_PATH];

/////////////////////////////////////////////////////////////////////////////
// CTestlibDlg dialog

CTestlibDlg::CTestlibDlg(CWnd* pParent /*=NULL*/)
   : CDialog(CTestlibDlg::IDD, pParent)
{
   //{{AFX_DATA_INIT(CTestlibDlg)
   m_strInput = _T("G:\\CO_DATA\\AMA\\AMA_Roll.txt");
   m_strOutput = _T("");
   m_iRecSize = 0;
   //}}AFX_DATA_INIT
   // Note that LoadIcon does not require a subsequent DestroyIcon in Win32
   m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestlibDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   //{{AFX_DATA_MAP(CTestlibDlg)
   DDX_Control(pDX, IDCANCEL, m_Cancel);
   DDX_Control(pDX, IDOK, m_OK);
   DDX_Text(pDX, IDC_EDITINPUT, m_strInput);
   DDX_Text(pDX, IDC_EDITOUTPUT, m_strOutput);
   DDX_Text(pDX, IDC_RECSIZE, m_iRecSize);
   //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTestlibDlg, CDialog)
   //{{AFX_MSG_MAP(CTestlibDlg)
   ON_WM_PAINT()
   ON_WM_QUERYDRAGICON()
   ON_BN_CLICKED(IDOK, OnRunTest)
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestlibDlg message handlers

BOOL CTestlibDlg::OnInitDialog()
{
   CDialog::OnInitDialog();

   // Set the icon for this dialog.  The framework does this automatically
   //  when the application's main window is not a dialog
   SetIcon(m_hIcon, TRUE);			// Set big icon
   SetIcon(m_hIcon, FALSE);		// Set small icon
   
   // TODO: Add extra initialization here
   
   return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestlibDlg::OnPaint() 
{
   if (IsIconic())
   {
      CPaintDC dc(this); // device context for painting

      SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

      // Center icon in client rectangle
      int cxIcon = GetSystemMetrics(SM_CXICON);
      int cyIcon = GetSystemMetrics(SM_CYICON);
      CRect rect;
      GetClientRect(&rect);
      int x = (rect.Width() - cxIcon + 1) / 2;
      int y = (rect.Height() - cyIcon + 1) / 2;

      // Draw the icon
      dc.DrawIcon(x, y, m_hIcon);
   }
   else
   {
      CDialog::OnPaint();
   }
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestlibDlg::OnQueryDragIcon()
{
   return (HCURSOR) m_hIcon;
}

/******************************* convertChar() ******************************
 *
 * Return 0 if successful.  Error otherwise.
 *
 ****************************************************************************/
char  *apTokens[64];
int   iTokens;

int convertChar(LPCSTR pInfile, LPCSTR pOutfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmp[128], *pRec;
   int      iRet, iTmp, iCnt=0;
   MB_CHAR  myCharRec;

   if (!(fdIn = fopen(pInfile, "r")))
      return -1;
   if (!(fdOut = fopen(pOutfile, "w")))
   {
      fclose(fdIn);
      //LogMsg("***** Error creating output file %s", pOutfile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (pRec)
         iRet = ParseStringNQ(pRec, ',', MB_CHAR_HASWELL+1, apTokens);
      else
         break;

      memset((void *)&myCharRec, ' ', sizeof(MB_CHAR));
      memcpy(myCharRec.Asmt, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));
      memcpy(myCharRec.LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));
      memcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));
      memcpy(myCharRec.YearBuilt, apTokens[MB_CHAR_YRBLT], strlen(apTokens[MB_CHAR_YRBLT]));
      memcpy(myCharRec.BuildingSize, apTokens[MB_CHAR_BLDGSQFT], strlen(apTokens[MB_CHAR_BLDGSQFT]));
      memcpy(myCharRec.SqFTGarage, apTokens[MB_CHAR_GARSQFT], strlen(apTokens[MB_CHAR_GARSQFT]));
      memcpy(myCharRec.Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
      memcpy(myCharRec.Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
      memcpy(myCharRec.HeatingSource, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
      memcpy(myCharRec.CoolingSource, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));

      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
      if (iTmp > 0)
         memcpy(myCharRec.NumPools, apTokens[MB_CHAR_POOLS], strlen(apTokens[MB_CHAR_POOLS]));

      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
         memcpy(myCharRec.NumBedrooms, apTokens[MB_CHAR_BEDS], strlen(apTokens[MB_CHAR_BEDS]));

      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
         memcpy(myCharRec.NumFullBaths, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));

      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
         memcpy(myCharRec.NumHalfBaths, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));

      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
         memcpy(myCharRec.NumFireplaces, apTokens[MB_CHAR_FP], strlen(apTokens[MB_CHAR_FP]));

      myCharRec.CRLF[0] = '\r';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Asmt[0], fdOut);

      iCnt++;
      //if (!(++iCnt % 1000))
      //   printf("\r%u", iCnt);

   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   return iRet;
}

long RemBytes(LPCSTR pInfile, LPCSTR pOutfile, char bChar, int iRecSize) 
{
   int      iTmp;
   long     lRet, lCnt=0;
   unsigned long   nBytesWritten, nBytesRead, iLen;
   bool     bRet;
   HANDLE   fhIn, fhOut;
   char     acBuf[8192];

   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return 3;

   // Open Output file
   fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   if (iRecSize > 0)
      iLen = iRecSize;
   else
      iLen = 4096;

   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] == bChar)
            acBuf[iTmp] = ' ';
         iTmp++;
      }
      
      bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      lCnt++;
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return lCnt;
}

int resetFldType(char *pFilename)
{
   int   iRet, iCnt;
   CDA_FLDDEF *pFldList;

   iRet = CddOpen(pFilename);
   if (iRet < 0)
      return iRet;

   pFldList = CddGetFldList();
   for (iCnt = 0; iCnt < CddNumFlds(); iCnt++)
   {
      if (pFldList->iType == 1)
         pFldList->iType = 0;
      if (pFldList->iID == 19 || pFldList->iID == 29 || pFldList->iID == 141)
         pFldList->iType = 1;
      pFldList++;
   }

   iRet = CddWriteFile();

   return iRet;
}

/********************************** atoin() *********************************

int atoin(LPCSTR pStr, int iLen)
{
   char  acVal[32];
   int   iRet;

   if (!pStr || !*pStr)
      return 0;

   memcpy(acVal, pStr, iLen);
   acVal[iLen] = '\0';
   iRet = atoi(acVal);
   return iRet;
}

bool isLeapYear(long year) 
{         
   if ( ((year % 4) == 0) &&                 
        (((year % 100) != 0) || ((year % 400) == 0))) 
   {                         
      return true;         
   } else 
   {                 
      return false;         
   } 
} 

// Convert CCYYDDD to YYYYMMDD
long JulianToGregorian(LPCSTR pDate)
{
   //static int startdays[] = {1,32,60,91,121,152,182,213,244,274,305,335};   
   static int monthdays[] = {31,28,31,30,31,30,31,31,30,31,30,31};   

   long  JDate;
   long  iMonth, iDay, iYear, lRet;

   JDate = atoin(pDate+4, 3);
   if (!JDate)
      return 0;

   iYear = atoin(pDate, 4);   
   for (iMonth=0, iDay=JDate; iMonth < 12; iMonth++)
   {
      if (iDay <= monthdays[iMonth])
         break;

      if (iMonth == 1 && isLeapYear(iYear))
      {
         if (iDay == monthdays[iMonth]+1)
            break;

         iDay--;
      }

      iDay -= monthdays[iMonth];
   }

   iMonth++;
   lRet = iYear*10000 + iMonth*100 + iDay;
   return lRet;
}

void getPrevMonth(char *pBuf, int iMonths)
{
   SYSTEMTIME  curSystemTime;

   GetLocalTime(&curSystemTime);

   if (iMonths > 11)
   {
      *pBuf = 0;
      return;
   }

   if (curSystemTime.wMonth <= iMonths)
   {
      curSystemTime.wMonth = (curSystemTime.wMonth+12)-iMonths;
      curSystemTime.wYear -= 1;
   } else
      curSystemTime.wMonth -= iMonths;

   // Calculate leap year
   if (curSystemTime.wMonth == 2 && curSystemTime.wDay > 28)
   {
      if (isLeapYear(curSystemTime.wYear))
         curSystemTime.wDay = 29;
      else
         curSystemTime.wDay = 28;
   }

   sprintf(pBuf, "%.4d%.2d%.2d", curSystemTime.wYear, curSystemTime.wMonth, curSystemTime.wDay);
}
*/
void CTestlibDlg::OnCancel() 
{
   CDialog::OnCancel();
}

void doTest(wchar_t *pszW, LPSTR pszA);
void CTestlibDlg::OnRunTest() 
{
   __int64  iTmp;
   MAPLINK  myMapLink[4];
   FILESTAT myStat;
   char     acTmp[128], acTmp1[256];
   char     acOF[32], acOL[32], acOM[32], acSF[32], acSM[32], acSL[32];
   long     lRet;
   int      iRet;

   UpdateData(true);

   m_OK.EnableWindow(false);
   m_Cancel.EnableWindow(false);

   //sprintf(acTmp, "H:\\CO_PROD\\_Dpf\\SAma.dpf");
   //iRet = resetFldType(m_strInput.GetBuffer(0));
   lRet = getFileDate(m_strInput, &acTmp[0]);
   m_strOutput = acTmp;

   //iTmp = ParseMapLink(&myMapLink[0], m_strInput.GetBuffer(0));
   //iTmp = fileStat(m_strInput, &myStat);
   //timeString(acTmp, myStat.ftCreationTime);
   //iTmp = doConvertEx(m_strInput, m_strOutput, m_iRecSize, 2, CONV_ADDCR|CONV_ADDLF, CONV_ASC2EBC);
   //iTmp = doCopyFixed(m_strInput, m_strOutput, m_iRecSize, 9);
   //iTmp = doSplit(m_strInput, m_strOutput, m_iRecSize, 1300000);
   //iTmp = doEBC2ASC(m_strInput, m_strOutput);
   //iTmp = doASC2EBC(m_strInput, m_strOutput);
   //iTmp = doConvert(m_strInput, m_strOutput, m_iRecSize, 0, 1300000, CONV_SPLIT);
   
   /* Sony's Encryption
   iTmp = SN_Encrypt(m_strInput.GetBuffer(0), m_strOutput.GetBuffer(256));
   memset(acTmp1, 0, 256);
   iTmp = SN_UnEncrypt(m_strOutput.GetBuffer(0), acTmp1);
   m_strOutput.ReleaseBuffer();
   */

   //char *pTest = "\"SLOC  \",\"CY SLO TR 1380 U \"E1 & 4.01% IN LOT 1             \",\"19870330\",\"19980101\"    ";   
   //strcpy(acTmp1, pTest);
   //iTmp = ParseStringNQ1(acTmp1, ',', 10, apTokens);

   //iTmp = PQCompress("SBT");
   //if (iTmp < 0)
   //   m_strOutput = "Test failed";
   //else
   //   m_strOutput = "Test passed";

   //lRet = RemBytes(m_strInput, m_strOutput, 0, m_iRecSize);

   // Test convertChar()
   //iTmp = convertChar(m_strInput, m_strOutput);

   // Test JulianToGregorian()
   //lRet = JulianToGregorian(m_strInput);
   //m_strOutput.Format("%ld", lRet);

   // Test getPrevMonth
   //getPrevMonth(acTmp, atoi(m_strInput));
   //m_strOutput = acTmp;

   // Test TiffLib
   //lRet = getTiffRes(m_strInput, 1);
   //m_iRecSize = lRet;


   // Test ansi to wide
   //wchar_t myTest[100];
   //char    *myAnsi = "This is an ansi test";
   /*
   // *myTest = 0;
   doTest(myTest, myAnsi);
   m_strInput = myTest;

   // Test wide to ansi
   iRet = 20;
   swprintf(myTest, L"This is a test of %d ansi chars in %S", iRet, myAnsi);
   m_strInput = myTest;
   acTmp[0] = 0;
   doTest(myTest, acTmp);
   m_strInput = acTmp;
   */
   UpdateData(false);

   m_OK.EnableWindow(true);
   m_Cancel.EnableWindow(true);
}

