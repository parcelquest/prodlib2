/****************************************************************************
 *
 *    TIFFIMAGE *TiffLoad()
 *       TIFFIMAGE *ReadTiffFile()     malloc
 *          TIFFIFD *ReadTiffIFH()     malloc
 *          TIFFIFD *ReadTiffIFD()     malloc
 *             TIFFTAG *ReadTiffTag()  malloc
 *                value                calloc
 *
 ****************************************************************************/

#include <stdafx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tiff.h"

short (*fgetshort)(FILE *);  /* Pointers to endian read functions */
long  (*fgetlong)(FILE *);   /* Declared extern in tifflook.c     */

/*****************************************************************************
 *
 *  Read a short (16-bit) value in little-endian order.
 *
 *  Parameter: fp - A file stream to read data.
 *
 *  Returns: A short word value in little-endian order.
 *
 *****************************************************************************/

short getlittleshort(FILE *fp)
{
   register short word;

   word  =  (fgetc(fp) & 0xff);
   word |= ((fgetc(fp) & 0xff) << 0x08);

   return(word);
}

/*
**  Read a short (16-bit) value in big-endian order.
**
**  Parameter: fp - A file stream to read data.
**
**  Returns: A short word value in big-endian order.
*/
short getbigshort(FILE *fp)
{
   register short word;

   word = (fgetc(fp) & 0xff);
   word = (fgetc(fp) & 0xff) | (word << 0x08);

   return(word);
}

/*
**  Read a long (32-bit) value in big-endian order.
**
**  Parameter: fp - A file stream to read data.
**
**  Returns: A long word value in big-endian order.
*/
long getbiglong(FILE *fp)
{
   register long word;

   word =  (long) (fgetc(fp) & 0xff);
   word = ((long) (fgetc(fp) & 0xff)) | (word << 0x08);
   word = ((long) (fgetc(fp) & 0xff)) | (word << 0x08);
   word = ((long) (fgetc(fp) & 0xff)) | (word << 0x08);

   return(word);
}

/*
**  Read a long (32-bit) value in little-endian order.
**
**  Parameter: fp - A file stream to read data.
**
**  Returns: A long word value in little-endian order.
*/
long getlittlelong(FILE *fp)
{
   register long word;

   word  =   (long) (fgetc(fp) & 0xff);
   word |= (((long) (fgetc(fp) & 0xff)) << 0x08);
   word |= (((long) (fgetc(fp) & 0xff)) << 0x10);
   word |= (((long) (fgetc(fp) & 0xff)) << 0x18);

   return(word);
}

/****************************************************************************
 *
 *  Open, read, load, and close a TIFF file.
 *
 *  Parameters: tifffile - Pointer to the name of a TIFF file.
 *
 *  Returns: A pointer to a valid TIFFIMAGE structure on success,
 *           otherwise NULL on failure.
 *
 ****************************************************************************/

TIFFIMAGE *TiffLoad(LPCSTR pTiffFile)
{
   FILE      *tifffp;          /* Pointer to the TIFF file */
   TIFFIMAGE *tiffimage;       /* The TIFF file structure */

   /* Open the TIFF file */
   if (!(tifffp = fopen(pTiffFile, "rb")))
      return((TIFFIMAGE *) NULL);

   /* Read the TIFF file into the TIFF structure */
   if (!(tiffimage = ReadTiffFile(tifffp, pTiffFile)))
   {
      fclose(tifffp);
      return((TIFFIMAGE *) NULL);
   }

   /* Close the TIFF image file */
   fclose(tifffp);

   return(tiffimage);
}

/*****************************************************************************
 *
 *  Load a TIFF file into a TIFFIMAGE structure.
 *
 *  Parameters: TiffFp    - an open file pointer to a TIFF image
 *              TiffImage - an unallocated TIFFIMAGE structure
 *              TiffName  - the name of the TIFF file
 *
 *  Returns: A pointer to a valid TIFFIMAGE structure on success,
 *           otherwise NULL on failure.
 *
 *****************************************************************************/

TIFFIMAGE *ReadTiffFile(FILE *TiffFp, LPCSTR TiffName)
{
   long offset;            /* Offset to next IFD */
   TIFFIMAGE *tiffimage;   /* TIFFIMAGE structure */
   TIFFIFD *ifd;           /* IFD structure */
   TIFFIFD *current_ifd;   /* Current node of linked list */
   TIFFIFD *head;          /* Head of linked list */

   /* Allocate TIFF structure */
   if (!(tiffimage = (TIFFIMAGE *) malloc(sizeof(TIFFIMAGE))))
      return((TIFFIMAGE *) NULL);

   /* Initialize TIFF structure */
   strncpy(tiffimage->TIFF_FileName, TiffName, 80);

   if (!(tiffimage->Header = ReadTiffIFH(TiffFp)))
      return((TIFFIMAGE *) NULL);

   /*
   ** Read and store the IFD information of a TIFF file.  If there
   ** are multiple IFD's (images) then they are linked together.
   */
   offset = tiffimage->Header->IFDOffset;  /* Offset of 1st IFD */
   tiffimage->TIFF_NumOfImages = 0;        /* Clear image counter */
   head = (TIFFIFD *) NULL;                /* Clear the head pointer */
   do
   {
      if (!(ifd = ReadTiffIFD(TiffFp, offset)))
         return((TIFFIMAGE *) NULL);

      if (head == (TIFFIFD *) NULL)
      {     /* Singly-linked list */
         head = ifd;
         current_ifd = head;
      } else
      {
         current_ifd->Next = ifd;
         ifd->Next = (TIFFIFD *) NULL;
         current_ifd = ifd;
      }
      offset = ifd->NextIFDOffset;      /* Get offset to next IFD */
      tiffimage->TIFF_NumOfImages++;    /* Increment image counter */
   } while (offset);

   tiffimage->IfdList = head;            /* Attach IFD list to structure */

   return(tiffimage);                    /* Return completed structure */
}

/*****************************************************************************
 *
 *  Read a TIFF Image File Header
 *
 *  Parameters: TiffFp - A pointer to the TIFF file.
 *
 *  Returns: A pointer to a valid IFH structure on success,
 *           otherwise NULL on failure.
 *
 *****************************************************************************/

TIFFIFH *ReadTiffIFH(FILE *TiffFp)
{
   TIFFIFH *ifh;       /* Image File Header structure */

   /* Allocate TIFF IFH structure */
   if (!(ifh = (TIFFIFH *) malloc(sizeof(TIFFIFH))))
      return((TIFFIFH *) NULL);

   /* Read the byte order from the TIFF file */
   ifh->ByteOrder  = (short) fgetc(TiffFp) & 0xff;
   ifh->ByteOrder += (short) (fgetc(TiffFp) & 0xff) << 8;

   /*
   ** Decide which file I/O functions to use.
   */
   switch (ifh->ByteOrder)
   {
      case TIFF_MOTOROLA:             /* Big-endian */
         fgetshort = getbigshort;
         fgetlong  = getbiglong;
         break;
      case TIFF_INTEL:                /* Little-endian */
         fgetshort = getlittleshort;
         fgetlong  = getlittlelong;
         break;
      default:
         return((TIFFIFH *) NULL);
   }

   /*
   ** Get the TIFF version number.
   */
   ifh->Version = fgetshort(TiffFp);
   if (ifh->Version != TIFF_VERSION)
      return((TIFFIFH *) NULL);

   /*
   ** Get the offset to the first IFD (if any).
   */
   ifh->IFDOffset = fgetlong(TiffFp);
   if (ifh->IFDOffset == 0L)
      return((TIFFIFH *) NULL);

   return(ifh);    /* Return a valid IFH pointer */
}

/*****************************************************************************
 *
 *  Read a TIFF Image File Directory.
 *
 *  An IFD is a singly-linked list of TIFF tags.
 *
 *  Parameters: TiffFp    - Pointer to TIFF file
 *              IfdOffset - Offset of IFD within the TIFF image file
 *
 *  Returns: A pointer to a valid TIFFIFD structure on success,
 *           otherwise NULL on failure.
 *
 *****************************************************************************/

TIFFIFD *ReadTiffIFD(FILE *TiffFp, long IfdOffset)
{
   short    tag_count;     /* Number of tags in an IFD */
   TIFFIFD *ifd;           /* Image File Directory structure */
   TIFFTAG *tag;           /* TIFF tag structure */
   TIFFTAG *head;          /* Head of linked list */
   TIFFTAG *current_tag;   /* Current node of linked list */

   /* Allocate TIFF IFD structure */
   if (!(ifd = (TIFFIFD *) malloc(sizeof(TIFFIFD))))
      return((TIFFIFD *) NULL);

   /* Seek to IFD */
   fseek(TiffFp, IfdOffset, SEEK_SET);

   /* Get the number of tags in the IFD */
   tag_count = ifd->NumDirEntries = fgetshort(TiffFp);

   /* Read each tag in the IFD */
   for (head = (TIFFTAG *) NULL; tag_count; tag_count--)
   {
      if (!(tag = ReadTiffTag(TiffFp)))
         return((TIFFIFD *) NULL);

      if (head == (TIFFTAG *) NULL)      /* Singly-linked list */
      {
         head = tag;
         current_tag = head;
      } else
      {
         current_tag->Next = tag;
         tag->Next = (TIFFTAG *) NULL;
         current_tag = tag;
      }
   }
   ifd->TagList = head;                    /* Attach tag list to IFD */
   ifd->NextIFDOffset = fgetlong(TiffFp);  /* Get offset to the next IFD */
   ifd->Next = (TIFFIFD *) NULL;

   return(ifd);                            /* Return a valid IFD pointer */
}

/*****************************************************************************
 *
 *  Read a tag from a TIFF file and return a tag structure.
 *
 *  Paramateres: TiffFp - A pointer to a TIFF file.
 *
 *  Returns: A valid tag structure on success, otherwise NULL.
 *
 *****************************************************************************/

TIFFTAG *ReadTiffTag(FILE *TiffFp)
{
   long      i;         /* Counter used to step through tag->TagLength */
   long      savepos;   /* Save the current FILE offset value */
   BYTE     *t_byte;    /* Index variable for byte-oriented data */
   ASCII    *t_ascii;   /* Index variable for ascii-oriented data */
   short    *t_short;   /* Index variable for short-oriented data */
   long     *t_long;    /* Index variable for long-oriented data */
   RATIONAL *num;       /* Index variable for rational-oriented data */
   RATIONAL *den;       /* Index variable for rational-oriented data */
   TIFFTAG  *tag;       /* The tag structure */

   /* Allocate TIFFTAG structure */
   if (!(tag = (TIFFTAG *) malloc(sizeof(TIFFTAG))))
      return((TIFFTAG *) NULL);

   /* Read the tag values from the TIFF file */
   tag->TagID       = fgetshort(TiffFp);
   tag->TagType     = fgetshort(TiffFp);
   tag->TagLength   = fgetlong(TiffFp);
   tag->ValueOffset = fgetlong(TiffFp);
   tag->Value       = (void *) NULL;

   /*
   ** Read the tag data value.
   **
   ** If the tag data is four bytes or less in length the
   ** tag->ValueOffset variable will contain the actual tag data.
   ** If the tag data is greater than four bytes in length, then
   ** tag->ValueOffset will contain a four-byte offset value to the
   ** location of the data within the TIFF file.  Note that data of
   ** type RATIONAL can never be found within a tag, as it takes
   ** 8 bytes to represent each RATIONAL value.
   */
   switch (tag->TagType)
   {
      case TYPE_BYTE:                 /* BYTE data */
         if (tag->TagLength < 5UL)
            return(tag);
         if (!(tag->Value = (BYTE *) calloc((size_t) tag->TagLength, sizeof(BYTE))))
            break;

         savepos = ftell(TiffFp);
         fseek(TiffFp, tag->ValueOffset, SEEK_SET);
         t_byte = (BYTE *) tag->Value;
         for (i = 0; i < tag->TagLength; i++)
            *t_byte++ = (BYTE) fgetc(TiffFp);
         fseek(TiffFp, savepos, SEEK_SET);
         break;
      case TYPE_ASCII:                /* ASCII data */
         if (tag->TagLength < 5UL)
            return(tag);
         if (!(tag->Value = (ASCII *) calloc((size_t) tag->TagLength + 1, sizeof(ASCII))))
            break;

         savepos = ftell(TiffFp);
         fseek(TiffFp, tag->ValueOffset, SEEK_SET);
         t_ascii = (ASCII *) tag->Value;
         for (i = 0; i <= tag->TagLength; i++)
            *t_ascii++ = (ASCII) fgetc(TiffFp);
         fseek(TiffFp, savepos, SEEK_SET);
         break;
      case TYPE_SHORT:                /* short data */
         if (tag->TagLength < 3UL)
            return(tag);
         if (!(tag->Value = (short *) calloc((size_t) tag->TagLength, sizeof(short))))
            break;

         savepos = ftell(TiffFp);
         fseek(TiffFp, tag->ValueOffset, SEEK_SET);
         t_short = (short *)tag->Value;
         for (i = 0; i < tag->TagLength; i++)
            *t_short++ = fgetshort(TiffFp);
         fseek(TiffFp, savepos, SEEK_SET);
         break;
      case TYPE_LONG:                 /* long data */
         if (tag->TagLength < 2UL)
            return(tag);
         if (!(tag->Value = (long *) calloc((size_t) tag->TagLength, sizeof(long))))
            break;

         savepos = ftell(TiffFp);
         fseek(TiffFp, tag->ValueOffset, SEEK_SET);
         t_long = (long *)tag->Value;
         for (i = 0; i < tag->TagLength; i++)
            *t_long++ = fgetlong(TiffFp);
         fseek(TiffFp, savepos, SEEK_SET);
         break;
      case TYPE_RATIONAL:             /* RATIONAL data */
         if (!(tag->Value = (RATIONAL *) calloc((size_t) tag->TagLength, sizeof(RATIONAL) * 2)))
            break;

         savepos = ftell(TiffFp);
         fseek(TiffFp, tag->ValueOffset, SEEK_SET);
         num = (RATIONAL *) tag->Value;
         den = (RATIONAL *) tag->Value;
         for (i = 0, den++; i < tag->TagLength; i++, num++, den++)
         {
            *num++ = fgetlong(TiffFp);
            *den++ = fgetlong(TiffFp);
         }
         fseek(TiffFp, savepos, SEEK_SET);
         break;
      default:
         return((TIFFTAG *) NULL);
   }
   return(tag);        /* Return a pointer to a valid tag structure */
}

/*****************************************************************************
 *
 *  Search tag value for a specified tiff and tag.  Search until tag found
 *  regardless of page.
 *
 *  Parameters: TiffFp    - Pointer to TIFF file
 *              IfdOffset - Offset of IFD within the TIFF image file
 *
 *  Returns: a long value if tag found.  -1 if not.
 *
 *****************************************************************************/

long lTiffSrch(TIFFIMAGE *image, int iTag)
{
   long      i, j;     /* Counters */
   long      offset;   /* Offset value holder */
   TIFFIFD  *ifd;      /* IFD structure */
   TIFFTAG  *tag;      /* Tag structure */
   long     lRet=-1;

   offset = image->Header->IFDOffset;
   ifd = image->IfdList;
   for (i = 1UL; i <= image->TIFF_NumOfImages && lRet < 0; i++)
   {
      tag = ifd->TagList;
      for (j = 1UL; j <= ifd->NumDirEntries; j++)
      {
         if (!tag->Value)
         {  /* Tag data is located within tag->Value */
            switch (tag->TagType)
            {
               case TYPE_SHORT:
                  if (image->Header->ByteOrder == TIFF_INTEL)
                  {
                     if (tag->TagLength > 1UL)
                        lRet = (tag->ValueOffset >> 16) & 0x0000ffff;
                     else
                        lRet = tag->ValueOffset & 0x0000ffff;
                  } else if (image->Header->ByteOrder == TIFF_MOTOROLA)
                  {
                     if (tag->TagLength > 1UL)
                        lRet = tag->ValueOffset & 0x0000ffff;
                     else
                        lRet =  (tag->ValueOffset >> 16) & 0x0000ffff;
                  }
                  break;
               case TYPE_LONG:
                  lRet =  tag->ValueOffset;
                  break;
               default:
                  break;
            }
         } else
         {  /* Tag data is located at an offset value */
            switch (tag->TagType)
            {
               case TYPE_SHORT:
               {
                  short *t_short = (short *) tag->Value;
                  lRet = *t_short;
                  break;
               }
               case TYPE_LONG:
               {
                  long *t_long = (long *) tag->Value;
                  lRet = *t_long;
                  break;
               }
               case TYPE_RATIONAL:
               {
                  RATIONAL *num = (RATIONAL *) tag->Value;
                  RATIONAL *den = (RATIONAL *) tag->Value;
                  lRet = *num;
                  break;
               }
               default:
                  break;
            }
         }

         // If tag found, exit
         if (iTag == tag->TagID && lRet >= 0)
            break;
         tag = tag->Next;     /* Get the next tag in the linked list */
      }
      ifd = ifd->Next;        /* Get the next IFD in the linked list */
      offset = image->IfdList->NextIFDOffset;
   }
   return lRet;
}

void remIfd(TIFFIFD  *ifd)
{
   TIFFTAG  *tag, *tagNext;

   tag = ifd->TagList;
   while (tag)
   {
      tagNext = tag->Next;
      if (tag->Value)
         free(tag->Value);
      free(tag);
      tag = tagNext;
   }
}

void remTiffImage(TIFFIMAGE *tiffimage)
{
   TIFFIFD  *ifd, *ifdNext;

   ifd = tiffimage->IfdList;
   while (ifd)
   {
      ifdNext = ifd->Next;
      remIfd(ifd);
      free(ifd);
      ifd = ifdNext;
   }

   free(tiffimage->Header);
   free(tiffimage);
}

int getTiffRes(LPCSTR pFilename, int iTag)
{
   TIFFIMAGE *tiffimage;
   long lTmp;

   /* Read a TIFF file, load the image, and look at it */
   if (tiffimage = TiffLoad(pFilename))
   {
      lTmp = lTiffSrch(tiffimage, T_XRESOLUTION);

      // Remove allocated memory
      remTiffImage(tiffimage);
   } else
      return(-2);

   return(lTmp);
}

