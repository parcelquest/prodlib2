#if !defined(AFX_TESTLIBDLG_H__0C17FD8D_2229_11D5_9E77_0060971C5823__INCLUDED_)
#define AFX_TESTLIBDLG_H__0C17FD8D_2229_11D5_9E77_0060971C5823__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define  OFF_M_STRNUM          321-1
#define  OFF_M_STR_SUB         328-1
#define  OFF_M_DIR             331-1
#define  OFF_M_STREET          333-1
#define  OFF_M_SUFF            357-1
#define  OFF_M_UNITNO          362-1
#define  OFF_M_CITY            368-1
#define  OFF_M_ST              385-1
#define  OFF_M_ZIP             387-1
#define  OFF_M_ZIP4            392-1
#define  OFF_TRACT             396-1

/////////////////////////////////////////////////////////////////////////////
// CTestlibDlg dialog

class CTestlibDlg : public CDialog
{
// Construction
public:
	CTestlibDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CTestlibDlg)
	enum { IDD = IDD_TESTLIB_DIALOG };
	CButton	m_Cancel;
	CButton	m_OK;
	CString	m_strInput;
	CString	m_strOutput;
	int		m_iRecSize;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestlibDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CTestlibDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnRunTest();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTLIBDLG_H__0C17FD8D_2229_11D5_9E77_0060971C5823__INCLUDED_)
