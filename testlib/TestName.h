#if !defined(AFX_TESTNAME_H__EBDDD159_0674_4B38_ACE5_C67094BD1548__INCLUDED_)
#define AFX_TESTNAME_H__EBDDD159_0674_4B38_ACE5_C67094BD1548__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestName.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestName dialog

typedef struct _tSdxName
{
   char  M_Flag[2];
   char  O_Flag[2];
   char  Name[96];
} SDX_NAME;

#define  RSIZ_MARITAL_STATUS            2
#define  RSIZ_OWNER_STATUS              2
#define  RSIZ_OWNER_NAME               96
typedef struct _tSdxRoll
{
   char  FractIntCode;
   char  Matital_Status[2];
   char  Owner_Status[2];
   char  OwnerName[96];
} SDX_ROLL;

class CTestName : public CDialog
{
// Construction
public:
	CTestName(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTestName)
	enum { IDD = IDD_TESTNAME };
	CString	m_strFName1;
	CString	m_strFName2;
	CString	m_strInput;
	CString	m_strLName1;
	CString	m_strLName2;
	CString	m_strMName1;
	CString	m_strMName2;
	CString	m_strSwapName;
	CString	m_strOwners;
	CString	m_strOwner2;
	CString	m_strCareOf;
	CString	m_strVesting;
	CString	m_strInput2;
	CString	m_strInput3;
	CString	m_strInput4;
	CString	m_strTitle;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestName)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
   int   m_iListCount, m_iCurItem;

protected:

	// Generated message map functions
	//{{AFX_MSG(CTestName)
	afx_msg void OnTestName();
	virtual void OnCancel();
	afx_msg void OnPreviousItem();
	afx_msg void OnNextitem();
	afx_msg void OnFirstitem();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTNAME_H__EBDDD159_0674_4B38_ACE5_C67094BD1548__INCLUDED_)
