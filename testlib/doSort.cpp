#include "stdafx.h"
#include <otsortw.h>
#include "Logs.h"

/******************************* DispOTSortMsg *******************************
 *
 * Display known error message
 *
 *****************************************************************************/

void DispOTSortMsg(int iErr)
{
   CString sMsg;

   int iCase = iErr / 100;
   switch(iCase)
   {
      case 210:
         sMsg = "Error creating output file.";
         break;
      case 211:
         sMsg = "Output filename invalid.";
         break;
      case 213:
         sMsg = "Input filename missing.";
         break;
      case 214:
         sMsg = "Error opening input file.";
         break;
      case 216:
         sMsg = "Error reading input file.";
         break;
      case 225:
         sMsg = "Insufficient dynamic workarea.";
         break;
      case 235:
         sMsg = "Control File not found.";
         break;
      case 245:
         sMsg = "End of record not found in sort buffer.";
         break;
      case 256:
         sMsg = "Dup area not big enough. Increase dup buffer to fix it";
         break;
      default:
         sMsg = "Unknown sort error.  Please contact programmer (or Sony)";
   }

   sMsg = "***** " + sMsg;
   LogMsg(sMsg);
}

/********************************** sortFile *********************************
 *
 * Sort file1 into file2
 * Return >0 if successful
 *
 *****************************************************************************/

long sortFile(char *pFile1, char *pFile2, char *pCmd, int *iErrNum, char *pWorkDrive)
{
   char  sWorkDrive[_MAX_PATH], sCmd[256], sTmpPath[_MAX_PATH];
   int   iRet;
   long  lNumRecs;

   if (pWorkDrive && *pWorkDrive > ' ')
   {
      strcpy(sWorkDrive, pWorkDrive);
      iRet = strlen(sWorkDrive);
   } else
      iRet = GetTempPath(_MAX_PATH, sWorkDrive);

   if (iRet > 1)
   {
      if (iRet > 2 && sWorkDrive[iRet-1] != '\\')
         sprintf(sCmd, "%s WORKDRIVE(%s\\)", pCmd, sWorkDrive);
      else
         sprintf(sCmd, "%s WORKDRIVE(%s)", pCmd, sWorkDrive);
   } else
      strcpy(sCmd, pCmd);

   // Begin Sort - put file name in quote in case it has embebded space
   LogMsg("Sorting %s to %s (%s)", pFile1, pFile2, sCmd);
   if (*pFile1 == 34 || !strchr(pFile1, ' '))
      strcpy(sTmpPath, pFile1);
   else
      sprintf(sTmpPath, "\"%s\"", pFile1);

   s_1mains(sTmpPath, pFile2, sCmd, &lNumRecs, &iRet);
   if (iRet > 1)
   {
      DispOTSortMsg(iRet);
      LogMsg("***** SORTING FILE ERROR!! %s (%d)", pFile1, iRet);
      lNumRecs = 0;
   } else
      LogMsg("Sorting complete.  Number of output records: %d", lNumRecs);

   if (iErrNum)
      *iErrNum = iRet;

   return lNumRecs;
}

