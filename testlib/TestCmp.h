#if !defined(AFX_TESTCMP_H__04635402_D719_4009_9157_DAB6CFAE792F__INCLUDED_)
#define AFX_TESTCMP_H__04635402_D719_4009_9157_DAB6CFAE792F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestCmp.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// TestCmp dialog

class TestCmp : public CDialog
{
// Construction
public:
	TestCmp(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(TestCmp)
	enum { IDD = IDD_TESTCMP };
	CButton	m_btnLoadCmp;
	CButton	m_btnGetRec;
	CString	m_sCmpFile;
	CString	m_sRec;
	long	m_lRecNum;
	//}}AFX_DATA

   HANDLE   m_hCmp, m_hMst;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TestCmp)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(TestCmp)
	afx_msg void OnTestCmp();
	virtual void OnCancel();
	afx_msg void OnLoadcmp();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTCMP_H__04635402_D719_4009_9157_DAB6CFAE792F__INCLUDED_)
