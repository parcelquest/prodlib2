// TestCmp.cpp : implementation file
//

#include "stdafx.h"
#include "testlib.h"
#include "TestCmp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TestCmp dialog


TestCmp::TestCmp(CWnd* pParent /*=NULL*/)
	: CDialog(TestCmp::IDD, pParent)
{
	//{{AFX_DATA_INIT(TestCmp)
	m_sCmpFile = _T("H:\\CO_PROD\\LAX_CD\\LAXassr\\LAX.cmp");
	m_sRec = _T("");
	m_lRecNum = 1;
	//}}AFX_DATA_INIT
}


void TestCmp::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(TestCmp)
	DDX_Control(pDX, IDC_LOADCMP, m_btnLoadCmp);
	DDX_Control(pDX, IDOK, m_btnGetRec);
	DDX_Text(pDX, IDC_CMPFILE, m_sCmpFile);
	DDX_Text(pDX, IDC_CMPREC, m_sRec);
	DDX_Text(pDX, IDC_RECNUM, m_lRecNum);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(TestCmp, CDialog)
	//{{AFX_MSG_MAP(TestCmp)
	ON_BN_CLICKED(IDOK, OnTestCmp)
	ON_BN_CLICKED(IDC_LOADCMP, OnLoadcmp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TestCmp message handlers

void TestCmp::OnTestCmp() 
{
   char  acBuf[4096];
	long  lOffset, lSize, lStart, lEnd;
   DWORD nBytesRead;
   bool  bRet;

   UpdateData(true);

   if (m_lRecNum < 1)
   {
      AfxMessageBox("Please enter record number to locate");
      return;
   }

   lOffset = (m_lRecNum-1)*4;

   SetFilePointer(m_hMst, lOffset, NULL, FILE_BEGIN);
   bRet = ReadFile(m_hMst, &lStart, 4, &nBytesRead, NULL);
   bRet = ReadFile(m_hMst, &lEnd, 4, &nBytesRead, NULL);
   lSize = lEnd - lStart;

   lStart--;
   SetFilePointer(m_hCmp, lStart, NULL, FILE_BEGIN);
   bRet = ReadFile(m_hCmp, acBuf, lSize, &nBytesRead, NULL);
   acBuf[lSize] = 0;
   m_sRec = acBuf;

   UpdateData(false);
}

void TestCmp::OnCancel() 
{
	if (m_hCmp)
      CloseHandle(m_hCmp);
	if (m_hMst)
      CloseHandle(m_hMst);
	
	CDialog::OnCancel();
}

void TestCmp::OnLoadcmp() 
{
	CString sMstFile;
   char    acBuf[512];

   UpdateData(true);
   sMstFile = m_sCmpFile;
   sMstFile.Replace(".cmp", ".mst");

   // Open files
   m_hCmp = CreateFile(m_sCmpFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (m_hCmp == INVALID_HANDLE_VALUE)
   {
      sprintf(acBuf, "Invalid input file: %s.  Please check again!", m_sCmpFile);
      AfxMessageBox(acBuf);
      return;
   }

   m_hMst = CreateFile(sMstFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (m_hMst == INVALID_HANDLE_VALUE)
   {
      sprintf(acBuf, "Invalid input file: %s.  Please check again!", sMstFile);
      AfxMessageBox(acBuf);
      return;
   }

   m_btnGetRec.EnableWindow(true);
   m_btnLoadCmp.EnableWindow(false);
}
