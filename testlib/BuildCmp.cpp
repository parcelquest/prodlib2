#include "stdafx.h"
#include "testlib.h"
#include "..\Prodlib.h"
#include "BuildCmp.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CDA_FLDDEF  m_asFld[256];
int         m_iFileCnt, m_iNumFlds;
bool        m_bRunning, m_bStop;
HWND        m_hStopCmd;

int __stdcall loadDpf(LPCSTR lpFilename, CDA_FLDDEF *pFld, int *piRecLen, int iMaxFlds);

/////////////////////////////////////////////////////////////////////////////
// BuildCmp dialog


BuildCmp::BuildCmp(CWnd* pParent /*=NULL*/)
   : CDialog(BuildCmp::IDD, pParent)
{
   //{{AFX_DATA_INIT(BuildCmp)
   m_strCntyCode = _T("SSBT");
   m_strStatus = _T("");
   m_strWorkDir = _T("F:\\CO_TEMP");
   m_iRecLen = 1934;
   m_strInfileExt = _T("G01");
   m_bSkipFirstRec = FALSE;
   m_bStopable = FALSE;
   //}}AFX_DATA_INIT

   m_bRunning = false;
   m_bStop = false;
}


void BuildCmp::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   //{{AFX_DATA_MAP(BuildCmp)
   DDX_Control(pDX, IDCANCEL, m_cmdExit);
   DDX_Control(pDX, IDOK, m_cmdStart);
   DDX_Text(pDX, IDC_CNTYCODE, m_strCntyCode);
   DDX_Text(pDX, IDC_STATUS, m_strStatus);
   DDX_Text(pDX, IDC_WORKDIR, m_strWorkDir);
   DDX_Text(pDX, IDC_RECLEN, m_iRecLen);
   DDX_Text(pDX, IDC_INFILEXT, m_strInfileExt);
   DDX_Check(pDX, IDC_SKIPREC, m_bSkipFirstRec);
   DDX_Check(pDX, IDC_ENABLESTOP, m_bStopable);
   //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BuildCmp, CDialog)
   //{{AFX_MSG_MAP(BuildCmp)
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BuildCmp message handlers

/******************************************************************************
 *
 ******************************************************************************/

int WaitForMsg(DWORD WAIT/*=2500L*/)
{                    
   int iRet = 0;
   DWORD StartTime;
   MSG   Msg;
   
   if (m_hStopCmd > 0)
   {
      StartTime = GetCurrentTime();    
      
      while (GetCurrentTime()-StartTime <= WAIT )  
      {
         if (PeekMessage(&Msg, m_hStopCmd, 0, 0, PM_REMOVE))
         {
            // Dispatch mesage
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
            
            // See if message belongs to 'allowed' window or a child of it
            if (m_bStop == TRUE)
            {
               iRet = 1;
               break;
            }    
         }        
      }
   } 
   return iRet;
}
       
/******************************************************************************
 *
 ******************************************************************************/

void doFld(char *pString)
{
   int   i, iTmp;
   char  acTmp[512], *pTmp;

   // Remove leading space
   pTmp = pString;
   while (*pTmp == ' ')
      pTmp++;


   strcpy(acTmp, pTmp);
   iTmp = strlen(acTmp);

   // remove trailing spaces and tabs and other non-printables
   i = iTmp-1;
   while (i >= 0)
   {
      if ((acTmp[i] > ' ') && (acTmp[i] <= '~'))
         break;
      acTmp[i] = 0;
      i--;
   }
   acTmp[i+1] = 31;
   acTmp[i+2] = 0;

   strcpy(pString, acTmp);
}

/******************************************************************************
 *
 ******************************************************************************/

void doRec(char *pInbuf, char *pOutbuf)
{
   int   iCnt;
   char  *pTmp, acTmp[512];

   *pOutbuf = 0;
   pTmp = pOutbuf;

   for (iCnt=0; iCnt < m_iNumFlds; iCnt++)
   {
      memcpy(acTmp, pInbuf+m_asFld[iCnt].iOffset, m_asFld[iCnt].iLength);
      acTmp[m_asFld[iCnt].iLength] = 0;
      doFld(acTmp);
      strcat(pTmp, acTmp);
   }
   strcat(pTmp, "\n");
}

/******************************************************************************
 *
 * Return number of output records
 *
 ******************************************************************************/

int __stdcall bldCmp(LPCSTR pCnty, LPCSTR pOutPath, LPCSTR pOutExt, int iRecSize, bool bSkip)
{
   HANDLE   fhIn;
   FILE     *fdOut, *fdMst;
   DWORD    nBytesRead;
   char     acRawFile[_MAX_PATH], acCmpFile[_MAX_PATH], acMstFile[_MAX_PATH];
   char     acOutRec[4096], acBuf[4096];
   int      iCnt, lRecCnt;
   long     lPos;

   iCnt = 1;
   sprintf(acRawFile, "%s\\%s.%s", pOutPath, pCnty, pOutExt);
   sprintf(acCmpFile, "%s\\%s.cmp", pOutPath, pCnty);
   sprintf(acMstFile, "%s\\%s.mst", pOutPath, pCnty);

   // Open files
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
      return -1;

   if (!(fdOut = fopen(acCmpFile, "w")))
   {
      CloseHandle(fhIn);
      return -2;
   }

   if (!(fdMst = fopen(acMstFile, "wb")))
   {
      CloseHandle(fhIn);
      fclose(fdOut);
      return -3;
   }

   // Skip first record
   if (bSkip)
      ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL);

   lRecCnt=0;
   while (ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // Reaching EOF, set up new file if available
         if (m_iFileCnt >= iCnt)
         {
            iCnt++;
            acRawFile[strlen(acRawFile)-1] = iCnt | 0x30;
            if (_access(acRawFile, 0))
               break;

            CloseHandle(fhIn);
            // Open files
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                   FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }
            ReadFile(fhIn, acBuf, iRecSize, &nBytesRead, NULL);
         } else
         {
            // No more file, get out
            break;
         }

      }

      doRec(acBuf, acOutRec);

      lPos = ftell(fdOut)+1;
      fwrite(&lPos, 1, sizeof(long), fdMst);
      fputs(acOutRec, fdOut);
      ++lRecCnt;

      if (WaitForMsg(1))
         break;
   }

   if (fhIn)
      CloseHandle(fhIn);

   fclose(fdOut);
   fclose(fdMst);

   return lRecCnt;
}

/******************************************************************************
 *
 ******************************************************************************/

int __stdcall loadDpf(LPCSTR lpFilename, CDA_FLDDEF *pFlds, int *piRecLen, int iMaxFlds)
{
   int   iRet, iCnt, iTmp, iNumFlds, iRecLen;
   FILE *fdRecDef;
   char  szBuf[256], *pTmp;
   char  *pszFields[NUMELEM];
   
   CDA_FLDDEF *pFld = pFlds;

   iRet = iRecLen = 0;
   iNumFlds = GetPrivateProfileInt("Data", "DataFlds", 0, lpFilename);
   if (!iNumFlds)
      iNumFlds = GetPrivateProfileInt("Data", "RecordNumber", 0, lpFilename);

   if (iNumFlds > 0 && iNumFlds < iMaxFlds)
   {
      fdRecDef = fopen(lpFilename, "r");
      if (!fdRecDef)
         return -1;

      do {
         pTmp = fgets(szBuf, 256, fdRecDef);
      } while (pTmp && _memicmp(pTmp, "[RecordDef]", 11));

      if (!pTmp)
      {
         fclose(fdRecDef);
         return -2;
      }

      for (iCnt = 0; iCnt < iNumFlds; iCnt++)
      {
         pTmp = fgets(szBuf, 256, fdRecDef);
         pTmp = strchr(szBuf, '=');
         if (!pTmp)
            continue;

         pTmp++;
         iTmp = ParseString(pTmp, ';', NUMELEM, pszFields);
         memset(pFld, 0, sizeof(CDA_FLDDEF));

         if (iTmp == NUMELEM)
         {
            iRecLen += pFld->iLength;
            pFld->iID = atoi(pszFields[FLD_ID]);
            strcpy(pFld->acTag, pszFields[FLD_TAG]);
            pFld->iType = atoi(pszFields[FLD_TYPE]);
            pFld->iOffset = atoi(pszFields[FLD_OFFSET])-1;
            pFld->iLength = atoi(pszFields[FLD_LENGTH]);
            pFld->bIndexed = atoi(pszFields[FLD_INDEXED]);
            pFld->bSorted = atoi(pszFields[FLD_SORTED]);
            pFld->iRecordArrayLength = atoi(pszFields[FLD_ARRAYLEN]);
            pFld->lTerms = atol(pszFields[FLD_TERMS]);
            pFld->lUniqueTerms = atol(pszFields[FLD_UTERMS]);
            pFld->bGroup = atoi(pszFields[FLD_GROUP]);
            pFld->bIndex = atoi(pszFields[FLD_INDEX]);
            pFld->bDisplay = atoi(pszFields[FLD_DISPLAY]);
            if (pszFields[FLD_TERMFILE])
               strcpy(pFld->acTermFile, pszFields[FLD_TERMFILE]);
            if (pszFields[FLD_NDXFILE])
               strcpy(pFld->acIndexFile, pszFields[FLD_NDXFILE]);
            if (pszFields[FLD_RECFILE])
               strcpy(pFld->acRecordFile, pszFields[FLD_RECFILE]);
            if (pszFields[FLD_WORDFILE])
               strcpy(pFld->acWordFile, pszFields[FLD_WORDFILE]);
            if (pszFields[FLD_MEMBEROF])
            {
               myTrim(pszFields[FLD_MEMBEROF]);
               strcpy(pFld->acMemberOf, pszFields[FLD_MEMBEROF]);
            }

            iRecLen += pFld->iLength;
         } else
         {
            iRet = -3;
            break;
         }

         pFld++;

      }
      fclose(fdRecDef);
   } else if (iNumFlds > iMaxFlds)
      iRet = -99;
   else
      iRet = -98;

   if (!iRet)
   {
      iRet = iNumFlds;
      *piRecLen = iRecLen;
   }

   return iRet;
}

/******************************************************************************
 *
 ******************************************************************************/

void BuildCmp::OnOK() 
{
   CString  strFilename;
   int      iRecLen, iRet, lTime;

   UpdateData(true);

   if (m_strCntyCode.IsEmpty())
   {
      MessageBox("Please enter county code!");
      return;
   }

   if (_access(m_strWorkDir, 0))
   {
      MessageBox("Please enter a valid working directory!");
      return;
   }

   // Load PDF file
   strFilename = m_strWorkDir + "\\" + m_strCntyCode + ".dpf";
   if (_access(strFilename, 0))
   {
      MessageBox("DPF file missing! " + strFilename);
      return;
   }

   m_iFileCnt = GetPrivateProfileInt("Data", "RawFileCount", 0, strFilename);
   if (!m_iRecLen)
      m_iRecLen = GetPrivateProfileInt("Data", "RecordLength", 0, strFilename);

   m_iNumFlds = loadDpf(strFilename, &m_asFld[0], &iRecLen, 256);
   if (m_iNumFlds < 10)
   {
      MessageBox("Error loading DPF file.  Please check!");
      return;
   }

   m_cmdStart.EnableWindow(false);
   m_bRunning = true;

   time_t   start, finish;
   time(&start);

   if (m_bStopable)
      m_hStopCmd = m_cmdExit.m_hWnd;
   else
      m_hStopCmd = 0;
   m_bStop = false;

   // Do compress
   iRet = bldCmp(m_strCntyCode, m_strWorkDir, m_strInfileExt, m_iRecLen, m_bSkipFirstRec);

   time(&finish);

   lTime = (int)difftime(finish, start);

   m_cmdStart.EnableWindow(true);
   m_bRunning = false;

   m_strStatus.Format("%d records output in %d minutes %d seconds", iRet, lTime/60, (lTime % 60));

   // Clean up
   UpdateData(false);
}

/******************************************************************************
 *
 ******************************************************************************/

void BuildCmp::OnCancel() 
{
   if (!m_bRunning)	
      CDialog::OnCancel();
   else
      m_bStop = true;
   m_bRunning = false;
}

