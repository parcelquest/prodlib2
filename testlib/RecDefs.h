#ifndef _RECDEFS_F
#define _RECDEFS_F
#define  MAX_COLS    4
#define  MAX_FLDS    512

typedef struct _tRecDefs
{
   char  acFldName[20];
   char  acFldType[4];     // B=binary, C=Asc, E=Ebc, P=pack dec, N=numeric
   int   iFldLen;
   int   iOffset;
} RECDEFS;

int   LoadDefFile(LPCSTR lpFilename);
void  *getOffsetTbl(void);
#endif