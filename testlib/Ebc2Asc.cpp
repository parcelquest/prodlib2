// Ebc2Asc.cpp : implementation file
//

#include "stdafx.h"
#include "testlib.h"
#include "..\prodlib.h"
#include "LoadDef.h"
#include "Ebc2Asc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern int     iERecSize;
extern int     iNumFlds;
extern RECINFO asInRec[];

/////////////////////////////////////////////////////////////////////////////
// Ebc2Asc dialog


Ebc2Asc::Ebc2Asc(CWnd* pParent /*=NULL*/)
   : CDialog(Ebc2Asc::IDD, pParent)
{
   //{{AFX_DATA_INIT(Ebc2Asc)
   m_strInfile = _T("G:\\CO_DATA\\TUL\\Tul_Lien");
   m_strOutfile = _T("G:\\CO_DATA\\TUL\\Tul_Lien.asc");
   m_iRecLen = 0;
   m_bAddCrLf = TRUE;
   m_strDefFile = _T("C:\\Tools\\Common\\Tul_ERoll.csv");
   //}}AFX_DATA_INIT

   char  acTmp[256], *pTmp;
   long  lTmp;

   pTmp = _getcwd(acTmp, 256);
   m_strIni = acTmp;
   m_strIni += "\\ebc2asc.ini";
   lTmp = GetPrivateProfileString("Default", "DefFile", "", acTmp, _MAX_PATH, m_strIni);
   if (lTmp > 0)
      m_strDefFile = acTmp;
   lTmp = GetPrivateProfileString("Default", "InFile", "", acTmp, _MAX_PATH, m_strIni);
   if (lTmp > 0)
      m_strInfile = acTmp;
   lTmp = GetPrivateProfileString("Default", "OutFile", "", acTmp, _MAX_PATH, m_strIni);
   if (lTmp > 0)
      m_strOutfile = acTmp;
   lTmp = GetPrivateProfileString("Default", "Reclen", "", acTmp, _MAX_PATH, m_strIni);
   if (lTmp > 0)
      m_iRecLen = atoi(acTmp);
}


void Ebc2Asc::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   //{{AFX_DATA_MAP(Ebc2Asc)
   DDX_Control(pDX, IDCANCEL, m_Cancel);
   DDX_Control(pDX, IDOK, m_OK);
   DDX_Text(pDX, IDC_INPUTFILE, m_strInfile);
   DDX_Text(pDX, IDC_OUTPUTFILE, m_strOutfile);
   DDX_Text(pDX, IDC_RECLEN, m_iRecLen);
   DDX_Check(pDX, IDC_ADDCRLF, m_bAddCrLf);
   DDX_Text(pDX, IDC_DEFFILE, m_strDefFile);
   //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Ebc2Asc, CDialog)
   //{{AFX_MSG_MAP(Ebc2Asc)
   ON_BN_CLICKED(IDCANCEL, OnExit)
   ON_BN_CLICKED(IDOK, OnConvert)
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Ebc2Asc message handlers

void Ebc2Asc::OnExit() 
{
   WritePrivateProfileString("Default", "DefFile", m_strDefFile, m_strIni);
   WritePrivateProfileString("Default", "InFile", m_strInfile, m_strIni);
   WritePrivateProfileString("Default", "OutFile", m_strOutfile, m_strIni);
   if (m_iRecLen > 0)
   {
      char acTmp[32];
      sprintf(acTmp, "%d", m_iRecLen);
      WritePrivateProfileString("Default", "Reclen", acTmp, m_strIni);
   }

   CDialog::OnCancel();
}

int Ebc2Asc::F_Ebc2Asc(LPCSTR strInfile, LPCSTR strOutfile, LPCSTR strDeffile, int iRecLen)
{
   int    iRet;
   bool   bRet, bEof;
   char   acInbuf[4096], acOutbuf[4096];
   DWORD  nBytesRead;
   DWORD  nBytesWritten;
   long   lCnt, iReadLen;

   HANDLE fhIn, fhOut;

   // Load definition file
   iRet = LoadDefFile(strDeffile);
   if (iRet <1)
      return -1;

   // Set record size
   if (!iRecLen)
      iReadLen = iERecSize;
   else
      iReadLen = iRecLen;

   // Open Input file
   //LogMsg("Open input file %s", strInfile);
   fhIn = CreateFile(strInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return 3;

   // Open Output file
   //LogMsg("Open output file %s", strOutfile);
   fhOut = CreateFile(strOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   
   // Loop through every record and convert
   bEof = false;

   // Merge loop
   lCnt = 0;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acInbuf, iReadLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
         break;

      if (!nBytesRead)
         break;

      // Convert data
      iRet = R_Ebc2Asc(acInbuf, acOutbuf);
      if (m_bAddCrLf)
         strcat(acOutbuf, "\r\n");
      iRet = strlen(acOutbuf);

      // Out to file
      bRet = WriteFile(fhOut, acOutbuf, iRet, &nBytesWritten, NULL);
      lCnt++;
      //if (!(++lCnt % 1000))
      //   printf("\r%u", lCnt);

      if (!bRet)
      {
         iRet = -1;
         break;
      }
   }
   
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return iRet;
}


int Ebc2Asc::R_Ebc2Asc(LPCSTR strInRec, LPSTR strOutRec)
{
   char  acTmp[1024], acTmp1[64];
   unsigned char  cTmp;
   unsigned short shTmp;
   unsigned int   iTmp, lTmp;

   *strOutRec = 0;
   for (iTmp = 0; iTmp < iNumFlds; iTmp++)
   {
      switch (asInRec[iTmp].acFldType[0])
      {
         case 'B':   // binary
            switch (asInRec[iTmp].iFldLen)
            {
               case 1:
                  cTmp = strInRec[asInRec[iTmp].iOffset];
                  sprintf(acTmp, "%2d", (int)cTmp);
                  break;
               case 2:
                  //memcpy((char *)&shTmp, (char *)&strInRec[asInRec[iTmp].iOffset], 2);
                  //shTmp >>= 8;
                  B2I((char *)&shTmp, (char *)&strInRec[asInRec[iTmp].iOffset]);
                  sprintf(acTmp, "%.5u", shTmp);
                  break;
               case 4:
                  B2L((char *)&shTmp, (char *)&strInRec[asInRec[iTmp].iOffset]);
                  sprintf(acTmp, "%.10u", shTmp);
                  break;
               default:
                  strcpy(acTmp, "????");
            }
            strcat(strOutRec, acTmp);
            break;
         case 'C':   // ascii
            memcpy(acTmp, (char *)&strInRec[asInRec[iTmp].iOffset], asInRec[iTmp].iFldLen);
            acTmp[asInRec[iTmp].iFldLen] = 0;
            strcat(strOutRec, acTmp);
            break;
         case 'E':   // ebcdic
            ebc2asc((unsigned char *)&acTmp[0], (unsigned char *)&strInRec[asInRec[iTmp].iOffset], asInRec[iTmp].iFldLen);
            acTmp[asInRec[iTmp].iFldLen] = 0;
            strcat(strOutRec, acTmp);
            break;
         case 'P':   // Packed decimal
            pd2num(acTmp, (char *)&strInRec[asInRec[iTmp].iOffset], asInRec[iTmp].iFldLen);

            strcat(strOutRec, acTmp);
            break;
         case 'Z':   // Zone decimal
            ebc2asc((unsigned char *)&acTmp[0], (unsigned char *)&strInRec[asInRec[iTmp].iOffset], asInRec[iTmp].iFldLen-1);
            cTmp = strInRec[asInRec[iTmp].iOffset+asInRec[iTmp].iFldLen-1];
            cTmp &= 0x0F;     // Remove high nibble
            cTmp |= 0x30;     // Replace it with 30H
            acTmp[asInRec[iTmp].iFldLen-1] = cTmp;
            acTmp[asInRec[iTmp].iFldLen] = 0;
            strcat(strOutRec, acTmp);
            break;
         default:    // Not used
            memset(acTmp, ' ', asInRec[iTmp].iFldLen);
            acTmp[asInRec[iTmp].iFldLen] = 0;
            strcat(strOutRec, acTmp);
            break;
      }

   }

   return 0;
}

void Ebc2Asc::OnConvert() 
{
   int   iRet;

   m_OK.EnableWindow(false);
   m_Cancel.EnableWindow(false);
   UpdateData(true);

   if (m_strDefFile.IsEmpty())
   {
      if (m_bAddCrLf)
         iRet = doEBC2ASCAddCR(m_strInfile, m_strOutfile, m_iRecLen);
      else
         iRet = doEBC2ASC(m_strInfile, m_strOutfile);
   } else
   {
      iRet = F_Ebc2Asc(m_strInfile, m_strOutfile, m_strDefFile, m_iRecLen);
   }

   UpdateData(false);

   m_OK.EnableWindow(true);
   m_Cancel.EnableWindow(true);
}

