#ifndef TIFF_H
#define TIFF_H  1

#define TIFF_VERSION    42          /* Always has this value */

#define TIFF_MOTOROLA   0x4D4D      /* Big endian */
#define TIFF_INTEL      0x4949      /* Little endian */

typedef unsigned char  BYTE;        /*  8-bit */
typedef unsigned char  ASCII;       /*  8-bit string */
typedef unsigned long  RATIONAL;    /* 32-bit Numerator and Denominator */

/*
**  Tag Data Types as Defined by TIFF 5.0
*/
#define TYPE_BYTE      1
#define TYPE_ASCII     2
#define TYPE_SHORT     3
#define TYPE_LONG      4
#define TYPE_RATIONAL  5

/*
**  TIFF Image File Header Format
*/
struct _TIFF_IFH {
    short ByteOrder;    /* Byte order of TIFF file */
    short Version;      /* TIFF version number */
    long  IFDOffset;    /* Offset to first Image File Directory */
};

/*
**  TIFF Tag Format
**
**  Tags are linked together in a list in the IFD.
*/
struct _TIFF_TAG {
    short  TagID;               /* Tag ID Number */
    short  TagType;             /* Tag Data Type */
    long   TagLength;           /* Tag Data Length */
    long   ValueOffset;         /* Tag Data or Offset to Data */
    void  *Value;               /* Tag Data */
    struct _TIFF_TAG *Next;     /* Link to next Tag */
};

/*
**  TIFF Image File Directory Format
**
**  There is one IFD per image in a TIFF file.
*/
struct _TIFF_IFD {
    short  NumDirEntries;       /* Number of Tags in IFD */
    long   NextIFDOffset;       /* Offset to next IFD (if any) */
    struct _TIFF_TAG *TagList;  /* Link to list of Tags */
    struct _TIFF_IFD *Next;     /* Link to next IFD */
};

/*
**  Definition for an entire TIFF image file.
*/
struct _TIFF_IMG {
    char   TIFF_FileName[81];   /* Name of the TIFF file */
    short  TIFF_NumOfImages;    /* Number of images in the TIFF file */
    struct _TIFF_IFH *Header;   /* IFH, one per TIFF file */
    struct _TIFF_IFD *IfdList;  /* Link to list of IFDs */
};

typedef struct _TIFF_IFH  TIFFIFH;      /* TIFF Header Structure */
typedef struct _TIFF_IFD  TIFFIFD;      /* TIFF Directory Structure */
typedef struct _TIFF_TAG  TIFFTAG;      /* TIFF Tag Structure */
typedef struct _TIFF_IMG  TIFFIMAGE;    /* TIFF Image File Structure */

/*
**  TIFF Tag ID Constants
**
**  Certain tags are required, or at least "highly recommended", for
**  for use in TIFF files.  Such tags are marked by the TIFF class
**  requiring their usage.  Tags marked "obsolete" are no longer
**  recommended for use by TIFF revision 5.0 image files.
**
**  TIFF Class Key: B - Bilevel
**                  G - Gray Scale
**                  P - Palette
**                  R - RGB
**                  F - Facsimile (TIFF/F)
*/
#define T_NEWSUBFILETYPE            254 /* B G P R F */
#define T_SUBFILETYPE               255 /*              Obsolete  */
#define T_IMAGEWIDTH                256 /* B G P R F */
#define T_IMAGELENGTH               257 /* B G P R F */
#define T_BITSPERSAMPLE             258 /* B G P R F */
#define T_COMPRESSION               259 /* B G P R F */

#define T_PHOTOMETRICINTERPRETATION 262 /* B G P R F */
#define T_THRESHHOLDING             263 /*              Obsolete */
#define T_CELLWIDTH                 264 /*              Obsolete */
#define T_CELLLENGTH                265 /*              Obsolete */
#define T_FILLORDER                 266 /*         F    Obsolete */

#define T_DOCUMENTNAME              269
#define T_IMAGEDESCRIPTION          270
#define T_MAKE                      271
#define T_MODEL                     272
#define T_STRIPOFFSETS              273 /* B G P R F */
#define T_ORIENTATION               274 /*              Obsolete */

#define T_SAMPLESPERPIXEL           277 /* B G P R F */
#define T_ROWSPERSTRIP              278 /* B G P R F */
#define T_STRIPBYTECOUNTS           279 /* B G P R F */
#define T_MINSAMPLEVALUE            280 /*              Obsolete */
#define T_MAXSAMPLEVALUE            281 /*              Obsolete */
#define T_XRESOLUTION               282 /* B G P R F */
#define T_YRESOLUTION               283 /* B G P R F */
#define T_PLANARCONFIGURATION       284
#define T_PAGENAME                  285
#define T_XPOSITION                 286
#define T_YPOSITION                 287
#define T_FREEOFFSETS               288 /*              Obsolete */
#define T_FREEBYTECOUNTS            289 /*              Obsolete */
#define T_GRAYRESPONSEUNIT          290
#define T_GRAYRESPONSECURVE         291

#define T_GROUP3OPTIONS             292
#define T_GROUP4OPTIONS             293

#define T_RESOLUTIONUNIT            296 /* B G P R F */
#define T_PAGENUMBER                297

#define T_COLORRESPONSECURVES       301

#define T_SOFTWARE                  305
#define T_DATETIME                  306

#define T_ARTIST                    315
#define T_HOSTCOMPUTER              316

#define T_PREDICTOR                 317
#define T_WHITEPOINT                318
#define T_PRIMARYCHROMATICITIES     319
#define T_COLORMAP                  320 /*     P     */

#define T_BADFAXLINES               326 /*         F    TIFF/F specific */
#define T_CLEANFAXDATA              327 /*         F    TIFF/F specific */
#define T_CONSECUTIVEBADFAXLINES    328 /*         F    TIFF/F specific */


TIFFIMAGE *TiffLoad(LPCSTR tifffile);
TIFFIMAGE *ReadTiffFile(FILE *, LPCSTR);
TIFFIFH   *ReadTiffIFH(FILE *);
TIFFIFD   *ReadTiffIFD(FILE *, long);
TIFFTAG   *ReadTiffTag(FILE *);
short      getbigshort(FILE *fp);
short      getlittleshort(FILE *fp);
long       getbiglong(FILE *);
long       getlittlelong(FILE *);
long       lTiffSrch(TIFFIMAGE *image, int iTag);
int        getTiffRes(LPCSTR pFilename, int iTag);

#endif
