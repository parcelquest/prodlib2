#if !defined(AFX_TESTADRDLG_H__F42FF8BC_44FC_4879_8B2C_7283F6953923__INCLUDED_)
#define AFX_TESTADRDLG_H__F42FF8BC_44FC_4879_8B2C_7283F6953923__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestAdrDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestAdrDlg dialog

class CTestAdrDlg : public CDialog
{
// Construction
public:
   CTestAdrDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
   //{{AFX_DATA(CTestAdrDlg)
   enum { IDD = IDD_TESTADR };
   CString	m_strCity;
   CString	m_strState;
   CString	m_strDir;
   CString	m_strName;
   CString	m_strNum;
   CString	m_strSfx;
   CString	m_strAddr1;
   CString	m_strAddr2;
   CString	m_strUnit;
   CString	m_strCareOf;
   CString	m_strAddr3;
   CString	m_strAddr4;
   CString	m_strAddr5;
   CString	m_strZip;
   //}}AFX_DATA


// Overrides
   // ClassWizard generated virtual function overrides
   //{{AFX_VIRTUAL(CTestAdrDlg)
   protected:
   virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
   //}}AFX_VIRTUAL

// Implementation
   int      m_iListCount, m_iCurItem;
   CString  m_strIni;

protected:

   // Generated message map functions
   //{{AFX_MSG(CTestAdrDlg)
   afx_msg void OnTestAdr();
   virtual void OnCancel();
   afx_msg void OnFirstadr();
   afx_msg void OnNextadr();
   afx_msg void OnPrevadr();
   //}}AFX_MSG
   DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTADRDLG_H__F42FF8BC_44FC_4879_8B2C_7283F6953923__INCLUDED_)
