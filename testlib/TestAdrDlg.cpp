#include "stdafx.h"
#include "testlib.h"
#include "Utils.h"
#include "TestAdrDlg.h"
#include "r01.h"
#include "..\\prodlib.h"
#include "AdrList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

long  *pAdrList, *pAdrSrc;

extern char *asDir[], *asDir1[], *asDir2[];

int LoadSuffixTbl(char *pFilename);
void Gle_ParseAdr(ADR_REC *pAdrRec, LPCSTR pAddr);
void Org_ParseMAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pAdr4, char *pAdr5);
void Org_ParseSAdr(ADR_REC *pAdrRec, char *pAdr1, char *pAdr2);
void Imp_ParseSAdr(ADR_REC *pAdrRec, char *pAdr1);
void Slo_ParseAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pState);
void Ccx_ParseSAdr(ADR_REC *pAdrRec, LPCSTR pAddr, LPCSTR pStrSfx, LPCSTR pStrNum, LPCSTR pStrFra, LPCSTR pUnit);
void Kin_ParseMAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pAdr4);
void Mrn_ParseMAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pAdr4);
void Nev_ParseSAdr(ADR_REC *pAdrRec, LPCSTR pAddr);
void Tul_ParseMAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pAdr4);
void Sbt_ParseSAdr(ADR_REC *pAdrRec, LPCSTR pAddr);
void Mpa_ParseSAdr(ADR_REC *pAdrRec, LPCSTR pAddr);

/////////////////////////////////////////////////////////////////////////////
// CTestAdrDlg dialog


CTestAdrDlg::CTestAdrDlg(CWnd* pParent /*=NULL*/)
   : CDialog(CTestAdrDlg::IDD, pParent)
{
   //{{AFX_DATA_INIT(CTestAdrDlg)
   m_strCity = _T("");
   m_strState = _T("");
   m_strDir = _T("");
   m_strName = _T("");
   m_strNum = _T("");
   m_strSfx = _T("");
   m_strAddr1 = _T("");
   m_strAddr2 = _T("");
   m_strUnit = _T("");
   m_strCareOf = _T("");
   m_strAddr3 = _T("");
   m_strAddr4 = _T("");
   m_strAddr5 = _T("");
   m_strZip = _T("");
   //}}AFX_DATA_INIT

   char  acTmp[256], *pTmp;
   long  lTmp;

   pTmp = _getcwd(acTmp, 256);
   m_strIni = acTmp;
   m_strIni += "\\TestLib.ini";
   lTmp = GetPrivateProfileString("TestAdr", "CurItem", "", acTmp, _MAX_PATH, m_strIni);
   if (lTmp > 0)
      m_iCurItem = atoi(acTmp);
   else
      m_iCurItem = 0;

   LoadSuffixTbl("C:\\Tools\\Common\\Suffix_Xlat.txt");
   pAdrList = (long *)&Mpa_SAdrList[m_iCurItem];
   pAdrSrc  = (long *)&Mpa_SAdrList[0];

   m_iListCount = MPA_SADRCNT-1;
   m_strAddr1 = Mpa_SAdrList[m_iCurItem];
}

/***************************************************************************/

void CTestAdrDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   //{{AFX_DATA_MAP(CTestAdrDlg)
   DDX_Text(pDX, IDC_CITYNAME, m_strCity);
   DDX_Text(pDX, IDC_STATEABR, m_strState);
   DDX_Text(pDX, IDC_STRDIR, m_strDir);
   DDX_Text(pDX, IDC_STRNAME, m_strName);
   DDX_Text(pDX, IDC_STRNUM, m_strNum);
   DDX_Text(pDX, IDC_STRSFX, m_strSfx);
   DDX_Text(pDX, IDC_TESTADDR1, m_strAddr1);
   DDX_Text(pDX, IDC_TESTADDR2, m_strAddr2);
   DDX_Text(pDX, IDC_STRUNIT, m_strUnit);
   DDX_Text(pDX, IDC_CAREOF, m_strCareOf);
   DDX_Text(pDX, IDC_TESTADDR3, m_strAddr3);
   DDX_Text(pDX, IDC_TESTADDR4, m_strAddr4);
   DDX_Text(pDX, IDC_TESTADDR5, m_strAddr5);
   DDX_Text(pDX, IDC_ZIPCODE, m_strZip);
   //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestAdrDlg, CDialog)
   //{{AFX_MSG_MAP(CTestAdrDlg)
   ON_BN_CLICKED(IDOK, OnTestAdr)
   ON_BN_CLICKED(IDC_FIRSTADR, OnFirstadr)
   ON_BN_CLICKED(IDC_NEXTADR, OnNextadr)
   ON_BN_CLICKED(IDC_PREVADR, OnPrevadr)
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestAdrDlg message handlers

void CTestAdrDlg::OnTestAdr() 
{
   ADR_REC  sRec;
   char     acTmp[256], acTmp1[256], *pTmp;
   char     acCareOf[32], adr1[64], adr2[64], adr3[64], adr4[64], adr5[64];
   int      iTmp;

   UpdateData(true);

   dateString(acTmp, 0);
   memset((void *)&sRec, 0, sizeof(ADR_REC));

   strcpy(adr1, m_strAddr1);
   strcpy(adr2, m_strAddr2);
   strcpy(adr3, m_strAddr3);
   strcpy(adr4, m_strAddr4);
   strcpy(adr5, m_strAddr5);
   acCareOf[0] = 0;

   //Org_ParseMAdr(&sRec, acCareOf, adr1, adr2, adr3, adr4, adr5);
   //Org_ParseSAdr(&sRec, adr1, adr2);

   //parseAdr1(&sRec, m_strAddr1.GetBuffer(0));
   //parseAdr1_1(&sRec, m_strAddr1.GetBuffer(0));
   //parseAdr2(&sRec, m_strAddr2.GetBuffer(0));
   //Imp_ParseSAdr(&sRec, m_strAddr1.GetBuffer(0));
   //Slo_ParseAdr(&sRec, acCareOf, adr1, adr2, adr3, adr4);

   //Gle_ParseAdr(&sRec, m_strAddr1);
   //Ccx_ParseSAdr(&sRec, m_strAddr1, m_strAddr2, m_strAddr3, m_strAddr4, m_strAddr5);
   //Mrn_ParseMAdr(&sRec, acCareOf, adr1, adr2, adr3, adr4);
   //Nev_ParseSAdr(&sRec, m_strAddr1);
   //Tul_ParseMAdr(&sRec, acCareOf, adr1, adr2, adr3, adr4);
   Sbt_ParseSAdr(&sRec, m_strAddr1);
   
   m_strNum = sRec.strNum;
   if (sRec.strSub[0] > ' ')
   {
      m_strNum += " ";
      m_strNum += sRec.strSub;
   }
   m_strDir = sRec.strDir;
   m_strName = sRec.strName;
   m_strSfx = sRec.strSfx;
   m_strUnit = sRec.Unit;
   m_strCity = sRec.City;
   m_strState = sRec.State;
   m_strCareOf = acCareOf;
   m_strZip = sRec.Zip;

   UpdateData(false);
}

void CTestAdrDlg::OnCancel() 
{
   // Save current position
   if (m_iCurItem > 1)
   {
      char acTmp[32];
      sprintf(acTmp, "%d", m_iCurItem);
      WritePrivateProfileString("TestAdr", "CurItem", acTmp, m_strIni);
   }

   CDialog::OnCancel();
}

void CTestAdrDlg::OnFirstadr() 
{
   m_iCurItem = 0;	
   pAdrList = pAdrSrc;
   m_strAddr1 = (char *)*pAdrList;

   /* NEV
   CString sTmp;
   sTmp = (char *)*pAdrList;
   m_strAddr1 = sTmp.Left(41);
   m_strAddr2 = sTmp.Mid(41,30);
   m_strAddr3 = sTmp.Mid(71,30);
   m_strAddr4 = sTmp.Right(30);
   */
   UpdateData(false);
}

void CTestAdrDlg::OnNextadr() 
{
   char  *pTmp;
   if (m_iCurItem < m_iListCount)
   {
      m_iCurItem++;	
      pAdrList++;
      pTmp = (char *)*pAdrList;
      m_strAddr1 = pTmp;

      /* NEV
      CString sTmp;
      sTmp = (char *)*pAdrList;
      m_strAddr1 = sTmp.Left(41);
      m_strAddr2 = sTmp.Mid(41,30);
      m_strAddr3 = sTmp.Mid(71,30);
      m_strAddr4 = sTmp.Right(30);
      */
      UpdateData(false);
   }
}

void CTestAdrDlg::OnPrevadr() 
{
   char  *pTmp;
   if (m_iCurItem > 0)
   {
      m_iCurItem--;	
      pAdrList--;
      pTmp = (char *)*pAdrList;
      m_strAddr1 = pTmp;

      /* NEV
      CString sTmp;
      sTmp = (char *)*pAdrList;
      m_strAddr1 = sTmp.Left(41);
      m_strAddr2 = sTmp.Mid(41,30);
      m_strAddr3 = sTmp.Mid(71,30);
      m_strAddr4 = sTmp.Right(30);
      */
      UpdateData(false);
   }
}

/***************************************************************************/

void Ccx_ParseSAdr(ADR_REC *pAdrRec, LPCSTR pAddr, LPCSTR pStrSfx, LPCSTR pStrNum, LPCSTR pStrFra, LPCSTR pUnit)
{
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[64], acAddr2[64], acCode[32], acToNum[16];
   int      iTmp, iStrNo, iLen, iSfxCode;
   long     lTmp;

   ADR_REC  sAdrRec;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));

   // Copy StrNum
   sAdrRec.lStrNum = atol(pStrNum);
   if (*pStrSfx > ' ')
      strcpy(sAdrRec.strSfx, pStrSfx);

   if (*pUnit > ' ')
      strcpy(sAdrRec.Unit, pUnit);

   if (*pAddr > 'Z' || *pAddr < ' ')
      pAddr++;

   if (*pAddr > ' ' && memcmp(pAddr, "NO ADDRESS", 10))
   {
      // Copy street name
      strcpy(acTmp, pAddr);
      //myTrim(acTmp);
      blankRem(acTmp);
      quoteRem(acTmp);

      // If there is '&' in the middle, leave strname alone
      if (strchr((char *)&acTmp[2], '&'))
         strcpy(sAdrRec.strName, acTmp);
      else
      {
         if (!strcmp(acTmp, "AVE"))
         {
            if (!strcmp(pStrFra, "61/4"))
               strcpy(sAdrRec.strName, "6 1/4");
            else
               strcpy(sAdrRec.strName, pStrFra);
            memcpy(sAdrRec.strSfx, "AVE", 3);
         } else if (!strcmp(acTmp, "ST"))
         {
            strcpy(sAdrRec.strName, pStrFra);
            memcpy(sAdrRec.strSfx, "ST", 2);
         } else if (!strcmp(acTmp, "1/2") )
         {
            memcpy(sAdrRec.strSub, "1/2", 3);
            strcpy(sAdrRec.strName, pStrFra);
         } else if (!memcmp(acTmp, "1/2 ", 4) )
         {
            memcpy(sAdrRec.strSub, "1/2", 3);
            strcpy(acTmp, (char *)&acTmp[4]);
            parseAdrND(&sAdrRec, acTmp);
         } else
         {
            if (acTmp[0] == '&')
            {
               // If strnum is present, skip next number
               if (sAdrRec.lStrNum)
               {
                  iTmp = 2;
                  while (acTmp[iTmp] && acTmp[iTmp] < 'A') iTmp++;
                  while (iTmp > 0 && acTmp[iTmp-1] > ' ') iTmp--;    // & 123 1/2 20TH
               } else
                  iTmp = 2;
               strcpy(acTmp, (char *)&acTmp[iTmp]);
            }
            parseAdrND(&sAdrRec, acTmp);
         }
      }

      if (sAdrRec.lStrNum > 0)
      {
         iTmp = sprintf(acAddr1, "%d", sAdrRec.lStrNum);
         memcpy(sAdrRec.strNum, acAddr1, iTmp);
      }
   }

   memcpy((void *)pAdrRec, (void *)&sAdrRec, sizeof(ADR_REC));
}

/***********************************************************************************/

void Gle_ParseAdr(ADR_REC *pAdrRec, LPCSTR pAddr)
{
   char  *pTmp, *pTmp1, acAdr[64];
   int   iStrNo, iTmp;

   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   strcpy(acAdr, myTrim((char *)pAddr));

   if (strstr(pAddr, "PCT"))
      return;
   if (strstr(pAddr, " LS-"))
      return;
   if (strchr(pAddr, '.'))
      return;

   if (pTmp=strchr(acAdr, '('))
   {
      *pTmp = 0;
      if (*(--pTmp) == ' ') *pTmp = 0;
   }

   // Take first number only
   iStrNo = atoi(acAdr);
   pTmp = acAdr;
   if (iStrNo > 0)
   {
      while (pTmp = strchr(pTmp, ' '))
      {
         pTmp++;
         if (*pTmp >= 'A')
            break;
         //iTmp = 
         if (!memcmp(pTmp+1, "TH", 2) || 
             !memcmp(pTmp+1, "ST", 2) || 
             !memcmp(pTmp+1, "RD", 2))
            break;
      }

      // Take out common words
      pTmp1 = strrchr(pTmp, ' ');
      if (pTmp1 && !memcmp(pTmp1, " AREA", 5))
         *pTmp1 = 0;
      pTmp1 = strrchr(pTmp, ' ');
      if (pTmp1 && !memcmp(pTmp1, " COMM", 5))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " GRN"))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " WIL "))
         *pTmp1 = 0;

      // Keep first address only
      // 158 S ENRIGHT  1030 OAK ST
      // 929 W WILLOW-305 N CULVER
      // 205 N SHASTA/321-325 W WALNUT
      // 705 S TEHAMA & 108 ELM
      if (pTmp1 = strstr(pTmp, "  "))
         *pTmp1 = 0;
      else if (pTmp1 = strchr(pTmp, '/'))
         *pTmp1 = 0;
      else if (pTmp1 = strchr(pTmp, '-'))
         *pTmp1 = 0;
      else if (pTmp1 = strchr(pTmp, '&'))
         *pTmp1 = 0;

      parseAdr1N(pAdrRec, pTmp);
      sprintf(pAdrRec->strNum, "%d", iStrNo);
      pAdrRec->lStrNum = iStrNo;

      // For Glenn 
      //if (!memcmp(pAdrRec->strName, "CO RD ", 6))
      //{
      //   sprintf(acAdr, "COUNTY ROAD %s", (char *)&pAdrRec->strName[6]);
      //   strcpy(pAdrRec->strName, acAdr);
      //}
   }
}

/*************************************************************************/

void Org_ParseMAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pAdr4, char *pAdr5)
{
   char  *pTmp, *pTmp1, *p0, *p1, *p2;
   char  acAddr1[64], acAddr2[64], acTmp[64], acMailAdr[64];

   int   iStrNo, iTmp, iStart=0;

   // Init output
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   *pCareOf = 0;

   if (*pAdr1 <= ' ' || *pAdr2 <= ' ')
      return;

   // Check for Care Of
   if (*pAdr1 == '%')
      strcpy(pCareOf, pAdr1+1);
   else if (!memcmp(pAdr1, "ATTN", 4))
      strcpy(pCareOf, pAdr1+5);
   else if (!memcmp(pAdr1, "C/O", 3))
      strcpy(pCareOf, pAdr1+4);

   if (*pAdr5)
   {
      p2 = pAdr5;
      p1 = pAdr4;
      p0 = pAdr3;
   } else if (*pAdr4)
   {
      p2 = pAdr4;
      p1 = pAdr3;
      p0 = pAdr2;
   } else if (*pAdr3)
   {
      p2 = pAdr3;
      p1 = pAdr2;
      p0 = pAdr1;
   } else 
   {
      p2 = pAdr2;
      p1 = pAdr1;
      p0 = NULL;
   }

   // Check for foreign address - DO NOT PARSE
   if (!strcmp(p2, "CANADA") || !strcmp(p2, "HONG KONG") || 
       strstr(p2, "MEXICO")  || !strcmp(p2, "AUSTRALIA") ||
      !strcmp(p2, "ENGLAND") || !strcmp(p2, "NORTH IRELAND") ||
      !strcmp(p2, "JAPAN")   || !strcmp(p2, "GREAT BRITIAN") ||
      !strcmp(p2, "KOREA")   || !strcmp(p2, "UNITED KINGDOM") ||
      !memcmp(p2, "CHINA RPC", strlen(p2))   || !strcmp(p2, "SCOTTLAND") ||
      !strcmp(p2, "ICELAND") || !strcmp(p2, "PHILIPPINES") ||
      !strcmp(p2, "BELGIUM") || !strcmp(p2, "GERMANY") ||
      !strcmp(p2, "ITALY")   || !strcmp(p2, "GREECE") ||
      !strcmp(p2, "CHILE")   || !strcmp(p2, "SINGAPORE") ||
      !strcmp(p2, "SPAIN")   || !strcmp(p2, "SAUDI ARABIA") ||
      !strcmp(p2, "THAILAND")|| !strcmp(p2, "SOUTH KOREA") ||
      !strcmp(p2, "EGYPT")   || !strcmp(p2, "PUERTO RICO") ||
      !strcmp(p2, "ISRAEL")  || !strcmp(p2, "COSTA RICA") ||
      !strcmp(p2, "FRANCE")  || !strcmp(p2, "NEW ZEALAND") ||
      !strcmp(p2, "BAHRAIN") || !strcmp(p2, "THE NETHERLANDS") ||
      !strcmp(p2, "KUWAIT")  || !strcmp(p2, "SWITZERLAND") ||
      !strcmp(p2, "TAIWAN")  || !strcmp(p2, "NEW GUINEA") ||
      !strcmp(p2, "PAKISTAN")|| !strcmp(p2, "SO AFRICA") ||
      !strcmp(p2, "FINLAND") || !strcmp(p2, "MALAYSIA") 
      )
   {
      strcpy(acAddr1, p0);
      strcpy(acAddr2, p1);
      strcat(acAddr2, " ");
      strcat(acAddr2, p2);
   } else if (p0 && (!memcmp(p0, "STE", 3) || 
      !memcmp(p0, "SUITE", 5) ||
      !memcmp(p0, "UNIT", 4) || *p0 == '#') )
   {
      strcpy(acAddr1, p1);
      strcat(acAddr1, " ");
      strcat(acAddr1, p0);
      strcpy(acAddr2, p2);
   } else if (!memcmp(p1, "UNIT", 4) && atoi(p0) > 0)
   {  // 93902020
      strcpy(acAddr1, p0);
      strcat(acAddr1, " ");
      strcat(acAddr1, p1);
      strcpy(acAddr2, p2);
   } else
   {
      strcpy(acAddr1, p1);
      strcpy(acAddr2, p2);
   }

   // Check for valid string
   iTmp = 0;
   pTmp = (char *)&acAddr1[0];
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   sprintf(acMailAdr, "%s %s", acTmp, acAddr2);

   parseAdr1(pAdrRec, acTmp);
   parseAdr2_1(pAdrRec, acAddr2);
}

void Org_ParseSAdr(ADR_REC *pAdrRec, char *pAdr1, char *pAdr2)
{
   char  *pTmp, *pTmp1, *p0, *p1, *p2;
   char  acAddr1[64], acAddr2[64], acTmp[64], acMailAdr[64];

   int   iStrNo, iTmp, iStart=0;

   // Init output
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   SITUS1 *pStr;
   pStr = (SITUS1 *)pAdr1;
   memcpy(pAdrRec->strNum, pStr->acHse, 7);
   memcpy(pAdrRec->strDir, pStr->acDir, sizeof(pStr->acDir));
   memcpy(pAdrRec->strName, pStr->acStreet, sizeof(pStr->acStreet));            

   // Situs continuation strtype, citycode
   if (*pAdr2)
   {
      SITUS2 *pCity;
      pCity = (SITUS2 *)pAdr2;
      memcpy(pAdrRec->strSfx, pCity->acSuffix, sizeof(pCity->acSuffix));
      memcpy(pAdrRec->Unit, pCity->acSuite, sizeof(pCity->acSuite));
      memcpy(pAdrRec->City, pCity->acCityCode, 2);
   }
}

/*****************************************************************************/

void Imp_ParseSAdr(ADR_REC *pAdrRec, char *pAdr1)
{
   int   iTmp, iCnt, iStrNo, iLen;
   char  acAdr1[64], acTmp[64], *apItems[16], *pTmp;
   bool  bSkip=false;

   iTmp = 0;
   while (*pAdr1)
   {
      // Skip '.' and what inside parenthesis
      if (*pAdr1 != '.')
      {
         if (*pAdr1 == '(')
            bSkip = true;
         else if (*pAdr1 == ')')
         {
            bSkip = false;
            if (*(pAdr1+1) == ' ' && acAdr1[iTmp-1] == ' ')
               pAdr1++;
         } else if (!bSkip)
            acAdr1[iTmp++] = *pAdr1;
      }
      pAdr1++;
   }
   acAdr1[iTmp] = 0;
   strcpy(acTmp, acAdr1);

   iCnt = ParseString(acAdr1, 32, 16, apItems); 
   if (iCnt > 2)
   {
      for (iTmp = 0; iTmp < iCnt; iTmp++)
      {

         if (*apItems[iTmp] == '&')
         {
            continue;
         }
         iStrNo = atoi(apItems[iTmp]);
         if (!iStrNo)
            break;

         iLen = strlen(apItems[iTmp]);
         if (iLen > 1 && !isdigit(apItems[iTmp][iLen-2]))
            break;
      }

      strcpy(acTmp, apItems[0]);
      while (iTmp < iCnt)
      {
         if (*apItems[iTmp])
         {
            if (*apItems[iTmp] != '&')
            {
               strcat(acTmp, " ");
               strcat(acTmp, apItems[iTmp++]);
            } else if (!isdigit(*apItems[iTmp-1]) && isdigit(*apItems[iTmp+1]))
               break;
            else
               iTmp++;
         } else
            break;
      }

   } else if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, acAdr1);
      return;
   }

   parseAdr1(pAdrRec, acTmp);

}

/*****************************************************************************/

void Slo_ParseAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pState)
{
   char  *pTmp, *p3, *p0, *p1, *p2;
   char  acAddr1[64], acAddr2[64], acAddr3[64], acTmp[64], acMailAdr[64];

   int   iStrNo, iTmp, iStart=0;

   // Init output
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   *pCareOf = 0;

   if (*pAdr1 <= ' ')
      return;
   if (*pState == ' ')
   {
      if (!memcmp(pAdr1, "C/O", 3))
      {
         strcpy(pCareOf, pAdr1+4);
         sprintf(acMailAdr, "%s %s", myTrim(pAdr2), myTrim(pAdr3));
      } else
      {
         sprintf(acMailAdr, "%s %s %s", myTrim(pAdr1), myTrim(pAdr2), myTrim(pAdr3));
      }
      strncpy(pAdrRec->strName, myTrim(acMailAdr), sizeof(pAdrRec->strName));
      pAdrRec->strName[sizeof(pAdrRec->strName)-1] = 0;
      return;
   }

   strcpy(acAddr1, myTrim(pAdr1));
   strcpy(acAddr2, myTrim(pAdr2));
   strcpy(acAddr3, myTrim(pAdr3));

   // Check for Care Of
   if (acAddr1[0] == '%' || !memcmp(acAddr1, "ATTN", 4) || !memcmp(acAddr1, "C/O", 3))
   {
      strcpy(pCareOf, acAddr1);
      acAddr1[0] = 0;
      p1 = acAddr2;
      p2 = acAddr3;
      p3 = NULL;
   } else if (acAddr2[0] && (acAddr2[0] == '%' || !memcmp(acAddr2, "ATTN", 4) || !memcmp(acAddr2, "C/O", 3)))
   {
      strcpy(pCareOf, acAddr2);
      acAddr2[0] = 0;
      p1 = acAddr1;
      p2 = acAddr3;
      p3 = NULL;
   } else if (acAddr3[0] && (acAddr3[0] == '%' || !memcmp(acAddr3, "ATTN", 4) || !memcmp(acAddr3, "C/O", 3)))
   {
      strcpy(pCareOf, acAddr3);
      acAddr3[0] = 0;
      p1 = acAddr1;
      p2 = acAddr2;
      p3 = NULL;
   } else
   {
      p1 = acAddr1;
      p2 = p3 = NULL;
      if (acAddr2[0] > ' ') p2 = acAddr2;
      if (acAddr3[0] > ' ') p3 = acAddr3;
   }

   p0 = NULL;
   strcpy(acMailAdr, p1);
   if (*pCareOf)
   {
      if (p2 && (atoi(p2) > 0 || !memcmp(p2, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p2);
         p0 = p1;
      } else
      {
         strcpy(acMailAdr, p1);
         p0 = p2;
      }
   } else if (p3)
   {
      if (p3 && (atoi(p3) > 0 || !memcmp(p3, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p3);
         p0 = p2;
      } else if (p2 && (atoi(p2) > 0 || !memcmp(p2, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p2);
         p0 = p3;
      }
   } else if (p2)
   {
      if (p1 && (atoi(p1) > 0)  )
      {
         strcpy(acMailAdr, p1);
         p0 = p2;
      } else if (p2 && (atoi(p2) > 0 || !memcmp(p2, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p2);
         p0 = p1;
      }
   }

   if (p0 && *p0)
   {
      if (*p0 == '#' || !memcmp(p0, "SUITE", 5) || !memcmp(p0, "APT", 3) || !memcmp(p0, "STE", 3)
         || !memcmp(p0, "SP ", 3) || !memcmp(p0, "SP#", 3))
      {
         strcat(acMailAdr, " ");
         strcat(acMailAdr, p0);
      }
   }


   // Check for valid string
   iTmp = 0;
   pTmp = (char *)&acMailAdr[0];
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   if (acTmp[0] >= '0' && acTmp[0] <= '9')
      parseAdr1(pAdrRec, acTmp);
   else
   {
      strncpy(pAdrRec->strName, acTmp, sizeof(pAdrRec->strName));
      pAdrRec->strName[sizeof(pAdrRec->strName)-1] = 0;
   }
}

/******************************************************************************
 *
 * Check for 
 *    - DBA, C/O, RE: Only keep C/O name.  Ignore others
 *    - P O BOX
 *
 ******************************************************************************/

void Kin_ParseMAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pAdr4)
{
   char  *pTmp, *pTmp1, *p0, *p1, *p2;
   char  acAddr1[64], acAddr2[64], acTmp[64], acMailAdr[64];

   int   iStrNo, iTmp, iStart=0;

   // Init output
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   *pCareOf = 0;

   if (*pAdr1 <= ' ' || *pAdr2 <= ' ')
      return;

   if (*pAdr4)
   {
      p2 = pAdr4;
      p1 = pAdr3;
      p0 = pAdr2;
   } else if (*pAdr3)
   {
      p2 = pAdr3;
      p1 = pAdr2;
      p0 = pAdr1;
   } else 
   {
      p2 = pAdr2;
      p1 = pAdr1;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      if (!memcmp(p0, "C/O", 3))
      {
         strcpy(acTmp, p0+4);
         blankRem(acTmp);
         strncpy(pCareOf, acTmp, 20);
      }
      *(pCareOf+20) = 0;
   }

   strcpy(acAddr1, p1);
   strcpy(acAddr2, p2);

   // Check for valid string
   iTmp = 0;
   pTmp = (char *)&acAddr1[0];
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   parseMAdr1(pAdrRec, acTmp);
   parseAdr2_1(pAdrRec, acAddr2);
}

/******************************************************************************
 *
 * Check for 
 *    - DBA, C/O, RE: Only keep C/O name.  Ignore others
 *    - P O BOX
 *
 ******************************************************************************/

void Mrn_ParseMAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pAdr4)
{
   char  *pTmp, *pTmp1, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[64], acTmp[64];

   int   iStrNo, iTmp, iStart=0;

   // Init output
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   *pCareOf = 0;

   if (*pAdr1 <= ' ')
      return;

   acAddr1[0] = 0;
   if (*pAdr4 > ' ')
   {
      p2 = pAdr4;
      if (!_memicmp(pAdr1, "C/O", 3) || !_memicmp(pAdr1, "ATTN", 4) || *pAdr1 == '%')
      {
         p0 = pAdr1;

         // If last word is not a number, drop it
         strcpy(acTmp, pAdr4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pAdr3;
            p1 = pAdr2;
         } else if (isdigit(*pAdr2))
         {
            // Merge line 2 and 3
            sprintf(acAddr1, "%.32s %.32s", pAdr2, pAdr3);
            p1 = acAddr1;
         } else
            p1 = pAdr3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pAdr4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pAdr3;
            
         // Merge line 1 and 2 - This is more likely foreign addr
         if (isdigit(*pAdr1))
         {
            sprintf(acAddr1, "%.32s %.32s", pAdr1, pAdr2);
            p1 = acAddr1;
         } else if (isdigit(*pAdr2))
            p1 = pAdr2;
         else
            p1 = pAdr1;

         p0 = NULL;
      }
   } else if (*pAdr3 > ' ')
   {
      p2 = pAdr3;
      if (!_memicmp(pAdr1, "C/O", 3) || !_memicmp(pAdr1, "ATTN", 4) || *pAdr1 == '%')
      {
         p0 = pAdr1;
         p1 = pAdr2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pAdr3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p2 = pAdr2;
            p1 = pAdr1;
         } else if (isdigit(*pAdr1))
         {
            // Merge line 2 and 3
            sprintf(acAddr1, "%.32s %.32s", pAdr1, pAdr2);
            p1 = acAddr1;
         } else
         {
            // Ignore PO BOX since we cannot put both PO BOX and street address on the same line
            // Ignore line #1: PO BOX 463                      36 SPRING ROAD                  LAGUNITAS CA 94938
            // Ignore line #2: PO BOX 4055                     CIVIC CENTER                    SAN RAFAEL CA 94913
            if (isdigit(*pAdr2))
               p1 = pAdr2;
            else
               p1 = pAdr1;
         }
         p0 = NULL;
      }
   } else if (*pAdr2 > ' ')
   {
      p2 = pAdr2;
      p1 = pAdr1;
      p0 = NULL;
   } else 
   {
      p2 = pAdr1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      if (!_memicmp(p0, "C/O", 3))
      {
         strcpy(acTmp, p0+4);
         blankRem(acTmp);
         strncpy(pCareOf, acTmp, 20);
      } else if (!_memicmp(p0, "ATTN", 4))
      {
         strcpy(acTmp, p0+5);
         blankRem(acTmp);
         strncpy(pCareOf, acTmp, 20);
      } else if (*p0 == '%')
      {
         strcpy(acTmp, p0+1);
         blankRem(acTmp);
         strncpy(pCareOf, acTmp, 20);
      }
      *(pCareOf+20) = 0;
   }

   if (p1)
   {
      if (!acAddr1[0])
         strcpy(acAddr1, p1);

      // Check for valid string
      iTmp = 0;
      pTmp = (char *)&acAddr1[0];
      while (*pTmp)
      {
         if (*pTmp == '-')
         {
            if (strstr(acAddr1, " STE ") || strchr(acAddr1, '#'))
               *pTmp = ' ';
            else
               *pTmp = '#';
         }
         pTmp++;
      }
      blankRem(acAddr1);

      Mrn_parseMAdr1(pAdrRec, acAddr1);
   }

   strcpy(acAddr2, p2);
   parseAdr2_1(pAdrRec, acAddr2);
}

/******************************************************************************
 *
 * Check for:
      -419A&BEAST MARYLAND DR
      -1171/2ALTA ST
      - 321-APLEASANT ST
      - 436 AHENDERSON ST
      -117 #BSCHOOL ALLEY
 *
 *
 ******************************************************************************/

#define RSIZ_NEV_SITUS_STRNUM		               	6 
#define RSIZ_NEV_SITUS_STRNAME	           		   24

void Nev_ParseSAdr(ADR_REC *pAdrRec, LPCSTR pAddr)
{
   char  *pTmp, *pTmp1, acAdr[64], acStrSub[8], acUnit[8], acTmp[128];
   int   iStrNo, iTmp;

   // Ignore N/A
   if (!memcmp(pAddr+RSIZ_NEV_SITUS_STRNUM, "N/A", 3))
      return;

   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   acStrSub[0]=acUnit[0] = 0;
   memcpy(acTmp, pAddr, RSIZ_NEV_SITUS_STRNUM);
   acTmp[RSIZ_NEV_SITUS_STRNUM] = 0;
   if (acTmp[4] == '/')
   {
      strcpy(acStrSub, &acTmp[3]);
      acTmp[3] = 0;
   } else if (acTmp[4] == '&')
   {
      strcpy(acUnit, &acTmp[3]);
      acTmp[3] = 0;

   } else if (acTmp[4] == '-' || acTmp[4] == ' ')
   {
      strcpy(acStrSub, &acTmp[5]);
      acTmp[4] = 0;
   } else if (acTmp[4] == '#')
   {
      strcpy(acUnit, &acTmp[4]);
      acTmp[4] = 0;
   } else if (pTmp = strpbrk(acTmp, "&/-#"))
   {
      iTmp = 0;
   }

   iStrNo = atoi(acTmp);

   // Street name
   strcpy(acAdr, pAddr+RSIZ_NEV_SITUS_STRNUM);
   if (pTmp=strchr(acAdr, '('))
      *pTmp = 0;

   parseAdr1N_1(pAdrRec, acAdr);
   if (iStrNo > 0)
   {
      sprintf(pAdrRec->strNum, "%d", iStrNo);
      pAdrRec->lStrNum = iStrNo;
      if (acStrSub[0] > ' ')
         strcpy(pAdrRec->strSub, acStrSub);
      if (acUnit[0] > ' ')
         strcpy(pAdrRec->Unit, acUnit);
   }
}

void Tul_ParseMAdr(ADR_REC *pAdrRec, char *pCareOf, char *pAdr1, char *pAdr2, char *pAdr3, char *pAdr4)
{
   char  *pTmp, *pTmp1, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[64], acTmp[64];

   int   iStrNo, iTmp, iStart=0;

   // Init output
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   *pCareOf = 0;

   if (*pAdr1 <= ' ')
      return;

   acAddr1[0] = 0;
   if (*pAdr4 > ' ')
   {
      p2 = pAdr4;
      if (!_memicmp(pAdr1, "C/O", 3) || !_memicmp(pAdr1, "C / O", 5) || *pAdr1 == '%')
      {
         p0 = pAdr1;

         // If last word is not a number, drop it
         strcpy(acTmp, pAdr4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pAdr3;
            p1 = pAdr2;
         } else if (isdigit(*pAdr2))
         {
            // Merge line 2 and 3
            sprintf(acAddr1, "%.32s %.32s", pAdr2, pAdr3);
            p1 = acAddr1;
         } else
            p1 = pAdr3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pAdr4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pAdr3;
            
         // Merge line 1 and 2 - This is more likely foreign addr
         if (isdigit(*pAdr1))
         {
            sprintf(acAddr1, "%.32s %.32s", pAdr1, pAdr2);
            p1 = acAddr1;
         } else if (isdigit(*pAdr2))
            p1 = pAdr2;
         else
            p1 = pAdr3;

         p0 = NULL;
      }
   } else if (*pAdr3 > ' ')
   {
      p2 = pAdr3;
      if (!_memicmp(pAdr1, "C/O", 3) || !_memicmp(pAdr1, "C / O", 5) || *pAdr1 == '%')
      {
         p0 = pAdr1;
         p1 = pAdr2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pAdr3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p2 = pAdr2;
            p1 = pAdr1;
         } else if (isdigit(*pAdr1))
         {
            // Merge line 2 and 3
            sprintf(acAddr1, "%.32s %.32s", pAdr1, pAdr2);
            p1 = acAddr1;
         } else
               p1 = pAdr2;
         p0 = NULL;
      }
   } else if (*pAdr2 > ' ')
   {
      p2 = pAdr2;
      p1 = pAdr1;
      p0 = NULL;
   } else 
   {
      p2 = pAdr1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      if (!_memicmp(p0, "C/O", 3))
      {
         strcpy(acTmp, p0+4);
         blankRem(acTmp);
         strncpy(pCareOf, acTmp, 20);
      } else if (!_memicmp(p0, "ATTN", 4))
      {
         strcpy(acTmp, p0+5);
         blankRem(acTmp);
         strncpy(pCareOf, acTmp, 20);
      } else if (*p0 == '%')
      {
         strcpy(acTmp, p0+1);
         blankRem(acTmp);
         strncpy(pCareOf, acTmp, 20);
      }
      *(pCareOf+20) = 0;
   }

   if (p1)
   {
      if (!acAddr1[0])
         strcpy(acAddr1, p1);

      // Check for valid string
      iTmp = 0;
      pTmp = (char *)&acAddr1[0];
      while (*pTmp)
      {
         if (*pTmp == '-')
         {
            if (strstr(acAddr1, " STE ") || strchr(acAddr1, '#'))
               *pTmp = ' ';
            else
               *pTmp = '#';
         }
         pTmp++;
      }
      blankRem(acAddr1);

      parseMAdr1_2(pAdrRec, acAddr1);
   }

   strcpy(acAddr2, p2);
   blankRem(acAddr2);
   // Correction
   if (!memcmp(acAddr2, "FRESNOC A", 9))
      memcpy(acAddr2, "FRESNO CA", 9);

   parseAdr2_1(pAdrRec, acAddr2);
}

/*****************************************************************************/

void Sbt_ParseSAdr(ADR_REC *pAdrRec, LPCSTR pAddr)
{
   char  acAdr[64], acStrName[64], acSfx[8], acCity[32], acTmp[128];
   char  *pTmp, *pTmp1, *apItems[16];
   int   iStrNo, iTmp, iCnt, iDir, iSfxCode, iIdx;
   bool  bDir, bRet, bAnd;

   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   acCity[0]=acStrName[0] = 0;
   strcpy(acTmp, pAddr);

   iStrNo = atoi(acTmp);

   if (iStrNo > 0)
   {
      // Street name
      strcpy(acAdr, acTmp);
      // Skip every thing within parenthesis
      if (pTmp=strchr(acAdr, '('))
      {
         while (*pTmp && *pTmp != ')')
            *pTmp++ = ' ';

         if (*pTmp == ')')
            *pTmp = ' ';
      }

      replChar(acAdr, '.', ' ');
      blankRem(acAdr);

      iCnt = ParseString(acAdr, ' ', 16, apItems);
      if (iCnt == 1)
      {
         // Invalid address
         return;
      }

      // Reset index
      iIdx = 0;

      // Validate StrNum
      // 416-A ROSE AVE
      // 106-108-110-112 PEARCE LANE SJB
      if (pTmp = strchr(apItems[iIdx], '-'))
      {
         *pTmp++ = 0;
         // If more than one '-' found, ignore them
         if (!strchr(pTmp, '-'))
            strcpy(pAdrRec->strSub, pTmp);
      } else if (pTmp = strchr(apItems[iIdx], ','))
      {
         // Drop extra number 
         // 348,348 1/2,350 CARPENTERIA RD
         if (strchr(apItems[iIdx+1], ',') || strchr(apItems[iIdx+1], '/'))
            iIdx++;
         else if (isNumber(apItems[iIdx+1]))
            iIdx++;
      } else if (!isNumber(apItems[iIdx]))
      {
         // Move alpha into strSub
         pTmp = apItems[iIdx];
         while (*pTmp && isdigit(*pTmp))
            pTmp++;
         if (*pTmp)
            strcpy(pAdrRec->strSub, pTmp);
      } else if (*apItems[iIdx+1] == '&')
      {
         //81 & 81-A 4TH ST SJB
         pTmp = apItems[iIdx+1];
         if (!*(pTmp+1))
            iIdx += 2;
         else
            iIdx++;
      } else if (iCnt > 4 && 
                 strlen(apItems[iIdx+1]) == 1 && 
                 *apItems[iIdx+2] == '&' )
      {
         // 803 A & B MARENTES CT - Drop A & B
         if (strlen(apItems[iIdx+3]) == 1) 
            iIdx += 3;
         else if (isdigit(*(apItems[iIdx+3])) )
         {
            // 504 A & 1026-1028 WEST ST
            if (pTmp = strchr(apItems[iIdx+3], '-'))
               *pTmp = 0;
         }
      } else if (*apItems[iIdx+1] == '#')
      {
         if (*apItems[iIdx+2] >= 'A')
            strncpy(pAdrRec->Unit, apItems[++iIdx], SIZ_M_UNIT);
         else 
         {  // Skip multi-unit
            iTmp = 3;
            while (iTmp+iIdx < iCnt)
            {
               if (*apItems[iIdx+iTmp] >= 'A')
                  break;

               iTmp++;
            }
            iIdx = iTmp-1;
         }

      } else if (*apItems[iIdx+1] != '&' && strchr(apItems[iIdx+1], '&'))
      {
         // Skip multi-unit
         // 1031 A,B,&C CAPUTO CT
         iIdx++;
      } else if (strchr(apItems[iIdx+1], ','))
      {
         // 1021 A, B, & C CAPUTO CT
         iTmp = 2;
         bAnd = false;
         while (iTmp+iIdx < iCnt)
         {
            if (*(apItems[iIdx+iTmp]) == '&') 
               bAnd = true;
            else if (!strchr(apItems[iIdx+iTmp], ',') )
            {
               if (bAnd)
                  iTmp++;
               break;
            }

            iTmp++;
         }
         iIdx = iTmp-1;
      } else if (*(apItems[iIdx+1]+1) == '-')
      {
         iIdx++;
         remChar(apItems[iIdx], '-');
         strncpy(pAdrRec->Unit, apItems[iIdx], SIZ_M_UNIT);
      }

      sprintf(pAdrRec->strNum, "%d", iStrNo);
      pAdrRec->lStrNum = iStrNo;    
      iIdx++;

      // Check for StrSub
      if (*(apItems[iIdx]) == '\"')
      {
         pTmp = apItems[iIdx];
         pAdrRec->strSub[0] = *++pTmp;
         iIdx++;
      }

      // Dir
      iTmp = 0;
      iSfxCode = 0;
      bRet = bDir = false;
      while (asDir[iTmp])
      {
         if (!strcmp(apItems[iIdx], asDir[iTmp]))
         {
            bDir = true;
            break;
         }
         iTmp++;
      }

      if (!bDir)
      {
         iTmp = 0;
         while (asDir1[iTmp])
         {
            if (!strcmp(apItems[iIdx], asDir1[iTmp]))
            {
               bDir = true;
               break;
            }
            iTmp++;
         }

         if (!bDir)
         {
            iTmp = 0;
            while (asDir2[iTmp])
            {
               if (!strcmp(apItems[iIdx], asDir2[iTmp]))
               {
                  bDir = true;
                  break;
               }
               iTmp++;
            }
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one might not be direction
         // ex: 123 S AVE B   --> S is direction and AVE B is street name
         //     321 N AVE     --> N is not a direction
         iSfxCode = GetSfxCodeX(apItems[iIdx+1], acSfx);
         if (iSfxCode && strcmp(apItems[iIdx+1], "HWY") &&
            (iCnt == iIdx+2 || !GetSfxDev(apItems[iIdx+2]))
            )
         {
            strcpy(acStrName, apItems[iIdx]);
            sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
            strcpy(pAdrRec->strSfx, acSfx);
            iIdx += 2;

            if (iCnt==iIdx)
               bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iTmp]);
            iIdx++;
            iSfxCode = 0;
         }
      }

      if (!bRet)
      {
         // Check for fraction
         if (!memcmp(apItems[iIdx], "1/2", 3))
         {
            strcpy(pAdrRec->strSub, "1/2");
            iIdx++;
         }

         acTmp[0] = 0;

         // Check for unit #
         if (apItems[iCnt-1][0] == '#' && strcmp(apItems[iCnt-2], "HWY"))
         {
            // NORTH SCHOOL ST #1
            // 1551 MEMORIAL DR  #1 THRU #4
            if (iCnt > 3 && !strcmp(apItems[iCnt-2], "THRU"))
            {
               sprintf(pAdrRec->Unit, "%s-%s", apItems[iCnt-3], apItems[iCnt-1]+1);
               iCnt -= 3;
            } else
            {
               strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
               iCnt--;

               // If last token is STE or SUITE, remove it.
               if (iCnt > 3 && 
                  (!strcmp(apItems[iCnt-1], "STE") || 
                   !strcmp(apItems[iCnt-1], "SUITE") ||
                   !strcmp(apItems[iCnt-1], "UNIT") ))
                  iCnt--;
            }
         } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "STE") ||
                                 !strcmp(apItems[iCnt-2], "SUITE") ||
                                 !strcmp(apItems[iCnt-2], "APT")))
         {
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
            iCnt -= 2;
         }

         if (!iSfxCode)
         {
            pTmp = NULL;
            while (iIdx < iCnt)
            {
               if (pTmp = strchr(apItems[iIdx], '-'))
                  *pTmp++ = 0;

               iTmp = 0;
               if ((iSfxCode = GetSfxCodeX(apItems[iIdx], acSfx)) &&
                   !(iCnt > iIdx+1 && (iTmp = GetSfxCodeX(apItems[iIdx+1], acTmp))) &&
                   !(!memcmp(apItems[iIdx], "HWY", 3) && apItems[iIdx+1] && isdigit(*apItems[iIdx+1]) )
                  )
               {
                  if (!memcmp(apItems[iIdx-1], "WAY", 3) && !memcmp(apItems[iIdx], "CR", 2) )
                     iSfxCode = GetSfxCodeX("CIR", acSfx);

                  // Format sfxCode
                  sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
                  strcpy(pAdrRec->strSfx, acSfx);
                  iIdx++;
                  break;
               } else
               {
                  if (!memcmp(apItems[iIdx], "LN", 2))
                     strcat(acStrName, "LANE");
                  else
                     strcat(acStrName, apItems[iIdx]);

                  iIdx++;
                  if (iIdx < iCnt)
                     strcat(acStrName, " ");

                  if (pTmp)
                     break;
               }
            }

            if (pTmp)
            {
               strcpy(acCity, pTmp);
               strcat(acCity, " ");
            }

            // Check for multiple address in strName
            if (pTmp = strchr(acStrName, '&'))
               *pTmp = 0;
         }

         // Left over could be city name, unit#, or notes
         while (iIdx < iCnt && *apItems[iIdx] != '&')
         {
            strcat(acCity, apItems[iIdx++]);
            strcat(acCity, " ");
         }
      }

      if (acStrName[0])
      {
         replCharEx(acStrName, ',', ' ');
         blankRem(acStrName);
         strncpy(pAdrRec->strName, acStrName, SIZ_M_STREET);
         if (pTmp = strchr(acCity, '-'))
            *pTmp++ = 0;
         if (!memcmp(acCity, "SP ", 3) && isdigit(acCity[3]))
         {
            strncpy(pAdrRec->Unit, acCity, SIZ_M_UNIT);
            if (pTmp && *pTmp)
               strcpy(acCity, pTmp);
         }

         if (acCity[0])
            strcpy(pAdrRec->City, acCity);
      } else
         memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   }
}

/*****************************************************************************/

void Mpa_ParseSAdr(ADR_REC *pAdrRec, LPCSTR pAddr)
{
   char  acAdr[64], acStrName[64], acSfx[8], acCity[32], acTmp[128];
   char  *pTmp, *pTmp1, *apItems[16];
   int   iStrNo, iTmp, iCnt, iDir, iSfxCode, iIdx;
   bool  bDir, bRet, bAnd;

   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   acCity[0]=acStrName[0] = 0;
   strcpy(acTmp, pAddr);

   iStrNo = atoi(acTmp);

}

