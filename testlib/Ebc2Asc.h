#if !defined(AFX_EBC2ASC_H__10AE5807_43B6_4251_9AC8_4C78438B6438__INCLUDED_)
#define AFX_EBC2ASC_H__10AE5807_43B6_4251_9AC8_4C78438B6438__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Ebc2Asc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Ebc2Asc dialog

class Ebc2Asc : public CDialog
{
// Construction
public:
	Ebc2Asc(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Ebc2Asc)
	enum { IDD = IDD_EBC2ASC };
	CButton	m_Cancel;
	CButton	m_OK;
	CString	m_strInfile;
	CString	m_strOutfile;
	int		m_iRecLen;
	BOOL	   m_bAddCrLf;
	CString	m_strDefFile;
	//}}AFX_DATA

   CString  m_strIni;

   int F_Ebc2Asc(LPCSTR strInfile, LPCSTR strOutfile, LPCSTR strDeffile, int iRecLen);
   int R_Ebc2Asc(LPCSTR strInRec, LPSTR strOutRec);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Ebc2Asc)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Ebc2Asc)
	afx_msg void OnExit();
	afx_msg void OnConvert();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EBC2ASC_H__10AE5807_43B6_4251_9AC8_4C78438B6438__INCLUDED_)
