#include "stdafx.h"
#include "Prodlib.h"
#include "testlib/Utils.h"
#include "R01.h"

typedef struct _tSuffix
{
   char  acSuffix[8];
   char  acOrgSfx[8];
   int   iSfxID;
   int   iLen;
} SUFFIX;

char *asDir[]=
{
   "N", "S", "E", "W", "NE", "NW", "SE", "SW", NULL
};

char *asDir1[]=
{
   "NORTH", "SOUTH", "EAST", "WEST", NULL
};

char *asDir2[]=
{
   "NO", "SO", NULL
};

#define  MAX_SUFFIX     256

//char  asSuffixTbl[MAX_SUFFIX][8];
SUFFIX  asSuffixTbl[MAX_SUFFIX];
int   iNumSuffixes;
bool  ValidateSfx(char *pSuffix);

/*****************************************************************************/

bool isNumber(char *pNumber)
{
   bool bRet=false;

   while (*pNumber)
   {
      if (isdigit(*pNumber) || *pNumber == '-')
         bRet = true;
      else
      {
         bRet = false;
         break;
      }
      pNumber++;
   }

   return bRet;
}

/******************************** LoadSuffixTbl ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int LoadSuffixTbl(char *pFilename)
{
   char  acTmp[_MAX_PATH], *pTmp;
   int   iRet=0, iTmp;
   FILE  *fd;

   fd = fopen(pFilename, "r");
   asSuffixTbl[0].acSuffix[0] = 0;              // Initialize first entry
   asSuffixTbl[0].acOrgSfx[0] = 0;
   asSuffixTbl[0].iLen = 0;

   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iRet = atoi(acTmp);
      if (iRet < 10 || iRet >= MAX_SUFFIX)
      {
         //LogMsg("Suffix table may be bad.  Please verify: %s", pFilename);
         fclose(fd);
         return 0;
      }

      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      for (iTmp = 1; iTmp < iRet; iTmp++)
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            myTrim(acTmp);
            //if (bUseXlat)
            {
               pTmp = strchr(acTmp, ',');
               *pTmp = 0;
               strcpy(asSuffixTbl[iTmp].acOrgSfx, ++pTmp);
               strcpy(asSuffixTbl[iTmp].acSuffix, acTmp);
               asSuffixTbl[iTmp].iLen = strlen(asSuffixTbl[iTmp].acSuffix);
            }
            strcpy(asSuffixTbl[iTmp].acSuffix, acTmp);
            asSuffixTbl[iTmp].iLen = strlen(asSuffixTbl[iTmp].acSuffix);
         } else
            break;
      }
      if (iTmp != iRet)
      {
         //LogMsg("Bad number of suffixes loaded %d.  Please verify %s", iTmp, pFilename);
         iRet = 0;
      }

      fclose(fd);
   } //else
      //LogMsg("Error opening suffix table %s", pFilename);

   iNumSuffixes = iRet -1;
   return iNumSuffixes;
}

/********************************* GetSfxCode ********************************
 *
 * Return suffix string.
 *
 *****************************************************************************/

char  *GetSfxStr(int iSfxIdx)
{
   char *pRet = NULL;

   if (iSfxIdx > 0 && iSfxIdx <= iNumSuffixes)
      pRet = (char *)&asSuffixTbl[iSfxIdx].acSuffix[0];

   return pRet;
}

/********************************* GetSfxCode ********************************
 *
 * Return suffix index.  If not found, return 0.
 *
 *****************************************************************************/

int GetSfxCode(char *pSuffix)
{
   int   iTmp, iRet=0;

   if (!*pSuffix || *pSuffix == ' ')
      return iRet;

   for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
   {
      //if (!memcmp(pSuffix, asSuffixTbl[iTmp].acSuffix, asSuffixTbl[iTmp].iLen))
      if (!strcmp(pSuffix, asSuffixTbl[iTmp].acSuffix))
      {
         iRet = iTmp;
         break;
      }
   }

   return iRet;
}

/********************************* GetSfxCodeX *******************************
 *
 * Return suffix index and translated suffix string.  If not found, return 0.
 * To use this function, make sure to set UseSfxDev=Y in ini file under each
 * county section.
 *
 *****************************************************************************/

int GetSfxCodeX(char *pOrgSfx, char *pSuffix)
{
   int   iTmp, iRet=0;

   *pSuffix = 0;
   if (!*pOrgSfx || *pOrgSfx == ' ')
      return iRet;

   for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
   {
      if (!strcmp(pOrgSfx, asSuffixTbl[iTmp].acOrgSfx) )
      {
         strcpy(pSuffix, asSuffixTbl[iTmp].acSuffix);
         iRet = iTmp;
         break;
      }
   }

   // Translate to standard code
   if (*pSuffix)
      iRet = GetSfxCode(pSuffix);

   return iRet;
}

int GetSfxDev(char *pSuffix)
{
   int   iTmp, iRet=0;

   if (!*pSuffix || *pSuffix == ' ')
      return iRet;

   for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
   {
      if (!strcmp(pSuffix, asSuffixTbl[iTmp].acOrgSfx) )
      {
         iRet = iTmp;
         break;
      }
   }

   return iRet;
}

/********************************** parseAdr1() *****************************
 *
 * Parse address line 1 into individual elements
 *
 ****************************************************************************/

void parseAdr1(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[128], acTmp[32], acSfx[4], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx;
   bool  bRet;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6) ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O BOX", 7))
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove '.'
   iTmp = 0;
   while (*pAdr)
   {
      if (*pAdr != '.')
         acAdr[iTmp++] = *pAdr;
      pAdr++;
   }
   acAdr[iTmp] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strcpy(pAdrRec->strName, pAdr);
      pAdrRec->lStrNum = 0;
   } else
   {
      iIdx = 0;
      if (apItems[0][strlen(apItems[0])-1] <= '9')
      {
         pAdrRec->lStrNum = iTmp;
      }
      strcpy(pAdrRec->strNum, apItems[0]);
      iIdx++;

      // Fraction
      if (strchr(apItems[iIdx], '/'))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][1], SIZ_M_UNIT);
         iIdx++;
      }

      // Dir
      iTmp = 0;
      if (strlen(apItems[iIdx]) < 3)
      {
         strcpy(acSfx, apItems[iIdx]);
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iTmp <= 7)
         {
            if (!strcmp(acSfx, asDir[iTmp]))
               break;
            iTmp++;
         }
      } else
         iTmp = 8;

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      if (iTmp <= 7 && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         {
            strcpy(pAdrRec->strName, apItems[iIdx]);
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, acSfx);
            iIdx++;
         }
      }

      if (!bRet)
      {
         // Not direction, token is str name
         acTmp[0] = 0;

         // Check for unit #
         if (apItems[iCnt-1][0] == '#')
         {
            if (memcmp(apItems[iCnt-2], "SP", 2))
            {
               // Skip #
               strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][1], SIZ_M_UNIT);
               iCnt--;

               // If last token is STE or SUITE, remove it.
               if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
                  || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")  )
                  iCnt--;
            }
         } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
            || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
            || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#"))
         {
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
            iCnt -= 2;
         } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
         {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
            if (!memcmp(apItems[iCnt-1], "SP", 2))
            {
               *pTmp = ' ';
            } else
            {
               *pTmp = 0;
               strncpy(pAdrRec->Unit, pTmp+1, SIZ_M_UNIT);
               if (!strcmp(apItems[iCnt-1], "APT"))
                  iCnt--;
            }
         } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
         {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
            iCnt -= 2;
         } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0)
         {  // 555 N DANEBO 33
            // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
            if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
               && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
               && strcmp(apItems[iCnt-2], "SP"))
            {
               strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
               iCnt--;
            }

            if (pTmp=strchr(apItems[iCnt-1], '#'))
               *pTmp = 0;
         }

         if (iTmp=GetSfxCode(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            while (iIdx < iCnt-1)
            {
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         } else
         {
            while (iIdx < iCnt)
            {
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         }
      }
   }
}

/********************************** parseAdr2() *****************************
 *
 * Parse address line 2 into individual elements
 *
 ****************************************************************************/

void parseAdr2(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[256], *apItems[10];
   int   iCnt, iTmp;

   strcpy(acAdr, pAdr);
   iCnt = ParseString(acAdr, 32, 10, apItems);

   if (iCnt >= 2)
   {
      iCnt--;

      // If last item is not state abbr, it may be part of city name
      if (strlen(apItems[iCnt]) == 2)
         strcpy(pAdrRec->State, apItems[iCnt]);
      else
         iCnt++;

      pAdrRec->City[0] = 0;
      for (iTmp = 0; iTmp < iCnt; iTmp++)
      {
         strcat(pAdrRec->City, apItems[iTmp]);
         if (iTmp < iCnt-1)
            strcat(pAdrRec->City, " ");
      }
   } else
   {
      pAdrRec->State[0] = 0;
      if (iCnt == 1)
         strcpy(pAdrRec->City, apItems[0]);
      else
         pAdrRec->City[0] = 0;
   }
}

void parseAdr2_1(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[256], *apItems[10], *pTmp;
   int   iCnt, iTmp, iIdx;

   // Remove comma in address line 1
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      if (*pTmp == ',')
         acAdr[iIdx++] = ' ';
      else
         acAdr[iIdx++] = *pTmp;
      pTmp++;
   }
   acAdr[iIdx++] = 0;

   iCnt = ParseString(acAdr, 32, 10, apItems);

   iTmp = atoi(apItems[iCnt-1]);
   if (iTmp > 0)
   {
      iCnt--;
      strcpy(pAdrRec->Zip, apItems[iCnt]);
   }

   if (iCnt >= 2)
   {
      // If last item is not state abbr, it may be part of city name
      if (strlen(apItems[iCnt-1]) == 2)
      {
         iCnt--;
         strcpy(pAdrRec->State, apItems[iCnt]);
      }

      pAdrRec->City[0] = 0;
      for (iTmp = 0; iTmp < iCnt; iTmp++)
      {
         strcat(pAdrRec->City, apItems[iTmp]);
         if (iTmp < iCnt-1)
            strcat(pAdrRec->City, " ");
      }
   } else
   {
      pAdrRec->State[0] = 0;
      if (iCnt == 1)
         strcpy(pAdrRec->City, apItems[0]);
      else
         pAdrRec->City[0] = 0;
   }
}

/******************************** parseAdr1_1() *****************************
 *
 * Parse address line 1 into individual elements.  If no street number, parse
 * street name and suffix.
 *
 ****************************************************************************/

void parseAdr1_1(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acTmp[32], *apItems[16];
   int   iCnt, iTmp, iIdx;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Check for PO Box
   if (!memcmp(pAdr, "PO BOX", 6) || !memcmp(pAdr, "P.O. BOX", 8))
   {
      strcpy(pAdrRec->strName, pAdr);
      return;
   }

   strcpy(acAdr, pAdr);
   iCnt = ParseString(acAdr, ' ', 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   iIdx = 0;
   if (iTmp > 0)
   {
      iIdx++;
      pAdrRec->lStrNum = iTmp;
      strcpy(pAdrRec->strNum, apItems[0]);
   }

   // Dir
   iTmp = 0;
   while (iTmp <= 7)
   {
      if (!strcmp(apItems[iIdx], asDir[iTmp]))
         break;
      iTmp++;
   }

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   if (iTmp < 7 && iCnt > 2)
   {
      // If next item is suffix, then this one is not direction
      if (GetSfxCode(apItems[iIdx+1]))
      {
         strcpy(pAdrRec->strName, apItems[iIdx]);
         strcpy(pAdrRec->strSfx, apItems[iIdx+1]);
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, apItems[iIdx]);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;
      if (GetSfxCode(apItems[iCnt-1]))
      {
         strcpy(pAdrRec->strSfx, apItems[iCnt-1]);
         while (iIdx < iCnt-1)
         {
            strcat(pAdrRec->strName, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(pAdrRec->strName, " ");
         }
      } else
      {
         while (iIdx < iCnt)
         {
            strcat(pAdrRec->strName, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(pAdrRec->strName, " ");
         }
      }
   }
}
/*****************************************************************/

void parseAdr1N(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acTmp[32], *apItems[16];
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot in address line 1
   strcpy(acAdr, pAdr);
   iTmp = replCharEx(acAdr, '.', ' ');
   blankRem(acAdr);

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, apItems[0]);
      return;
   }

   iIdx = 0;
   iTmp = 0;

   // Remove period after direction if exist
   if (apItems[0][1] == '.')
      apItems[0][1] = 0;

   // Dir
   while (asDir[iTmp])
   {
      if (!strcmp(apItems[iIdx], asDir[iTmp]))
         break;
      iTmp++;
   }


   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   if (iTmp <= 7 && iCnt > 1)
   {
      // If next item is suffix, then this one might not be direction
      // ex: 123 S AVE B   --> S is direction and AVE B is street name
      //     321 N AVE     --> N is not a direction
      iSfxCode = GetSfxCode(apItems[iIdx+1]);
      if (iSfxCode && iCnt==iIdx+2)
      {
         strcpy(pAdrRec->strName, apItems[iIdx]);
         //sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         strcpy(pAdrRec->strSfx, apItems[iIdx+1]);
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, apItems[iIdx]);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (iCnt > 2 && (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")))
            iCnt--;
      } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")))
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
         iCnt -= 2;
      }

      iSfxCode = GetSfxCode(apItems[iCnt-1]);
      if (iSfxCode)
      {
         // Multi-word street name with suffix
         //sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         strcpy(pAdrRec->strSfx, apItems[iCnt-1]);
         while (iIdx < iCnt-1)
         {
            strcat(pAdrRec->strName, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(pAdrRec->strName, " ");
         }
      } else
      {
         // Multi-word street name without suffix
         while (iIdx < iCnt)
         {
            strcat(pAdrRec->strName, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(pAdrRec->strName, " ");
         }
      }
   }
}

/*************************************************************************
 *
 * For NEV
 *
 *************************************************************************/

void parseAdr1N_1(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acSfx[32], acTmp[32], *apItems[16], *pTmp;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet, bDir=false;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot in address line 1
   strcpy(acAdr, strupr(pAdr));
   replCharEx(acAdr, ".", ' ');

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, apItems[0]);
      return;
   }

   iIdx = 0;
   iTmp = 0;
   iSfxCode = 0;

   // Dir
   while (asDir[iTmp])
   {
      if (!strcmp(apItems[iIdx], asDir[iTmp]))
      {
         bDir = true;
         break;
      }
      iTmp++;
   }

   if (!bDir)
   {
      iTmp = 0;
      while (asDir1[iTmp])
      {
         if (!strcmp(apItems[iIdx], asDir1[iTmp]))
         {
            bDir = true;
            break;
         }
         iTmp++;
      }

      if (!bDir)
      {
         iTmp = 0;
         while (asDir2[iTmp])
         {
            if (!strcmp(apItems[iIdx], asDir2[iTmp]))
            {
               bDir = true;
               break;
            }
            iTmp++;
         }
      }
   }

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   if (bDir && iCnt > 1)
   {
      // If next item is suffix, then this one might not be direction
      // ex: 123 S AVE B   --> S is direction and AVE B is street name
      //     321 N AVE     --> N is not a direction
      iSfxCode = GetSfxCodeX(apItems[iIdx+1], acSfx);
      if (iSfxCode && strcmp(apItems[iIdx+1], "HWY"))
      {
         strcpy(pAdrRec->strName, apItems[iIdx]);
         sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
         strcpy(pAdrRec->strSfx, acSfx);
         iIdx += 2;

         if (iCnt==iIdx)
            bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, asDir[iTmp]);
         iIdx++;
         iSfxCode = 0;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;

      // Check for unit #
      if (apItems[iCnt-1][0] == '#' && strcmp(apItems[iCnt-2], "HWY"))
      {
         // NORTH SCHOOL ST #1
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (iCnt > 2 && (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")))
            iCnt--;
      } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "STE") ||
                              !strcmp(apItems[iCnt-2], "SUITE") ||
                              !strcmp(apItems[iCnt-2], "APT")))
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
         iCnt -= 2;
      } else if (!memcmp(apItems[iCnt-1], "1/2", 3))
      {
         // WILSON ST 1/2
         strcpy(pAdrRec->strSub, "1/2");
         iCnt--;
      } else if (isdigit(*apItems[iCnt-1]) && strcmp(apItems[iCnt-2], "HIGHWAY") && strcmp(apItems[iCnt-2], "HWY"))
      {
         // MURPHY ST 116-1/2
         // W MAIN ST - 311
         // Drop 2nd number
         if (strchr(apItems[iCnt-1], '/'))
            iCnt--;
         else if (strchr(apItems[iCnt-1], '-'))
         {
            // SO AUBURN ST 1-4
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
            iCnt--;
         } else if (!strcmp(apItems[iCnt-2], "BLDG"))
         {
            // LOMA RICA DR. BLDG 1
            sprintf(acTmp, "BLDG %s", apItems[iCnt-1]);
            strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNIT);
            iCnt -= 2;
         } else
            iCnt--;

         if (*apItems[iCnt-1] == '-')
            iCnt--;
      } else if (*apItems[iCnt-1] == '-')
      {
         // BRIGHTON ST -AB
         // BUTLER ST -562
         if (isalpha(apItems[iCnt-1][1]))
            strncpy(pAdrRec->Unit, &apItems[iCnt-1][1], SIZ_M_UNIT);
         iCnt--;
      } else if (pTmp = strchr(apItems[iCnt-1], '-'))
      {
         // TOWNSEND ST-1/2
         // BUTLER ST-554
         // EAST MAIN ST A-B
         // BITNEY SPRINGS RD-GRANNY
         if (*(pTmp+2) == 0)
         {
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
            iCnt--;
         } else
         {
            if (!memcmp(pTmp+1, "1/2", 3))
               strcpy(pAdrRec->strSub, "1/2");
            *pTmp = 0;
         }
      } else if (*apItems[iCnt-2] == '-')
      {
         // PLEASANT ST - B
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
         iCnt -= 2;
      } else if (pTmp = strchr(apItems[iCnt-2], '-'))
      {
         // RACE ST-APT ABC
         // N BLOOMFIELD-GRNTVILL RD
         if (!memcmp(pTmp+1, "APT", 3))
         {
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
            *pTmp = 0;
         }
      }

      if (!iSfxCode)
      {
         if (iSfxCode = GetSfxCodeX(apItems[iCnt-1], acSfx))
         {
            // Multi-word street name with suffix
            sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
            strcpy(pAdrRec->strSfx, acSfx);
            while (iIdx < iCnt-1)
            {
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         } else if (iCnt > 1 && (iSfxCode = GetSfxCodeX(apItems[iCnt-2], acSfx)))
         {
            // PARK AVE AB
            // EAST MAIN ST A
            // STATE HWY 49
            if (memcmp(acSfx, "HWY", 3))
            {
               sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
               strcpy(pAdrRec->strSfx, acSfx);
               strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
               iCnt -= 2;
            }
            while (iIdx < iCnt)
            {
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         } else
         {
            // Multi-word street name without suffix
            while (iIdx < iCnt)
            {
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         }
      } else if (iIdx < iCnt)
      {
         // MAIN ST BC
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNIT);
      }
   }
}

/******************************** parseAdrND() ******************************
 *
 * Parse address line 1 that has street name and direction only.  Direction comes
 * after street name. It may have strnum and strsfx, so this need to be checked too.
 * Design for CCX.
 *
 * - Extract last word and check if street name has more than one tokens
 * - If suffix is empty, check if last word is suffix ( as in CALIFORNIA N BLVD).
 *   Ignore suffix if '&' present (as in 7TH ST & VAQUEROS AVE)
 * - Check last word for direction
 * - If StrNum is empty, check first word for number (as in 1744-1750 PARKSIDE)
 *
 ****************************************************************************/

void parseAdrND(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], *apItems[16], *pTmp, *pDir;
   int   iCnt, iIdx, iSfxCode;

   // Remove dot, quote and extra space
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      acAdr[iIdx++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' and single quote too
      if (*pTmp == '.' || *pTmp == ',' || *pTmp == '`' || *pTmp == 39)
         pTmp++;
      else if (*pTmp == ' ' && *(pTmp-1) == '#')
         while (*pTmp == ' ') pTmp++;

      if (*pTmp == '$')
         break;
   }
   acAdr[iIdx++] = 0;

   // Check for PO BOX
   if (!memcmp(acAdr, "P 0 BOX", 7) ||
       !memcmp(acAdr, "P O BOX", 7) ||
       !memcmp(acAdr, "PIO BOX", 7) ||
       !memcmp(acAdr, "PO BOIX", 7) ||
       !memcmp(acAdr, "PO BOPX", 7) ||
       !memcmp(acAdr, "PO BOXO", 7) ||
       !memcmp(acAdr, "P OB OX", 7) )
   {
      strcpy(pAdrRec->strName, "PO BOX");
      strcat(pAdrRec->strName, (char *)&acAdr[7]);
      return;
   } else if (!memcmp(acAdr, "P OBOX", 6) ||
       !memcmp(acAdr, "PO BIX", 6) ||
       !memcmp(acAdr, "PO BPX", 6) ||
       !memcmp(acAdr, "PO BXO", 6) ||
       !memcmp(acAdr, "PO OBX", 6) ||
       !memcmp(acAdr, "PO POX", 6) )
   {
      strcpy(pAdrRec->strName, "PO BOX");
      strcat(pAdrRec->strName, (char *)&acAdr[6]);
      return;
   } else if (!memcmp(acAdr, "P BOX", 5) || !memcmp(acAdr, "POBOX", 5) )
   {
      strcpy(pAdrRec->strName, "PO BOX");
      strcat(pAdrRec->strName, (char *)&acAdr[5]);
      return;
   }

   // Parse str name
   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, pAdr);
      return;
   }

   // Check suffix
   if (pAdrRec->strSfx[0] < 'A' && *apItems[iCnt-1] >= 'A')
   {
      if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
      {
         strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
         iCnt -= 1;
      }
   }

   iIdx = 0;
   // Check for StrNum
   if (!pAdrRec->lStrNum && iCnt > 1)
   {
      if (isNumber(apItems[iIdx]))
      {
         pAdrRec->lStrNum = atol(apItems[iIdx]);
         iIdx++;
      }
   }

   // Check for unit#
   if (pAdrRec->Unit[0] <= ' ' && pAdrRec->lStrNum > 0)
   {
      if (*apItems[iIdx] == '#')
         strcpy(pAdrRec->Unit, apItems[iIdx++]);
      else if (strlen(apItems[iIdx]) == 1)
      {
         if (*apItems[iIdx] >= 'A' && *apItems[iIdx] <= 'D' && !GetSfxDev(apItems[iIdx+1]) )
            strcpy(pAdrRec->Unit, apItems[iIdx++]);
      }
   }

   if (iCnt > iIdx+1)
   {
      pDir = apItems[iIdx];

      // Check for pre direction
      if ((strlen(pDir) == 1) && (*pDir=='N' || *pDir=='S' || *pDir=='E' || *pDir=='W') )
      {
         pAdrRec->strDir[0] = *pDir;
         iIdx++;
      }
   }

   if (iCnt > iIdx+1)
   {
      pDir = apItems[iCnt-1];
      pTmp = apItems[iCnt-2];

      // Check for post direction
      if ((strlen(pTmp) == 1) && (*pTmp == 'N' || *pTmp == 'S') &&
          (strlen(pDir) == 1) && (*pDir=='E' || *pDir=='W') )
      {
         strcat(pTmp, pDir);
         iCnt--;
      } else if (pAdrRec->strDir[0] <= ' ' && (strlen(pDir) == 1) && (*pDir=='N' || *pDir=='S' || *pDir=='E' || *pDir=='W') )
      {
         pAdrRec->strDir[0] = *pDir;
         iCnt -= 1;
      }
   }

   // Check for Suite#
   if (iCnt > iIdx+2 && pAdrRec->Unit[0] <= ' ')
   {
      if (!strcmp(apItems[iCnt-2], "STE") )
      {
         strcpy(pAdrRec->Unit, apItems[iCnt-1]);
         iCnt -= 2;
      } else if (*apItems[iCnt-1] == '#')
      {
         strcpy(pAdrRec->Unit, apItems[iCnt-1]);
         iCnt--;
      }
   }

   // Check suffix again
   if (pAdrRec->strSfx[0] < 'A')
   {
      if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
      {
         strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
         iCnt -= 1;
      }
   }

   if (!strcmp(apItems[iIdx], "RD"))
   {
      strcpy(pAdrRec->strName, "ROAD ");
      iIdx++;
   } else if (!strcmp(apItems[iIdx], "AVE"))
   {
      strcpy(pAdrRec->strName, "AVENUE ");
      iIdx++;
   } else if (!strcmp(apItems[iIdx], "ST") && !strcmp(apItems[iIdx+1], "HWY"))
   {
      strcpy(pAdrRec->strName, "STATE HIGHWAY ");
      iIdx += 2;
   } else if (!strcmp(apItems[iIdx], "-") )
      iIdx++;

   for (; iIdx < iCnt; iIdx++)
   {
      strcat(pAdrRec->strName, apItems[iIdx]);
      strcat(pAdrRec->strName, " ");
   }
}

void parseMAdr1(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[128], acTmp[32], acSfx[4], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6) ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4) ||
       !memcmp(pAddr, "PO BX", 5))
   {
      strncpy(pAdrRec->strName, pAddr, SIZ_M_STREET);
      return;
   }

   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iTmp] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strncpy(pAdrRec->strName, pAddr, SIZ_M_STREET);
      pAdrRec->lStrNum = 0;
   } else
   {
      iIdx = 0;
      if (apItems[0][strlen(apItems[0])-1] <= '9')
      {
         pAdrRec->lStrNum = iTmp;
      }
      strcpy(pAdrRec->strNum, apItems[0]);
      iIdx++;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], sizeof(pAdrRec->Unit));
         iIdx++;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 7)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         if (iTmp=GetSfxDev(apItems[iIdx+1]))
         {
            strcpy(pAdrRec->strName, apItems[iIdx]);
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], sizeof(pAdrRec->Unit));
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM") || !strcmp(apItems[iCnt-2], "SP") )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, sizeof(pAdrRec->Unit));
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0)
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
               iCnt--;
            } else
            {
               sprintf(pAdrRec->strName, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet)
      {
         // Check suffix
         if (iTmp=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               if ((iIdx < iCnt-2) && (iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  break;
               }
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            strcpy(pAdrRec->strName, apItems[iCnt-2]);
            strcat(pAdrRec->strName, " ");
            strcat(pAdrRec->strName, apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1 && (iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  break;
               }

               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         }
      }
   }
}

/*****************************************************************************
 *
 * Design for SDX
 *
 *****************************************************************************/

void parseMAdr1_2(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[128], acTmp[32], acSfx[4], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6) ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4) ||
       !memcmp(pAddr, "PO BX", 5))
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove space between # and unit number
      if (*pAdr == '#' && *(pAdr+1) == ' ')
         pAdr++;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iTmp] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // Check for possible miss keying
   if (strlen(apItems[0]) > 7 && iCnt < 3)
   {
      pTmp = apItems[0];
      while (*pTmp && isdigit(*pTmp))
         pTmp++;
      if (pTmp != apItems[0] && *pTmp && isalpha(*pTmp))
      {
         // Possible 12345MAIN ST
         if (strlen(pTmp) > 2)
         {
            strcpy(pAdrRec->strName, pTmp);
            *pTmp = 0;
         }
      }
   }

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strcpy(pAdrRec->strName, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      if (apItems[0][strlen(apItems[0])-1] > '9')
      {
         // StrSub ?
         pTmp = apItems[0];
         while (*pTmp && isdigit(*pTmp) )
            pTmp++;
         if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
            pTmp++;
         if (*pTmp)
            strcpy(pAdrRec->strSub, pTmp);
      }
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      iIdx = 1;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         iIdx++;
      }

      // Dir
      iDir = 0;
      bDir = false;
      if (iIdx < iCnt)
      {
         if (strlen(apItems[iIdx]) < 3)
         {
            strcpy(acSfx, apItems[iIdx]);
            if (acSfx[1] == '.')
               acSfx[1] = 0;

            while (iDir <= 7)
            {
               if (!strcmp(acSfx, asDir[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         } else
         {
            // Check for long direction
            while (iDir < 4)
            {
               if (!strcmp(apItems[iIdx], asDir1[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && (iCnt > 2) && (iCnt > iIdx+1))
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         //if (iTmp=GetSfxDev(apItems[iIdx+1]))
         if ((iTmp=GetSfxDev(apItems[iIdx+1])) && (memcmp(apItems[iIdx+1], "MT", 2)))
         {
            strcpy(pAdrRec->strName, apItems[iIdx]);
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         if (memcmp(apItems[iCnt-2], "SP", 2))
         {
            // Skip #
            strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
            iCnt--;

            // If last token is STE or SUITE, remove it.
            if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
               || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")  )
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "RM")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#") )
      {
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         iCnt -= 2;
      } else if (!strcmp(apItems[iCnt-2], "SUITE") && iCnt > 3)
      {
         // Prevent case: 4611 SUITE DR HUNTINGTON BEACH
         if (!GetSfxDev(apItems[iCnt-1]))
         {
            sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
            strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
            iCnt -= 2;
         }
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0 && !strchr(apItems[iCnt-1], '/'))
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               iCnt--;
            } else
            {
               sprintf(pAdrRec->strName, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iTmp=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            while (iIdx < iCnt-1)
            {
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            strcpy(pAdrRec->strName, apItems[iCnt-2]);
            strcat(pAdrRec->strName, " ");
            strcat(pAdrRec->strName, apItems[iCnt-1]);
         } else
         {
            while (iIdx < iCnt)
            {
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         }
      }
   }
}

/********************************************************************************/

void Mrn_parseMAdr1(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[128], acTmp[32], acSfx[4], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6) ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4) ||
       !memcmp(pAddr, "PO BX", 5))
   {
      strncpy(pAdrRec->strName, pAddr, SIZ_M_STREET);
      return;
   }

   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iTmp] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strncpy(pAdrRec->strName, pAddr, SIZ_M_STREET);
      pAdrRec->lStrNum = 0;
   } else
   {
      iIdx = 0;
      if (apItems[0][strlen(apItems[0])-1] <= '9')
      {
         pAdrRec->lStrNum = iTmp;
      }
      strcpy(pAdrRec->strNum, apItems[0]);
      iIdx++;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], sizeof(pAdrRec->Unit));
         iIdx++;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 7)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         if ((iTmp=GetSfxDev(apItems[iIdx+1])) && (memcmp(apItems[iIdx+1], "MT", 2)))
         {
            strcpy(pAdrRec->strName, apItems[iIdx]);
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], sizeof(pAdrRec->Unit));
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM") || !strcmp(apItems[iCnt-2], "SP") )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, sizeof(pAdrRec->Unit));
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0)
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
               iCnt--;
            } else
            {
               sprintf(pAdrRec->strName, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet)
      {
         // Check suffix
         if (iTmp=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               if ((iIdx < iCnt-2) && (iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  break;
               }

               if (*apItems[iIdx] == '&' || *apItems[iIdx] == '-')
               {
                  iIdx++;
                  if (iIdx == 2 && iIdx < iCnt-1)
                     iIdx++;
               }
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            strcpy(pAdrRec->strName, apItems[iCnt-2]);
            strcat(pAdrRec->strName, " ");
            strcat(pAdrRec->strName, apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1 && (iTmp=GetSfxDev(apItems[iIdx])) && ((iCnt == iIdx+1) || (iCnt > iIdx+2)) )
               {
                  strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  break;
               }

               if (*apItems[iIdx] == '&' || *apItems[iIdx] == '-')
               {
                  iIdx++;
                  if (iIdx == 2 && iIdx < iCnt-1)
                     iIdx++;
               }
               strcat(pAdrRec->strName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(pAdrRec->strName, " ");
            }
         }
      }
   }
}

