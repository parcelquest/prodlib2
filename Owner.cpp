#include "stdafx.h"
#include "prodlib.h"
#define  MAX_WORDS   16

char *pAndWords[] = {"AND","&",NULL};
char *pCompNames[] = {"DEL","SAN","EL","VAN","BEN","DI","DE","DU","DA","LA","MC","MAC","O","ST","VON",NULL};
char *pTitles[] = {"JR","SR","ESQ","2ND","3RD","4TH","II","III","IV",NULL};
char *pCompanyCodes[] = {"CO  ","CR","PT","DI",NULL};
//char *pFilterWords[] = {"ETAL","CO-TR","ET-AL","(DBA)","TR","TRS",NULL};
//char *pFilterWords[] = {"ETAL","CO-TR","ET-AL","TR","TRS","HEIRS","OF","EST","CUSTODIAN","SUCC",NULL};
char *pFilterWords[] = {"ETAL","CO-TR*","ET-AL","TR","TRS","EST","CUSTODIAN","(DBA)",NULL};
char *pCompanyList[] =
{
   "OF","CO","CORP*","COMPAN*","PROPERT*","INC","ASSOC*","ASSN*","S&L*","CITY","APT*","ABORT*",
   "APPRAI*","BANK","SAVING*","NATION*","INSTITU*","TRUST","TRST","CALIF*","PT*","PARTNER*",
   "FIRST","1ST","LTD","LIMIT*","STATE*","INVEST*","UNION","ESTATE*","TITLE","THE",
   "COMM*","INS*","APART*","VENTURE*","FINANC*","BUILDING","COUNTY","WESTERN","MINIST*","CENTER",
   "DIST*","LP","MORTUAR*","MORTGAGE","FEDERAL","LIVING","FAMILY","HOME","HOMES","BUSINESS*",
   "ENTERPRI*","EVANGELIC*","CATHOLI*","CHURCH","VETERAN*","DEPT","DEPARTM*","HOUSING","ADMIN*",
   "DEVELOP*","SCHOOL*","PACIFIC","SECURIT*","LOAN*","MANAGE*","SHOPPING","MUTUAL","SOCIETY",
   "#*","WAREHOUSE","RESORT","LAND","COMPLEX","CEMETER*","USA","USDA",NULL
};

/****************************************************************************
 *
 * Desc: Matching two string and return index of first character that did
 *       not match.
 *
 ****************************************************************************/

int strMatch(LPCSTR p1, LPCSTR p2)
{
  int i;

  for (i=0; p1[i] == p2[i]; i++);
  if (i>0 && p2[i-1] != ' ')
     for (; i>0 && p2[i-1] != ' '; i--);

  return(i);
}

/****************************************************************************
 *
 * Desc: Check if token is in table
 *
 * Return: return index number+1 if found. 0 otherwise
 *
 ****************************************************************************/

int isInTable(char *str, char **table)
{
   int i, iRet=0;
   int iLen;

   for (i = 0; table[i]; ++i)
   {
      iLen = strlen(table[i])-1;
      if ((table[i][iLen] == '*' && !_strnicmp(str, table[i], iLen)) || !_stricmp(str,table[i]))
      {
         iRet = i+1;
         break;
      }
   }
   return iRet;
}

/****************************************************************************
 *
 * Desc:
 *
 ****************************************************************************/

void Dealloc_Word(char **words,int iCount)
{
   while (iCount > 0)
   {
      iCount--;
      if (words[iCount])
         free(words[iCount]);
   }
   return;
}

/****************************************************************************
 *
 * Desc: Tokenize string into list of words.
 *
 ****************************************************************************/

int Tokenize(char words[MAX_WORDS][64], char *str, char *delims)
{
   int   iWordCnt;
   char  tbuf[64], *p;
   bool  bOpened;

   iWordCnt = 0;
   while (*str)
   {
      p = tbuf;

      // Skip Space and Line Noise
      while (*str && !isgraph(*str))
         ++str;   

      if (!*str)
         break;

      bOpened = false;
      if (strchr(delims,*str))
         *p++ = *str++;
      else
      {
         while (isgraph(*str) && !strchr(delims,*str))
         {
            if (*str == '(')
               bOpened = true;
            if (*str == ')')
               bOpened = false;
            *p++ = *str++;
         }

         while (bOpened && *str)
         {
            if (*str == ')')
               bOpened = false;
            *p++ = *str++;
         }

         /* 1/2 is one token */
         if (*str == '/' && isdigit(*(str-1)) && isdigit(*(str+1)))
         {
            *p++ = *str++;
            while (isgraph(*str) && strchr(delims,*str) == NULL)
               *p++ = *str++;
         }
      }

      //if (iWordCnt < MAX_WORDS)
      //   words[iWordCnt] = (char *)malloc(52);
      //else
      //   return iWordCnt;
      if (iWordCnt >= MAX_WORDS)
         return iWordCnt;

      *p = 0;
      strcpy(&words[iWordCnt][0], tbuf);
      ++iWordCnt;
   }
   return iWordCnt;
}

/****************************************************************************
 *
 * Desc: Merge last name first will merge Owner1 and Owner2 if they share the
 *       same last name.
 *
 ****************************************************************************/

void __stdcall MergeLF(LPCSTR pName1, LPCSTR pName2, LPSTR pMergeName)
{
   char sTmp[64];
   int  MatchPosition = 0;

   strcpy(pMergeName, pName1);
   strcpy(sTmp, pName2);
   MatchPosition = strMatch(pName1, sTmp);
   if (MatchPosition > 0)
   {
      strcat(pMergeName, " & " );
      strcat(pMergeName, (char *)&sTmp[MatchPosition] );
   }
}

/****************************************************************************
 *
 * Desc: Merge first name first for mailing purposes.
 *
 ****************************************************************************/

void __stdcall MergeFF(LPCSTR pName1, LPCSTR pName2, LPSTR pMergeName)
{
   char sTmp2[64], sTmp1[64];
   int  MatchPosition = 0;

   char *pTmp;
   char sLastName[32];
   char sFirstName1[32], sFirstName2[32];

   strcpy(sTmp1, pName1);
   strcpy(sTmp2, pName2);
   rTrim(sTmp1);
   rTrim(sTmp2);
   sLastName[0] = 0;
   sFirstName1[0] = 0;
   sFirstName2[0] = 0;

   //Looking for last name in Name1
   pTmp = strchr(sTmp1, ' ');

   if (pTmp)
   {
      *pTmp = 0;
      strcpy(sLastName, sTmp1);
      strcpy(sFirstName1, ++pTmp);

      if (sTmp2[0] > ' ')
      {
         MatchPosition = strMatch(pName1, sTmp2);
         if (MatchPosition > 0)
         {
            strcpy(sFirstName2, (char *)&sTmp2[MatchPosition]);
         }
      }
   }

   strcpy(pMergeName, sFirstName1);

   if (sFirstName2[0] > ' ')
   {
      strcat(pMergeName," & " );
      strcat(pMergeName, sFirstName2);
   }

   strcat(pMergeName," " );
   strcat(pMergeName, sLastName);
}

/****************************************************************************
 * 
 * This function is the basic owner parser that support all normal situation
 * where names are format with last name first and no ',' after last name
 * as well as special names.
 *
 ****************************************************************************/

void __stdcall ParseOwnerName(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
     LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
     LPSTR pTitle)
{
   char  t,i,andpos,lastlen,*p;
   int   iWordCnt, iSaveCount;
   char  apWords[MAX_WORDS][64], acTmp[64], acOwner[256];
   bool  bSpouseLast;


   *pOwnerFirst = 0;
   *pOwnerMiddle = 0;
   *pOwnerLast = 0;
   *pSpouseFirst = 0;
   *pSpouseMiddle = 0;
   *pSpouseLast = 0;
   if (pTitle)
      *pTitle = 0;

   if (!*pOwner)
      return;

   andpos = 0;
   for (i=0; i < MAX_WORDS; i++)
      apWords[i][0] = 0;

   strcpy(acOwner, pOwner);

   // Replace comma with space
   while (p = strchr(acOwner, ','))
      *p = ' ';

   //iWordCnt = ParseString(acOwner, ' ', 16, words);
   iSaveCount = iWordCnt = Tokenize(apWords, acOwner, "&/");

   // Identify Companies. Names that start with initials, besides O MALLY 
   if (toupper(apWords[0][0]) != 'O' && (strlen(apWords[0]) == 1 || apWords[0][1] == '.'))
      return;

   // Names that start with a digit 
   if (isdigit(apWords[0][0]))
      return;

   // Special case "LLC" and "LP"
   if (strstr(acOwner, "L L C") || strstr(acOwner, " LLC") || strstr(acOwner, " L P"))
      return;

   for (i = 0; i < iWordCnt; ++i)
   {
      if (isInTable(apWords[i], pCompanyList))
         return;
      //else if (i == iWordCnt-2 && !strcmp(apWords[i],"/") && isalpha(apWords[i+1][0]))
      else if (i == iWordCnt-2 && *apWords[i] == '/' && isalpha(apWords[i+1][0]))
      {
         ++i;
         if (isInTable(apWords[i], pCompanyCodes))
            return;
         p = strrchr(acOwner,'/');
         *p = 0;
         iWordCnt -= 2;
      } else if (isInTable(apWords[i], pAndWords))
      {
         if (andpos == 0)
         {
            andpos = i;
            strcpy(apWords[i],"&");
         } else
         {
            // Drop all words after second &
            iWordCnt = i;
            *apWords[i] = 0;
         }
      } else if ((*apWords[i] == '(') || 
                 (*apWords[i] == '*') || 
                 isInTable(apWords[i], pFilterWords))
      {
         for (t = i+1; t < iWordCnt; ++t)
            strcpy(apWords[t-1], apWords[t]);
         --iWordCnt;
         --i;
      }
   }

   // Assume spouse has the same last name with owner
   bSpouseLast = false;

   if (iWordCnt <= 1) /* One Word Names are Companies */
      return;
   else
   {
      /* Load Last Name */
      strcpy(pOwnerLast, apWords[0]);

      /* Check for Compound Names:
       * VAN NUYS JOHN
       */
      lastlen = 1;
      if (isInTable(apWords[0],pCompNames) && (apWords[2] && strlen(apWords[2]) > 1))
      {
         strcat(pOwnerLast, " ");
         if (!strcmp(apWords[0], "DE") && !strcmp(apWords[1], "LA"))
         {  // Case DE LA TORRE GARCIA
            strcat(pOwnerLast, apWords[1]);
            strcat(pOwnerLast, " ");
            strcat(pOwnerLast, apWords[2]);
            lastlen = 3;
         } else
         {
            strcat(pOwnerLast, apWords[1]);
            lastlen = 2;
         }
      } else if (andpos == 0 && iWordCnt == 4 && strlen(apWords[1]) > 1 &&
         strlen(apWords[2]) > 1 && strlen(apWords[3]) == 1)
      {  /* SMITH WESSON JANE S */
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      }

      /* Load First Name */
      if (lastlen < iWordCnt)
      {
         //lastlen++;
         strcpy(pOwnerFirst, apWords[lastlen]);
         //if (strlen(pOwnerFirst) == 1)
         //   strcpy(&pOwnerFirst[1],".");
         if (*pOwnerFirst == '&')
         {
            strcpy(pOwnerFirst, pOwnerLast);
            strcpy(pOwnerLast, " ");
         }
      }

      /* Load Spouse Name */
      if (andpos)
      {
         for (i = andpos+1; i < iWordCnt; ++i)
         {
            if (i > andpos+2 && !bSpouseLast)
               strcat(pSpouseMiddle," ");
            else if (i > andpos+3)
               strcat(pSpouseMiddle," ");

            if (i > andpos+1)
            {
               // Check for title in spouse name
               if (!isInTable(apWords[i], pTitles))
                  strcat(pSpouseMiddle, apWords[i]);
            }

            if (i == andpos+1)
            {
               // Eliminate same last name as in "JENSEN ROLAND J AND JENSEN HELEN M"
               if (andpos > 0 && !strcmp(apWords[i], pOwnerLast))
               {
                  if (i < iWordCnt-1)
                     i++;
                  bSpouseLast = true;
               } else if (iWordCnt >= andpos+4 && strlen(apWords[andpos+1]) > 1)
               {
                  // Set flag to tell spouse has different last name
                  bSpouseLast = true;                 
                  strcpy(pSpouseLast, apWords[i]);
                  i++;
               }
               strcpy(pSpouseFirst, apWords[i]);
            }
         }
      }

      // Look for title and initials
      i = lastlen+1;
      while (andpos > i || (andpos == 0 && iWordCnt > i))
      {
         if (!isInTable(apWords[i], pTitles))
         {
            //if (strlen(apWords[i]) == 1)
            //   strcpy(&apWords[i][1],".");
            if (pOwnerMiddle[0])
               strcat(pOwnerMiddle," ");
            strcat(pOwnerMiddle,apWords[i]);
         } else if (pTitle)
            strcpy(pTitle, apWords[i]);
         ++i;
      }

      // Check for questionable name
      if ((strlen(pOwnerFirst) == 1 && strlen(pOwnerMiddle) > 1 && !strchr(pOwnerMiddle, 32)) || 
         ((strlen(pOwnerFirst) == 2) && (*(pOwnerFirst+1) == '.')) )
      {
         // Reverse owner name
         strcpy(acTmp, pOwnerFirst);
         strcpy(pOwnerFirst, pOwnerLast);
         strcpy(pOwnerLast, pOwnerMiddle);
         strcpy(pOwnerMiddle, acTmp);

         // Added 7/11/2005 for SBX
         if (!*pOwnerLast && *pSpouseLast && pSpouseFirst)
         {
            if (*pSpouseMiddle)
            {
               // WARREN R & MARY LYNN STALEY
               // Reverse spouse also
               strcpy(acTmp, pSpouseFirst);
               strcpy(pSpouseFirst, pSpouseLast);

               // Since both spouses having the same last name, only first owner 
               // can carry it
               strcpy(pOwnerLast, pSpouseMiddle);
               strcpy(pSpouseMiddle, acTmp);
               *pSpouseLast = 0;
            } else if (strlen(pSpouseFirst) > 1)
            {
               // WARREN R & MARIA SMITH
               strcpy(pOwnerLast, pSpouseFirst);
               strcpy(pSpouseFirst, pSpouseLast);
               *pSpouseLast = 0;
            }
         } else if (!strcmp(pOwnerLast, pSpouseMiddle))
         {  // SIEGFRIED O STUEWE & ERIKA K STUEWE
            // Reverse spouse also
            strcpy(pSpouseMiddle, pSpouseFirst);
            strcpy(pSpouseFirst, pSpouseLast);
            *pSpouseLast = 0;
         }
      }

      // Check for: GASTELUM GUADALUPE & JOSE R MONTOYA
      if (strlen(pSpouseLast) > 1 &&  strlen(pSpouseMiddle) > 1 && !strchr(pSpouseMiddle, ' ') && strlen(pSpouseFirst) == 1)
      {
         strcpy(acTmp, pSpouseFirst);
         strcpy(pSpouseFirst, pSpouseLast);
         strcpy(pSpouseLast, pSpouseMiddle);
         strcpy(pSpouseMiddle, acTmp);
      } else if (strlen(pSpouseMiddle) > 1 && !strchr(pSpouseMiddle, ' ') && strlen(pSpouseFirst) == 1)
      {
         strcpy(acTmp, pSpouseFirst);
         strcpy(pSpouseFirst, pSpouseMiddle);
         strcpy(pSpouseMiddle, acTmp);
      }
   }

   //Dealloc_Word(apWords,iSaveCount);
}

/****************************************************************************
 *
 * This function takes of some specific cases and used for the following counties:
 *    LAS
 *
 * Return:
 *    0 : Name parsed.
 *   >0 : Not parsed, use as is.
 *   -1 : NULL input
 *    
 ****************************************************************************/

int __stdcall ParseOwnerName1(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
     LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast)
{
   char  t,i,andpos,lastlen,*p;
   int   iWordCnt, iSaveCount;
   //char  *apWords[MAX_WORDS], acSave[32];
   char  apWords[MAX_WORDS][64], acOwner[256];
   bool  bSpouseLast;

   *pOwnerFirst = 0;
   *pOwnerMiddle = 0;
   *pOwnerLast = 0;
   *pSpouseFirst = 0;
   *pSpouseMiddle = 0;
   *pSpouseLast = 0;

   if (!*pOwner)
      return -1;

   andpos = 0;
   for (i=0; i < MAX_WORDS; i++)
      apWords[i][0] = 0;

   strcpy(acOwner, pOwner);

   // Replace comma with space
   while (p = strchr(acOwner, ','))
      *p = ' ';

   iSaveCount = iWordCnt = Tokenize(apWords, acOwner, "&/");

   // Identify Companies. Names that start with initials, besides O MALLY 
   if (toupper(apWords[0][0]) != 'O' && (strlen(apWords[0]) == 1 || apWords[0][1] == '.'))
      return 1;

   // Names that start with a digit 
   if (isdigit(apWords[0][0]))
      return 1;

   // Special case "LLC" and "LP" - These cases cannot be in pCompanyList because
   // Tokenize will break them apart.
   if ((p=strstr(acOwner, " L L C")) || (p=strstr(acOwner, " LLC")) )
   {
      strcpy(p, " LLC");
      return 1;
   }
   if (p=strstr(acOwner, " L P"))
   {
      strcpy(p, " LP");
      return 1;
   }

   for (i = 0; i < iWordCnt; ++i)
   {
      if (isInTable(apWords[i], pCompanyList))
         return i+1;
      if (isdigit(apWords[i][0]))
         return i+1;
      else if (isInTable(apWords[i], pAndWords))
      {
         if (andpos == 0)
         {
            andpos = i;
            strcpy(apWords[i],"&");
         } else
         {
            // Drop all words after second &
            iWordCnt = i;
            *apWords[i] = 0;
         }
      } else if ((*apWords[i] == '(') || 
                 (*apWords[i] == '*') || 
                 isInTable(apWords[i], pFilterWords))
      {
         for (t = i+1; t < iWordCnt; ++t)
            strcpy(apWords[t-1], apWords[t]);
         --iWordCnt;
         --i;
      }
   }

   // Assume spouse has the same last name with owner
   bSpouseLast = false;

   if (iWordCnt <= 1) /* One Word Names are Companies */
      return 1;
   else
   {
      /* Load Last Name */
      strcpy(pOwnerLast, apWords[0]);

      /* Check for Compound Names:
       * VAN NUYS JOHN
       */
      lastlen = 1;
      if (isInTable(apWords[0],pCompNames) && (apWords[2] && strlen(apWords[2]) > 1))
      {
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      } else if (andpos == 0 && iWordCnt == 4 && strlen(apWords[1]) > 1 &&
         strlen(apWords[2]) > 1 && strlen(apWords[3]) == 1)
      {  /* SMITH WESSON JANE S */
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      }

      /* Load First Name */
      if (lastlen < iWordCnt)
      {
         //lastlen++;
         strcpy(pOwnerFirst, apWords[lastlen]);
         //if (strlen(pOwnerFirst) == 1)
         //   strcpy(&pOwnerFirst[1],".");
         if (*pOwnerFirst == '&')
         {
            strcpy(pOwnerFirst, pOwnerLast);
            strcpy(pOwnerLast, " ");
         }
      }

      /* Load Spouse Name */
      if (andpos)
      {
         for (i = andpos+1; i < iWordCnt; ++i)
         {
            /*  & condition  */
            //if (strlen(apWords[i]) == 1 && apWords[i][0] != '&')
            //   strcpy(&apWords[i][1],".");

            if (i > andpos+2 && !bSpouseLast)
               strcat(pSpouseMiddle," ");
            else if (i > andpos+3)
               strcat(pSpouseMiddle," ");

            if (i > andpos+1)
            {
               // Check for title in spouse name
               if (!isInTable(apWords[i], pTitles))
                  strcat(pSpouseMiddle, apWords[i]);
            }

            if (i == andpos+1)
            {
               // Eliminate same last name as in "JENSEN ROLAND J AND JENSEN HELEN M"
               if (andpos > 0 && !strcmp(apWords[i], pOwnerLast))
               {
                  if (i < iWordCnt-1)
                     i++;
                  bSpouseLast = true;
               } else if (iWordCnt >= andpos+4)
               {
                  // Set flag to tell spouse has different last name
                  bSpouseLast = true;                 
                  strcpy(pSpouseLast, apWords[i]);
                  i++;
               }
               strcpy(pSpouseFirst, apWords[i]);
            }
         }
      }

      // Look for title and initials
      i = lastlen+1;
      while (andpos > i || (andpos == 0 && iWordCnt > i))
      {
         if (!isInTable(apWords[i], pTitles))
         {
            //if (strlen(apWords[i]) == 1)
            //   strcpy(&apWords[i][1],".");
            if (pOwnerMiddle[0])
               strcat(pOwnerMiddle," ");
            strcat(pOwnerMiddle,apWords[i]);
         }
         ++i;
      }

      // Check for questionable name
      if ((strlen(pOwnerFirst)==1) || ((strlen(pOwnerFirst) == 2) && (*(pOwnerFirst+1) == '.')) )
      {
         char acTmp[64];

         // Swap Last to First, First to Middle, and Middle to Last
         strcpy(acTmp, pOwnerFirst);
         strcpy(pOwnerFirst, pOwnerLast);
         strcpy(pOwnerLast, pOwnerMiddle);
         strcpy(pOwnerMiddle, acTmp);
      }
   }
   return 0;
}

/****************************************************************************
 *
 * This function takes of some specific cases and used for the following counties:
 *    DNX, GLE
 * This function takes care of Last, First Middle better than any of the previous.
 * But fails to process Last, First & Last, First.  In this case, 
 * use ParseOwnerName1() instead.
 *
 * Return:
 *    0 : Name parsed.
 *   >0 : Not parsed, use as is.
 *   -1 : NULL input
 *
 *  08/02/2006  sn  Adding pTitle to support SCL.
 *    
 ****************************************************************************/

int __stdcall ParseOwnerName2(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
     LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast, LPSTR pTitle)
{
   char  t,i,andpos,lastlen,*p;
   int   iWordCnt, iSaveCount=0;
   char  apWords[MAX_WORDS][64], acSave[32], acOwner[256];
   bool  bSpouseLast, bNoSwap=false;

   *pOwnerFirst = 0;
   *pOwnerMiddle = 0;
   *pOwnerLast = 0;
   *pSpouseFirst = 0;
   *pSpouseMiddle = 0;
   *pSpouseLast = 0;

   if (!*pOwner)
      return -1;

   acSave[0] = 0;
   andpos = 0;
   for (i=0; i < MAX_WORDS; i++)
      apWords[i][0] = 0;

   strcpy(acOwner, pOwner);

   // Replace comma with space
   while (p = strchr(acOwner, ','))
   {
      // Count words before comma
      *p = 0;
      iSaveCount = CountBytes(acOwner, ' ');

      bNoSwap = true;   // Cannot swap last name
      *p = ' ';
   }

   iWordCnt = Tokenize(apWords, acOwner, "&/");

   // Identify Companies. Names that start with initials, besides O MALLY 
   if (toupper(apWords[0][0]) != 'O' && (strlen(apWords[0]) == 1 || apWords[0][1] == '.'))
      return 1;

   // Names that start with a digit 
   if (isdigit(apWords[0][0]))
      return 1;

   // Special case "LLC" and "LP" - These cases cannot be in pCompanyList because
   // Tokenize will break them apart.
   if ((p=strstr(acOwner, " L L C")) || (p=strstr(acOwner, " LLC")) )
   {
      strcpy(p, " LLC");
      return 1;
   }
   if (p=strstr(acOwner, " L P"))
   {
      strcpy(p, " LP");
      return 1;
   }

   for (i = 0; i < iWordCnt; ++i)
   {
      if (isInTable(apWords[i], pCompanyList))
         return i+1;
      if (isdigit(apWords[i][0]))
         return i+1;
      else if (isInTable(apWords[i], pAndWords))
      {
         if (andpos == 0)
         {
            andpos = i;
            strcpy(apWords[i],"&");
         } else
         {
            // Drop all words after second &
            iWordCnt = i;
            *apWords[i] = 0;
         }
      } else if ((*apWords[i] == '(') || 
                 (*apWords[i] == '*') || 
                 isInTable(apWords[i], pFilterWords))
      {
         for (t = i+1; t < iWordCnt; ++t)
            strcpy(apWords[t-1], apWords[t]);
         --iWordCnt;
         --i;
      }
   }

   // Assume spouse has the same last name with owner
   bSpouseLast = false;

   if (iWordCnt <= 1) /* One Word Names are Companies */
      return 1;
   else
   {
      /* Load Last Name */
      strcpy(pOwnerLast, apWords[0]);

      /* Check for Compound Names:
       * VAN NUYS JOHN
       */
      lastlen = 1;
      if (iSaveCount > 0)
      {
         while (iSaveCount > 0)
         {
            strcat(pOwnerLast, " ");
            strcat(pOwnerLast, apWords[lastlen++]);
            iSaveCount--;
         }
      } else if (isInTable(apWords[0],pCompNames) && (apWords[2] && strlen(apWords[2]) > 1))
      {
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      } else if (andpos == 0 && iWordCnt == 4 && strlen(apWords[1]) > 1 &&
         strlen(apWords[2]) > 1 && strlen(apWords[3]) == 1)
      {  /* SMITH WESSON JANE S */
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      }

      /* Load First Name */
      if (lastlen < iWordCnt)
      {
         //lastlen++;
         strcpy(pOwnerFirst, apWords[lastlen]);
         //if (strlen(pOwnerFirst) == 1)
         //   strcpy(&pOwnerFirst[1],".");
         if (*pOwnerFirst == '&')
         {
            strcpy(pOwnerFirst, pOwnerLast);
            strcpy(pOwnerLast, " ");
         }
      }

      /* Load Spouse Name */
      if (andpos)
      {
         for (i = andpos+1; i < iWordCnt; ++i)
         {
            /*  & condition  */
            //if (strlen(apWords[i]) == 1 && apWords[i][0] != '&')
            //   strcpy(&apWords[i][1],".");

            if (i > andpos+2 && !bSpouseLast)
               strcat(pSpouseMiddle," ");
            else if (i > andpos+3)
               strcat(pSpouseMiddle," ");

            if (i > andpos+1)
            {
               // Check for title in spouse name
               if (!isInTable(apWords[i], pTitles))
               {
                  if (*pSpouseMiddle > ' ')
                     strcat(pSpouseMiddle," ");                
                  strcat(pSpouseMiddle, apWords[i]);
               } else if (pTitle)
               {
                  strcpy(pTitle, "2");
                  strcat(pTitle, apWords[i]);
               }
            }

            if (i == andpos+1)
            {
               // Eliminate same last name as in "JENSEN ROLAND J AND JENSEN HELEN M"
               if (andpos > 0 && !strcmp(apWords[i], pOwnerLast))
               {
                  if (i < iWordCnt-1)
                     i++;
                  bSpouseLast = true;
               } else if (iWordCnt >= andpos+4)
               {
                  // Set flag to tell spouse has different last name
                  bSpouseLast = true;                 
                  strcpy(pSpouseLast, apWords[i]);
                  i++;
               }
               strcpy(pSpouseFirst, apWords[i]);
            }
         }
      }

      // Look for title and initials
      i = lastlen+1;
      while (andpos > i || (andpos == 0 && iWordCnt > i))
      {
         if (!isInTable(apWords[i], pTitles))
         {
            if (pOwnerMiddle[0])
               strcat(pOwnerMiddle," ");
            strcat(pOwnerMiddle,apWords[i]);
         }  else if (pTitle)
         {
            strcpy(pTitle, "1");
            strcat(pTitle, apWords[i]);
         }
         ++i;
      }

      // Check for questionable name
      if ((strlen(pOwnerFirst)==1) || ((strlen(pOwnerFirst) == 2) && (*(pOwnerFirst+1) == '.')) )
      {
         char acTmp[64];

         if (bNoSwap)
         {
            // Don't swap last name.  Swap first and middle only
            strcpy(acTmp, pOwnerFirst);
            strcpy(pOwnerFirst, pOwnerMiddle);
            strcpy(pOwnerMiddle, acTmp);
         } else
         {
            // Swap Last to First, First to Middle, and Middle to Last
            strcpy(acTmp, pOwnerFirst);
            strcpy(pOwnerFirst, pOwnerLast);
            strcpy(pOwnerLast, pOwnerMiddle);
            strcpy(pOwnerMiddle, acTmp);
         }
      }
   }
   return 0;
}

/****************************************************************************
 *
 * This function is the same as ParseOwnerName() except that it doesn't support
 * '&' for owner with different last name.  It's assumed that word after '&'
 * is spouse first name and everything after that is spouse middle name.
 * Ex: Last First Middle & First Middle
 *
 * This has been tested for the following counties: SCR, ORG, SBX
 *    
 ****************************************************************************/

int __stdcall ParseOwnerName3(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
     LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
     LPSTR pTitle)
{
   char  *p;
   int   t, i, andpos, lastlen, iWordCnt, iSaveCount;
   char  apWords[MAX_WORDS][64], acOwner[256];
   bool  bSpouseLast, bNoChg=false;

   *pOwnerFirst = 0;
   *pOwnerMiddle = 0;
   *pOwnerLast = 0;
   *pSpouseFirst = 0;
   *pSpouseMiddle = 0;
   *pSpouseLast = 0;
   if (pTitle)
      *pTitle = 0;

   if (!*pOwner)
      return -1;

   andpos = 0;
   for (i=0; i < MAX_WORDS; i++)
      apWords[i][0] = 0;

   strcpy(acOwner, pOwner);

   // Replace comma with space
   while (p = strchr(acOwner, ','))
   {
      *p = ' ';
      bNoChg = true;
   }

   //iWordCnt = ParseString(acOwner, ' ', 16, words);
   iSaveCount = iWordCnt = Tokenize(apWords, acOwner, "&/");

   // Identify Companies. Names that start with initials, besides O MALLY 
   if (toupper(apWords[0][0]) != 'O' && (strlen(apWords[0]) == 1 || apWords[0][1] == '.'))
      return 1;

   // Names that start with a digit 
   if (isdigit(apWords[0][0]))
      return 1;

   // Special case "LLC" and "LP"
   if (strstr(acOwner, "L L C") || strstr(acOwner, " LLC") || 
      !memcmp((char *)&acOwner[strlen(acOwner)-4], " L P", 4))
      return 1;

   for (i = 0; i < iWordCnt; ++i)
   {
      if (isInTable(apWords[i], pCompanyList))
         return 1;
      //else if (i == iWordCnt-2 && !strcmp(apWords[i],"/") && isalpha(apWords[i+1][0]))
      else if (i == iWordCnt-2 && *apWords[i] == '/' && isalpha(apWords[i+1][0]))
      {
         ++i;
         if (isInTable(apWords[i], pCompanyCodes))
            return 1;
         p = strrchr(acOwner,'/');
         *p = 0;
         iWordCnt -= 2;
      } else if (isInTable(apWords[i], pAndWords))
      {
         if (andpos == 0)
         {
            andpos = i;
            strcpy(apWords[i],"&");
         } else
         {
            // Drop all words after second &
            iWordCnt = i;
            *apWords[i] = 0;
         }
      } else if ((*apWords[i] == '(') || 
                 (*apWords[i] == '*') || 
                 isInTable(apWords[i], pFilterWords))
      {
         for (t = i+1; t < iWordCnt; ++t)
            strcpy(apWords[t-1], apWords[t]);
         --iWordCnt;
         --i;
      }
   }

   // Assume spouse has the same last name with owner
   bSpouseLast = false;

   // One Word Names can be Companies
   if (iWordCnt <= 1)  
      return 1;
   else
   {
      // Load Last Name
      strcpy(pOwnerLast, apWords[0]);

      // Check for Compound Names: MC DONALD JIMMY or VAN NUYS JOHN
      // or MC DONALD W K & WENDY
      lastlen = 1;
      if (isInTable(apWords[0],pCompNames) && 
          ((apWords[2] && strlen(apWords[2]) > 1) || (apWords[3] && andpos > 3) )
         )
      {
         strcat(pOwnerLast, " ");
         if (!strcmp(apWords[0], "DE") && !strcmp(apWords[1], "LA"))
         {  // Case DE LA TORRE GARCIA
            strcat(pOwnerLast, apWords[1]);
            strcat(pOwnerLast, " ");
            strcat(pOwnerLast, apWords[2]);
            lastlen = 3;
         } else
         {
            strcat(pOwnerLast, apWords[1]);
            lastlen = 2;
         }
      } else if (andpos == 0 && iWordCnt == 4 && strlen(apWords[1]) > 1 &&
         strlen(apWords[2]) > 1 && strlen(apWords[3]) == 1 && !bNoChg)
      {  // SMITH WESSON JANE S 
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      }

      // Load First Name 
      if (lastlen < iWordCnt)
      {
         //lastlen++;
         strcpy(pOwnerFirst, apWords[lastlen]);
         //if (strlen(pOwnerFirst) == 1)
         //   strcpy(&pOwnerFirst[1],".");
         if (*pOwnerFirst == '&')
         {
            strcpy(pOwnerFirst, pOwnerLast);
            strcpy(pOwnerLast, " ");
         }
      }

      // Load Spouse Name
      if (andpos)
      {
         for (i = andpos+1; i < iWordCnt; ++i)
         {
            if (i > andpos+2 && !bSpouseLast)
               strcat(pSpouseMiddle," ");
            else if (i > andpos+3 && *pSpouseMiddle > ' ')
               strcat(pSpouseMiddle," ");

            if (i > andpos+1)
            {
               // Check for title in spouse name - Fernando drops them
               // If we want to keep spouse title, uncomment the if statement
               if (!isInTable(apWords[i], pTitles))
                  strcat(pSpouseMiddle, apWords[i]);
            }

            if (i == andpos+1)
            {
               // Eliminate same last name as in "JENSEN ROLAND J AND JENSEN HELEN M"
               //                             or "LA ROSA JOHN M AND LA ROSA MARY S"
               if (andpos > 0)
               {
                  if (!strcmp(apWords[i], pOwnerLast))
                  {
                     if (i < iWordCnt-1)
                        i++;
                     bSpouseLast = true;
                  } else if (isInTable(apWords[i], pCompNames) )
                  {
                     if (i < iWordCnt-2 && strstr(pOwnerLast, apWords[i+1]))
                     {
                        i += 2;
                        bSpouseLast = true;
                     }

                  }
               }
               strcpy(pSpouseFirst, apWords[i]);
            }

         }

      }

      // Look for title and initials
      i = lastlen+1;
      while (andpos > i || (andpos == 0 && iWordCnt > i))
      {
         if (!isInTable(apWords[i], pTitles))
         {
            //if (strlen(apWords[i]) == 1)
            //   strcpy(&apWords[i][1],".");
            if (pOwnerMiddle[0])
               strcat(pOwnerMiddle," ");
            strcat(pOwnerMiddle,apWords[i]);
         } else if (pTitle)
            strcpy(pTitle, apWords[i]);
         ++i;
      }

      // Check for questionable name
      if ((strlen(pOwnerFirst) == 1 && strlen(pOwnerMiddle) > 1) || 
         ((strlen(pOwnerFirst) == 2) && (*(pOwnerFirst+1) == '.')) )
      {
         char acTmp[64];

         // Reverse owner name
         strcpy(acTmp, pOwnerFirst);
         strcpy(pOwnerFirst, pOwnerLast);
         strcpy(pOwnerLast, pOwnerMiddle);
         strcpy(pOwnerMiddle, acTmp);

         // Added 7/11/2005 for SBX
         if (!*pOwnerLast && *pSpouseLast && pSpouseFirst)
         {
            if (*pSpouseMiddle)
            {
               // WARREN R & MARY LYNN STALEY
               // Reverse spouse also
               strcpy(acTmp, pSpouseFirst);
               strcpy(pSpouseFirst, pSpouseLast);

               // Since both spouses having the same last name, only first owner 
               // can carry it
               strcpy(pOwnerLast, pSpouseMiddle);
               strcpy(pSpouseMiddle, acTmp);
               *pSpouseLast = 0;
            } else if (strlen(pSpouseFirst) > 1)
            {
               // WARREN R & MARIA SMITH
               strcpy(pOwnerLast, pSpouseFirst);
               strcpy(pSpouseFirst, pSpouseLast);
               *pSpouseLast = 0;
            }
         } else if (!strcmp(pOwnerLast, pSpouseMiddle))
         {  // SIEGFRIED O STUEWE & ERIKA K STUEWE
            // Reverse spouse also
            strcpy(pSpouseMiddle, pSpouseFirst);
            strcpy(pSpouseFirst, pSpouseLast);
            *pSpouseLast = 0;
         }
      }
   }

   return 0;
}

/****************************************************************************
 *
 * This function is similar to ParseOwnerName1() with enhancement to handle Title
 * compound last name.  This function is designed to take care of the following
 * formats:
 *    Last First Middle & Last First Middle
 *    Last First Middle & Last First
 *    Last First & Last First
 *    
 * and used for the following counties: GLE
 *
 * Return:
 *    0 : Name parsed.
 *   >0 : Not parsed, use as is.
 *   -1 : NULL input
 *    
 ****************************************************************************/

int __stdcall ParseOwnerName4(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
     LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
     LPSTR pTitle)
{
   char  t,i,andpos,lastlen,*p;
   int   iWordCnt, iSaveCount;
   char  apWords[MAX_WORDS][64], acOwner[256];
   bool  bSpouseLast;

   *pOwnerFirst = 0;
   *pOwnerMiddle = 0;
   *pOwnerLast = 0;
   *pSpouseFirst = 0;
   *pSpouseMiddle = 0;
   *pSpouseLast = 0;

   if (!*pOwner)
      return -1;

   andpos = 0;
   for (i=0; i < MAX_WORDS; i++)
      apWords[i][0] = 0;

   strcpy(acOwner, pOwner);

   // Replace comma with space
   while (p = strchr(acOwner, ','))
      *p = ' ';

   iSaveCount = iWordCnt = Tokenize(apWords, acOwner, "&/");

   // Identify Companies. Names that start with initials, besides O MALLY 
   if (toupper(apWords[0][0]) != 'O' && (strlen(apWords[0]) == 1 || apWords[0][1] == '.'))
      return 1;

   // Names that start with a digit 
   if (isdigit(apWords[0][0]))
      return 1;

   // Special case "LLC" and "LP" - These cases cannot be in pCompanyList because
   // Tokenize will break them apart.
   if ((p=strstr(acOwner, " L L C")) || (p=strstr(acOwner, " LLC")) )
   {
      strcpy(p, " LLC");
      return 1;
   }
   if (p=strstr(acOwner, " L P"))
   {
      strcpy(p, " LP");
      return 1;
   }

   for (i = 0; i < iWordCnt; ++i)
   {
      if (isInTable(apWords[i], pCompanyList))
         return i+1;
      if (isdigit(apWords[i][0]))
         return i+1;
      else if (isInTable(apWords[i], pAndWords))
      {
         if (andpos == 0)
         {
            andpos = i;
            strcpy(apWords[i],"&");
         } else
         {
            // Drop all words after second &
            iWordCnt = i;
            *apWords[i] = 0;
         }
      } else if ((*apWords[i] == '(') || 
                 (*apWords[i] == '*') || 
                 isInTable(apWords[i], pFilterWords))
      {
         for (t = i+1; t < iWordCnt; ++t)
            strcpy(apWords[t-1], apWords[t]);
         --iWordCnt;
         --i;
      }
   }

   // Assume spouse has the same last name with owner
   bSpouseLast = false;

   if (iWordCnt <= 1) 
      return 1;
   else
   {
      // Load Last Name
      strcpy(pOwnerLast, apWords[0]);

      // Check for Compound Names: VAN NUYS JOHN
      lastlen = 1;
      if (isInTable(apWords[0],pCompNames) && (apWords[2] && strlen(apWords[2]) > 1))
      {
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      } else if (andpos == 0 && iWordCnt == 4 && strlen(apWords[1]) > 1 &&
         strlen(apWords[2]) > 1 && strlen(apWords[3]) == 1)
      {  // SMITH WESSON JANE S 
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      }

      // Load First Name 
      if (lastlen < iWordCnt)
      {
         //lastlen++;
         strcpy(pOwnerFirst, apWords[lastlen]);
         if (*pOwnerFirst == '&')
         {
            strcpy(pOwnerFirst, pOwnerLast);
            strcpy(pOwnerLast, " ");
         }
      }

      // Load Spouse Name
      if (andpos)
      {
         for (i = andpos+1; i < iWordCnt; ++i)
         {
            //  & condition
            if (i > andpos+1)
            {
               // Check for title in spouse name
               if (!isInTable(apWords[i], pTitles))
               {
                  if (*pSpouseMiddle > ' ')
                     strcat(pSpouseMiddle," ");                
                  strcat(pSpouseMiddle, apWords[i]);
               } else if (pTitle)
               {
                  strcpy(pTitle, "2");
                  strcat(pTitle, apWords[i]);
               }
            } else if (i == andpos+1)
            {
               // Eliminate same last name as in 
               // "JENSEN ROLAND J AND JENSEN HELEN M" or
               // "MC GINLEY GEROLD E & MC GINLEY SHIRLEY"
               if (andpos > 0 && !memcmp(apWords[i], pOwnerLast, strlen(apWords[i])))
               {
                  int iLast1Len, iLast2Len;
                  iLast1Len = strlen(pOwnerLast);
                  iLast2Len = strlen(apWords[i]);

                  if (iLast1Len > iLast2Len)
                  {
                     char acTmp[64];

                     sprintf(acTmp, "%s %s", apWords[i], apWords[i+1]);
                     if (!strcmp(pOwnerLast, acTmp) && i < iWordCnt-2)
                        i += 2;
                     else
                     {  // different last name e.g. MC MARTIN THOMAS E & MC DONALD ROSEMARY P
                        bSpouseLast = true;                 
                        if (isInTable(apWords[i],pCompNames) && i < iWordCnt-2)
                        {
                           strcpy(pSpouseLast, acTmp);
                           i += 2;
                        } else
                        {
                           strcpy(pSpouseLast, apWords[i]);
                           i++;
                        }
                     }
                  } else if (i < iWordCnt-1)
                     i++;                     
               } else if (iWordCnt >= andpos+3)
               {
                  // Set flag to tell spouse has different last name
                  bSpouseLast = true;                 
                  strcpy(pSpouseLast, apWords[i]);
                  i++;
               }
               strcpy(pSpouseFirst, apWords[i]);
            }
         }
      }

      // Look for title and initials
      i = lastlen+1;
      while (andpos > i || (andpos == 0 && iWordCnt > i))
      {
         if (!isInTable(apWords[i], pTitles))
         {
            if (pOwnerMiddle[0])
               strcat(pOwnerMiddle," ");
            strcat(pOwnerMiddle,apWords[i]);
         } else if (pTitle)
         {
            strcpy(pTitle, "1");
            strcat(pTitle, apWords[i]);
         }
         ++i;
      }

      // Check for questionable name
      /*
      if ((strlen(pOwnerFirst)==1) || ((strlen(pOwnerFirst) == 2) && (*(pOwnerFirst+1) == '.')) )
      {
         char acTmp[64];

         // Swap Last to First, First to Middle, and Middle to Last
         strcpy(acTmp, pOwnerFirst);
         strcpy(pOwnerFirst, pOwnerLast);
         strcpy(pOwnerLast, pOwnerMiddle);
         strcpy(pOwnerMiddle, acTmp);
      }
      */
   }
   return 0;
}

/****************************************************************************
 *
 * This function is the same as ParseOwnerName3() except that it won't
 * check for questionable names.  Name is processed as is.
 * This function also takes care of compound last name like "DE LA CRUZ", 
 * "VAN DEN BROEKE",
 * Ex: Last First Middle & First Middle
 *
 * This has been tested for the following counties: GLE, VEN, EDX
 *    
 ****************************************************************************/

int __stdcall ParseOwnerName5(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
     LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
     LPSTR pTitle)
{
   char  t,i,andpos,lastlen,*p;
   int   iWordCnt, iSaveCount;
   char  apWords[MAX_WORDS][64], acOwner[256];
   bool  bSpouseLast, bNoChg=false;

   *pOwnerFirst = 0;
   *pOwnerMiddle = 0;
   *pOwnerLast = 0;
   *pSpouseFirst = 0;
   *pSpouseMiddle = 0;
   *pSpouseLast = 0;
   if (pTitle)
      *pTitle = 0;

   if (!*pOwner)
      return -1;

   andpos = 0;
   for (i=0; i < MAX_WORDS; i++)
      apWords[i][0] = 0;

   strcpy(acOwner, pOwner);

   // Replace comma with space
   while (p = strchr(acOwner, ','))
   {
      *p = ' ';
      bNoChg = true;
   }

   //iWordCnt = ParseString(acOwner, ' ', 16, words);
   iSaveCount = iWordCnt = Tokenize(apWords, acOwner, "&/");

   // Identify Companies. Names that start with initials, besides O MALLY 
   if (toupper(apWords[0][0]) != 'O' && (strlen(apWords[0]) == 1 || apWords[0][1] == '.'))
      return 1;

   // Names that start with a digit 
   if (isdigit(apWords[0][0]))
      return 1;

   // Special case "LLC" and "LP"
   if (strstr(acOwner, "L L C") || strstr(acOwner, " LLC") || strstr(acOwner, " L P"))
      return 1;

   for (i = 0; i < iWordCnt; ++i)
   {
      if (isInTable(apWords[i], pCompanyList))
         return 1;
      else if (i == iWordCnt-2 && *apWords[i] == '/' && isalpha(apWords[i+1][0]))
      {
         ++i;
         if (isInTable(apWords[i], pCompanyCodes))
            return 1;
         p = strrchr(acOwner,'/');
         *p = 0;
         iWordCnt -= 2;
      } else if (isInTable(apWords[i], pAndWords))
      {
         if (andpos == 0)
         {
            andpos = i;
            strcpy(apWords[i],"&");
         } else
         {
            // Drop all words after second &
            iWordCnt = i;
            *apWords[i] = 0;
         }
      } else if ((*apWords[i] == '(') || 
                 (*apWords[i] == '*') || 
                 isInTable(apWords[i], pFilterWords))
      {
         for (t = i+1; t < iWordCnt; ++t)
            strcpy(apWords[t-1], apWords[t]);
         --iWordCnt;
         --i;
      }
   }

   // Assume spouse has the same last name with owner
   bSpouseLast = false;

   // One Word Names can be Companies
   if (iWordCnt <= 1)  
      return 1;
   else
   {
      // Load Last Name
      strcpy(pOwnerLast, apWords[0]);

      // Check for Compound Names: MC DONALD JIMMY or VAN NUYS JOHN
      // or MC DONALD W K & WENDY
      lastlen = 1;
      if (isInTable(apWords[0],pCompNames) && 
          ((apWords[2] && strlen(apWords[2]) > 1) || (apWords[3] && andpos > 3) )
         )
      {
         strcat(pOwnerLast, " ");
         if (!strcmp(apWords[0], "DE") && !strcmp(apWords[1], "LA"))
         {  // Case DE LA TORRE GARCIA
            strcat(pOwnerLast, apWords[1]);
            strcat(pOwnerLast, " ");
            strcat(pOwnerLast, apWords[2]);
            lastlen = 3;
         } else if (!strcmp(apWords[0], "VAN") && !strcmp(apWords[1], "DEN"))
         {  // Case DE LA TORRE GARCIA
            strcat(pOwnerLast, apWords[1]);
            strcat(pOwnerLast, " ");
            strcat(pOwnerLast, apWords[2]);
            lastlen = 3;
         } else
         {
            strcat(pOwnerLast, apWords[1]);
            lastlen = 2;
         }
      } else if (andpos == 0 && iWordCnt == 4 && strlen(apWords[1]) > 1 &&
         strlen(apWords[2]) > 1 && strlen(apWords[3]) == 1 && !bNoChg)
      {  // SMITH WESSON JANE S 
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      }

      // Load First Name 
      if (lastlen < iWordCnt)
      {
         //lastlen++;
         strcpy(pOwnerFirst, apWords[lastlen]);
         if (*pOwnerFirst == '&')
         {
            strcpy(pOwnerFirst, pOwnerLast);
            strcpy(pOwnerLast, " ");
         }
      }

      // Load Spouse Name
      if (andpos)
      {
         for (i = andpos+1; i < iWordCnt; ++i)
         {
            if (i > andpos+2 && !bSpouseLast)
               strcat(pSpouseMiddle," ");
            else if (i > andpos+3)
               strcat(pSpouseMiddle," ");

            if (i > andpos+1)
            {
               // Check for title in spouse name - Fernando drops them
               // If we want to keep spouse title, uncomment the if statement
               if (!isInTable(apWords[i], pTitles))
                  strcat(pSpouseMiddle, apWords[i]);
            }

            if (i == andpos+1)
            {
               // Eliminate same last name as in "JENSEN ROLAND J AND JENSEN HELEN M"
               if (andpos > 0 && !strcmp(apWords[i], pOwnerLast))
               {
                  if (i < iWordCnt-1)
                     i++;
               }
               strcpy(pSpouseFirst, apWords[i]);
            }

         }

      }

      // Look for title and initials
      i = lastlen+1;
      while (andpos > i || (andpos == 0 && iWordCnt > i))
      {
         if (!isInTable(apWords[i], pTitles))
         {
            if (pOwnerMiddle[0])
               strcat(pOwnerMiddle," ");
            strcat(pOwnerMiddle,apWords[i]);
         } else if (pTitle)
            strcpy(pTitle, apWords[i]);
         ++i;
      }

   }

   return 0;
}

/****************************************************************************
 *
 * This function is the same as ParseOwnerName3() except that it will support
 * cases where first name go first.
 * This function can handle following names:
 *    DONALD & SARA SWEET REVOC TR
 *    CINCOTTA ANTONE G JR & FAYE L
 *    ROBERT M BAKMAZ REVOC TRUST 20
 *    PETER I C TURK & RONDA IRIS TU
 *    ENNIS SMITH & FELICIA L. SMITH
 *    ROBERT H. C. AU & PHYLLIS W. A
 *    BARNEY T HOPKINS & DIANA J HOP
 *    BRYCE I. MORRICE, MD & ELLEN S
 *    C.H. OZBUN & DIANE OZBUN
 *    J. CAMERON CAUSEY & DIANE CAUS
 *    KENDERICK C & BETSY J WONG REV
 *    DR ANVAR M VELJI & PARI VELJI
 *    PAUL J. TIAO & KAYE P. HO-TIAO
 *    QUEST JR CHARLES JR & BARBARA
 *    AARON C & PAMELA STEVENSON 198
 *
 *
 * Copy from ParseOwnerName3() and modify for SFX & KIN
 *    
 ****************************************************************************/

int __stdcall ParseOwnerName6(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
     LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
     LPSTR pTitle)
{
   char  t,i,andpos,lastlen,*p;
   int   iWordCnt, iSaveCount;
   char  apWords[MAX_WORDS][64], acTmp[64], acOwner[256];
   bool  bSpouseLast, bNoChg=false, bSwapped=false;

   *pOwnerFirst = 0;
   *pOwnerMiddle = 0;
   *pOwnerLast = 0;
   *pSpouseFirst = 0;
   *pSpouseMiddle = 0;
   *pSpouseLast = 0;
   if (pTitle)
      *pTitle = 0;

   if (!*pOwner)
      return -1;

   andpos = 0;
   for (i=0; i < MAX_WORDS; i++)
      apWords[i][0] = 0;

   strcpy(acOwner, pOwner);

   // Replace comma with space
   while (p = strchr(acOwner, ','))
   {
      *p = ' ';
      bNoChg = true;
   }

   //iWordCnt = ParseString(acOwner, ' ', 16, words);
   iSaveCount = iWordCnt = Tokenize(apWords, acOwner, "&/");

   // Identify Companies. Names that start with initials, besides O MALLY 
   //if (toupper(apWords[0][0]) != 'O' && (strlen(apWords[0]) == 1 || apWords[0][1] == '.'))
   //   return 1;

   // Names that start with a digit 
   if (isdigit(apWords[0][0]))
      return 1;

   // Special case "LLC" and "LP"
   if (strstr(acOwner, "L L C") || strstr(acOwner, " LLC") || 
      !memcmp((char *)&acOwner[strlen(acOwner)-4], " L P", 4))
      return 1;

   // Filter out unused words
   for (i = 0; i < iWordCnt; ++i)
   {
      if (isInTable(apWords[i], pCompanyList))
         return 1;
      //else if (i == iWordCnt-2 && !strcmp(apWords[i],"/") && isalpha(apWords[i+1][0]))
      else if (i == iWordCnt-2 && *apWords[i] == '/' && isalpha(apWords[i+1][0]))
      {
         ++i;
         if (isInTable(apWords[i], pCompanyCodes))
            return 1;
         p = strrchr(acOwner,'/');
         *p = 0;
         iWordCnt -= 2;
      } else if (isInTable(apWords[i], pAndWords))
      {
         if (andpos == 0)
         {
            andpos = i;
            strcpy(apWords[i],"&");
         } else
         {
            // Drop all words after second &
            iWordCnt = i;
            *apWords[i] = 0;
         }
      } else if ((*apWords[i] == '(') || 
                 (*apWords[i] == '*') || 
                 isInTable(apWords[i], pFilterWords))
      {
         for (t = i+1; t < iWordCnt; ++t)
            strcpy(apWords[t-1], apWords[t]);
         --iWordCnt;
         --i;
      }
   }

   // Drop last word if it's a number
   if (atol(apWords[iWordCnt-1]) > 0)
      iWordCnt--;

   // Check for ENNIS SMITH & FELICIA L SMITH and turns into ENNIS & FELICIA L SMITH
   if (andpos && strlen(apWords[andpos-1]) > 1 && !strcmp(apWords[andpos-1], apWords[iWordCnt-1]) )
   {
      for (i = andpos; i < iWordCnt; ++i)
         strcpy(apWords[i-1], apWords[i]);
      *apWords[i-1] = 0;
      andpos--;
      iWordCnt--;
   }

   // Assume spouse has the same last name with owner
   bSpouseLast = false;

   // One Word Names can be Companies
   if (iWordCnt <= 1)  
      return 1;
   else
   {
      // Load Last Name
      strcpy(pOwnerLast, apWords[0]);

      // Check for Compound Names: MC DONALD JIMMY or VAN NUYS JOHN
      lastlen = 1;
      if (isInTable(apWords[0],pCompNames) && (apWords[2] && strlen(apWords[2]) > 1))
      {
         strcat(pOwnerLast, " ");
         if (!strcmp(apWords[0], "DE") && !strcmp(apWords[1], "LA"))
         {  // Case DE LA TORRE GARCIA
            strcat(pOwnerLast, apWords[1]);
            strcat(pOwnerLast, " ");
            strcat(pOwnerLast, apWords[2]);
            lastlen = 3;
         } else
         {
            strcat(pOwnerLast, apWords[1]);
            lastlen = 2;
         }
      } else if (andpos == 0 && iWordCnt == 4 && strlen(apWords[1]) > 1 &&
         strlen(apWords[2]) > 1 && strlen(apWords[3]) == 1 && !bNoChg)
      {  // SMITH WESSON JANE S 
         strcat(pOwnerLast, " ");
         strcat(pOwnerLast, apWords[1]);
         lastlen = 2;
      }

      // Load First Name 
      if (lastlen < iWordCnt)
      {
         //lastlen++;
         strcpy(pOwnerFirst, apWords[lastlen]);
         // BRYCE I MORRICE & ELLEN S
         // DONALD & SARA SWEET
         if (*pOwnerFirst == '&' || strlen(pOwnerFirst) == 1)
         {
            bSwapped = true;
            strcpy(pOwnerFirst, pOwnerLast);
            if (*apWords[lastlen] == '&' || *apWords[lastlen+1] == '&')
            {  // KENDERICK C & BETSY J WONG                                        
               if (*apWords[lastlen] != '&')
                  strcpy(pOwnerMiddle, apWords[lastlen]);

               strcpy(pOwnerLast, apWords[iWordCnt-1]);
               iWordCnt--;
            } else if (apWords[lastlen+1] && strlen(apWords[lastlen+1]) == 1)
            {  
               sprintf(pOwnerMiddle, "%s %s", apWords[lastlen], apWords[lastlen+1]);
               lastlen += 2;
               if ((lastlen) < andpos)
               {  // PETER I C TURK & RONDA IRIS TU
                  strcpy(pOwnerLast, apWords[lastlen]);
               } else
               {  // PETER I C & RONDA IRIS TURK
                  strcpy(pOwnerLast, apWords[iWordCnt-1]);
                  iWordCnt--;
               }
            } else
            {
               strcpy(pOwnerMiddle, apWords[lastlen]);
               strcpy(pOwnerLast, apWords[++lastlen]);
            }
         }
      }

      // Check for incomplete spouse last name
      if (bSwapped && andpos)
      {
         // Compare last word against Owner last name.  If they match more than one character,
         // It might be the same name that has been chopped off.
         p = apWords[iWordCnt-1];
         if (*pOwnerLast == *p && *(pOwnerLast+1) == *(p+1) && iWordCnt >= andpos+2)
            iWordCnt--;
      }

      // Load Spouse Name
      if (andpos)
      {
         for (i = andpos+1; i < iWordCnt; ++i)
         {
            if (i > andpos+2 && !bSpouseLast)
               strcat(pSpouseMiddle," ");
            else if (i > andpos+3 && *pSpouseMiddle > ' ')
               strcat(pSpouseMiddle," ");

            if (i > andpos+1)
            {
               // Check for title in spouse name - Fernando drops them
               // If we want to keep spouse title, uncomment the if statement
               if (!isInTable(apWords[i], pTitles))
                  strcat(pSpouseMiddle, apWords[i]);
            }

            if (i == andpos+1)
            {
               // Eliminate same last name as in "JENSEN ROLAND J AND JENSEN HELEN M"
               //                             or "LA ROSA JOHN M AND LA ROSA MARY S"
               if (andpos > 0)
               {
                  if (!strcmp(apWords[i], pOwnerLast))
                  {
                     if (i < iWordCnt-1)
                        i++;
                     bSpouseLast = true;
                  } else if (isInTable(apWords[i], pCompNames) )
                  {
                     if (i < iWordCnt-2 && strstr(pOwnerLast, apWords[i+1]))
                     {
                        i += 2;
                        bSpouseLast = true;
                     }

                  }
               }
               strcpy(pSpouseFirst, apWords[i]);
            }

         }

         // Check for misplace of first and middle name
         if (strlen(pSpouseFirst) == 1 && strlen(pSpouseMiddle) > 1)
         {
            // Swap first and middle
            strcpy(acTmp, pSpouseMiddle);
            strcpy(pSpouseMiddle, pSpouseFirst);
            strcpy(pSpouseFirst, acTmp);
         }
      }

      // Look for title and initials
      i = lastlen+1;
      while (andpos > i || (andpos == 0 && iWordCnt > i))
      {
         if (!isInTable(apWords[i], pTitles))
         {
            //if (strlen(apWords[i]) == 1)
            //   strcpy(&apWords[i][1],".");
            if (pOwnerMiddle[0])
               strcat(pOwnerMiddle," ");
            strcat(pOwnerMiddle,apWords[i]);
         } else if (pTitle)
            strcpy(pTitle, apWords[i]);
         ++i;
      }

      // Check for questionable name
      if ((strlen(pOwnerFirst) == 1 && strlen(pOwnerMiddle) > 1) || 
         ((strlen(pOwnerFirst) == 2) && (*(pOwnerFirst+1) == '.')) )
      {
         char acTmp[64];

         // Reverse owner name
         strcpy(acTmp, pOwnerFirst);
         strcpy(pOwnerFirst, pOwnerLast);
         strcpy(pOwnerLast, pOwnerMiddle);
         strcpy(pOwnerMiddle, acTmp);

         // Added 7/11/2005 for SBX
         if (!*pOwnerLast && *pSpouseLast && pSpouseFirst)
         {
            if (*pSpouseMiddle)
            {
               // WARREN R & MARY LYNN STALEY
               // Reverse spouse also
               strcpy(acTmp, pSpouseFirst);
               strcpy(pSpouseFirst, pSpouseLast);

               // Since both spouses having the same last name, only first owner 
               // can carry it
               strcpy(pOwnerLast, pSpouseMiddle);
               strcpy(pSpouseMiddle, acTmp);
               *pSpouseLast = 0;
            } else if (strlen(pSpouseFirst) > 1)
            {
               // WARREN R & MARIA SMITH
               strcpy(pOwnerLast, pSpouseFirst);
               strcpy(pSpouseFirst, pSpouseLast);
               *pSpouseLast = 0;
            }
         } else if (!strcmp(pOwnerLast, pSpouseMiddle))
         {  // SIEGFRIED O STUEWE & ERIKA K STUEWE
            // Reverse spouse also
            strcpy(pSpouseMiddle, pSpouseFirst);
            strcpy(pSpouseFirst, pSpouseLast);
            *pSpouseLast = 0;
         }
      }
   }

   return 0;
}

/************************************* swapName ***********************************
 *
 * Assume pOwner is formatted as first middle last.  We have to swap and make it
 * last first middle.
 *
 **********************************************************************************/

LPSTR __stdcall swapName(LPCSTR pOwner, LPSTR pSwapName)
{
   int   iWordCnt, i, t;
   char  apWords[MAX_WORDS][64], acOwner[256];
   char  acFirst[64], acMiddle[64], acLast[64];


   acFirst[0] = 0;
   acMiddle[0] = 0;
   acLast[0] = 0;
   *pSwapName = 0;

   if (!*pOwner)
      return (LPSTR)pOwner;

   for (i=0; i < MAX_WORDS; i++)
      apWords[i][0] = 0;

   strcpy(acOwner, pOwner);
   iWordCnt = Tokenize(apWords, acOwner, "&/");
   if (iWordCnt == 1 || (iWordCnt == 2 && strlen(apWords[1]) == 1))
   {
      strcpy(pSwapName, acOwner);
      return pSwapName;
   }

   if (!memcmp(apWords[iWordCnt-1], "TR", 2) || !memcmp(apWords[iWordCnt-1], "CO-TR", 5))
      iWordCnt--;                   // Filter out TR, TRS

   // Names that start with a digit 
   if (isdigit(apWords[0][0]))
      return (LPSTR)pOwner;

   // Check for filtered word
   for (i = 0; i < iWordCnt; ++i)
   {
      if (isInTable(apWords[i], pFilterWords))
      {
         for (t = i+1; t < iWordCnt; ++t)
            strcpy(apWords[t-1], apWords[t]);
         --iWordCnt;
         --i;
      }
   }

   // Load first Name
   strcpy(acFirst, apWords[0]);

   if (iWordCnt == 2)
   {
      if (strlen(apWords[1]) == 1)
         strcpy(acMiddle, apWords[1]);
      else
         strcpy(acLast, apWords[1]);
   }

   if (iWordCnt > 2)
   {
      // Check for Compound Names: Vincent De la Cruz
      if (!strcmp(apWords[iWordCnt-3], "DE") && 
          !strcmp(apWords[iWordCnt-2], "LA") &&
          strlen(apWords[iWordCnt-1]) > 1)
      {  // Case GARCIA DE LA TORRE 
         strcpy(acLast, "DE LA ");
         strcat(acLast, apWords[iWordCnt-1]);
         iWordCnt -= 3;
      } else if (isInTable(apWords[iWordCnt-2], pCompNames) && strlen(apWords[iWordCnt-1]) > 1)
      {
         sprintf(acLast, "%s %s", apWords[iWordCnt-2], apWords[iWordCnt-1]);
         iWordCnt -= 2;
      } else 
      {  
         strcpy(acLast, apWords[iWordCnt-1]);
         iWordCnt--;
      }
   }
   
   // Load middle name
   for (i = 1; i < iWordCnt; i++)
   {
      strcat(acMiddle, apWords[i]);
      strcat(acMiddle, " ");
   }
   if (i > 1)
      acMiddle[strlen(acMiddle)-1] = 0;

   sprintf(pSwapName, "%s %s %s", acLast, acFirst, acMiddle);
   return pSwapName;
}