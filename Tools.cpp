 // TOOLS.C - Example DLL for Visual Basic applications.

#include "stdafx.h"

#define  GEO_TO_LONG    11930464.70556           

void __stdcall Geo2Long(LPSTR strGeo, LPSTR strLong)
{
   char     acTmp[32];
   double   dVal;
   unsigned long  lVal;

   dVal = atof(strGeo);
   lVal = dVal*GEO_TO_LONG;

   sprintf(acTmp, "%u", lVal);
   strcpy((char *)strLong, acTmp);
}

void __stdcall Long2Geo(LPSTR strLong, LPSTR strGeo)
{
   char     acTmp[32];
   double   dVal;
   ULONG    lVal;

   //lVal = atol(strLong);
   //dVal = lVal / GEO_TO_LONG;
   lVal = (ULONG)atof(strLong);
   dVal = (signed int)lVal / GEO_TO_LONG;

   sprintf(acTmp, "%.6f", dVal);
   strcpy((char *)strGeo, acTmp);
}

// Return Basic pointer to thing passed by reference
// Declare Function GetRefPtr LIB "VBUTIL*" (lpVoid As Any) As Long
DWORD __stdcall GetRefPtr( LPVOID lpVoid )
{
    return (DWORD)lpVoid;
}


// Extract Word's most significant Byte
WORD __stdcall HiByte(WORD w)
{
   return w >> 8;
}

// Extract Word's least significant Byte
WORD __stdcall LoByte(WORD w)
{
   return w & 0xFF;
}

// Combine two Bytes into a Word
WORD __stdcall MakeWord(WORD bHi, WORD bLo)
{
   return ( bHi << 8 ) | ( bLo & 0xFF );
}

// Shift bits of Word right
WORD __stdcall RShiftWord(WORD w, WORD c)
{
   return w >> c;
}

// Shift bits of Word left
WORD __stdcall LShiftWord(WORD w, WORD c)
{
   return w << c;
}

// Extract DWord's most significant Word
WORD __stdcall HiWord(DWORD dw)
{
   return (WORD)( dw >> 16 );
}

// Extract DWord's least significant Word
WORD __stdcall LoWord(DWORD dw)
{
   return (WORD) ( dw & 0xFFFF );
}

// Combines two Words into DWord
DWORD __stdcall MakeDWord(DWORD wHi, DWORD wLo)
{
   return ( wHi << 16 ) | ( wLo & 0xFFFF );
}

// Shift bits of DWord right
DWORD __stdcall RShiftDWord(DWORD dw, unsigned c)
{
   return dw >> c;
}

// Shift bits of DWord left
DWORD __stdcall LShiftDWord(DWORD dw, unsigned c)
{
    return dw << c;
}

// Check for file existence
short __stdcall ExistFile(LPSTR lpFileName)
{
   if (lpFileName == NULL) {
      return FALSE;
   }
   return (!_access(lpFileName, 0));
}

int __stdcall CountBytes(LPSTR lpStr, char c)
{
   int   iRet=0;
   char  *pTmp;

   pTmp = lpStr;
   while (*pTmp)
   {
      if (*pTmp++ == c)
         iRet++;
   }

   return iRet;
}

int __stdcall GetPath(LPCSTR strFullName, LPSTR strPath)
{
   char     acTmp[256], *pTmp;

   strcpy(acTmp, strFullName);
   pTmp = strrchr(acTmp, '\\');
   if (pTmp)
   {
      *pTmp = 0;
      //strcpy(strPath, acTmp);
   } else
      *strPath = 0;
   return 0;
}
