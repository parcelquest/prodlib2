#include "stdafx.h"
#include "prodlib.h"

int __stdcall SN_Encrypt(LPSTR pStr, LPSTR pEncryptStr)
{
   char  acTmp1[256], acTmp2[256];
   char  *pTmp;
   int   iLen, iTmp, iRnd;

   iLen = strlen(pStr);
   if (iLen > 255)
      iLen = 255;

   pTmp = pStr;
   for (iTmp = 0; iTmp < iLen; iTmp++)
   {
      iRnd = abs(rand()*122 + 68);
      acTmp1[iTmp] = (*pTmp++) + iRnd;
      acTmp2[iTmp] = iRnd;
   }

   pTmp = pEncryptStr;
   for (iTmp = 0; iTmp < iLen; iTmp++)
   {
      *pTmp++ = acTmp1[iTmp];
      *pTmp++ = acTmp2[iTmp];
   }
   *pTmp = 0;

   return 0;
}

int __stdcall SN_UnEncrypt(LPSTR pEncryptStr, LPSTR pStr)
{
   char  *pTmp, *pTmp1;
   int   iLen, iTmp, iTmp1;

   iLen = strlen(pEncryptStr)/2;
   pTmp = pEncryptStr;
   pTmp1 = pStr;

   for (iTmp = 0; iTmp < iLen; iTmp++)
   {
      iTmp1 = (*pTmp++);
      *pTmp1++ = iTmp1 - (*pTmp++);
   }
   *pTmp1 = 0;
   return 0;
}

