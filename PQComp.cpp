/***********************************************************************
 * PQComp
 * This module does compress/uncompress text file.
 *
 * Algorithm: 
 *    - PQZ file: ParcelQuest Zip file
 *          First 64 bytes is compress header
 *          Next is the first block of compressed data, then next
 *    - IDX file:
 *          IDX file contains series 8-bytes block.  See IDXBLK for info
 *          First 4-byte is pointer into PQZ file and last 4-byte is block size
 *          
 *
 ***********************************************************************/

#include "stdafx.h"
#include <implode.h>
#include "ProdLib.h"
#include "PQComp.h"
#include "Logs.h"
  
void doCleanUp()
{
   if (outbuf)
      free(outbuf);
   if (inbuf)
      free(inbuf);
   if (workbuf)
      free(workbuf);
  
   if (fdCmpr)
      fclose(fdCmpr);
   if (fdIdx)
      fclose(fdIdx);
   if (fdUnCmpr)
      fclose(fdUnCmpr);

   inbuf = NULL;
   outbuf = NULL;
   workbuf = NULL;
   fdCmpr = (FILE *)NULL;
   fdIdx = (FILE *)NULL;
   fdUnCmpr = (FILE *)NULL;

   // close log file
   close_log();
}

/* Routine to read uncompressed data.  Used only by implode(). 
** This routine reads the uncompressed data that is to be compressed.
*/
unsigned int ReadUnCompressed(char *buff, unsigned int *size, void *Param)
{
   PARAM *Ptr = (PARAM *) Param;

   if (Ptr->UnCompressedSize == 0L)  
   {
      /* This will terminate the compression or extraction process */
      return(0);
   }

   if (Ptr->UnCompressedSize < (unsigned long)*size)
   {
      *size = (unsigned int)Ptr->UnCompressedSize;
   }

   memcpy(buff, Ptr->pSource + Ptr->SourceOffset, *size);
   Ptr->SourceOffset += (unsigned long)*size;
   Ptr->UnCompressedSize -= (unsigned long)*size;
   Ptr->Crc = crc32(buff, size, &Ptr->Crc);

   return(*size);
}

/* Routine to read compressed data.  Used only by explode(). 
** This routine reads the compressed data that is to be uncompressed.
*/

unsigned int ReadCompressed(char *buff, unsigned int *size, void *Param)
{
   PARAM *Ptr = (PARAM *) Param;

   if (Ptr->CompressedSize == 0L)  
   {
      /* This will terminate the compression or extraction process */
      return(0);
   }

   if (Ptr->CompressedSize < (unsigned long)*size)
   {
      *size = (unsigned int)Ptr->CompressedSize;
   }

   memcpy(buff, Ptr->pSource + Ptr->SourceOffset, *size);
   Ptr->SourceOffset += (unsigned long)*size;
   Ptr->CompressedSize -= (unsigned long)*size;

   return(*size);
}

/* Routime to write compressed data.  Used only by implode(). 
** This routine writes the compressed data to a memory buffer.
*/

void WriteCompressed(char *buff, unsigned int *size, void *Param)
{
   PARAM *Ptr = (PARAM *) Param;

   if (Ptr->CompressedSize + (unsigned long)*size > Ptr->BufferSize)
   {
      puts("Compressed data will overflow buffer. Increase size of buffer!");
      exit(1);
   }
   memcpy(Ptr->pDestination + Ptr->DestinationOffset, buff, *size);
   Ptr->DestinationOffset += (unsigned long)*size;
   Ptr->CompressedSize += (unsigned long)*size;
}

/* Routine to write uncompressed data. Used only by explode().
** This routine writes the uncompressed data to a memory buffer.
*/

void WriteUnCompressed(char *buff, unsigned int *size, void *Param)
{
   PARAM *Ptr = (PARAM *) Param;

   if (Ptr->CompressedSize + (unsigned long)*size > Ptr->BufferSize)
   {
      puts("Compressed data will overflow buffer. Increase size of buffer!");
      exit(1);
   }
   memcpy(Ptr->pDestination + Ptr->DestinationOffset, buff, *size);
   Ptr->DestinationOffset += (unsigned long)*size;
   Ptr->UnCompressedSize += (unsigned long)*size;
   Ptr->Crc = crc32(buff, size, &Ptr->Crc);
}

/******************************* getVBlock() *******************************/
// Read buffer for compress
int getRecs(char *pBuf)
{
   int   iRet;
   char  acTmp[2048], *pTmp;

   *pBuf = 0;
   for (iRet = 0; iRet < cmprHdr.iGrpSize && !feof(fdUnCmpr); iRet++)
   {
      pTmp = fgets(acTmp, 2048, fdUnCmpr);
      if (!pTmp)
         break;
      strcat(pBuf, acTmp);
   }

   return iRet;
}

/******************************* getFBlock() *******************************/
// Read buffer for compress
int getRecs(char *pBuf, int iSize)
{
   int   iRet;

   iRet = fread(pBuf, 1, iSize, fdUnCmpr);
   if (iRet >= cmprHdr.iRecSize)
      iRet = iRet/cmprHdr.iRecSize;
   else
      iRet = 0;

   return iRet;
}

/******************************* retrieveRecord() ****************************
 *
 * Return:  0 if database opened without problem.
 *          <0 if error occured.
 *
 *****************************************************************************/
 
int retrieveRecord(char *pBuf, long lRecNum)
{
   static long lLastBlkIndex = -1;

   long  lBlkIndex;
   long  lTmp;
   int   iRet;
   char  *pTmp1, *pTmp2, cTmp;

   // Calculating block that record belongs to
   lBlkIndex = (lRecNum-1) / cmprHdr.iGrpSize;

   // If the block is in memory, use it.  If not, read it from file
   if (lBlkIndex != lLastBlkIndex)
   {
      lLastBlkIndex = lBlkIndex;

      // Seeking block
      fseek(fdIdx, (lBlkIndex*sizeof(IDXBLK)), SEEK_SET);

      // Read the compressed block offset and size
      iRet = fread(&idxBlk, sizeof(IDXBLK), 1, fdIdx);
      if (iRet < 1)
         return(-3);

      // get data from file
      fseek(fdCmpr, idxBlk.lDatPtr, SEEK_SET);
      if (fread(inbuf, 1, idxBlk.iBlkSize, fdCmpr) != idxBlk.iBlkSize)
         return(-2);

      // Setup parameters for compression
      Param.CompressedSize    = idxBlk.iBlkSize;
      Param.UnCompressedSize  = 0;
      Param.BufferSize        = buf_size;
      Param.pSource           = inbuf;
      Param.pDestination      = outbuf;
      Param.SourceOffset      = 0L;
      Param.DestinationOffset = 0L;
      Param.Crc               = (unsigned long) -1;
      iRet = explode(ReadCompressed, WriteUnCompressed, workbuf, &Param);
      Param.Crc = ~Param.Crc;
      outbuf[Param.UnCompressedSize] = 0;

      //if (iRet || (Param.Crc != Param.OrigCrc))
      if (iRet)
      {
         printf("Error in compressed data!");
         iRet = -1;
      }
   }

   // Finding record within the block
   lTmp = (lRecNum-1) % cmprHdr.iGrpSize;

   if (cmprHdr.iRecType == 1)
   {  // Variable length
      pTmp1 = outbuf;
      pTmp2 = strchr(pTmp1, 10);
      while (lTmp > 0)
      {
         pTmp1 = pTmp2 + 1;                  // Point to next record
         pTmp2 = strchr(pTmp1, 10);          // Search for end of line
         lTmp--;
      }

      if (pTmp2)
      {
         pTmp2++;                // Want to keep LF character
         cTmp = *pTmp2;
         *pTmp2 = 0;
         strcpy(pBuf, pTmp1);
         *pTmp2 = cTmp;
         iRet = 0;
      } else
      {
         *pBuf = 0;
         iRet = -1;
      }
   } else
   {  // Fixed length
      memcpy(pBuf, outbuf+lTmp*cmprHdr.iRecSize, cmprHdr.iRecSize);
      *(pBuf+cmprHdr.iRecSize) = 0;
      iRet = 0;
   }

   return(iRet);
}

/*****************************************************************************/

int initPQComp(LPCSTR pCntyCode, int iMode)
{
   char  acTmp[_MAX_PATH], *pTmp;
   char  acIniFile[_MAX_PATH], acLogFile[_MAX_PATH];

   int   iRet=0;

   strcpy(acIniFile, ".\\PQComp.ini");
   if (_access(acIniFile, 0))
      strcpy(acIniFile, "C:\\Tools\\PQComp.ini");

   // Open log file
   GetPrivateProfileString("System", "LogFile", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0])
   {
      pTmp = DateStamp();
      sprintf(acLogFile, acTmp, pTmp);
      open_log(acLogFile, "a+", "PQComp");
   }

   // Initialize compressed header record
   strcpy(cmprHdr.acIdHdr, "ParcelQuest.com");
   cmprHdr.iRecSize = GetPrivateProfileInt("Data", "RecSize", 0, acIniFile);
   cmprHdr.iGrpSize = GetPrivateProfileInt("Data", "GrpSize", 10, acIniFile);
   cmprHdr.iRecType = GetPrivateProfileInt("Data", "RecType", 1, acIniFile);

   // Check compressed file if in uncompressed mode
   GetPrivateProfileString("Data", "CmprFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acCmprFile, acTmp, pCntyCode, pCntyCode, pCntyCode);
   if (iMode == 2 && _access(acCmprFile, 0) )
   {
      LogMsg("Error - Missing file: %s\n", acCmprFile);
      return -1;
   }

   // Check indexed file if in uncompressed mode
   GetPrivateProfileString("Data", "IdxFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acIdxFile, acTmp, pCntyCode, pCntyCode, pCntyCode);
   if (iMode == 2 && _access(acIdxFile, 0) )
   {
      LogMsg("Error - Missing file: %s\n", acIdxFile);
      return -1;
   }

   // Check uncompressed file if in compressed mode
   GetPrivateProfileString("Data", "UnCmprFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acUnCmprFile, acTmp, pCntyCode, pCntyCode, pCntyCode);
   if (iMode == 1 && _access(acUnCmprFile, 0) )
   {
      LogMsg("Error - Missing file: %s\n", acUnCmprFile);
      return -1;
   }

   return iRet;
}
   
/*****************************************************************************/

int __stdcall PQCompress(LPCSTR pCnty)
{
   int      iCnt, iRet, iTmp;
   int      iCmprSize=0, iTotalRecs=0, iUnCmprSize=0;

   unsigned int type;
   unsigned int dsize;
  
   // Set data version.  Don't change unless you know the consequences
   cmprHdr.iDataVer = 1;

   // Initialize index block
   idxBlk.lDatPtr = 0;
   idxBlk.iBlkSize = 0;

   // Initialize file names and check for availability.
   iRet = initPQComp(pCnty, 1);
   if (iRet < 0)
   {
      LogMsg("Error initializing PQComp");
      return iRet;
   }

   // Calculate buffer size
   buf_size  = cmprHdr.iGrpSize*MAX_RECSIZE;

   // Input buffer
   inbuf = (char *) malloc(buf_size+1);
   // Output buffer
   outbuf = (char *) malloc(buf_size+1);
   // Work buffer
   workbuf=(char *) malloc(CMP_BUFFER_SIZE);

   if (!inbuf || !outbuf || !workbuf)
   {
      if (inbuf) free(inbuf);
      if (outbuf) free(outbuf);
      LogMsg("Unable to allocate buffer %d", buf_size);
      return M_NOT_AVAIL;
   } 

   // Open uncompressed file
   if (cmprHdr.iRecType == 1)                // Variable length
      fdUnCmpr = fopen(acUnCmprFile, "r");
   else
      fdUnCmpr = fopen(acUnCmprFile, "rb");
   if (!fdUnCmpr)
   {
      LogMsg("Error open uncompressed file: %s", acUnCmprFile);
      doCleanUp();
      return F_NOT_OPEN;
   }

   // Create compressed file
   fdCmpr = fopen(acCmprFile, "wb");
   if (!fdCmpr)
   {
      LogMsg("Error open compressed file: %s", acCmprFile);
      doCleanUp();
      return F_NOT_OPEN;
   }

   // Create indexed file
   fdIdx = fopen(acIdxFile, "wb");
   if (!fdIdx)
   {
      LogMsg("Error open indexed file: %s", acIdxFile);
      doCleanUp();
      return F_NOT_OPEN;
   }

   fseek(fdIdx,0,0);

   iTmp = sizeof(CMPRHDR);

   // Set starting data block
   idxBlk.lDatPtr = iTmp;

   // Write header record to compressed file
   iRet = fwrite(&cmprHdr, iTmp, 1, fdCmpr);
   if (!iRet)
   {
      LogMsg("Error writing to compressed file: %s", acCmprFile);
      doCleanUp();
      return F_WRITE_ERR;
   }

   type = CMP_ASCII;
   dsize = 4096;
   iCnt = cmprHdr.iGrpSize;

   LogMsg("Start compressing %s", pCnty);
   while (iCnt == cmprHdr.iGrpSize)
   {
      // Read input buffer
      if (cmprHdr.iRecType == 1)
      {
         iCnt = getRecs(inbuf);
         iTmp = strlen(inbuf);
      } else
      {
         iCnt = getRecs(inbuf, cmprHdr.iRecSize*iCnt);
         iTmp = cmprHdr.iRecSize*iCnt;
      }
      if (!iCnt)
         break;

      iTotalRecs += iCnt;
      iUnCmprSize += iTmp;

      // Setup parameters for compression
      Param.CompressedSize    = 0;
      Param.UnCompressedSize  = iTmp;
      Param.BufferSize        = buf_size;
      Param.pSource           = inbuf;
      Param.pDestination      = outbuf;
      Param.SourceOffset      = 0L;
      Param.DestinationOffset = 0L;
      Param.Crc               = (unsigned long) -1;
      implode(ReadUnCompressed, WriteCompressed, workbuf, &Param, &type, &dsize); 
      Param.OrigCrc           = ~Param.Crc;

      iCmprSize += Param.CompressedSize;

      // Write compressed data to output file
      iRet = fwrite(outbuf, Param.CompressedSize, 1, fdCmpr);
      if (!iRet)
      {
         LogMsg("Error writing to compressed file" );
         break;
      }

      // Output index record
      idxBlk.iBlkSize = Param.CompressedSize;
      iRet = fwrite(&idxBlk, sizeof(IDXBLK), 1, fdIdx);
      if (iRet != 1)
      {
         LogMsg("Error writing to indexed file" );
         break;
      }

      // Calculating offset for next block
      idxBlk.lDatPtr += Param.CompressedSize;
   }

   // Update compress file header
   cmprHdr.iRecCount = iTotalRecs;
   fseek(fdCmpr,0,0);
   iRet = fwrite(&cmprHdr, sizeof(CMPRHDR), 1, fdCmpr);

   LogMsg("Input:%7lu  Output:%7lu  TotalRecords:%7lu\n", iUnCmprSize, iCmprSize, iTotalRecs);

   // Clean up buffer
   doCleanUp();

   return iRet;
}
