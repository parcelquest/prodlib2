#ifndef _PQCOMP_H
#define _PQCOMP_H

#define     F_NOT_FOUND       -1
#define     F_NOT_OPEN        -2
#define     F_READ_ERR        -3
#define     F_WRITE_ERR       -4
#define     F_EOF             -5
#define     M_NOT_AVAIL       -10

#define     MAX_RECSIZE       2048

typedef struct PassedParam
{
   char *pSource;                   /* Pointer to source buffer           */
   char *pDestination;              /* Pointer to destination buffer      */
   unsigned long SourceOffset;      /* Offset into the source buffer      */
   unsigned long DestinationOffset; /* Offset into the destination buffer */
   unsigned long CompressedSize;    /* Need this for extracting!          */
   unsigned long UnCompressedSize;  /* Size of uncompressed data file     */
   unsigned long BufferSize;
   unsigned long Crc;               /* Calculated CRC value               */
   unsigned long OrigCrc;           /* Original CRC value of data         */
} PARAM;

// 64-byte record compressed file header
typedef struct cmpr_hdr
{   
   char     acIdHdr[16];            // Identity header to identify our data
   char     filler[24];             // For future use
   int      iDataType;              // 0=text, 1=binary
   int      iDataVer;               // Start out with 1
   int      iRecSize;               // Rec. size or 0 for variable length
   int      iGrpSize;               // Number of records per group
   int      iRecType;               // 1=variable, 2=fixed
   int      iRecCount;              // Total records in file
} CMPRHDR;

typedef struct idx_blk
{
   unsigned lDatPtr;
   unsigned iBlkSize;
} IDXBLK;

int         buf_size;

char        *inbuf, *outbuf, *workbuf;
char        acCmprFile[_MAX_PATH], acUnCmprFile[_MAX_PATH], acIdxFile[_MAX_PATH];
FILE        *fdCmpr, *fdUnCmpr, *fdIdx;

CMPRHDR     cmprHdr;
IDXBLK      idxBlk;
PARAM       Param;

#endif