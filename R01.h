#define  SIZ_APN_S             14
#define  SIZ_APN_D             17
#define  SIZ_CO_NUM            2
#define  SIZ_CO_ID             3
#define  SIZ_STATUS            8
#define  SIZ_TRA               8
#define  SIZ_NAME1             52
#define  SIZ_NAME2             26
#define  SIZ_NAME_TYPE1        28
#define  SIZ_NAME_TYPE2        10
#define  SIZ_VEST              16
#define  SIZ_ZONE              10
#define  SIZ_USE_STD           36
#define  SIZ_USE_CO            8
#define  SIZ_S_STRNUM          7
#define  SIZ_S_STR_SUB         3
#define  SIZ_S_DIR             2
#define  SIZ_S_STREET          24
#define  SIZ_S_SUFF            5
#define  SIZ_S_UNITNO          6
#define  SIZ_S_CITY            24
#define  SIZ_S_ST              2
#define  SIZ_S_ZIP             5
#define  SIZ_S_ZIP4            4
#define  SIZ_M_STRNUM          7
#define  SIZ_M_STR_SUB         3
#define  SIZ_M_DIR             2
#define  SIZ_M_STREET          24
#define  SIZ_M_SUFF            5
#define  SIZ_M_UNIT            6
#define  SIZ_M_UNITNO          6
#define  SIZ_M_CITY            17
#define  SIZ_M_ST              2
#define  SIZ_M_ZIP             5
#define  SIZ_M_ZIP4            4
#define  SIZ_RATIO             3
#define  SIZ_LAND              10
#define  SIZ_IMPR              10
#define  SIZ_OTHER             10
#define  SIZ_EXE_TOTAL         10
#define  SIZ_GROSS             10
#define  SIZ_HO_FL             1
#define  SIZ_TAX_AMT           12
#define  SIZ_DEL_YR            4
#define  SIZ_TIMBER            11
#define  SIZ_AG_PRE            7
#define  SIZ_SALE1_AMT         10
#define  SIZ_SALE1_CODE        13
#define  SIZ_SALE1_DOCTYPE     22
#define  SIZ_MULTI_APN         1
#define  SIZ_TD1_CODE          13
#define  SIZ_TD2_CODE          13
#define  SIZ_SELLER            24
#define  SIZ_SALE2_AMT         10
#define  SIZ_SALE2_CODE        13
#define  SIZ_SALE2_DOCTYPE     22
#define  SIZ_SALE3_AMT         10
#define  SIZ_SALE3_CODE        13
#define  SIZ_SALE3_DOCTYPE     22
#define  SIZ_TRANSFER_DT       8
#define  SIZ_TRANSFER_DOC      12
#define  SIZ_SALE1_DT          8
#define  SIZ_SALE1_DOC         12
#define  SIZ_SALE2_DT          8
#define  SIZ_SALE2_DOC         12
#define  SIZ_SALE3_DT          8
#define  SIZ_SALE3_DOC         12
#define  SIZ_LOT_ACRES         9
#define  SIZ_LOT_SQFT          9
#define  SIZ_YR_BLT            4
#define  SIZ_YR_EFF            4
#define  SIZ_BLDG_SF           9
#define  SIZ_UNITS             6
#define  SIZ_STORIES           4
#define  SIZ_BEDS              2
#define  SIZ_BATH_F            2
#define  SIZ_BATH_H            2
#define  SIZ_ROOMS             3
#define  SIZ_BLDG_CLASS        1
#define  SIZ_BLDG_QUAL         4
#define  SIZ_YR_ASSD           5
#define  SIZ_IMPR_COND         9
#define  SIZ_FIRE_PL           2
#define  SIZ_BLDGS             2
#define  SIZ_AIR_COND          23
#define  SIZ_HEAT              21
#define  SIZ_GAR_SQFT          6
#define  SIZ_PARK_TYPE         17
#define  SIZ_PARK_SPACE        6
#define  SIZ_VIEW              19
#define  SIZ_STYLE             13
#define  SIZ_IMPR_TYPE         18
#define  SIZ_CONS_TYPE         16
#define  SIZ_FNDN_TYPE         14
#define  SIZ_ROOF_TYPE         15
#define  SIZ_ROOF_MAT          19
#define  SIZ_FRAME_TYPE        8
#define  SIZ_EX WALL_TYPE      19
#define  SIZ_WATER             12
#define  SIZ_SEWER             14
#define  SIZ_POOL              12
#define  SIZ_LEGAL             40
#define  SIZ_MAPDIV_LA         2
#define  SIZ_CARE_OF           20
#define  SIZ_OTHER_ROOM        112
#define  SIZ_OTHER_IMPR        64
#define  SIZ_AMENITIES         42
#define  SIZ_EQUIPMENT         46
#define  SIZ_REMODEL           20
#define  SIZ_S_ADDR_D          52
#define  SIZ_S_CTY_ST_D        38
#define  SIZ_M_ADDR_D          52
#define  SIZ_M_CTY_ST_D        31
#define  SIZ_NAME_SWAP         52
#define  SIZ_TD1_AMT           10
#define  SIZ_TD2_AMT           10
#define  SIZ_N_SCR             4
#define  SIZ_N_LON             10
#define  SIZ_N_LAT             9
#define  SIZ_N_TRACT           9
#define  SIZ_N_DPBC            2

#define  OFF_APN_S             1-1
#define  OFF_APN_D             15-1
#define  OFF_CO_NUM            32-1
#define  OFF_CO_ID             34-1
#define  OFF_STATUS            37-1
#define  OFF_TRA               45-1
#define  OFF_NAME1             53-1
#define  OFF_NAME2             105-1
#define  OFF_NAME_TYPE1        131-1
#define  OFF_NAME_TYPE2        159-1
#define  OFF_VEST              169-1
#define  OFF_ZONE              185-1
#define  OFF_USE_STD           195-1
#define  OFF_USE_CO            231-1
#define  OFF_S_STRNUM          239-1
#define  OFF_S_STR_SUB         246-1
#define  OFF_S_DIR             249-1
#define  OFF_S_STREET          251-1
#define  OFF_S_SUFF            275-1
#define  OFF_S_UNITNO          280-1
#define  OFF_S_CITY            286-1
#define  OFF_S_ST              310-1
#define  OFF_S_ZIP             312-1
#define  OFF_S_ZIP4            317-1
#define  OFF_M_STRNUM          321-1
#define  OFF_M_STR_SUB         328-1
#define  OFF_M_DIR             331-1
#define  OFF_M_STREET          333-1
#define  OFF_M_SUFF            357-1
#define  OFF_M_UNIT            362-1
#define  OFF_M_CITY            368-1
#define  OFF_M_ST              385-1
#define  OFF_M_ZIP             387-1
#define  OFF_M_ZIP4            392-1
#define  OFF_RATIO             466-1
#define  OFF_LAND              469-1
#define  OFF_IMPR              479-1
#define  OFF_OTHER             489-1
#define  OFF_EXE_TOTAL         499-1
#define  OFF_GROSS             509-1
#define  OFF_HO_FL             519-1
#define  OFF_TAX_AMT           535-1
#define  OFF_DEL_YR            547-1
#define  OFF_TIMBER            551-1
#define  OFF_AG_PRE            562-1
#define  OFF_SALE1_AMT         583-1
#define  OFF_SALE1_CODE        593-1
#define  OFF_SALE1_DOCTYPE     606-1
#define  OFF_MULTI_APN         628-1
#define  OFF_TD1_CODE          643-1
#define  OFF_TD2_CODE          670-1
#define  OFF_SELLER            683-1
#define  OFF_SALE2_AMT         721-1
#define  OFF_SALE2_CODE        731-1
#define  OFF_SALE2_DOCTYPE     744-1
#define  OFF_SALE3_AMT         780-1
#define  OFF_SALE3_CODE        790-1
#define  OFF_SALE3_DOCTYPE     803-1
#define  OFF_TRANSFER_DT       825-1
#define  OFF_TRANSFER_DOC      843-1
#define  OFF_SALE1_DT          855-1
#define  OFF_SALE1_DOC         873-1
#define  OFF_SALE2_DT          885-1
#define  OFF_SALE2_DOC         903-1
#define  OFF_SALE3_DT          915-1
#define  OFF_SALE3_DOC         933-1
#define  OFF_LOT_ACRES         956-1
#define  OFF_LOT_SQFT          976-1
#define  OFF_YR_BLT            985-1
#define  OFF_YR_EFF            989-1
#define  OFF_BLDG_SF           1004-1
#define  OFF_UNITS             1013-1
#define  OFF_STORIES           1019-1
#define  OFF_BEDS              1023-1
#define  OFF_BATH_F            1025-1
#define  OFF_BATH_H            1027-1
#define  OFF_ROOMS             1029-1
#define  OFF_BLDG_CLASS        1032-1
#define  OFF_BLDG_QUAL         1033-1
#define  OFF_YR_ASSD           1037-1
#define  OFF_IMPR_COND         1042-1
#define  OFF_FIRE_PL           1051-1
#define  OFF_BLDGS             1053-1
#define  OFF_AIR_COND          1055-1
#define  OFF_HEAT              1078-1
#define  OFF_GAR_SQFT          1099-1
#define  OFF_PARK_TYPE         1105-1
#define  OFF_PARK_SPACE        1122-1
#define  OFF_VIEW              1128-1
#define  OFF_STYLE             1147-1
#define  OFF_IMPR_TYPE         1160-1
#define  OFF_CONS_TYPE         1178-1
#define  OFF_FNDN_TYPE         1194-1
#define  OFF_ROOF_TYPE         1208-1
#define  OFF_ROOF_MAT          1223-1
#define  OFF_FRAME_TYPE        1242-1
#define  OFF_EX WALL_TYPE      1250-1
#define  OFF_WATER             1269-1
#define  OFF_SEWER             1281-1
#define  OFF_POOL              1295-1
#define  OFF_LEGAL             1307-1
#define  OFF_MAPDIV_LA         1347-1
#define  OFF_CARE_OF           1349-1
#define  OFF_OTHER_ROOM        1369-1
#define  OFF_OTHER_IMPR        1481-1
#define  OFF_AMENITIES         1545-1
#define  OFF_EQUIPMENT         1587-1
#define  OFF_REMODEL           1633-1
#define  OFF_S_ADDR_D          1653-1
#define  OFF_S_CTY_ST_D        1705-1
#define  OFF_M_ADDR_D          1743-1
#define  OFF_M_CTY_ST_D        1795-1
#define  OFF_NAME_SWAP         1826-1
#define  OFF_TD1_AMT           1878-1
#define  OFF_TD2_AMT           1888-1
#define  OFF_N_SCR             1901-1
#define  OFF_N_LON             1905-1
#define  OFF_N_LAT             1915-1
#define  OFF_N_TRACT           1924-1
#define  OFF_N_DPBC            1933-1

#define  SIZ_XTRAS             40

/* Old format
typedef struct _tAdr_Rec
{
   char  strNum[SIZ_S_STRNUM+1];
   long  lStrNum;
   char  strSub[SIZ_M_STR_SUB+1];
   char  strDir[SIZ_M_DIR+1];
   char  strName[SIZ_M_STREET+1];
   char  strSfx[SIZ_M_SUFF+1];
   char  Unit[SIZ_M_UNIT+1];
   char  City[SIZ_M_CITY+1];
   char  State[SIZ_M_ST+1];
   char  Zip[SIZ_M_ZIP+1];
   char  Zip4[SIZ_M_ZIP4+1];
} ADR_REC;
*/

typedef struct _tAdr_Rec
{
   char  strNum[SIZ_M_STRNUM+1];
   long  lStrNum;
   char  strSub[SIZ_M_STR_SUB+1];
   char  strDir[SIZ_M_DIR+1];
   char  strName[SIZ_M_STREET+1];
   char  strSfx[SIZ_M_SUFF+1];
   char  Unit[SIZ_M_UNITNO+1];
   char  City[SIZ_M_CITY+1];
   char  State[SIZ_M_ST+1];
   char  Zip[SIZ_M_ZIP+1];
   char  Zip4[SIZ_M_ZIP4+1];
   char  SfxCode[SIZ_M_SUFF+1];
   char  Xtras[SIZ_XTRAS+1];
} ADR_REC;

typedef struct _Situs1
{
   char  acHse[7];
   char  acDir[2];
   char  acStreet[16];
} SITUS1;

typedef struct _Situs2
{
   char  acFiller[9];
   char  acSuffix[2];
   char  acSuite[5];
   char  acCityCode[2];
} SITUS2;

void parseAdr1(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1N(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1N_1(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1_1(ADR_REC *pAdrRec, char *pAdr);
void parseAdr2(ADR_REC *pAdrRec, char *pAdr);
void parseAdr2_1(ADR_REC *pAdrRec, char *pAdr);
void parseAdrND(ADR_REC *pAdrRec, char *pAdr);
void parseMAdr1(ADR_REC *pAdrRec, char *pAddr);
void Mrn_parseMAdr1(ADR_REC *pAdrRec, char *pAddr);
void parseMAdr1_2(ADR_REC *pAdrRec, char *pAddr);

bool isNumber(char *pNumber);
int  GetSfxCodeX(char *pOrgSfx, char *pSuffix);
int  GetSfxDev(char *pSuffix);
