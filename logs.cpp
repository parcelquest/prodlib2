
/*
   This file contains loging, tracing functions
 */

#include "stdafx.h"
#include "logs.h"

static FILE *fdLog = NULL;
static int  Trace_level;
static char myLogMsg[256];

#define WFL_MAX_TIMEBUF 80
char *TimeStamp()
{
   static char timeStr[WFL_MAX_TIMEBUF];
   time_t t = 0;
   struct tm *pTime  = NULL;
   
   time(&t);
   pTime = localtime(&t);
   strftime( timeStr ,WFL_MAX_TIMEBUF ,"%H:%M:%S %B %d, %Y" ,pTime);
   return timeStr;
}

char *DateStamp()
{
   static char dateStr[WFL_MAX_TIMEBUF];
   time_t t = 0;
   struct tm *pTime  = NULL;
   
   time(&t);
   pTime = localtime(&t);
   strftime(dateStr ,WFL_MAX_TIMEBUF ,"%Y%m%d" ,pTime);
   return dateStr;
}

void open_log(char *pLogFile, char *pMode, char *pFrom)
{
   if (fdLog)
      fclose(fdLog);

   if ((fdLog=fopen(pLogFile, pMode))==NULL) 
   {
      fprintf(stderr ,"error opening log file %s" ,pLogFile);
      exit(1);
   }

   if (pFrom && *pFrom)
      fprintf(fdLog ,"%s: Open log file %s\n", pFrom ,pLogFile);
   else
      fprintf(fdLog ,"Open log file %s\n" ,pLogFile);

   myLogMsg[0] = 0;
}

void close_log()
{
   if (fdLog) 
   {
      fprintf(fdLog, "Closing log\n\n");
      fflush(fdLog);
      fclose(fdLog);
      fdLog = NULL;
   }
}
/* 
void TRACE(char *fname, int lineno, char *fmt, ...)
{
   va_list ap;
   if (!Trace_level) return; 
   if (!fdLog) fdLog = stdout;
   if (fdLog) 
   {
      va_start(ap, fmt);
      fprintf(fdLog, "(%s) %12s :%4d\t" ,TimeStamp() ,fname, lineno);
      vfprintf(fdLog, fmt, ap);
      fprintf(fdLog, "\n");
      va_end(ap);
      fflush(fdLog);
   }
}
*/

/* clone of LOG */
void LOG_ERROR(char *fname, int lineno, char *fmt, ...)
{
   char buffer[4096];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   sprintf(bp, "(%s) %12s :%4d\t" ,TimeStamp() ,fname ,lineno);
   bp += strlen( bp ); 
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (!fdLog) fdLog = stdout;
   fprintf( fdLog ,"%s" ,buffer );
}
void LOG(char *fname, int lineno, char *fmt, ...)
{
   char buffer[4096];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   sprintf(bp, "(%s) %12s :%4d\t" ,TimeStamp() ,fname ,lineno);
   bp += strlen( bp ); 
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (!fdLog) fdLog = stdout;
   fprintf( fdLog ,"%s" ,buffer );
}

void LogMsg(char *fmt, ...)
{
   char buffer[4096];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   sprintf(bp, "(%s) \t" ,TimeStamp());
   bp += strlen( bp ); 
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (!fdLog) 
      fdLog = stdout;
   else
   {
      if (strstr(buffer, "***"))
      {
         strncpy(myLogMsg, buffer, 256);
         myLogMsg[255] = 0;
      }
   }

   fprintf(fdLog ,"%s" ,buffer);
#ifdef _DEBUG
   fflush(fdLog);
#endif
}

void LogMsg0(char *fmt, ...)
{
   char buffer[4096];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (!fdLog) fdLog = stdout;
   fprintf(fdLog ,"%s" ,buffer);
#ifdef _DEBUG
   fflush(fdLog);
#endif
}

void LogMsg1(char *fmt, ...)
{
   char buffer[4096];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   sprintf(bp, "(%s) \t", DateStamp());
   bp += strlen( bp ); 
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (!fdLog) fdLog = stdout;
   fprintf(fdLog ,"%s" ,buffer);
#ifdef _DEBUG
   fflush(fdLog);
#endif
}

void LogMsgD(char *fmt, ...)
{
   char buffer[4096];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (!fdLog) 
      fdLog = stdout;
   else
   {
      if (strstr(buffer, "***"))
      {
         strncpy(myLogMsg, buffer, 256);
         myLogMsg[255] = 0;
      }
   }

   fprintf(fdLog ,"%s" ,buffer);
   printf("%s\n" ,buffer);
#ifdef _DEBUG
   fflush(fdLog);
#endif
}

char *getLastErrorLog()
{
   return myLogMsg;
}